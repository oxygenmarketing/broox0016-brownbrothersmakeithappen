<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

	Route::any('/public/{something}/terms.md', function() {
		return Response::make('OH NO!',500);
	});

	Route::get('verify/update/{rankey}', 'VerifyController@update');
	Route::post('verify/update/{rankey}', 'VerifyController@update_save');
	Route::get('verify/{rankey}', 'VerifyController@index');
	Route::post('verify/{rankey}', 'VerifyController@submit');


	Route::get('/preview/{id}', 'HomeController@preview');
	Route::get('/pre', 'HomeController@prePromo');
	Route::get('/post', 'HomeController@postPromo');

	Route::post('/sms/receipt', 'SmsController@receipt');

	Route::get('/sms', 'SmsController@index');
	Route::post('/sms', 'SmsController@index');

	Route::get('login/fb', 'LoginFacebookController@login');
	Route::get('login/fb/callback', 'LoginFacebookController@callback');

	Route::group(array('before'=>'promo'), function() {

		Route::get( '/', 'HomeController@index');

		Route::get( '/enter', 'EntriesController@index');
		Route::post( '/enter', 'EntriesController@submit');
		Route::post( '/fbshare', 'EntriesController@fbshare');
		Route::get( '/limit', 'EntriesController@limit');
		Route::get( '/result', 'EntriesController@result');
		Route::get( 'help', 'ContentController@help');
		Route::get( 'terms', 'ContentController@terms');

	});

	Route::group(array('before'=>'testing'), function() {

		/** Only available if the password has been used **/

		Route::get( '/thanks', 'EntriesController@thanks');
		Route::get( '/winner', 'EntriesController@winner');
		Route::get('test/reset', 'TestController@reset');
		Route::get('test/email', 'TestController@email');
		Route::resource('test', 'TestController');
		Route::get('/sms/test', 'SmsController@sms_test');

	});


	/*
	*/

	// AUTHORISATION
	Route::get( 'login', 'Admin_AuthController@login');
	Route::post('login', 'Admin_AuthController@do_login');
	Route::get( 'admin/user/forgot', 'Admin_AuthController@forgot_password');
	Route::post('admin/user/forgot', 'Admin_AuthController@do_forgot_password');
	Route::get( '/user/reset/{token}', 'Admin_AuthController@reset_password');
	Route::post('/user/reset', array('as' => 'user/reset', 'uses' => 'Admin_AuthController@do_reset_password'));
	Route::get( 'logout', 'Admin_AuthController@logout');

/*
|--------------------------------------------------------------------------
*/

	// PASSWORD PROTECTED ROUTES
	Route::group(array('before'=>'auth'), function() {

		//\Route::get('elfinder', 'Barryvdh\Elfinder\ElfinderController@showIndex');
        //\Route::any('elfinder/connector', 'Barryvdh\Elfinder\ElfinderController@showConnector');
		//\Route::get('elfinder/tinymce', 'Barryvdh\Elfinder\ElfinderController@showTinyMCE4');

		Route::get('admin/form/edit', array('uses' => 'Admin_FormController@edit'));
		Route::post('admin/form/save', array('uses' => 'Admin_FormController@checkForm'));
		Route::resource('admin/form', 'Admin_FormController');

		// Promotion Manager
		Route::get('admin/content', 'Admin_PromoController@content');
		Route::get('admin/content/new', 'Admin_PromoController@addNew');

		Route::get('admin/content/{id}', 'Admin_PromoController@show');
		Route::get('admin/promotion/update',  array('uses' => 'Admin_PromoController@update'));
		Route::post('admin/promotion/promo_update',  array('uses' => 'Admin_PromoController@promoUpdate'));
		Route::get('admin/promotion', 'Admin_PromoController@index');

		Route::get('admin/sms', 'Admin_SmsController@index');
		Route::post('admin/sms/sms_update',  array('uses' => 'Admin_SmsController@smsUpdate'));

		Route::get('admin/prizes/createPrize', 'Admin_PrizesController@createPrize');
		Route::get('admin/prizes/generatePrizes', 'Admin_PrizesController@generatePrizes');
		Route::get('admin/prizes/createPools', 'Admin_PrizesController@createPools');
		Route::get('admin/prizes/pools/{id}', 'Admin_PrizesController@viewPools');
		Route::post('admin/prizes/saveDetails',  array('uses' => 'Admin_PrizesController@saveDetails'));
		Route::post('admin/prizes/savePrizePools',  array('uses' => 'Admin_PrizesController@savePrizePools'));
		Route::get('admin/prizes/pools/{prize_id}/publish/{pool_id}', array('uses' => 'Admin_PrizesController@do_publish'));
		Route::get('admin/prizes/pools/{prize_id}/{pool_id}/{state_id}/{action}', array('uses' => 'Admin_PrizesController@poolState'));

		Route::resource('admin/prizes', 'Admin_PrizesController');

		Route::get('admin/prizedraw/generateSchedule', 'Admin_PrizeDrawController@generateSchedule');
		Route::get('admin/prizedraw/runDraw', 'Admin_PrizeDrawController@runDraw');

		Route::get('admin/entries/publish/{id}', array('uses' => 'Admin_EntriesController@do_publish'));
		Route::get('admin/entries/export', array('uses' => 'Admin_EntriesController@export'));
		Route::resource('admin/entries', 'Admin_EntriesController');

		Route::get('admin/winners/publish/{id}', array('uses' => 'Admin_WinnersController@do_publish'));
		Route::get('admin/winners/export', array('uses' => 'Admin_WinnersController@export'));
		Route::get('admin/winners/resend/{id}', array('uses' => 'Admin_WinnersController@_notifyWinner'));
		Route::resource('admin/winners', 'Admin_WinnersController');

		Route::get('admin/uniquecodes/publish/{id}', array('uses' => 'Admin_UniquecodeController@do_publish'));
		Route::resource('admin/uniquecodes', 'Admin_UniquecodeController');
		Route::resource('admin/google', 'Admin_GoogleController');
		Route::resource('admin/promotion', 'Admin_PromoController');

		// ADMIN. USER MANAGEMENT
		Route::get('admin/privileges/publish/{id}', array('uses' => 'Admin_PrivilegesController@do_publish'));
		Route::resource('admin/privileges', 'Admin_PrivilegesController');

		Route::resource('admin/instantwin/prizes', 'Admin_PrizesController');
		Route::get('admin/instantwin/prizes/publish/{id}', array('uses' => 'Admin_PrizesController@do_publish'));
		Route::get('admin/uniquecodes/publish/{id}', array('uses' => 'Admin_UniquecodeController@do_publish'));
		Route::get('admin/instantwin/revoke/{id}', array('uses' => 'Admin_IWController@revoke'));
		Route::get('admin/instantwin/prizes/ordering/{id}/{order}/{dir}', array('uses' => 'Admin_PrizesController@ordering'));
		Route::resource('admin/instantwin', 'Admin_IWController');

		// SNAPSHOTS
		Route::get( 'admin/snapshots', 'Admin_SnapshotsController@index');
		Route::get( 'admin/snapshots/week', 'Admin_SnapshotsController@regWeek');
		Route::get( 'admin/snapshots/state', 'Admin_SnapshotsController@stateDay');
		Route::get( 'admin/snapshots/age', 'Admin_SnapshotsController@ageDay');
		Route::get( 'admin/snapshots/uniqueday', 'Admin_SnapshotsController@uniqueDay');
		Route::get( 'admin/snapshots/uniqueweek', 'Admin_SnapshotsController@uniqueWeek');
		Route::get( 'admin/snapshots/productsday', 'Admin_SnapshotsController@productsDay');

		/*
		Route::get( 'admin/snapshots/export/{type}', 'Admin_SnapshotsController@export');
		Route::get( 'admin/snapshots/exportAge/{type}', 'Admin_SnapshotsController@exportAge');
		Route::get( 'admin/snapshots/exportState/{type}', 'Admin_SnapshotsController@exportState');
		*/
		Route::get( 'admin/snapshots/export/entriesdays', 'Admin_SnapshotsController@expEntriesDays');
		Route::get( 'admin/snapshots/export/entriesweek', 'Admin_SnapshotsController@expEntriesWeek');
		Route::get( 'admin/snapshots/export/entriesstates', 'Admin_SnapshotsController@expEntriesStates');
		Route::get( 'admin/snapshots/export/entriesage', 'Admin_SnapshotsController@expEntriesAge');

		Route::get( 'admin/snapshots/export/uniquedays', 'Admin_SnapshotsController@expUniqueDays');
		Route::get( 'admin/snapshots/export/uniqueweek', 'Admin_SnapshotsController@expUniqueWeek');

		// HELPER FUNCTIONS

		Route::post('admin/search',	function() {

			$url		= URL::previous();
			/*
			$url		= parse_url($url);
			unset($url['query']);
			*/
			list($shorturl) = explode('?', $url);
			$src		= Input::get('search');
			$sessLoc	= AdminHelper::_getSession($shorturl);

			Session::put($shorturl, (object) array(
				'searchterm'	=> ($src == '----------------------------------------------------------------------------------/reset') ? NULL : $src
			,	'filterby'		=> (isset($sessLoc->filterby)) ? $sessLoc->filterby : NULL
			,	'orderby'		=> (isset($sessLoc->orderby)) ? $sessLoc->orderby : NULL
			,	'dir'			=> (isset($sessLoc->dir)) ? $sessLoc->dir : NULL
			,	'perpage'		=> (isset($sessLoc->perpage)) ? $sessLoc->perpage : NULL
			));

			return Redirect::to($url);

		});

		Route::post('admin/filter',	function() {

			//_e::prex($_POST);

			$url		= URL::previous();
			/*
			$url		= parse_url($url);
			unset($url['query']);
			*/
			list($shorturl) = explode('?', $url);
			$filters	= Input::get('filter');
			$sessLoc	= AdminHelper::_getSession($shorturl);

			foreach ($filters as $i=>$v) {
				if ($v == '-' || $v == '--' || $v == 'all') unset($filters[$i]);
			};

			Session::put($shorturl, (object) array(
				'searchterm'	=> (isset($sessLoc->searchterm)) ? $sessLoc->searchterm : NULL
			,	'filterby'		=> $filters
			,	'orderby'		=> (isset($sessLoc->orderby)) ? $sessLoc->orderby : NULL
			,	'dir'			=> (isset($sessLoc->dir)) ? $sessLoc->dir : NULL
			,	'perpage'		=> (isset($sessLoc->perpage)) ? $sessLoc->perpage : NULL
			));

			return Redirect::to($url);

		});

		Route::resource('admin', 'Admin_HomeController');
		Route::post('admin/uploader', array('uses' => 'Admin_PromoController@do_upload'));

	});
