<?php

return array (

	'file_path'			=> public_path(). DIRECTORY_SEPARATOR .'images/_fbshare'. DIRECTORY_SEPARATOR,
	'file_url'			=> url('images/_fbshare/'),
	'file_width'			=> 1024,
	'file_height'		=> 768,

	'file_sm_path'		=> public_path(). DIRECTORY_SEPARATOR .'images/_fbshare'. DIRECTORY_SEPARATOR .'small'. DIRECTORY_SEPARATOR,
	'file_sm_url'		=> url('images/_fbshare/small/'),
	'file_sm_width'		=> 300,
	'file_sm_height'	=> 225,

	'file_th_path'		=> public_path(). DIRECTORY_SEPARATOR .'images/_fbshare'. DIRECTORY_SEPARATOR .'thumbnail'. DIRECTORY_SEPARATOR,
	'file_th_url'		=> url('images/_fbshare/thumbnail/'),
	'file_th_width'		=> 120,
	'file_th_height'	=> 90,
	
);