<?php

return array(

    'id' => 'captcha',
    'fontsizes' => array(14, 15, 16, 17, 18),
    'length' => 6,
    'width' => 140,
    'height' => 30,
    'space' => 20,
    'colors' => array('0,0,0'),
    'sensitive' => false, // case sensitive (params: true, false)
    'quality' => 80 // image quality

);