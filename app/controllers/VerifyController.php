<?php

/**
	This Controller manages both the displaying of the form
	on the entry page as well as validating the user entry
	and running the entry through the instant win mechanic
**/

class VerifyController extends BaseController {

	public function index($rankey = NULL) {
		if($rankey == NULL) {
			return Redirect::to('/');
		}else{
			return View::make('verify.form');
		}
  }

	public function submit($rankey=NULL) {

		if($rankey == NULL) {
			return Redirect::to('/');
		}else{

			// Check User Information is Correct

			$getWinner = VerifyWinner::Where('rankey','=',$rankey)->Where('active','=',0)->Where('email','=',Input::get('email'))->Where('pcn','=',Input::get('pcn'))->first();
			//_e::prex($getWinner);
			if($getWinner) {
				Session::put('winner', $getWinner->toArray());

				return Redirect::to('verify/update/'.$getWinner['rankey']);

			}else{
				return Redirect::to('/');
			}
		}
	}

	public function update($rankey=NULL) {

		$checkUser = Session::get('winner');

		if(isset($checkUser)) {

			return View::make('verify.update')->with('data', $checkUser);

			//_e::prex($checkUser);
		}else{
			return Redirect::to('/');
		}

	}

	public function update_save() {

		if (Request::getMethod() == 'POST')
		{

			$rules = array(
				'receipt'						=>	'image|required',
				'necktag'						=>	'image|required',
				'address_line_1'		=>	'required',
				'suburb'						=>	'required'
			);

			$messages = array();

			$validator = Validator::make(Input::all(), $rules, $messages);
			if ($validator->fails())
			{
					return Redirect::back()
					->withInput(Input::except('password', 'entercaptcha'))
					->withErrors($validator);
			}
			else
			{
				$winner = VerifyWinner::find(Session::get('winner')['id']);

				$receipt = Input::file('receipt');
				$newreceipt = 'receipt-'.Str::random(20).'.'.$receipt->getClientOriginalExtension();
				$receipt->move('uploads/user_uploads', $newreceipt);

				$necktag = Input::file('necktag');
				$newnecktag = 'necktag-'.Str::random(20).'.'.$necktag->getClientOriginalExtension();
				$necktag->move('uploads/user_uploads', $newnecktag);

				$winner->address_line_1 = Input::get('address_line_1');
				$winner->address_line_2 = Input::get('address_line_2');
				$winner->suburb = Input::get('suburb');
				$winner->receipt_file = $newreceipt;
				$winner->necktag_file = $newnecktag;
				$winner->active = 1;
				$winner->save();

				return View::make('verify.thanks');
			}
		}
	}

}
