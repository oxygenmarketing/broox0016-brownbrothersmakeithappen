<?php

class Admin_UniquecodeController extends \Admin_AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		
		$uID			= Auth::user()->id;
		$_z				= AdminHelper::init();
		$searchterm		= $_z->searchterm;
		$filterby		= $_z->filterby;
		$orderby		= $_z->orderby;
		$dir			= $_z->dir;
		$perpage		= $_z->pagination['perPage'];
		//_e::pre($_z);
		// Get xtotal items for query
		
		

		
		$_total		= UniqueCodes::Where('id','>',0);
		if ($searchterm) {
			
			$_term = explode(' ', $searchterm);
			//_e::prex($_term);
			foreach($_term as $_t) {
				 $_total	= $_total->where( function($_total) use ($_t) {
					 $_total
						->where('uniquecode',	'like', '%'.$_t.'%')
						;
				});
			};
		};
		if ($filterby) {
			foreach ($filterby as $f=>$v) {
				$_total->where($f, '=', $v);
			};
		}
		$_total		= $_total->count();
		
		// return paginated results
		$_data		= UniqueCodes::Where('id','>',0);
		
		if ($searchterm) {
			 $_term = explode(' ', $searchterm);
			//_e::prex($_term);
			foreach($_term as $_t) {
				 $_data	= $_data->where( function($_data) use ($_t) {
					 $_data
						->where('uniquecode',	'like', '%'.$_t.'%')
						;
				});
			};
		};
		if ($filterby) {
			foreach ($filterby as $f=>$v) {
				$_data->where($f, '=', $v);
			};
		}
		$_data		= $_data->orderBy('active','ASC');
		$_data		= $_data->paginate($perpage);

		$data	= (object) array(
			'_d'		=> $_data
		,	'_total'	=> $_total
		,	'filterby'	=> $filterby
		);
				
		//e::sql(); exit;
		return View::make('admin.uniquecodes.index')
			->with('_data', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 
	 * @return Response
	 */
	public function create() {
		return Redirect::action('Admin_IWController@index');
	}

	public function revoke($id) {
		
		$data = Entries::findOrFail($id);
		$data->instantwin_id = -1;
		$data->save();
		
		
		return Redirect::action('Admin_IWController@index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {
		return Redirect::action('Admin_IWController@index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$data	= InstantWin::findOrFail($id);
		return View::make('admin.instantwin.view')
			->with('_data', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$data	= InstantWin::findOrFail($id);
		return View::make('admin.instantwin.form')
			->with('_data', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		
		//_e::prex('Check for winners first...');
		
		$messages	= array();
		$except		= array();
		
		// ALL VALIDATION
		$rules	= array (
			'time_slot'	=> 'required',
		);

		$validator	= Validator::make(Input::all(), $rules, $messages);		
		if ($validator->fails()) {
			Msg::add('error', 'Form validation failed.');
			return Redirect::back()
				->withInput()
				->withErrors($validator);
		} else {
			$data		= InstantWin::findOrFail($id);
			$data->fill(Input::except($except));
			if ( $data->save() ) {
				//
			};
			return Redirect::action('Admin_IWController@index');
		};
			
	}

	public function do_publish($id) {
		$data			= UniqueCodes::find($id);
		$data->active	= ($data->active == 1) ? 0 : 1;
		$data->save();
		return Redirect::action('Admin_UniquecodeController@index');
	}

	
}