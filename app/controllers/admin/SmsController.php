<?php

class Admin_SmsController extends \Admin_AdminController {
	
	public function index() {
		
		$sms_setup	=	Promotion::get();
		$smsdetails = array();
		foreach($sms_setup as $p) {
			$smsdetails[$p->option] = $p->value;
		}
		
		
		
		return View::make('admin.sms.home')
			->with('sms_setup', $smsdetails)
			;
	
	}
	
	public function smsUpdate() {
			
			$except		= array();
			$rules = array();
			$allinputs = Input::except('_token');

            $rules =  array(
				'sms_pre'		=> 'max:160',
				'sms_thanks'		=> 'max:160',
				'sms_invalid'	=> 'max:160',
				'sms_limit'		=> 'max:160',
				'sms_post'		=> 'max:160',
				'sms_unique'		=> 'max:160',
				'sms_barcode'	=> 'max:160',
				'sms_winner'		=> 'winner_message',
			);
			
			Validator::extend('winner_message', function($attribute, $value, $parameters)
			{
				$sms_message = PrizeDetails::orderBy(DB::raw('LENGTH(sms_name)'), 'desc')->first();
				$replace = array('%%PCN%%', '%%PRIZE%%');
				$pcn = 'XXXXXX';
				$prize = $sms_message['sms_name'];
				$_replace = array($pcn, $prize);
				$reply	= str_replace($replace, $_replace, $value); 
				
				if(strlen($reply) < 160) {
					return TRUE;
				}else{
					return FALSE;
				}
			});
			
			$messages = array (
				'winner_message'		=> 'The Winner Message length is too long.',
			);

			
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails())
            {
                return Redirect::back()
					->withInput()
					->withErrors($validator);
            }
            else
            {
				foreach($allinputs as $i=>$input) {
					
					
					$data		= Promotion::Where('option','=',$i)->firstOrFail();
					$data->value = $input;
					$data->save();
				}
				
				return Redirect::to('/admin/sms');
				
            }
			

		

		
			
	}
	
	}