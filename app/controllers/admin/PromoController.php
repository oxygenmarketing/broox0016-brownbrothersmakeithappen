<?php

class Admin_PromoController extends \Admin_AdminController {

	public function index() {
		
		$promo	=	Promotion::get();
		$promodetails = array();
		foreach($promo as $p) {
			$promodetails[$p->option] = $p->value;
		}
		
		//_e::prex($username);
		return View::make('admin.promo.home')
			->with('promo', $promodetails)
			;
	}
	
	public function promoUpdate() {
		//_e::prex(Input::all());
			$except		= array();
			$rules = array();
			$allinputs = Input::except('_token');
			//_e::prex($allinputs);
            $rules =  array(
				'prizes_per_day'		=> 'numeric',
				'start_date'				=> 'required|date_format:Y-m-d H:i:s',
				'end_date'				=> 'required',
				//'support_email'			=> 'required',
				'preview_code'			=> 'required',
				'pre_promo'				=> 'required',
				'post_promo'				=> 'required',
				'account_manager'		=> 'required',
				'client'					=> 'required',
				'project_number'		=> 'required',
				'project_name'			=> 'required',
				//'redirect'				=> 'required',
				'number_of_entries'		=> 'required|numeric',
				'number_of_entries_per'	=> 'required',
				'limited_by'				=> 'required',
			);
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails())
            {
                return Redirect::back()
					->withInput()
					->withErrors($validator);
            }
            else
            {
				if(!array_key_exists('has_prizes', $allinputs)) {
					$allinputs['has_prizes'] = 0;
				};
				if(array_key_exists('prizes_per_day', $allinputs) && $allinputs['prizes_per_day'] == '') {
					$allinputs['prizes_per_day'] = 0;
				};
				if(!array_key_exists('prizes_by_state', $allinputs)) {
					$allinputs['prizes_by_state'] = 0;
				};
				if(!array_key_exists('sms_entry', $allinputs)) {
					$allinputs['sms_entry'] = 0;
				};
				if(!array_key_exists('unique_codes', $allinputs)) {
					$allinputs['unique_codes'] = 0;
				};
				if(!array_key_exists('barcode_validation', $allinputs)) {
					$allinputs['barcode_validation'] = 0;
				};
				foreach($allinputs as $i=>$input) {
					
					
					$data		= Promotion::Where('option','=',$i)->firstOrFail();
					$data->value = $input;
					$data->save();
				}
				
				return Redirect::to('/admin/promotion');
				
            }
			

		

		
			
	}
	
	public function content() {
		
		$uID			= Auth::user()->id;
		$_z				= AdminHelper::init();
		$searchterm		= $_z->searchterm;
		$filterby		= $_z->filterby;
		$orderby		= $_z->orderby;
		$dir			= $_z->dir;
		$perpage		= $_z->pagination['perPage'];
		
		//_e::pre($_z);
		
		// Get total items for query
		if(Auth::user()->user_type == 'ADMIN') {
			$_total		= Content::where('id', '>', 0);
		} else {
			$_total		= Content::where('active','!=',9);
		}
		if ($searchterm) {
			
			$_term = explode(' ', $searchterm);
			//_e::prex($_term);
			foreach($_term as $_t) {
				 $_total	= $_total->where( function($_total) use ($_t) {
					 $_total
						->where('title',	'like', '%'.$_t.'%')
						->orWhere('name',	'like', '%'.$_t.'%')
						->orWhere('value',		'like', '%'.$_t.'%')
						;
				});
			};
		};
		if ($filterby) {
			foreach ($filterby as $f=>$v) {
				$_total->where($f, '=', $v);
			};
		}
		$_total		= $_total->count();
		
		// return paginated results
		if(Auth::user()->user_type == 'ADMIN') {
			$_data		= Content::where('id', '>', 0);
		} else {
			$_data		= Content::where('active','!=',9);
		}
		if ($searchterm) {
			 $_term = explode(' ', $searchterm);
			//_e::prex($_term);
			foreach($_term as $_t) {
				 $_data	= $_data->where( function($_data) use ($_t) {
					 $_data
						->where('title',	'like', '%'.$_t.'%')
						->orWhere('name',	'like', '%'.$_t.'%')
						->orWhere('value',		'like', '%'.$_t.'%')
						;
				});
			};
		};
		if ($filterby) {
			foreach ($filterby as $f=>$v) {
				$_data->where($f, '=', $v);
			};
		}
		$_data		= $_data->orderBy($orderby,$dir);
		$_data		= $_data->paginate($perpage);

		$data	= (object) array(
			'_d'		=> $_data
		,	'_total'	=> $_total
		,	'filterby'	=> $filterby
		);
				
		//_e::sql(); exit;
		return View::make('admin.promo.content')
			->with('_data', $data);
		
	}
	
	public function show($id) {
		$data	= Content::Where('id','=',$id)->first()->toArray();
		
		return View::make('admin.promo.editcontent')
			->with('_data', $data);
	}

	public function addNew() {
		
		return View::make('admin.promo.editcontent')
		;
	}

	public function store() {
		$rules	= array (
			'value'	=> 'required',
			'name'  => 'required',
		);
		
		$messages	= array();
		$validator	= Validator::make(Input::all(), $rules, $messages);
		
		if ($validator->fails()) {
			Msg::add('error', 'Form validation failed.');
			return Redirect::back()
				->withInput(Input::except('password'))
				->withErrors($validator);
		} else {			
			$content	= new Content;
			
			$content->name		= Input::get('name');
			$content->title		= Input::get('title');
			$content->value		= Input::get('value');
	
			
			$content->save();
			

		}
		return Redirect::action('Admin_PromoController@content');	}
	public function update($id) {
		
								
		// ALL VALIDATION
		$rules	= array (
			'value'	=> 'required'
		);
		
		$messages	= array();
		$validator	= Validator::make(Input::all(), $rules, $messages);
		
		if ($validator->fails()) {
			Msg::add('error', 'Form validation failed.');
			return Redirect::back()
				->withInput(Input::except('password'))
				->withErrors($validator);
		} else {			
			$content	= Content::findOrFail($id);

			$content->value		= Input::get('value');
	
			
			$content->save();
			

		}
		return Redirect::action('Admin_PromoController@content');
	}
	
	
	public function do_upload() {
					
			/**
			 * upload.php
			 *
			 * Copyright 2013, Moxiecode Systems AB
			 * Released under GPL License.
			 *
			 * License: http://www.plupload.com/license
			 * Contributing: http://www.plupload.com/contributing
			 */
			
			#!! IMPORTANT: 
			#!! this file is just an example, it doesn't incorporate any security checks and 
			#!! is not recommended to be used in production environment as it is. Be sure to 
			#!! revise it and customize to your needs.
			
			
			// Make sure file is not cached (as it happens for example on iOS devices)
			header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header("Cache-Control: no-store, no-cache, must-revalidate");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			
			/* 
			// Support CORS
			header("Access-Control-Allow-Origin: *");
			// other CORS headers if any...
			if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
				exit; // finish preflight CORS requests here
			}
			*/
			
			// 5 minutes execution time
			@set_time_limit(5 * 60);
			
			// Uncomment this one to fake upload time
			// usleep(5000);
			
			// Settings
			//$targetDir		= ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";
			$targetDir			= Config::get('_system.file_path');
			$targetSmDir		= Config::get('_system.file_sm_path');
			$targetThDir		= Config::get('_system.file_th_path');
			
			$cleanupTargetDir	= true; // Remove old files
			$maxFileAge		= 5 * 3600; // Temp file age in seconds
			
			// Create target dir
			if (!file_exists($targetDir)) {
				@mkdir($targetDir);
			}
			
			// Get a file name
			if (isset($_REQUEST["name"])) {
				$fileName = $_REQUEST["name"];
			} elseif (!empty($_FILES)) {
				$fileName = $_FILES["file"]["name"];
			} else {
				$fileName = uniqid("file_");
			}
			
			$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
			
			// Chunking might be enabled
			$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
			$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
			
			// Remove old temp files	
			if ($cleanupTargetDir) {
				if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
					die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
				}
			
				while (($file = readdir($dir)) !== false) {
					$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
			
					// If temp file is current file proceed to the next
					if ($tmpfilePath == "{$filePath}.part") {
						continue;
					}
			
					// Remove temp file if it is older than the max age and is not the current file
					if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
						@unlink($tmpfilePath);
					}
				}
				closedir($dir);
			}	
			
			// Open temp file
			if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			}
			
			if (!empty($_FILES)) {
				if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
					die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
				}
			
				// Read binary input stream and append it to temp file
				if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
				}
			} else {	
				if (!$in = @fopen("php://input", "rb")) {
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
				}
			}
			
			while ($buff = fread($in, 4096)) {
				fwrite($out, $buff);
			}
			
			@fclose($out);
			@fclose($in);
			
			// Check if file has been uploaded
			if (!$chunks || $chunk == $chunks - 1) {
				// Strip the temp .part suffix off 
				rename("{$filePath}.part", $filePath);
				// Make Smaller image for sliders
				Image::make($filePath)->resize(Config::get('_system.file_sm_width'), Config::get('_system.file_sm_height'))->save($targetSmDir.DIRECTORY_SEPARATOR.$fileName);
				// Make thumbnails for admin
				Image::make($filePath)->resize(Config::get('_system.file_th_width'), Config::get('_system.file_th_height'))->save($targetThDir.DIRECTORY_SEPARATOR.$fileName);
			}
			
			// Return Success JSON-RPC response
			die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
					
		}

}