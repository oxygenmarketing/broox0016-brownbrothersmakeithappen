<?php

class Admin_PrizesController extends \Admin_AdminController {

	public function index() {

		$prizeDetails = PrizeDetails::Where('active','=',1)->get();
		//_e::prex($prizeDetails);
		$allprizes = array();
		$i = 0;
		foreach($prizeDetails as $_pd) {
			$prizePools = PrizePool::Where('prize_details_id','=',$_pd->id)->get();
			$allprizes[$i] = array(
				'id'				=> $_pd->id,
				'name'			=> $_pd->name,
				'web_name'		=> $_pd->web_name,
				'sms_name'		=> $_pd->sms_name,
				'description'	=> $_pd->description,
				'link'			=> $_pd->link,
				'price'			=> $_pd->price,
				'img_url'		=> $_pd->img_url,
				'quantity'		=> 0,
				'active'			=> $_pd->active,

			);
			foreach($prizePools as $_pp) {
				$allprizes[$i]['quantity'] = $allprizes[$i]['quantity'] + $_pp->quantity;
			}

			$i++;
		}

		//_e::prex($prizeDetails);
		return View::make('admin.prize.home')
			->with('_data',$allprizes)
			;
	}

	public function createPrize() {
		if(count(InstantWin::get()) > 0) {
			return Redirect::to('/admin/prizes')
				->with('prizeWarning','You have already generated your prizes')
				;
		}else{
			return View::make('admin.prize.form')
				;
		}
	}

	public function saveDetails() {
		$except		= array();
		$rules = array();
		//$rules =  array('captcha' => array('required', 'captcha'));
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::back()
				->withInput()
				->withErrors($validator);
		} else {
			$data		= new PrizeDetails(Input::except('_token'));
			$data->save();
			return Redirect::to('/admin/prizes/createPools');
		}
	}

	public function update($id) {
			//_e::prex(Input::all());

			$except		= array();
			$rules = array();
            $rules =  array(
				'price' => array('regex:^\d+\.\d{0,2}$^'
				,'description'	=>	array('required')
				,'web_name'		=>	array('required')
			));
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails())
            {
                return Redirect::back()
					->withInput()
					->withErrors($validator);
            }
            else
            {

				$data		= PrizeDetails::findOrFail($id);
				$data->name				= Input::get('name');
				$data->description		= Input::get('description');
				$data->web_name			= Input::get('web_name');
				$data->sms_name			= Input::get('sms_name');
				$data->link				= Input::get('link');
				$data->price			= Input::get('price');
				$data->img_url			= Input::get('img_url');

				$data->save();


				return Redirect::to('/admin/prizes')
				;

            }
	}

	public function createPools() {
			if(count(InstantWin::get()) > 0) {
				return Redirect::to('/admin/prizes')
					->with('prizeWarning','You have already generated your prizes')
					;
			}else{
				$prizesByState = Promotion::Where('option','=','prizes_by_state')->pluck('value');
				$start_date = Promotion::Where('option','=','start_date')->pluck('value');
				$end_date = Promotion::Where('option','=','end_date')->pluck('value');

				$prizeDetails = PrizeDetails::get();


				if(count($prizeDetails) != 0) {
					return View::make('admin.prize.poolsForm')
						->with('prizeDetails',$prizeDetails)
						->with('prizesByState',$prizesByState)
						->with('start_date',$start_date)
						->with('end_date',$end_date)
						;
				}else{
					return Redirect::to('/admin/prizes/createPrize')
					;
				}
			}


	}

	public function savePrizePools() {
		$except		= array();
		$rules = array(
				'prize_details_id'		=>	'required|not_in:0'
				,'quantity'				=>	'required|numeric'

		);

		$prizesByState = Promotion::Where('option','=','prizes_by_state')->pluck('value');
		if($prizesByState == 1) {
			$rules['state_id'] = 'required';
		}

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()
				->withInput()
				->withErrors($validator);
		}
		else
		{
			//_e::prex(Input::all());
			$data		= new PrizePool(Input::except('_token', 'state_id'));
			$data->save();
			$insertedId = $data->id;

			if($prizesByState == 1) {
				foreach(Input::get('state_id') as $stateid) {
					$state		= new StatePrize;
					$state->state_id		= $stateid;
					$state->prize_pools_id	= $insertedId;
					$state->save();
				};
			};

			return Redirect::to('/admin/prizes/')
			;

		}
	}

	public function viewPools($id) {

		$prizeDetails = PrizeDetails::Where('active','=',1)->Where('id','=',$id)->firstOrFail();
		$data	= PrizeDetails::findOrFail($id);

		//_e::prex($data->id);
		$allprizes = array();
		//$i = 0;
		$statePrizeId = array();

			$prizePools = PrizePool::Where('prize_details_id','=',$id)->get();
			$allprizes = array(
				'id'				=> $prizeDetails->id,
				'name'			=> $prizeDetails->name,
				'web_name'		=> $prizeDetails->web_name,
				'sms_name'		=> $prizeDetails->sms_name,
				'description'	=> $prizeDetails->description,
				'link'			=> $prizeDetails->link,
				'price'			=> $prizeDetails->price,
				'img_url'		=> $prizeDetails->img_url,
				'active'			=> $prizeDetails->active,
			);
			$prizesByState = Promotion::Where('option','=','prizes_by_state')->pluck('value');

			foreach($prizePools as $_pp) {
				$statePrizeId[$_pp['id']] = array();

				$stateprize = StatePrize::Where('prize_pools_id','=',$_pp['id'])->get();

				$states	=	States::get();
				foreach($states as $_s) {
						$statePrizeId[$_pp['id']][$_s->id] = 0;
				}

				foreach($stateprize as $_sp) {
					foreach($states as $_s) {
						if($_s->id == $_sp->state_id) {
							$statePrizeId[$_pp['id']][$_s->id] = 1;
						}
					}
					//_e::pre($statePrizeId);
				}
				//_e::pre($statePrizeId);
				$allprizes['data'][] = array(
					'quantity'		=> $_pp['quantity'],
					'start_date'	=> $_pp['start_date'],
					'end_date'		=> $_pp['end_date'],
					'id'			=> $_pp['id'],
					'active'		=> $_pp['active']
				);

				//_e::pre($stateprize);
			}

			//_e::prex($statePrizeId);
		//	$i++;
		//}

		return View::make('admin.prize.viewpools')
			->with('_data', $allprizes)
			->with('_form', $data)
			->with('stateprize', $statePrizeId)
			->with('prizesByState', $prizesByState)
			;

	}

	public function do_publish($prize_id,$pool_id) {
		$data			= PrizePool::find($pool_id);
		$data->active	= ($data->active == 1) ? 0 : 1;
		$data->save();
		return Redirect::to('/admin/prizes/pools/'.$prize_id);
	}

	public function poolState($prize_id, $pool_id, $state_id, $action) {
		if($action == 'insert') {
			$data = new StatePrize;
			$data->state_id = $state_id;
			$data->prize_pools_id = $pool_id;
			$data->save();
			return Redirect::to('admin/prizes/pools/'.$prize_id);
		}else if($action == 'delete'){
			$data = StatePrize::Where('state_id','=',$state_id)->Where('prize_pools_id','=',$pool_id)->delete();
			return Redirect::to('admin/prizes/pools/'.$prize_id);
		}
	}

	public function generatePrizes() {
		if(count(InstantWin::get()) > 0) {
			return Redirect::to('/admin/prizes')
				->with('prizeWarning', 'You have already generated your prizes!');
		}
		//die('no');
		$prizeArray = array();
		$prizePools = PrizePool::Where('active','=',1)->get();
		$prizesPerDay = Promotion::Where('option','=','prizes_per_day')->pluck('value');

		foreach($prizePools as $_pp) {

			$prizeArray[$_pp->id] = array(
				'amount'			=> $_pp->quantity,
				'start_date'		=> $_pp->start_date,
				'end_date'		=> $_pp->end_date
			);
		}
		if($prizesPerDay > 0) {
			// FOR WHEN THERE ARE A SPECIFIC AMOUNT OF PRIZES PER DAY
			$startDate = Promotion::Where('option','=','start_date')->pluck('value');
			$endDate = Promotion::Where('option','=','end_date')->pluck('value');
			$testStartDate = date('Y-m-d H:i:s', strtotime($startDate.' -8 weeks'));
			$testEndDate = date('Y-m-d H:i:s', strtotime($startDate.' -1 day'));

			$allPrizes = array();

			foreach($prizeArray as $_ii=>$_pa) {
				$prizeCount = 0;
				while($prizeCount < $_pa['amount']) {
					$allPrizes[] = $_ii;
					$prizeCount++;
				}
				$prizeCount = $prizeCount + $_pa['amount'];
			}

			$totalPrizeCount = count($allPrizes);
			shuffle($allPrizes);
			$prizeDays = $this->_create_DateRangeArray($startDate,$endDate);
			$testPrizeDays = $this->_create_DateRangeArray($testStartDate,$testEndDate);

			$prizeDaysCount = count($prizeDays);
			//_e::prex($prizeDays);
			if($prizesPerDay > ($totalPrizeCount/$prizeDaysCount)) {
				echo "You Don't have enough prizes set!";
			}else{
				$distributionAmount	= floor( $totalPrizeCount/$prizeDaysCount );
				$remaindernAmount	= $totalPrizeCount % $prizeDaysCount;

				$prizeIndexCount = 0;
				$testPrizeIndexCount = 0;

				foreach($prizeDays as $_pd) {
					$spreadCount = 1;

					while($spreadCount <= $distributionAmount) {
						$winSlot = $this->_create_winslot($_pd);

						$data = new InstantWinGen;
						$data->prize_pool_id	= $allPrizes[$prizeIndexCount];
						$data->time_slot		= $winSlot;
						$data->save();
						$spreadCount++;
						$prizeIndexCount++;
					}
				}
				$countRemaining = 1;
				while($remaindernAmount >= $countRemaining) {
					$winSlot = $this->_create_winslot($prizeDays[array_rand($prizeDays)]);
					$data = new InstantWinGen;
					$data->prize_pool_id	= $allPrizes[$prizeIndexCount];
					$data->time_slot		= $winSlot;
					$data->save();

					$countRemaining++;
					$prizeIndexCount++;
				}


				foreach($testPrizeDays as $_tpd) {
					$spreadCount = 1;

					while($spreadCount <= $distributionAmount) {
						$winSlot = $this->_create_winslot($_tpd);

						$data = new TestWinGen;
						$data->prize_pool_id	= $allPrizes[$testPrizeIndexCount];
						$data->time_slot		= $winSlot;
						$data->save();
						$spreadCount++;
						$testPrizeIndexCount++;
					}
				}
				$countRemaining = 1;
				while($remaindernAmount >= $countRemaining) {
					$winSlot = $this->_create_winslot($prizeDays[array_rand($testPrizeDays)]);
					$data = new TestWinGen;
					$data->prize_pool_id	= $allPrizes[$testPrizeIndexCount];
					$data->time_slot		= $winSlot;
					$data->save();

					$countRemaining++;
					$testPrizeIndexCount++;
				}
			}
			//die();
			  Artisan::call('python:pcngen', array());
			return Redirect::to('/admin/prizes');
		}else{
			// FOR WHEN THERE IS A SPECIFIC PRIZE SPREAD
			foreach($prizeArray as $_ii=>$_pa) {
				$prizeDays = $this->_create_DateRangeArray($_pa['start_date'],$_pa['end_date']);
				$prizeDaysCount = count($prizeDays);
				$totalPrizeCount = (int)$_pa['amount'];
				$_slotArray = array(
					'str'		=> $_pa['start_date'],
					'end'		=> $_pa['end_date']
				);
				$spreadCount = 1;

				while($spreadCount <= $totalPrizeCount) {
					$winSlot = $this->_create_winslot($_slotArray);

					$data = new InstantWinGen;
					$data->prize_pool_id	= $_ii;
					$data->time_slot		= $winSlot;
					$data->save();

					$spreadCount++;
				}

				$testStartDate = date('Y-m-d H:i:s', strtotime($_pa['start_date'].' -8 weeks'));
				$testEndDate = date('Y-m-d H:i:s', strtotime($_pa['start_date'].' -1 day'));

				$testprizeDays = $this->_create_DateRangeArray($testStartDate,$testEndDate);

				$testprizeDaysCount = count($testprizeDays);
				$_slotArray = array(
					'str'		=> $testStartDate,
					'end'		=> $testEndDate
				);
				$spreadCount = 1;

				while($spreadCount <= $totalPrizeCount) {
					$winSlot = $this->_create_winslot($_slotArray);

					$data = new TestWinGen;
					$data->prize_pool_id	= $_ii;
					$data->time_slot		= $winSlot;
					$data->save();

					$spreadCount++;
				}
			}
      Artisan::call('python:pcngen', array());
			return Redirect::to('/admin/prizes');
		}
		die();
	}

	private function _create_winslot($_pd) {
		$min_epoch = strtotime($_pd['str']);
		$max_epoch = strtotime($_pd['end']);
		$rand_epoch = rand($min_epoch, $max_epoch);
		return date('Y-m-d H:i:s', $rand_epoch);
	}

	private function _create_DateRangeArray($strDateFrom,$strDateTo) {

		$aryRange=array();

		$iDateFrom	= strtotime($strDateFrom);
		$iDateTo	= strtotime($strDateTo);

		if ($iDateTo>=$iDateFrom) {
			$tempDate = array(
				//'str'	=> date('Y-m-d H:i:s', ($iDateFrom + 3600)),
				'str'	=> date('Y-m-d H:i:s', ($iDateFrom + 3600)),
				'end'	=> date('Y-m-d 23:59:59',$iDateFrom)
			);

			array_push($aryRange,$tempDate); // first entry
			$iDateFrom+=86400; // add 24 hours
			//_e::pre(date('Y-m-d H:i:s', $iDateFrom));
			while (date('Y-m-d' ,$iDateFrom) < date('Y-m-d' ,$iDateTo)) {

					$tempDate = array(
						'str'	=> date('Y-m-d 00:00:00',$iDateFrom),
						'end'	=> date('Y-m-d 23:59:59',$iDateFrom)
					);

				array_push($aryRange,$tempDate);
				$iDateFrom = $iDateFrom + 86400; // add 24 hours
				//_e::pre(date('Y-m-d H:i:s', $iDateFrom));
			}
				if(date('Y-m-d' ,$iDateFrom) == date('Y-m-d' ,$iDateTo)) {
					$tempDate = array(
						'str'	=> date('Y-m-d 00:00:00',$iDateFrom),
						//'end'	=> date('Y-m-d H:i:s',($iDateTo - 3600))
						'end'	=> date('Y-m-d 23:59:59',$iDateTo)
					);
				}
				array_push($aryRange,$tempDate);
		}

		$aryRange = array_map("unserialize", array_unique(array_map("serialize", $aryRange)));

		return $aryRange;

	}

}
