<?php

class Admin_HomeController extends \Admin_AdminController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index() {
		/*
		**
		**	Set the session that holds the title and project name which is displayed in the header
		**	Then set promo dates, retrieve the Google Analytics code and get account manager details
		**
		*/
		Session::set('title', Promotion::Where('option','=','client')->pluck('value').' : '.Promotion::Where('option','=','project_name')->pluck('value'));

		$promoOptions = array();
		$promoOptions['promoDates'] = 	Admin_HomeController::_promoDates();
		$promoOptions['gacode']		=	Promotion::gacode();
		$promoOptions['promoManager'] = Admin_HomeController::_accountManager();
		$products = Barcode::get()->toArray();
		$promotype = Promotion::type();

		if($promotype == 1) {
			$promoOptions['nextfive'] = Admin_HomeController::_nextfive();
		}

		if(Schema::hasColumn('entries','dob')) {
			$promoOptions['age'] = Admin_HomeController::_ageBreakdown();
		}else{
			//$promoOptions['age'] = array();
		}

			$stateRange = Admin_HomeController::_stateBreakdown();

		if(!empty($products)){
			$bCodes = Admin_HomeController::_productBreakdown();
		}
		/*
		**
		**	Check if promotion has SMS or not
		**	Get all entries from today
		**
		*/

		$today		= date('Y-m-d',time());
		$checksms	= Promotion::sms();

		if($checksms == 1) {
			$regTotWEB	= Entries::where('created_at','>',$today.' 00:00:00')->where('created_at','<',$today.' 23:59:59')->where('sms_id','=',0)->count();

			$regTotSMS	= Entries::where('created_at','>',$today.' 00:00:00')->where('created_at','<',$today.' 23:59:59')->where('sms_id','>',0)->count();
			$data	= array(
				'regTot'	=> array(
					'name'	=> "Total new entries",
					'data'	=> array(
						'Web'	=> $regTotWEB,
						'SMS'	=> $regTotSMS
					)
				),
				'state' => array(
					'name'	=>	"State Breakdown",
					'data'	=>	$stateRange
				),

			);

			if(isset($bCodes)) {
				$data['products'] = array(
					'name'	=>	"Product Breakdown",
					'data'	=>	$bCodes
				);

			}

		}else{
			$regTotWEB	= Entries::where('created_at','>',$today.' 00:00:00')->where('created_at','<',$today.' 23:59:59')->count();
			$data	= array(
				'regTot'	=> array(
					'name'	=> "Total new entries",
					'data'	=> array(
						'Web'	=> $regTotWEB
					)
				),
				'state' => array(
					'name'	=>	"State Breakdown",
					'data'	=>	$stateRange
				)

			);

			if(isset($bCodes)) {
				$data['products'] = array(
					'name'	=>	"Product Breakdown",
					'data'	=>	$bCodes
				);

			}
		}
		return View::make('admin.dashboard.home')
			->with('_data', $data)
			->with('_options', $promoOptions)
			;
	}

	private function _accountManager() {
		$accountmanagerid	=	Promotion::Where('option','=','account_manager')->firstOrFail();

		$accountManager = User::Where('id','=',$accountmanagerid['value'])->firstOrFail();

		$promoManager = array(
			'name'		=>	$accountManager['first_name'].' '.$accountManager['last_name'],
			'email'		=>	$accountManager['email'],
			'phone'		=>	$accountManager['phone'],
			'position'	=>	$accountManager['position']
		);

		return $promoManager;

	}

	private function _nextfive() {

		$instantwin	= InstantWin::Where('active','=',1)->orderby('time_slot')->limit(3)->get();
		//_e::prex($instantwin);
		$nextfive 		= array();
		foreach($instantwin as $_iw) {
			$prizeDetailsid = PrizePool::Where('id','=',$_iw['prize_pool_id'])->pluck('prize_details_id');
			$prizeDetails = PrizeDetails::Where('id','=',$prizeDetailsid)->pluck('name');
			$nextfive[] = array(
				'name'	=>	$prizeDetails,
				'time_slot'	=>	$_iw['time_slot']
			);

		}

		return $nextfive;
	}

	private function _ageBreakdown() {
		$ageArr		= array();
		$ageRange	= array();
		$ageR_13	= 0;
		$ageR_18	= 0;
		$ageR_25	= 0;
		$ageR_35	= 0;
		$ageR_45	= 0;
		$ageR_55	= 0;
		$allAges		= Entries::lists('age');
		//_e::prex($allAges);

		foreach ($allAges as $age) {
			$_age = $age;
			if (array_key_exists($_age, $ageArr)) {
				$ageArr[$_age] = $ageArr[$_age]+1;
			} else {
				$ageArr[$_age] = 1;
			};
			// AGE RANGE
			if ($_age >= 18 && $_age <=24) {
				$ageR_18 = $ageR_18+1;
			} else if ($_age >= 25 && $_age <=34) {
				$ageR_25 = $ageR_25+1;
			} else if ($_age >= 35 && $_age <=44) {
				$ageR_35 = $ageR_35+1;
			} else if ($_age >= 45 && $_age <=54) {
				$ageR_45 = $ageR_45+1;
			} else if ($_age >= 55) {
				$ageR_55 = $ageR_55+1;
			};

		};
		$ageRange	= array(
			'18-24'	=> $ageR_18,
			'25-34'	=> $ageR_25,
			'35-44'	=> $ageR_35,
			'45-54'	=> $ageR_45,
			'55+'	=> $ageR_55
		);
		ksort($ageArr);

		$age	= array(
			'range'	=> array(
				'name' => "Age Breakdown by Range",
				'data' => $ageRange
			),
			'all'	=> array(
				'name' => "All Ages",
				'data' => $ageArr
			),
		);

		return $age;
	}

	private function _productBreakdown() {
		$allProduct	= Entries::get(array('barcode'));
		// _e::prex($allProduct);
		$allBarcode	= Barcode::get(array('product_name'));

		$bCodes	= array();
		foreach ($allBarcode as $bc) {
			$bCodes[$bc->product_name]	= 0;
		};

		$products	= array();
		foreach ($allProduct as $product) {

			$bcc = Barcode::where('first_set','=',$product->barcode)->orWhere('second_set','=',$product->barcode)->first(array('product_name'));
			$bCodes[$bcc->product_name]	= $bCodes[$bcc->product_name]+1;
		};

		return $bCodes;
	}

	private function _stateBreakdown() {
		$stateArr	= array();
		$getState	= Entries::state();
		$stateRange	= array();
		foreach($getState as $state) {
			$stateRange[$state['state']] = $state['count'];
		};

		return $stateRange;
	}

	private function _promoDates() {
		$preDate	=	Promotion::pre();
		$startDate	=	Promotion::start();
		$endDate	=	Promotion::finish();
		$postDate	=	Promotion::post();

		$promoDates = array(
			'preDate'		=>	$preDate['value'],
			'startDate'		=>	$startDate['value'],
			'endDate'		=>	$endDate['value'],
			'postDate'		=>	$postDate['value']
		);

		return $promoDates;

	}

}
