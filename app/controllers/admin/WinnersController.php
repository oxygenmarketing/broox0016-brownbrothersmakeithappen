<?php

class Admin_WinnersController extends \Admin_AdminController {

	public function index() {

		$uID			= Auth::user()->id;
		$_z				= AdminHelper::init();
		$searchterm		= $_z->searchterm;
		$filterby		= $_z->filterby;
		$orderby		= $_z->orderby;
		$dir			= $_z->dir;
		$perpage		= $_z->pagination['perPage'];

		//_e::pre($_z);

		// Get total items for query
		if(Auth::user()->user_type == 'ADMIN') {
			$_total		= Entries::where('id', '>', 0)->Where('instantwin_id','!=',0);
		} else {
			$_total		= Entries::where('active','!=',9)->Where('instantwin_id','!=',0);
		}
		if ($searchterm) {

			$_term = explode(' ', $searchterm);
			//_e::prex($_term);
			foreach($_term as $_t) {
				 $_total	= $_total->where( function($_total) use ($_t) {
					 $_total
						->where('first_name',	'like', '%'.$_t.'%')
						->orWhere('last_name',	'like', '%'.$_t.'%')
						->orWhere('email',		'like', '%'.$_t.'%')
						->orWhere('phone', 'like', '%'.$_t.'%')
						;
				});
			};
		};
		if ($filterby) {
			foreach ($filterby as $f=>$v) {

				if ($f == 'method') {
					switch ($v) {
						case 'web':
							$_total->where('sms_id', '=',0);
							break;
						case 'sms':
							$_total->where('sms_id', '>',0);
							break;
					};
				} else if ($f == 'date-start') {
					if ($v != '')
						$_total->where('created_at', '>=', date('Y-m-d H:i:s',strtotime($v.' 00:00:00')));
				} else if ($f == 'date-end') {
					if ($v != '')
						$_total->where('created_at', '<=', date('Y-m-d H:i:s',strtotime($v.' 23:59:59')));
				} else {
					$_total->where($f, '=', $v);
				};

				//$_total->where('created_at', '>=', date('Y-m-d H:i:s',strtotime($filterby['date-start'].' 00:00:00')))->where('created_at', '<=', date('Y-m-d H:i:s',strtotime($filterby['date-end'].' 23:59:59')));
			};
		}
		$_total		= $_total->count();

		// return paginated results
		if(Auth::user()->user_type == 'ADMIN') {
			$_data		= Entries::where('id', '>', 0)->Where('instantwin_id','!=',0);
		} else {
			$_data		= Entries::where('active','!=',9)->Where('instantwin_id','!=',0);
		}
		if ($searchterm) {
			 $_term = explode(' ', $searchterm);
			//_e::prex($_term);
			foreach($_term as $_t) {
				 $_data	= $_data->where( function($_data) use ($_t) {
					 $_data
						->where('first_name',	'like', '%'.$_t.'%')
						->orWhere('last_name',	'like', '%'.$_t.'%')
						->orWhere('email',		'like', '%'.$_t.'%')
						->orWhere('phone', 'like', '%'.$_t.'%')
						;
				});
			};
		};
		if ($filterby) {
			foreach ($filterby as $f=>$v) {

				if ($f == 'method') {
					switch ($v) {
						case 'web':
							$_data->where('sms_id', '=',0);
							break;
						case 'sms':
							$_data->where('sms_id', '>',0);
							break;
					};
				} else if ($f == 'date-start') {
					if ($v != '')
						$_data->where('created_at', '>=', date('Y-m-d H:i:s',strtotime($v.' 00:00:00')));
				} else if ($f == 'date-end') {
					if ($v != '')
						$_data->where('created_at', '<=', date('Y-m-d H:i:s',strtotime($v.' 23:59:59')));
				} else {
					$_data->where($f, '=', $v);
				};

				//$_data->where('created_at', '>=', date('Y-m-d H:i:s',strtotime($filterby['date-start'].' 00:00:00')))->where('created_at', '<=', date('Y-m-d H:i:s',strtotime($filterby['date-end'].' 23:59:59')));
			};
		}
		$_data		= $_data->orderBy($orderby,$dir);
		$_data		= $_data->paginate($perpage);

		$data	= (object) array(
			'_d'		=> $_data
		,	'_total'	=> $_total
		,	'filterby'	=> $filterby
		);

		//_e::sql(); exit;
		return View::make('admin.winners.index')
			->with('_data', $data);
	}


	public function create() {
		return Redirect::action('Admin_EntriesController@index');
	}

	public function store() {
		return Redirect::action('Admin_EntriesController@index');
	}

	public function show($id) {
		$data	= Entries::Where('id','=',$id)->first()->toArray();
		return View::make('admin.winners.form')
			->with('_data', $data);
	}

	public function edit($id) {
		return Redirect::action('Admin_EntriesController@index');
	}

	public function update($id) {
		//_e::prex(Input::all());
		$rules	= array ();
		$messages	= array();
		$validator	= Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails()) {
			Msg::add('error', 'Form validation failed.');
			return Redirect::back()
				->withInput()
				->withErrors($validator);
		} else {
			$entry	= Entries::findOrFail($id);
			foreach(Input::except('_method','_token','id','instantwin_id','terms','proof','active','rawdata','created_at','updated_at') as $_i=>$_v) {
				$entry->$_i		= $_v;
			}
			//_e::prex($entry);
			$entry->save();

			return Redirect::action('Admin_EntriesController@index');
		};

		return Redirect::action('Admin_EntriesController@index');
	}

	public function destroy($id) {
		return Redirect::action('Admin_EntriesController@index');
	}

	// EXPORT

	public function export() {

		$_data	= Entries::where('id', '>', 0)
							->where('instantwin_id', '>', 0);
		$_data	= $_data->get();
		//_e::prex($_data);

		$titleData = array(
			'id',
			'instantwin_id',
			'prize',
			'prize_grp',
			'pcn',
		);
		$schema = Schema::getColumnListing('entries');

		$titleExclude = array(
			'id',
			'instantwin_id',
			'rawdata',
			'blackboxx',
			'created_at',
		);

		foreach($schema as $s) {
			if (!in_array($s, $titleExclude)) {
				$titleData[] = $s;
			};
		};
		//_e::pre($titleData);

		$contentData = array();
		$exportData[]	= $titleData;
		foreach ($_data as $d) {
				$date	= array_flatten($d->created_at);
				$contentData = $d['attributes'];
				$_dataTmp	= array();
				$_dataTmp[]	= $contentData['id'];
				$_dataTmp[]	= $contentData['instantwin_id'];
				$prize	= InstantWin::find($contentData['instantwin_id']);
				$_dataTmp[]	= $prize->PrizePool->PrizeDetails->name;
				$_dataTmp[]	= $prize->prize_pool_id;
				$_dataTmp[]	= $prize->pcn;
				foreach ($contentData as $_i=>$_d) {
					if (!in_array($_i, $titleExclude)) {
						$_dataTmp[]	= $_d;
					};
				};
				$exportData[]	= $_dataTmp;
		};
		//_e::prex($exportData);

		$now	= strtotime(date('d-m-Y h:i:s'));
		Excel::create('BrownBrothers-Winners_'.$now, function($excel) use($exportData) {
			$excel->sheet('All Entries', function($sheet) use($exportData) {
				$sheet->fromArray($exportData, null, 'A1', false, false);
			});
		})->download('xls');

	}

	// PUBLISH ENTRIES

	public function do_publish($id) {
		$data			= Entries::find($id);
		$data->active	= ($data->active == 1) ? 0 : 1;
		$data->save();
		return Redirect::action('Admin_WinnersController@index');
	}

	public function _notifyWinner($id) {
		//_e::prex($input);
		$input = Entries::Where('id','=',$id)->firstOrFail();

		//_e::prex($input);

		$tmpl		= 'edm.winner';
		$edm['edm_address'] = Promotion::Where('option','=','edm_address')->pluck('value');
		$edm['edm_name'] = Promotion::Where('option','=','edm_name')->pluck('value');
		$edm['edm_subject'] = Promotion::Where('option','=','edm_subject')->pluck('value');
		$edm['email'] = $input['email'];
		$edm['name'] = $input['first_name'].' '.$input['last_name'];
		$data['pcn'] = InstantWin::Where('id','=',$input['instantwin_id'])->pluck('pcn');

		Mail::send('edm.winner', $data, function($message) use ($edm)
		{
				$message->from($edm['edm_address'], $edm['edm_name']);
				$message->to($edm['email'], $edm['name'])->subject('Provisional Instant Prize Winner - Brown Brothers Bucket List Promotion');
		});
			//die('hello');
		$input['edm_sent'] = $input['edm_sent'] + 1;
		$input->save();

		return Redirect::back();

	}


}
