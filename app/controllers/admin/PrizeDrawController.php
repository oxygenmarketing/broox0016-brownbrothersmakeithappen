<?php

class Admin_PrizeDrawController extends \Admin_AdminController {

	public function index() {

	}

	public function generateSchedule() {

		//Check if schedule has already been generated
		if(count(PrizeDraw::get()) == 0) {

				//Get all days in Promotion from start and end date (check if only weekdays are required)
				$promoStartDate = date("Y-m-d H:i:s", strtotime(Promotion::Where('option','=','start_date')->pluck('value')));
				$promoEnd = date("Y-m-d", strtotime(Promotion::Where('option','=','end_date')->pluck('value')));

				$promoStart = $promoStartDate;

				$dateRangeArray = array();

				if($promoEnd >= $promoStart) {

					while($promoStart<=$promoEnd) {
						$dateArray = array();
						$dateArray[$promoStart] = array(
							'day'	=> date('D', strtotime($promoStart))
						);
						array_push($dateRangeArray, $dateArray);
						$promoStart = date('Y-m-d', strtotime($promoStart . " +1days"));
					}
				}

				$prizeDrawArray = array();

				foreach($dateRangeArray as $d) {

					foreach($d as $i=>$_d) {
						if($_d['day'] != 'Fri' && $_d['day'] != 'Sat') {

							if($_d['day'] == 'Sun') {
								$prizeDrawArray[] = array(
									'draw_date'	=>	date('Y-m-d', strtotime($i . " +1days")),
									'start_day'	=> $promoStartDate,
									'end_day'		=>	date('Y-m-d 23:59:59', strtotime($i))
								);
							}else{
								$prizeDrawArray[] = array(
									'draw_date'	=>	date('Y-m-d', strtotime($i . " +1days")),
									'start_day'	=> $promoStartDate,
									'end_day'		=>	date('Y-m-d 23:59:59', strtotime($i))
								);
							}

						}
					}

				}


				foreach($prizeDrawArray as $_data) {
					$create = new PrizeDraw();
					$create->draw_date = $_data['draw_date'];
					$create['start_day'] = $_data['start_day'];
					$create['end_day'] = $_data['end_day'];

					$create->save();
					//_e::prex($create);
				}

				_e::prex($prizeDrawArray);
				//Save schedule to DB
		}else{
			_e::prex('Schedule already created');
		}

	}

	public function runDraw() {
		$today = date('Y-m-d');
		$prizeDrawDay = PrizeDraw::Where('winner_id', '=', 0)->Where('draw_date','<=',$today)->get();

		foreach($prizeDrawDay as $pdd) {
			$winners = PrizeDraw::Where('winner_id','>', 0)->lists('winner_id');
			$winner = Entries::Where('created_at','<=',$pdd->end_day)->Where('state','=','SA')->OrWhereNotIn('id', $winners)->Where('created_at','<=',$pdd->end_day)->orderByRaw("RAND()")->pluck('id');
			if($winner) {
				$pdd->winner_id = $winner;
				$pdd->save();
			};
		}

	}

}
