<?php

class Admin_EntriesController extends \Admin_AdminController {

	public function index() {
		
		$uID			= Auth::user()->id;
		$_z				= AdminHelper::init();
		$searchterm		= $_z->searchterm;
		$filterby		= $_z->filterby;
		$orderby		= $_z->orderby;
		$dir			= $_z->dir;
		$perpage		= $_z->pagination['perPage'];

		//_e::pre($_z);
		
		// Get total items for query
		if(Auth::user()->user_type == 'ADMIN') {
			$_total		= Entries::where('id', '>', 0);
		} else {
			$_total		= Entries::where('active','!=',9);
		}
		if ($searchterm) {
			
			$_term = explode(' ', $searchterm);
			//_e::prex($_term);
			foreach($_term as $_t) {
				 $_total	= $_total->where( function($_total) use ($_t) {
					 $_total
						->where('fn_1',	'like',	'%'.$_t.'%')
						->orWhere('last_name',	'like', '%'.$_t.'%')
						->orWhere('email',	'like', '%'.$_t.'%')
						->orWhere('phone', 'like', '%'.$_t.'%')
						->orWhere('address', 'like', '%'.$_t.'%')
						->orWhere('postcode', 'like', '%'.$_t.'%')
						->orWhere('uniquecode', 'like', '%'.$_t.'%')
						;
				});
			};
		};
		if ($filterby) {
			foreach ($filterby as $f=>$v) {
				
				if ($f == 'method') {
					switch ($v) {
						case 'web':
							$_total->where('sms_id', '=',0);
							break;
						case 'sms':
							$_total->where('sms_id', '>',0);
							break;
					};
				} else if ($f == 'date-start') {
					if ($v != '')
						$_total->where('created_at', '>=', date('Y-m-d H:i:s',strtotime($v.' 00:00:00')));
				} else if ($f == 'date-end') {
					if ($v != '')
						$_total->where('created_at', '<=', date('Y-m-d H:i:s',strtotime($v.' 23:59:59')));
				} else {
					$_total->where($f, '=', $v);
				};
				
				//$_total->where('created_at', '>=', date('Y-m-d H:i:s',strtotime($filterby['date-start'].' 00:00:00')))->where('created_at', '<=', date('Y-m-d H:i:s',strtotime($filterby['date-end'].' 23:59:59')));
			};
		}
		$_total		= $_total->count();
		
		// return paginated results
		if(Auth::user()->user_type == 'ADMIN') {
			$_data		= Entries::where('id', '>', 0);
		} else {
			$_data		= Entries::where('active','!=',9);
		}
		if ($searchterm) {
			 $_term = explode(' ', $searchterm);
			//_e::prex($_term);
			foreach($_term as $_t) {
				 $_data	= $_data->where( function($_data) use ($_t) {
					 $_data
						->where('fn_1',	'like', '%'.$_t.'%')
						->orWhere('last_name',	'like', '%'.$_t.'%')
						->orWhere('email',		'like', '%'.$_t.'%')
						->orWhere('phone', 'like', '%'.$_t.'%')
						->orWhere('address', 'like', '%'.$_t.'%')
						->orWhere('postcode', 'like', '%'.$_t.'%')
						->orWhere('uniquecode', 'like', '%'.$_t.'%')
						;
				});
			};
		};
		if ($filterby) {
			foreach ($filterby as $f=>$v) {
				
				if ($f == 'method') {
					switch ($v) {
						case 'web':
							$_data->where('sms_id', '=',0);
							break;
						case 'sms':
							$_data->where('sms_id', '>',0);
							break;
					};
				} else if ($f == 'date-start') {
					if ($v != '')
						$_data->where('created_at', '>=', date('Y-m-d H:i:s',strtotime($v.' 00:00:00')));
				} else if ($f == 'date-end') {
					if ($v != '')
						$_data->where('created_at', '<=', date('Y-m-d H:i:s',strtotime($v.' 23:59:59')));
				} else {
					$_data->where($f, '=', $v);
				};
				
				//$_data->where('created_at', '>=', date('Y-m-d H:i:s',strtotime($filterby['date-start'].' 00:00:00')))->where('created_at', '<=', date('Y-m-d H:i:s',strtotime($filterby['date-end'].' 23:59:59')));
			};
		}
		$_data		= $_data->orderBy($orderby,$dir);
		$_data		= $_data->paginate($perpage);

		$data	= (object) array(
			'_d'		=> $_data
		,	'_total'	=> $_total
		,	'filterby'	=> $filterby
		);
				
		//_e::sql(); exit;
		return View::make('admin.entries.index')
			->with('_data', $data);
	}

	public function create() {
		return Redirect::action('Admin_EntriesController@index');
	}

	public function store() {
		return Redirect::action('Admin_EntriesController@index');
	}

	public function show($id) {
		$data	= Entries::Where('id','=',$id)->first()->toArray();
		return View::make('admin.entries.form')
			->with('_data', $data);
	}

	public function edit($id) {
		return Redirect::action('Admin_EntriesController@index');
	}

	public function update($id) {
		//_e::prex(Input::all());
		$rules	= array ();	
		$messages	= array();
		$validator	= Validator::make(Input::all(), $rules, $messages);
		
		if ($validator->fails()) {
			Msg::add('error', 'Form validation failed.');
			return Redirect::back()
				->withInput()
				->withErrors($validator);
		} else {			
			$entry	= Entries::findOrFail($id);
			foreach(Input::except('_method','_token','id','instantwin_id','terms','proof','active','rawdata','created_at','updated_at') as $_i=>$_v) {
				$entry->$_i		= $_v;
			}
			//_e::prex($entry);
			$entry->save();
			
			return Redirect::action('Admin_EntriesController@index');
		};
		
		return Redirect::action('Admin_EntriesController@index');
	}

	public function destroy($id) {
		return Redirect::action('Admin_EntriesController@index');
	}
	
	// EXPORT
	
	public function export() {
		
		$uID			= Auth::user()->id;
		
		$_data	= Entries::where('id', '>', 0);
		$_data	= $_data->get();
		//_e::prex($_data);

		$titleData = array();
		$schema = Schema::getColumnListing('entries');
		foreach($schema as $s) {
			$titleData[] = $s;
		}

		unset($titleData[20]);
		unset($titleData[21]);
		unset($titleData[23]);
		//_e::prex($titleData);

		$contentData = array();
		$exportData[]	= $titleData;
		foreach ($_data as $d) {
			
			unset($d['rawdata']);
			unset($d['blackboxx']);
			unset($d['updated_at']);
			
			$date	= array_flatten($d->created_at);
			$contentData = $d['attributes'];
			$exportData[]	= $contentData;
		};
		//_e::prex($exportData);
		
		$now	= strtotime(date('d-m-Y h:i:s'));
		Excel::create('BrownBrothers-AllEntries_'.$now, function($excel) use($exportData) {
			$excel->sheet('All Entries', function($sheet) use($exportData) {
				$sheet->fromArray($exportData, null, 'A1', false, false);
			});
		})->download('xls');	
		
	}

	// PUBLISH ENTRIES
	
	public function do_publish($id) {
		$data			= Entries::find($id);
		$data->active	= ($data->active == 1) ? 0 : 1;
		$data->save();
		return Redirect::action('Admin_EntriesController@index');
	}
	
}