<?php

class Admin_AuthController extends \Admin_AdminController {

    /**
     * Displays the login form
     *
     */
    public function login() {
		if ( Auth::check() ) {
			return Redirect::to('/admin');
		} else {
			return View::make('admin.login');
		}
    }
	
    /**
     * Attempt to do login
     *
     */
    public function do_login() {
		
        //_e::prex( Input::all() );
		$usernameinput		=	Input::get('email');
		$input	= array(
			'email'    	=> $usernameinput,
			'password' 	=> Input::get('password'),
			'remember' 	=> Input::get('remember'),
		);
		$rules	= array(
			'email'			=> 'required',
			'password'		=> 'required'
		);
		$validator = Validator::make($input,$rules);
		if ($validator->passes()) {
			$field			= filter_var($usernameinput, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';	
			$credentials	= array(
				$field			=> $usernameinput,
				'password' 	=> $input['password'],
				'active' 		=> 1,
			);
			$remember	= (Input::get('remember') == 1) ? true : false ;
			$allow		= array('ADMIN','MANAGER','CLIENT');
			
			if (Auth::attempt($credentials,$remember)) {
				return (in_array(Auth::user()->user_type, $allow)) ? Redirect::action('Admin_HomeController@index') : $this->logout() ;
			} else {
				$err_msg = Lang::get('users.message.login.auth_failed');
			};
		} else {
			$err_msg = Lang::get('users.message.login.fields_required');
		};
		
		return Redirect::action('Admin_AuthController@login')
			->withInput(Input::except('password'))
			->with( 'error', $err_msg );

    }
	
    /**
     * Log the user out of the application.
     *
     */
    public function logout() {
        Auth::logout();
        return Redirect::to('/');
    }
	
    /**
     * Displays the forgot password form
     *
     */
    public function forgot_password() {
		return View::make('admin.auth.forgot_password_form');
    }

    /**
     * Attempt to send change password link to the given email
     *
     */
    public function do_forgot_password() {
		
		$email	= Input::only('email');
	
		switch ($response = Password::remind($email, function($message) {
			$message->subject('Your Password Reminder');
		})) {
			case Password::INVALID_USER:
				return Redirect::back()
					->withInput()
					->with('error', Lang::get($response));
			case Password::REMINDER_SENT:
				/* return Redirect::back() */
				return Redirect::to('login')
					->with('status', Lang::get($response));
		}
		
    }
	
    /**
     * Shows the change password form with the given token
     *
     */
    public function reset_password( $token ) {
		return View::make('admin.auth.reset_password_form')
			->with('token', $token);
    }

    /**
     * Attempt change password of the user
     *
     */
    public function do_reset_password() {
		
		$token = '/'.Input::get('token');
		
		$rules = array(
			'email'						=> 'required|email',
			'password'					=> 'required|min:6',
			'password_confirmation'	=> 'same:password',
			//'token'					=> 'exists:token,token'
		);
		$validator = Validator::make(Input::all(),$rules);		
		if ($validator->passes()) {
			
			$credentials = Input::only(
				'email', 'password', 'password_confirmation', 'token'
			);
	
			$response = Password::reset($credentials, function($user, $password) {
				$user->password = Hash::make($password);
				$user->save();
			});
	
			switch ($response) {
				case Password::INVALID_PASSWORD:
				case Password::INVALID_TOKEN:
				case Password::INVALID_USER:
					return Redirect::back()->with('error', Lang::get($response));
	
				case Password::PASSWORD_RESET:
					//Auth::login($user);
					return Redirect::to('/admin');
			}
		};	
		
		$data	= array('email' => Input::get('email'));
		$errors	= $validator->errors()->first();
		return Redirect::to(URL::route('user/reset').$token)
			->withInput($data)
			->with('error', $errors);
			
    }

}