<?php

class Admin_SnapshotsController extends \Admin_AdminController {

	public function index() {

		$_z		= AdminHelper::init();
		$_out	= $this->queryDay();
		//$_out	= $this->queryDay('EntriesByDay');
		//_e::prex($_out);

		$data	= (object) array(
			'_d'		=> $_out
		,	'title'		=> 'Entries by Day'
		,	'filterby'	=> $_z->filterby
		,	'export'	=> 'expEntriesDays'
		);

		return View::make('admin.snapshots.overview')
			->with('_data', $data);
	}

	public function regWeek() {

		$_z		= AdminHelper::init();
		$_out = $this->queryWeek();
		//_e::prex($_out);

		$data	= (object) array(
			'_d'		=> $_out
		,	'title'		=> 'Entries per Week'
		,	'filterby'	=> $_z->filterby
		,	'export'	=> 'expEntriesWeek'
		);

		return View::make('admin.snapshots.weeks')
			->with('_data', $data);

	}

	public function stateDay() {

		//return Redirect::action('Admin_SnapshotsController@index');

		$_z		= AdminHelper::init();
		$returnArray	= $this->stateQuery();
		$_out = $returnArray['_out'];
		$table = $returnArray['table'];

		$data	= (object) array(
			'_d'		=> $_out
		,	'title'		=> 'Entries by State'
		,	'filterby'	=> $_z->filterby
		,	'export'	=> 'expEntriesStates'
		,	'table'	=> $table
		);

		return View::make('admin.snapshots.state')
			->with('_data', $data);

	}

	public function ageDay() {

		$_z		= AdminHelper::init();
		$_out	= $this->ageQuery();
		//_e::prex($_out);

		$data	= (object) array(
			'_d'		=> $_out
		,	'title'		=> 'Entries by Age Range'
		,	'filterby'	=> $_z->filterby
		,	'export'	=> 'expEntriesAge'
		);

		return View::make('admin.snapshots.age')
			->with('_data', $data);
	}

	public function uniqueDay() {

		$_z		= AdminHelper::init();
		$_out	= $this->queryUniqueDay();
		//_e::prex($_out);

		$data	= (object) array(
			'_d'		=> $_out
		,	'title'		=> 'Unique Entries by Day'
		,	'filterby'	=> $_z->filterby
		,	'export'	=> 'expUniqueDays'
		);

		return View::make('admin.snapshots.uniqueday')
			->with('_data', $data);

	}

	public function uniqueWeek() {

		$_z		= AdminHelper::init();
		$_out	= $this->queryUniqueWeek();
		//_e::prex($_out);

		$data	= (object) array(
			'_d'		=> $_out
		,	'title'		=> 'Unique Entries by Week'
		,	'filterby'	=> $_z->filterby
		,	'export'	=> 'expUniqueWeek'
		);

		return View::make('admin.snapshots.uniqueweek')
			->with('_data', $data);

	}

	// EXPORT FUNCTIONS

	public function expEntriesDays() {

		$qStr	= Session::get('expQry');
		$qry	= DB::select($qStr);
		//_e::prex($qry[0]);

		$wtotal = 0;
		$stotal = 0;
		$gtotal = 0;

		$exportData[]		= array('DATE','SMS','WEB','TOTAL');
		foreach ($qry as $o) {
			$exportData[]	= array($o->dateA, (int)$o->sms_total, (int)$o->web_total, (int)$o->total);
			$wtotal			+= (int)$o->web_total;
			$stotal			+= (int)$o->sms_total;
			$gtotal			+= (int)$o->total;
		};
		$exportData[]		= array('TOTAL',$stotal,$wtotal,$gtotal);
		//_e::pre($exportData);

		$this->doExportExcel($exportData,'EntriesWeek');

	}

	public function expEntriesWeek() {

		$qStr	= Session::get('expQry');
		$qry	= DB::select($qStr);
		//_e::pre($qry);

		$wtotal = 0;
		$stotal = 0;
		$gtotal = 0;

		$exportData[]		= array('WK BEG.','SMS','WEB','TOTAL');
		foreach ($qry as $o) {
			$exportData[]	= array($o->weekBeg, (int)$o->sms_total, (int)$o->web_total, (int)$o->total);
			$wtotal			+= (int)$o->web_total;
			$stotal			+= (int)$o->sms_total;
			$gtotal			+= (int)$o->total;
		};
		$exportData[]		= array('TOTAL',$stotal,$wtotal,$gtotal);
		//_e::pre($exportData);

		$this->doExportExcel($exportData,'EntriesWeek');

	}

	public function expEntriesStates() {

		$qStr	= Session::get('expQry');
		$qry	= DB::select($qStr);
		//_e::pre($qry);

		$auStates	= array('ACT','NSW','NT','QLD','SA','TAS','VIC','WA');

		$something	= new Admin_SnapshotsController();
		$TIMESTEP	= $something->getDateRange();
		//_e::pre($TIMESTEP);

		$tablearray = array();
		foreach($TIMESTEP as $ts) {
			$tablearray[$ts]['SMS'] = array();
			$tablearray[$ts]['WEB'] = array();
		};
		foreach($auStates as $os) {
			foreach($TIMESTEP as $_timestep) {
				$tablearray[$_timestep]['SMS'][$os] = 0;
				$tablearray[$_timestep]['WEB'][$os] = 0;
			};
		};
		foreach($qry as $_de) {
			foreach($tablearray as $i=>$_ta) {
				if($i == $_de->dateA){
					$tablearray[$i]['SMS'][$_de->stateA] = $_de->sms_total;
					$tablearray[$i]['WEB'][$_de->stateA] = $_de->web_total;
				};
			};
		};
		//_e::pre($tablearray);

		//_e::prex($_out);
		//exit;

		$zz 	= 0;
		$hPad	= 8;

		$exportData[$zz][] = '';
		$firstkey	= reset($tablearray);
		foreach($firstkey as $i=>$v) {
			$exportData[$zz][] = $i;
			for ($q = 1; $q <= $hPad; $q++) {
				$exportData[$zz][] = '';
			};
		};
		$exportData[$zz][] = '';
		$zz++;

		$exportData[$zz][] = 'DATE';
		$firstkey	= reset($tablearray);
		foreach($firstkey as $i=>$v) {
			foreach($v as $state => $value) {
				$exportData[$zz][] = $state;
			};
			$exportData[$zz][] = 'Total';
		};
		$exportData[$zz][] = 'TOTAL';
		$zz++;

		$footTot	= array('TOTAL');

		foreach ($tablearray as $i=>$o) {
			$exportData[$zz][] = $i; // date
			$_ltot = 0; // line total
			$fx = 1;
			foreach ($o as $_o) {
				$_tot = 0; // running total
				foreach ($_o as $vv) {
					$exportData[$zz][] = (int)$vv;
					$_tot += $vv;
					$_ltot += $vv;
					if (isset($footTot[$fx])) {
						$footTot[$fx] += $vv;
					} else {
						$footTot[$fx] = $vv;
					};
					$fx++;
				};
				$exportData[$zz][] = $_tot;
				if(isset($footTot[$fx])) {
					$footTot[$fx] += $_tot;
				} else {
					$footTot[$fx] = $_tot;
				};
				$fx++;
			};
			$exportData[$zz][] = $_ltot;
			$zz++;

			if(isset($footTot[$fx])) {
				$footTot[$fx] += $_ltot;
			} else {
				$footTot[$fx] = $_ltot;
			};

		};

		$exportData[]	= $footTot;
		//_e::prex($exportData);

		$this->doExportExcel($exportData,'EntriesStates');

	}

	public function expEntriesAge() {

		$qStr	= Session::get('expQry');
		$qry	= DB::select($qStr);
		//_e::pre($qry);

		$_dataE		= $qry;
		$ageRanges = array(
			array('start' => 13, 'end' => 17, 'txt' => '13 - 17'),
			array('start' => 18, 'end' => 24, 'txt' => '18 - 24'),
			array('start' => 25, 'end' => 34, 'txt' => '25 - 34'),
			array('start' => 35, 'end' => 44, 'txt' => '35 - 44'),
			array('start' => 45, 'end' => 54, 'txt' => '45 - 54'),
			array('start' => 55, 'end' => 999999, 'txt' => '55+'),
		);
		$_out		= array();

		foreach ($_dataE as $_d) {
			foreach ($ageRanges as $ai=>$av) {
				$_out[$_d->dateA][$av['txt']]		= 0;
				$_out[$_d->dateA]['total']		= 0;
				foreach ($_dataE as $i=>$v) {
					if ($v->dateA == $_d->dateA) {
						if ($v->ageA >= $av['start'] && $v->ageA <= $av['end']) {
							$_out[$_d->dateA][$av['txt']]	 += $v->total;
						};
						$_out[$_d->dateA]['total']	 += $v->total;
					};
				};
				ksort($_out[$_d->dateA]);
			};
		};

		//_e::prex($_out);
		//exit;

		$total1 = 0;
		$total2 = 0;
		$total3 = 0;
		$total4 = 0;
		$total5 = 0;
		$total6 = 0;
		$gtotal = 0;

		$exportData[]		= array('DATE','13 - 17','18 - 24','25 - 34','35 - 44','45 - 54','55+','TOTAL');
		foreach ($_out as $i=>$o) {
			$o = (array)$o;
			$exportData[]	= array($i, (int)$o['13 - 17'], (int)$o['18 - 24'], (int)$o['25 - 34'], (int)$o['35 - 44'], (int)$o['45 - 54'], (int)$o['55+'], (int)$o['total']);

			$total1 += (int)$o['13 - 17'];
			$total2 += (int)$o['18 - 24'];
			$total3 += (int)$o['25 - 34'];
			$total4 += (int)$o['35 - 44'];
			$total5 += (int)$o['45 - 54'];
			$total6 += (int)$o['55+'];
			$gtotal += (int)$o['total'];

		};
		$exportData[]		= array('TOTAL',$total1,$total2,$total3,$total4,$total5,$total6,$gtotal);
		//_e::prex($exportData);

		$this->doExportExcel($exportData,'EntriesWeek');

	}

	public function expUniqueDays() {

		$qStr	= Session::get('expQry');
		//$qry	= DB::select($qStr);
		//_e::prex($qStr);

		$wtotal = 0;
		$stotal = 0;
		$gtotal = 0;

		$exportData[]		= array('DATE','SMS','WEB','TOTAL');
		foreach ($qStr as $o) {

			$_totl	= (int)$o['sms_total'] + (int)$o['web_total'];

			$exportData[]	= array($o['dateA'], (int)$o['sms_total'], (int)$o['web_total'], $_totl);
			$wtotal			+= (int)$o['web_total'];
			$stotal			+= (int)$o['sms_total'];
			$gtotal			+= $_totl;
		};
		$exportData[]		= array('TOTAL',$stotal,$wtotal,$gtotal);
		//_e::prex($exportData);

		$this->doExportExcel($exportData,'EntriesWeek');

	}

	public function expUniqueWeek() {

		$qry	= Session::get('expQry');

		$tableData		= array();
		$tableTotalW	= 0;
		$tableTotalS	= 0;
		$tableTotalA	= 0;

		foreach ($qry as $ii=>$_gdd) {

				$tableData[$ii]['SMS'] = $_gdd['SMS'];
				$tableData[$ii]['WEB'] = $_gdd['WEB'];

		};

		$wtotal = 0;
		$stotal = 0;
		$gtotal = 0;

		$exportData[]		= array('WK BEG.','SMS','WEB','TOTAL');
		foreach ($tableData as $i=>$_o) {

					$_totl = (int)$_o['SMS'] + (int)$_o['WEB'];

					$exportData[]	= array($i, (int)$_o['SMS'], (int)$_o['WEB'], $_totl);
					$wtotal			+= (int)$_o['WEB'];
					$stotal			+= (int)$_o['SMS'];
					$gtotal			+= $_totl;

		};
		$exportData[]		= array('TOTAL',$stotal,$wtotal,$gtotal);

		$this->doExportExcel($exportData,'EntriesWeek');

	}

	// QUERY FUNCTIONS

	private static function queryDay() {
		$_out = array();
		$_z				= AdminHelper::init();
		$filterby		= $_z->filterby;
		$perpage		= $_z->pagination['perPage'];

		$something		= new Admin_SnapshotsController();
		$TIMESTEP 		= $something->getDateRange($filterby['date-start'],$filterby['date-end']);
		$_dateArr		= array();
		$_format		= 'Y-m-d';

		$qStr	= "
		select
			cast(`entries`.`created_at` as date) AS dateA,
			(
				select
					count(`entries`.`id`)
				from
					`entries`
				where
					(
						(`entries`.`sms_id` = 0)
						and
						(dateA = cast(`entries`.`created_at` as date))
					)
			) AS `web_total`,
			(
				select
					count(`entries`.`id`)
				from
					`entries`
				where
					(
						(`entries`.`sms_id` > 0)
						and
						(dateA = cast(`entries`.`created_at` as date))
					)
			) AS `sms_total`,
			count(`entries`.`id`) AS `total`
		from
			`entries`
		group by
			dateA
		";
		Session::put('expQry',$qStr);

		$qry	= DB::select($qStr);
		//_e::prex($qry);

		$_dataE = $qry;

		$ii	= 0;
		$ii = 0;
		foreach($TIMESTEP as $_ts) {
			$_out['Web'][$ii] = array(
				'name' => date('d/m/Y',strtotime($_ts)),
				'date' => $_ts,
				'data' => array(0)
			);
			foreach($_dataE as $_d) {
				if($_ts == $_d->dateA) {
					$_out['Web'][$ii]['data'] = array($_d->web_total);
				};
			};
			$ii++;
		};
		foreach($TIMESTEP as $_ts) {
			$_out['SMS'][$ii] = array(
				'name' => date('d/m/Y',strtotime($_ts)),
				'date' => $_ts,
				'data' => array(0)
			);
			foreach($_dataE as $_d) {
				if($_ts == $_d->dateA) {
					$_out['SMS'][$ii]['data'] = array($_d->sms_total);
				};
			};
			$ii++;
		};
		//_e::prex($_out);

		return $_out;
	}

	private static function queryWeek() {
		$_out = array();
		$_z				= AdminHelper::init();
		$filterby		= $_z->filterby;
		$perpage		= $_z->pagination['perPage'];

		$qStr	= "
		select
			str_to_date(concat(yearweek(`entries`.`created_at`,0),' Sunday'),'%X%V %W') AS weekBeg,
			(
				select
					count(`entries`.`id`)
				from
					`entries`
				where
					`entries`.`sms_id` = 0
				AND
					`entries`.`created_at` BETWEEN
						weekBeg
						AND
						weekBeg + INTERVAL 1 WEEK
			) AS `web_total`,
			(
				select
					count(`entries`.`id`)
				from
					`entries`
				where
					`entries`.`sms_id` > 0
				AND
					`entries`.`created_at` BETWEEN
						weekBeg
						AND
						weekBeg + INTERVAL 1 WEEK
			) AS `sms_total`,
			count(distinct `entries`.`id`) AS `total`
		from
			`entries`
		group by
			weekBeg
		order by
			weekBeg
		";
		Session::put('expQry',$qStr);

		$qry	= DB::select($qStr);
		//_e::prex($qStr);

		$_dataE = $qry;
		foreach ($_dataE as $d) {
			$_out['Web'][] = array(
				'date'	=> $d->weekBeg,
				'data'	=> array($d->web_total)
			);
			$_out['SMS'][] = array(
				'date'	=> $d->weekBeg,
				'data'	=> array($d->sms_total)
			);
		};

		return $_out;

	}

	private static function stateQuery() {
		$_out = array();
		$_z				= AdminHelper::init();
		$filterby		= $_z->filterby;
		$perpage		= $_z->pagination['perPage'];
		$qstr = "
		select
			cast(`entries`.`created_at` AS date) AS dateA,
			`entries`.`state` AS stateA,
			(
				select
					count(`entries`.`id`)
				from
					`entries`
				where
					(
						(`entries`.`sms_id` = 0)
					and
						(dateA = cast(`entries`.`created_at` AS date))
					and
						(stateA = `entries`.`state`)
					)
			) AS `web_total`,
			(
				select
					count(`entries`.`id`)
				from
					`entries`
				where
					(
						(`entries`.`sms_id` > 0)
					and
						(dateA = cast(`entries`.`created_at` AS date))
					and
						(stateA = `entries`.`state`)
					)
			) AS `sms_total`,
			count(distinct `entries`.`id`) AS `total`
		from
			`entries`
		where
			`entries`.`id` > 0
			";

		if ($filterby) {
			foreach ($filterby as $f=>$v) {

				if ($f == 'date-start') {
					if ($v != '')
						$qstr .= " AND `entries`.`created_at` >= '".date('Y-m-d H:i:s',strtotime($v.' 00:00:00'))."'";
				} else if ($f == 'date-end') {
					if ($v != '')
						$qstr .= " AND `entries`.`created_at` <= '".date('Y-m-d H:i:s',strtotime($v.' 23:59:59'))."'";
				} else {
					//$_total->where($f, '=', $v);
				};

				//$_total->where('created_at', '>=', date('Y-m-d H:i:s',strtotime($filterby['date-start'].' 00:00:00')))->where('created_at', '<=', date('Y-m-d H:i:s',strtotime($filterby['date-end'].' 23:59:59')));
			};
		}

		$qstr .="
		group by
			dateA,
			stateA
		order by
			dateA
		";
		//_e::prex($qstr);
		Session::put('expQry',$qstr);

		$qry	= DB::select($qstr);

		$results = array();
		$_dataE		= $qry;
		$_out	= array();
		$auStates	= array('ACT','NSW','NT','QLD','SA','TAS','VIC','WA');
		$types = array('sms_total','web_total');
		$startDate = Promotion::Where('option','=','start_date')->pluck('value');
		$endDate = date('Y-m-d');

		$something		= new Admin_SnapshotsController();
		$TIMESTEP 		= $something->getDateRange($filterby['date-start'],$filterby['date-end']);
		//_e::prex($TIMESTEP);

		$web = array();
		$sms = array();
		foreach($auStates as $_st) {
			$data[$_st] = array();
			foreach($types as $_t) {
				$results[] = array(
					'name' => $_t,
					'data' => array(),
					'stack'	=> $_st
				);
			};
		};
		foreach($auStates as $os) {
			foreach($TIMESTEP as $_timestep) {
				$sms[$os][$_timestep] = 0;
				$web[$os][$_timestep] = 0;
			};
		};
		//_e::prex($TIMESTEP);
		foreach($_dataE as $_de) {
			foreach($sms as $i=>$_as) {
				if($i == $_de->stateA) {
					$sms[$i][$_de->dateA] = $_de->sms_total;
				};
			};
			foreach($web as $i=>$_as) {
				if($i == $_de->stateA) {
					$web[$i][$_de->dateA] = $_de->web_total;
				};
			};
		};
		foreach($results as $ind=>$_re) {
			if($_re['name'] == 'sms_total') {
				$datarun = $sms[$_re['stack']];
			} else if($_re['name'] == 'web_total') {
				$datarun = $web[$_re['stack']];
			};
			$results[$ind]['data'] = $datarun;
		};
		$_out = $results;

		$tablearray = array();
		foreach($TIMESTEP as $ts) {
			$tablearray[$ts]['SMS'] = array();
			$tablearray[$ts]['WEB'] = array();
		};
		foreach($auStates as $os) {
			foreach($TIMESTEP as $_timestep) {
				$tablearray[$_timestep]['SMS'][$os] = 0;
				$tablearray[$_timestep]['WEB'][$os] = 0;
			};
		};
		foreach($_dataE as $_de) {
			foreach($tablearray as $i=>$_ta) {
				if($i == $_de->dateA){
					$tablearray[$i]['SMS'][$_de->stateA] = $_de->sms_total;
					$tablearray[$i]['WEB'][$_de->stateA] = $_de->web_total;
				};
			};
		};

		//_e::pre($_out);
		//_e::prex($tablearray);

		$returnArray = array(
			'_out' => $_out,
			'table' => $tablearray
		);

		return $returnArray;

	}

	private static function ageQuery() {
		$_out = array();
		$_z				= AdminHelper::init();
		$filterby		= $_z->filterby;
		$perpage		= $_z->pagination['perPage'];

		$something 		= new Admin_SnapshotsController();
		$TIMESTEP 		= $something->getDateRange($filterby['date-start'],$filterby['date-end']);

		$qStr	= "
		select
			cast(`entries`.`updated_at` as date) AS dateA,
			count(`entries`.`id`) AS `total`,
			floor(((to_days(cast(now() as date)) - to_days(cast(`entries`.`dob` as date))) / 365.25)) AS ageA
		from
			`entries`
		where
			(`entries`.`active` = 1)
		group by
			ageA ASC,
			dateA
		order by
			dateA
		";
		$qry	= DB::select($qStr);
		Session::put('expQry',$qStr);
		//_e::pre($qry[0]);
		//_e::sql($qry);
		//exit();
		$_dataE		= $qry;
		$ii = 0;
		$ageRanges = array(
			array('start' => 13, 'end' => 17),
			array('start' => 18, 'end' => 24),
			array('start' => 25, 'end' => 34),
			array('start' => 35, 'end' => 44),
			array('start' => 45, 'end' => 54),
			array('start' => 55, 'end' => 999999),
		);

		foreach($ageRanges as $_ar) {
			$_out[$ii] = array(
				'name'	=> ($_ar['start'] == 55) ? '55+' : $_ar['start'].' - '.$_ar['end'],
				'data'	=> array()
			);
			foreach($TIMESTEP as $_ts) {
				$_out[$ii]['data'][$_ts] = 0;
				foreach($_dataE as $_d) {
					if($_ts == $_d->dateA && $_d->ageA >= $_ar['start'] && $_d->ageA <= $_ar['end']){
						$_out[$ii]['data'][$_ts] += (int)$_d->total;
					};
				};
			};
			$ii++;
		}
		//_e::prex($_out);

		return $_out;

	}

	private static function queryUniqueDay() {
		$_out = array();
		$_z				= AdminHelper::init();
		$filterby		= $_z->filterby;
		$perpage		= $_z->pagination['perPage'];

		$something		= new Admin_SnapshotsController();
		$TIMESTEP 		= $something->getDateRange($filterby['date-start'],$filterby['date-end']);
		$_dateArr		= array();

		$smsQry	= "
		SELECT  DATE(`created_at`) Date, COUNT(DISTINCT `email`) count
		FROM    `entries`
		WHERE `sms_id` > 0
		GROUP   BY  DATE(`created_at`)
		";

		$webQry	= "
		SELECT  DATE(`created_at`) Date, COUNT(DISTINCT `email`) count
		FROM    `entries`
		WHERE `sms_id` = 0
		GROUP   BY  DATE(`created_at`)
		";

		$sms	= DB::select($smsQry);
		$web	= DB::select($webQry);

		foreach($TIMESTEP as $ii=>$_d) {
			$_out['Web'][$ii] = array(
				'date'	=> $_d,
				'data'	=> array(0)
			);
			$_out['SMS'][$ii] = array(
				'date'	=> $_d,
				'data'	=> array(0)
			);
			$exprt[$ii] = array(
				'dateA'		=> $_d,
				'web_total'		=> 0,
				'sms_total'		=> 0
			);

			foreach($sms as $s) {

				if($_d == $s->Date) {
					$_out['SMS'][$ii]['data'][0] = $s->count;
					$exprt[$ii]['sms_total'] = $s->count;
				}
			}
			foreach($web as $w) {

				if($_d == $w->Date) {
					$_out['Web'][$ii]['data'][0] = $w->count;
					$exprt[$ii]['web_total'] = $w->count;
				}
			}
		}
		//_e::prex($exprt);
		Session::put('expQry',$exprt);

		return $_out;

	}

	private static function queryUniqueWeek() {

		$_z				= AdminHelper::init();
		$filterby		= $_z->filterby;
		$perpage		= $_z->pagination['perPage'];

		$something		= new Admin_SnapshotsController();
		$TIMESTEP 		= $something->getDateRange($filterby['date-start'],$filterby['date-end']);
		$_dateArr		= array();

		$startweek = Promotion::Where('option','=','start_date')->pluck('value');
		$swn = new DateTime($startweek);
		$startweeknumber = $swn->format('YW');

		$endweek = Promotion::Where('option','=','end_date')->pluck('value');
		$ewn = new DateTime($endweek);
		$endweeknumber = $ewn->format('YW');
		$startweek = date('Y-m-d', strtotime($startweek));
		$datesArray = array();

		while((int)$startweeknumber <= (int)$endweeknumber) {

			$datesArray[$startweek] = array(
					'SMS'		=> 0,
					'WEB'		=> 0
			);
			//_e::pre($startweek);
			//_e::pre($startweeknumber);

			$startweek = date('Y-m-d', strtotime($startweek."+1 week"));

			$startweeknumber = new DateTime($startweek);
			//$startweeknumber->add(new DateInterval('P7D'));
			$startweeknumber = $startweeknumber->format('YW');
			//_e::prex($startweeknumber);
		}
		//_e::prex($datesArray);


		$sms = "SELECT extract(week from `created_at`) as week,
       extract(year from `created_at`) as year,
       COUNT(DISTINCT `email`) count
FROM entries
WHERE `sms_id` > 0
GROUP BY extract(week from `created_at`),
         extract(year from `created_at`)";

	 $web = "SELECT extract(week from `created_at`) as week,
      extract(year from `created_at`) as year,
      COUNT(DISTINCT `email`) count
FROM entries
WHERE `sms_id` = 0
GROUP BY extract(week from `created_at`),
        extract(year from `created_at`)";

				$sms_qry	= DB::select($sms);
				$web_qry	= DB::select($web);
		//_e::prex($sms_qry);
		foreach($sms_qry as $i=>$d) {
			$sms_qry[$i]->date = date("Y-m-d", strtotime($d->year."W".($d->week + 1)));
		};
		foreach($web_qry as $i=>$d) {
			$web_qry[$i]->date = date("Y-m-d", strtotime($d->year."W".($d->week + 1)));
		};

		foreach($datesArray as $ii=>$dd) {

			foreach($sms_qry as $i=>$d) {
				if($ii == $d->date){
					$datesArray[$ii]['SMS'] = $d->count;
				}
			};

			foreach($web_qry as $i=>$d) {
				if($ii == $d->date){
					$datesArray[$ii]['WEB'] = $d->count;
				}
			};


		}
		//_e::prex($datesArray);
		Session::put('expQry',$datesArray);
		return $datesArray;

	}

	private static function queryProductsDay() {
		$_out = array();
		$_z				= AdminHelper::init();
		$filterby		= $_z->filterby;
		$perpage		= $_z->pagination['perPage'];

		$something		= new Admin_SnapshotsController();
		$TIMESTEP 		= $something->getDateRange($filterby['date-start'],$filterby['date-end']);
		$_dateArr		= array();

		$qry	= DB::select("
		select
			cast(`entries`.`created_at` AS date) AS dateA,
			`entries`.`barcode` AS barcodeA,
			(
				select
					count(`entries`.`id`)
				from
					`entries`
				where
					(
						(`entries`.`sms_id` = 0)
					and
						(dateA = cast(`entries`.`created_at` AS date))
					and
						(barcodeA = `entries`.`barcode`)
					)
			) AS `web_total`,
			(
				select
					count(`entries`.`id`)
				from
					`entries`
				where
					(
						(`entries`.`sms_id` > 0)
					and
						(dateA = cast(`entries`.`created_at` AS date))
					and
						(barcodeA = `entries`.`barcode`)
					)
			) AS `sms_total`,
			count(distinct `entries`.`id`) AS `total`
		from
			`entries`
		group by
			dateA,
			barcodeA
		order by
			dateA
		");

		//_e::pre($qry);
		$_dataE		= $qry;

		foreach ($_dataE as $d) {

			$barcodes	= Barcode::where('last_five_first','=',$d->barcodeA)->orWhere('last_five_second','=',$d->barcodeA)->first(array('product_name'));
			//_e::prex($barcodes->product_name);

			$_out[$d->dateA][$d->barcodeA]	= array(
				'Name'	=> $barcodes->product_name,
				'Web'	=> $d->web_total,
				'SMS'	=> $d->sms_total,
			);

			/*
			$_out[$d->dateA]['Barcode']	= $d->barcodeA;
			$_out[$d->dateA]['Web'][]	= $d->web_total;
			$_out[$d->dateA]['SMS'][]	= $d->sms_total;
			*/
		};
		//_e::prex($_out);

		return $_out;

	}

	// TRANSFORM FUNCTIONS

	private function formatDays($TIMESTEP,$_data) {
		$_dateArr	= array();
		$_out		= array();
		foreach ($TIMESTEP as $_ts) {
			$_dateArr[$_ts]	= 0;
			foreach ($_data as $_d) {
				if ( ($_d->created_at >= $_ts.' 00:00:00' ) && ($_d->created_at <= $_ts.' 23:59:59' )) {
					//_e::pre($_d->updated_at);
					$_dateArr[$_ts]++;
				};
			};
		};
		foreach ($_dateArr as $i=>$_d) {
			$_out[]	= array(
				'name' => date('d/m/Y',strtotime($i)),
				'date' => $i,
				'data' => array($_d)
			);
		};
		return $_out;
	}

	private function formatWeek($data) {
		$_out		= array();
		$_dateArr = $this->groupToWeeks($data);
		foreach ($_dateArr as $i=>$_d) {
			$_out[]	= array(
				'name' => date('d/m/Y',strtotime($_d['str'])).' - '.date('d/m/Y',strtotime($_d['end'])),
				'date' => $_d['str'],
				'data' => array($_d['entries'])
			);
		}
		return $_out;
	}

	// UTITLIY FUNCTION

	private function getDateRange($startDate=NULL,$endDate=NULL) {

		$format		= 'Y-m-d';
		$allEntries	= Entries::groupBy(DB::raw('DATE(created_at)'))->orderBy('created_at','ASC')->get(array('created_at'));

		if($startDate) {
			$startDate = date('Y-m-d', strtotime($startDate));
		}
		if($endDate) {
			$endDate = date('Y-m-d', strtotime($endDate));
		}

		$_dates = array();
		foreach ($allEntries as $entry) {
			$dd			= array_flatten($entry->created_at);
			$_dates[] =  date('Y-m-d',strtotime($dd[0]));
		};
		if ($_dates) {
			$_entriesMin	= min($_dates);
			$_entriesMax	= max($_dates);
		} else {
			$now 			= date($format,time());
			$_entriesMin	= $now;
			$_entriesMax	= $now;
		};

		$getStartTmp	= ($startDate)	? $startDate : $_entriesMin;
		$getEndTmp		= ($endDate)	? $endDate : $_entriesMax;

		if ($getStartTmp < $getEndTmp) {
			$getStart		= $getStartTmp;
			$getEnd			= $getEndTmp;
		} else {
			$getStart		= $getEndTmp;
			$getEnd			= $getStartTmp;
		};

		//_e::pre($getStart);
		//_e::prex($getEnd);

		//return $this->createDateRangeArray($allEntriesMin->created_at,$allEntriesMax->created_at);
		return $this->createDateRangeArray($getStart,$getEnd);
	}

	private function createDateRangeArray($strDateFrom,$strDateTo) {
		// takes two dates formatted as YYYY-MM-DD and creates an
		// inclusive array of the dates between the from and to dates.

		// could test validity of dates here but I'm already doing
		// that in the main script

		//_e::pre($strDateFrom);
		//_e::pre($strDateTo);

		$aryRange=array();

		//$iDateFrom	= mktime(1,0,0,date('d',strtotime($strDateFrom)),date('m',strtotime($strDateFrom)),date('Y',strtotime($strDateFrom)));
		//$iDateTo	= mktime(1,0,0,date('d',strtotime($strDateTo)),date('m',strtotime($strDateTo)),date('Y',strtotime($strDateTo)));
		$iDateFrom	= strtotime($strDateFrom);
		$iDateTo	= strtotime($strDateTo);

		//_e::pre($iDateFrom);
		//_e::prex($iDateTo);

		if ($iDateTo>=$iDateFrom) {
			array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
			while ($iDateFrom<$iDateTo) {
				$iDateFrom+=86400; // add 24 hours
				array_push($aryRange,date('Y-m-d',$iDateFrom));
			}
		}

		//_e::prex($aryRange);
		return $aryRange;
	}

	private function groupToWeeks($data) {

		$key_dates		= Config::get('_promotion.key_dates');
		$now			= time();
		$TIMESTEPWEEK 	= $this->createDateRangeArray(date('Y-m-d',strtotime($key_dates['start'])),date('Y-m-d',$now));
		//$TIMESTEPWEEK 	= $this->createDateRangeArray(date('Y-m-d',strtotime($key_dates['start'])),date('Y-m-d',strtotime($key_dates['end'])));
		$DAYCOUNT		= count($TIMESTEPWEEK);
		$WEEKCOUNT		= ceil(count($TIMESTEPWEEK)/7);
		$weekGrp		= array();
		$c				= 0;

		do {
			$x 			= (int)$c*7;
			$y 			= (int)$x+6;
			$wkStart	= $TIMESTEPWEEK[$x];
			$wkEnd		= array_key_exists($y, $TIMESTEPWEEK) ? $TIMESTEPWEEK[$y] : end($TIMESTEPWEEK);
			$weekGrp[$c]	= array(
				'str'		=> $wkStart,
				'end'		=> $wkEnd,
				'entries'	=> 0
			);
			foreach ($data as $i=>$v) {
				if ( ($i >= $wkStart ) && ( $i <= $wkEnd) ) {
					$weekGrp[$c]['entries'] = $weekGrp[$c]['entries'] + (int)$v['Entries'];
				}
			};
			$c++;
		} while ($c < $WEEKCOUNT);

		return $weekGrp;

	}

	private function doExportExcel($data,$name) {
		$client = 'TheVoice';
		$now	= strtotime(date('d-m-Y h:i:s'));
		Excel::create($client.'-'.$name.'_'.$now, function($excel) use($data) {
			$excel->sheet('Data', function($sheet) use($data) {
				$sheet->fromArray($data, null, 'A1', false, false);
			});
		})->download('xls');
	}

}
