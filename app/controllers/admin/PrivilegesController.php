<?php

class Admin_PrivilegesController extends \Admin_AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		
		$uID			= Auth::user()->id;
		$_z				= AdminHelper::init();
		$searchterm	= $_z->searchterm;
		$filterby		= $_z->filterby;
		$orderby		= $_z->orderby;
		$dir			= $_z->dir;
		$perpage		= $_z->pagination['perPage'];
		
		//_e::pre($_z);
		
		// Get total items for query
		$_total		= User::where('id', '!=', $uID)
						->where('user_type','!=','ADMIN')
						->where('active','!=',9);
		if (Auth::user()->user_type == 'ADMIN') $_total = $_total->orWhere('user_type','=','ADMIN');
		//->whereIn('active',array(0,1))
		if ($searchterm) {
			 $_total	= $_total->where( function($_total) use ($searchterm) {
				 $_total
				 	->where('username', 'like', '%'.$searchterm.'%')
					->orWhere('email', 'like', '%'.$searchterm.'%');
            });
		};
		if ($filterby) {
			foreach ($filterby as $f=>$v) {
				$_total->where($f, '=', $v);
			};
		}
		$_total		= $_total->count();
		
		// return paginated results
		$_data		= User::where('id', '!=', $uID)
						->where('user_type','!=','ADMIN')
						->where('active','!=',9);
		if (Auth::user()->user_type == 'ADMIN') $_data = $_data->orWhere('user_type','=','ADMIN');
		//->whereIn('active',array(0,1));
		if ($searchterm) {
			 $_data		= $_data->where( function($_data) use ($searchterm) {
                $_data
					->where('username', 'like', '%'.$searchterm.'%')
					->orWhere('email', 'like', '%'.$searchterm.'%');
            });
		};
		if ($filterby) {
			foreach ($filterby as $f=>$v) {
				$_data->where($f, '=', $v);
			};
		}
		$_data		= $_data->orderBy($orderby,$dir);
		$_data		= $_data->paginate($perpage);

		$data	= (object) array(
			'_d'		=> $_data
		,	'_total'	=> $_total
		,	'filterby'	=> $filterby
		);
				
		//_e::sql();
		return View::make('admin.privileges.index')
			->with('_data', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		return View::make('admin.privileges.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {
				
		// ALL VALIDATION
        $rules	= array (
            'username'	=> 'required|unique:users|max:255',
            'email'		=> 'required|email|unique:users|max:255',
            'password'	=> 'required|alpha_num|min:6|max:12|confirmed',
        );
		if (Auth::user()->user_type == 'ADMIN') {
			$rules['user_type']	= 'required|not_in:0';
		};
		$messages	= array();
		$validator	= Validator::make(Input::all(), $rules, $messages);
		if ($validator->fails()) {
			Msg::add('error', 'Form validation failed.');
			return Redirect::back()
				->withInput(Input::except('password'))
				->withErrors($validator);
		} else {
			$user	= new User();
			$user->user_type	= (Input::get('user_type')) ? Input::get('user_type') : 'MANAGER';
			$user->username		= Input::get('username');
			$user->first_name		= Input::get('first_name');
			$user->last_name		= Input::get('last_name');
			$user->phone		= Input::get('phone');
			$user->position		= Input::get('position');
			$user->email		= Input::get('email');
			$user->password 	= Input::get('password');
			$user->active		= (Input::get('active')) ? Input::get('active') : 0;
			$user->save();

			return Redirect::action('Admin_PrivilegesController@index');
		};
			
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		return Redirect::action('Admin_PrivilegesController@index');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$data	= User::findOrFail($id);
		if ( Auth::user()->user_type == 'MANAGER' && $data->user_type != 'MANAGER') return Redirect::action('Admin_PrivilegesController@index');
		return View::make('admin.privileges.form')
			->with('_data', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
						
		// ALL VALIDATION
		$rules	= array (
			'username'	=> 'required|max:255|unique:users,username,'.$id,
			'email'		=> 'required|email|max:255|unique:users,email,'.$id,
		);
		if (Input::get('password')) {
			$rules['password']	 = 'alpha_num|min:6|max:12|confirmed';
		};
		if (Input::get('user_type')) {
			$rules['user_type']	= 'required|not_in:0';
		};
		
		$messages	= array();
		$validator	= Validator::make(Input::all(), $rules, $messages);
		
		if ($validator->fails()) {
			Msg::add('error', 'Form validation failed.');
			return Redirect::back()
				->withInput(Input::except('password'))
				->withErrors($validator);
		} else {			
			$user	= User::findOrFail($id);

			$user->username		= Input::get('username');
			$user->first_name		= Input::get('first_name');
			$user->last_name		= Input::get('last_name');
			$user->phone		= Input::get('phone');
			$user->position		= Input::get('position');
			$user->email		= Input::get('email');
			if (Input::get('password')) $user->password = Input::get('password');
			$user->user_type	= Input::get('user_type');
			$user->active		= (Input::get('active')) ? Input::get('active') : 0;

			$user->save();
			
			return Redirect::action('Admin_PrivilegesController@index');
		};
			
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$data			= User::find($id);
		$data->active	= 9; // soft delete
		$data->save();
		return Redirect::action('Admin_PrivilegesController@index');
	}

		/**
		 * Activeate/Deactivate the specified resource in storage.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function do_publish($id) {
			$data			= User::find($id);
			$data->active	= ($data->active == 1) ? 0 : 1;
			$data->save();
			return Redirect::action('Admin_PrivilegesController@index');
		}
		
}
