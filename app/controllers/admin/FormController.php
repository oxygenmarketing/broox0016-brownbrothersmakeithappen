<?php

class Admin_FormController extends \Admin_AdminController {

  public function index() {
    $rowElements = FormElement::where('active', '=', 1)->orderBy('rowNumber', 'asc')->orderBy('ordering', 'ASC')->get();
    $responseArray = array();
    foreach($rowElements->toArray() as $element){
  		if(isset($element['options'])){
  			$element['options'] = json_decode($element['options'], true);
  		}
  		if(!isset($responseArray[$element['rowNumber']])) {
  			$responseArray[$element['rowNumber']] = [];
  		}
  		$responseArray[$element['rowNumber']][] = $element;
    }
    //_e::prex($rowElements->toArray());

    return View::make('admin.form.index', array('rows' => $responseArray));
  }

  public function store() {
    //_e::prex(Input::all()['rows']);

    if(isset(Input::all()['rows'])){
      $rows = Input::all()['rows'];
      foreach($rows as $id => $row){
        foreach($row as $order => $element) {
          $rowEle = "{$id}.{$order}";
          $elementRules[$rowEle.'.id'] = 'required';
          $elementRules[$rowEle.'.title'] = 'required';
          $elementRules[$rowEle.'.name'] = 'required|alpha_dash';
          $elementRules[$rowEle.'.type'] = 'required|exists:form_element_types,form_type';
          $elementRules[$rowEle.'.options'] = 'required|array';
        }
      }
      //echo "<pre>"; print_r($elementRules); echo "</pre>"; die();
      $messages = array(
        'alpha_num' => ':attribute must contain only numbers and letters',
        'alpha_dash' => ':attribute must contain only numbers, letters, dashes and underscores',
        'exists' => ':attribute is not a valid option',
        'required' => ':attribute is required'
      );

      $validator = Validator::make(Input::all()['rows'], $elementRules, $messages);
      if($validator->fails()){
        return Redirect::to('admin/form')->withErrors($validator)->withInput();
      }else {
        //Re-order everything so that it's all as it came from the form.
        $i = 1;
        foreach(Input::all()['rows'] as $id => $row){
          $elementCount = 1;
          foreach($row as $element){
            $element['ordering'] = $elementCount;
            $element['rowNumber'] = $i;
            $elementCount++;
            $elements[] = $element;
          }
          $i++;
        }

        foreach($elements as $element){
          foreach($element['options'] as $option_name => $option){
            if(empty($option)){
              unset($element['options'][$option_name]);
            }
          }
          $element['options'] = json_encode($element['options']);
          if($element['id'] == 'x'){
            //_e::pre('Creating a new element!');
            unset($element['id']);
            FormElement::create($element);
          }else {
            //_e::pre('Updating an old element!');
            //_e::pre($element);
            FormElement::find($element['id'])->update($element);
          }
        }
      }
    }
    if(isset(Input::all()['deleted'])){
      $arr = explode(',', Input::all()['deleted'][0]);
      foreach($arr as $deathRow){
        if($deathRow != ''){
          FormElement::find($deathRow)->update(array('active' => 9, 'rowNumber' => 0, 'ordering' => 0));
        }
      }
    }

    return Redirect::to('admin/form');
  }
  
	public function checkForm() {
		_e::prex(Input::all());
	}

  public function edit() {
    return View::make('admin.form.edit');
  }

}
