<?php

class Admin_IWController extends \Admin_AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		
		$uID			= Auth::user()->id;
		$_z				= AdminHelper::init();
		$filterby		= $_z->filterby;
		$orderby		= $_z->orderby;
		$dir			= $_z->dir;
		$perpage		= $_z->pagination['perPage'];
		//_e::pre($_z);
		
		// Get total items for query
		$_total		= InstantWinView::where('id','>',0);
		if ($filterby) {
			foreach ($filterby as $f=>$v) {
				$_total->where($f, '=', $v);
			};
		}
		$_total		= $_total->count();
		
		// return paginated results
		$_data		= InstantWinView::where('id','>',0);
		if ($filterby) {
			foreach ($filterby as $f=>$v) {
				$_data->where($f, '=', $v);
			};
		}
		$_data		= $_data->orderBy('time_slot','ASC');
		$_data		= $_data->paginate($perpage);

		$data	= (object) array(
			'_d'		=> $_data
		,	'_total'	=> $_total
		,	'filterby'	=> $filterby
		);
				
		//e::sql(); exit;
		return View::make('admin.instantwin.index')
			->with('_data', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 
	 * @return Response
	 */
	public function create() {
		return Redirect::action('Admin_IWController@index');
	}

	public function revoke($id) {
		
		$data = Entries::findOrFail($id);
		$data->instantwin_id = -1;
		$data->save();
		
		
		return Redirect::action('Admin_IWController@index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {
		return Redirect::action('Admin_IWController@index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$data	= InstantWin::findOrFail($id);
		return View::make('admin.instantwin.view')
			->with('_data', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$data	= InstantWin::findOrFail($id);
		return View::make('admin.instantwin.form')
			->with('_data', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		
		//_e::prex('Check for winners first...');
		
		$messages	= array();
		$except		= array();
		
		// ALL VALIDATION
		$rules	= array (
			'time_slot'	=> 'required',
		);

		$validator	= Validator::make(Input::all(), $rules, $messages);		
		if ($validator->fails()) {
			Msg::add('error', 'Form validation failed.');
			return Redirect::back()
				->withInput()
				->withErrors($validator);
		} else {
			$data		= InstantWin::findOrFail($id);
			$data->fill(Input::except($except));
			if ( $data->save() ) {
				//
			};
			return Redirect::action('Admin_IWController@index');
		};
			
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		return Redirect::action('Admin_IWController@index');
	}

	/**
	 * Generate Instant Win schedule.
	 */
	public function generate() {
		
		die('YOU SINGLE HANDEDLY JUST BROKE THE INTERNET!');
		
		//_e::pre(Config::get('_promotion.prizes'));
		
		$key_dates		= Config::get('_promotion.key_dates');
		$format			= 'Y-m-d';
		
		// GET DATE RANGE
		$strDateFrom	= date($format, strtotime($key_dates['start']));
		$strDateTo		= date($format, strtotime($key_dates['end']));
		$dateRangeArray	= $this->_create_DateRangeArray($strDateFrom,$strDateTo);

		_e::pre('strDateFrom = '.$strDateFrom);
		_e::pre('strDateTo = '.$strDateTo);
		//_e::pre($dateRangeArray);
		echo '<hr>';
		
		// TOTAL PRIZE COUNT
		$totalPrizeCount	= 0;
		$prizePool			= array();
		$compRange			= count($dateRangeArray);
		_e::pre('compRange = '.$compRange);
		
		
		// GET PRIZES HERE....
		$prizes				= Prizes::where('active','=',1)->orderBy('ordering','ASC')->get();
		$_genPrizeList		= array();
		//_e::pre($prizes);
		foreach ($prizes as $_prize) {
			$_genPrizeList[$_prize->id]	= array(
				'amount'	=> $_prize->quantity,
				'prize'		=> $_prize->title,
			);
		};
		_e::pre($_genPrizeList);
		//_e::prex(Config::get('_promotion.prizes'));
		
		//foreach (Config::get('_promotion.prizes') as $i=>$v) {
		foreach ($_genPrizeList as $i=>$v) {
			
			$prizeAmount			= (int)$v['amount'];
			$totalPrizeCount		= $totalPrizeCount + $prizeAmount;
			$distributionAmount	= floor( $prizeAmount/$compRange );
			$remaindernAmount		= $prizeAmount % $compRange;

			_e::pre('distributionAmount = '.$distributionAmount);
			_e::pre('$distributionAmount * $compRange = '.$distributionAmount * $compRange);
			_e::pre('remaindernAmount = '.$remaindernAmount);
			_e::pre('prizeAmount = '.$prizeAmount);
			_e::pre('totalPrizeCount = '.$totalPrizeCount);
			
			if ( $prizeAmount < $compRange ) { // IF GIVEAWAY IS LESS THAN DAYS OF COMPETITION
				// select random dates
				$randDays = array_rand($dateRangeArray, $prizeAmount);
				foreach ($randDays as $r=>$s) {
					$prizePool[$i][]	= $this->_prizePool($v['prize'],$dateRangeArray[$s]);
				};
			} else {
				// DISTRIBUTE PER DAY ITEMS
				foreach ($dateRangeArray as $dateRangeDay) {
					for ($x=1; $x<=$distributionAmount; $x++) {
						$prizePool[$i][]	= $this->_prizePool($v['prize'],$dateRangeDay);
					}
				}
				// select random dates
				if ($remaindernAmount > 0) {
					$randDays = array_rand($dateRangeArray, $remaindernAmount);
					foreach ($randDays as $r=>$s) {
						$prizePool[$i][]	= $this->_prizePool($v['prize'],$dateRangeArray[$s]);
					};
				};
			};

		};

		echo '<hr>';
		_e::pre($prizePool);
		
		$iWin	= new InstantWin;
		if ( $iWin->count() == 0 ) {
			foreach ($prizePool as $i=>$v) {
				foreach ($v as $data) {
					$this->_insertPrize($i,$data);
				};
			};
		} else {
			_e::prex('<strong>NOTICE</strong> : There are already prizes in the database.');
		};

		exit; // ALL DONE
		
	}

		private function _create_DateRangeArray($strDateFrom,$strDateTo) {
			$aryRange	= array();
			$iDateFrom	= mktime(1,0,0,	substr($strDateFrom,5,2),	substr($strDateFrom,8,2),	substr($strDateFrom,0,4));
			$iDateTo	= mktime(1,0,0,	substr($strDateTo,5,2),		substr($strDateTo,8,2),		substr($strDateTo,0,4));
			if ($iDateTo>=$iDateFrom) {
				array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
				while ($iDateFrom<$iDateTo) {
					$iDateFrom+=86400; // add 24 hours
					array_push($aryRange,date('Y-m-d',$iDateFrom));
				}
			}
			return $aryRange;
		}	
		
		private function _prizePool($prize,$windate,$ignore=0) {
			
			$key_dates	= Config::get('_promotion.key_dates');
			$format		= 'Y-m-d H:m:s';
			$data		= array();
			
			// GENERATE RANDOM TIMES
			$hour 	= rand(00, 23);
			$min  	= rand(00, 59);
			$sec  	= rand(01, 59);
			
			$iWin	= new InstantWin;
				
			$data['prize']		= $prize;
			$data['time_slot']	= $windate.' '.str_pad($hour, 2, '0', STR_PAD_LEFT).':'.str_pad($min, 2, '0', STR_PAD_LEFT).':'.str_pad($sec, 2, '0', STR_PAD_LEFT);
			$data['claim_no']	= $iWin->set_pcn(6);
			
			$sDate	= date($format, strtotime($key_dates['start']));
			$eDate	= date($format, strtotime($key_dates['end']));
			
			if ($ignore == 1) {
				return $data;
			} else {
				if ( ($data['time_slot'] > $sDate) && ($data['time_slot'] < $eDate) ) {
					return $data;
				} else {
					return $this->_prizePool($prize,$windate);
				};
			};
			
			
		}
		
		private function _insertPrize($i,$data) {
			$instantWin				= new InstantWin;
			$instantWin->prize_id	= $i;
			$instantWin->prize		= $data['prize'];
			$instantWin->time_slot	= $data['time_slot'];
			$instantWin->claim_no	= $data['claim_no'];
			
			return ($instantWin->save()) ? TRUE : FALSE ;
		}

}