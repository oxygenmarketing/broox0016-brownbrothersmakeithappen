<?php
class SmsController extends BaseController {

	public function __construct() {
		// GET CONFIG INFORMATION FROM DATABASE
		$this->_url			= Promotion::Where('option','=','smsGW_host')->pluck('value');
		$this->_username	= Promotion::Where('option','=','smsGW_user')->pluck('value');
		$this->_password	= Promotion::Where('option','=','smsGW_pass')->pluck('value');
		$this->_pcn			= 'XXXXXX';
		$this->_prize		= PrizeDetails::Where('sms_name','!=','')->pluck('sms_name');

		$this->_fields = array(
	   	  'MSISDN'       => '',
	 	  'Shortcode'    => '',
	      'Reference'    => '',
	      'Content'      => '',
	      'Channel'      => '',
	      'DataType'     => '',
	      'DateReceived' => '',
	      'CampaignID'   => '',
	      'Trigger'      => ''
		);
	}

	public function index() {

		if ($_POST) {

			// Get fields from POST
		    $post_data = array_merge($this->_fields, $_POST);
			$duplicate = SmsReceived::Where('msg_reference','=',$post_data['Reference'])->count();

			/*if ($duplicate > 0) {
				exit('Duplicate');
			}*/

			$insert_data = new SmsReceived;

			$insert_data->msg_msisdn      	= preg_replace('/^0/', '61', $post_data['MSISDN']);
			$insert_data->msg_shortcode   	= $post_data['Shortcode'];
			$insert_data->msg_reference    	= $post_data['Reference'];
			$insert_data->msg_content      	= $post_data['Content'];
			$insert_data->msg_channel     	= $post_data['Channel'];
			$insert_data->msg_datatype    	= $post_data['DataType'];
			$insert_data->msg_datereceived 	= date('Y-m-d H:i:s', $post_data['DateReceived']);
			$insert_data->msg_campaignid   	= $post_data['CampaignID'];
			$insert_data->msg_trigger      	= $post_data['Trigger'];

			$insert_data->save();


            if (count($insert_data) > 0) {
				$testType	= (isset($_POST['type'])) ? $_POST['type'] : $_POST['Content'];

				if 			(preg_match('/pre$/i', $testType)) {
		        	$this->_reply($insert_data, 'sms_pre');
    			} else if	(preg_match('/limit$/i', $testType)) {
		        	$this->_reply($insert_data, 'sms_limit');
                } else if	(preg_match('/winner$/i', $testType)) {
		        	$this->_reply($insert_data, 'sms_winner');
                } else if	(preg_match('/thankyou$/i', $testType)) {
		        	$this->_reply($insert_data, 'sms_thanks');
    			} else if	(preg_match('/invalid$/i', $testType)) {
		        	$this->_reply($insert_data, 'sms_invalid');
    			} else if	(preg_match('/post$/i', $testType)) {
		        	$this->_reply($insert_data, 'sms_post');

    			} else if	(preg_match('/test$/i', $testType)) {
		        	$this->_reply($insert_data, 'live');

    			} else {
		        	$this->_check_promo_period($insert_data);
    			};
            };

        }else{
			_e::prex('not POST');
		};


	}

	function _check_promo_period($insert_data) {
		$startDate = Promotion::Where('option','=','start_date')->pluck('value');
		$endDate = Promotion::Where('option','=','end_date')->pluck('value');
		$today = date('Y-m-d H:i:s');

		if ($startDate > $today) {
			$this->_reply($insert_data, 'sms_pre');
		} else if ($startDate <= $today && $endDate >= $today) {
			$this->_reply($insert_data, 'live');
		} else if ($endDate < $today) {
			$this->_reply($insert_data, 'sms_post');
		};
	}

	function _reply($insert_data='', $reply_type='') {

		if ($reply_type == 'live') {
            $reply_type	= $this->_create_entry($insert_data);
       	};

		$reply				= $this->_get_reply($reply_type);

		$gateway_response	= $this->_send_reply($insert_data, $reply);
		$reference			= $this->_extract_reference($gateway_response);
		$this->_log_reply($insert_data, $reply, $reference);
	}

	function _get_reply($type='') {

		$smsResponse	= Promotion::Where('option','=',$type)->pluck('value');

		if ($type == 'sms_winner') {
			$pcn = $this->_pcn;
			$prize = $this->_prize;
			$_replace = array($pcn, $prize);
			$replace = array('%%PCN%%', '%%PRIZE%%');
			$reply	= str_replace($replace, $_replace, $smsResponse);

		} else if ($type != '') {
			$reply	= $smsResponse;
		} else {
			$reply	= "";
		};
		return urlencode($reply);
	}

	function _send_reply($sms, $content) {
		$b_CodeReturned = NULL;
		$X_HOST ="http1.ie.oxygen8.com";
		$X_PORT ="8080";

		$X_URL      = $this->_url;
		$X_USERNAME = $this->_username;
		$X_PASSWORD = $this->_password;

		$s_POST_DATA  = "&MSISDN=". $sms->msg_msisdn;
		$s_POST_DATA .= "&Shortcode=". $sms->msg_shortcode;
		$s_POST_DATA .= "&Channel=". $sms->msg_channel;
		$s_POST_DATA .= "&Content=". $content;
		$s_POST_DATA .= "&SourceReference=". $sms->msg_reference;
	    $s_POST_DATA .= "&Reference=". $sms->msg_reference;
		$s_POST_DATA .= "&Premium=1"; // Premium

		$s_Request  = "POST ".$X_URL." HTTP/1.0\r\n";
		$s_Request .="Host: ".$X_HOST.":".$X_PORT."\r\n";
		$s_Request .="Authorization: Basic ".base64_encode($X_USERNAME.":".$X_PASSWORD)."\r\n";
		$s_Request .="Content-Type: application/x-www-form-urlencoded\r\n";
		$s_Request .="Content-Length: ".strlen($s_POST_DATA)."\r\n";
		$s_Request .="\r\n".$s_POST_DATA;

		$s_GatewayResponse = "";

		//Sends out the request to the server.
		$fp = fsockopen ($X_HOST, $X_PORT, $errno, $errstr, 30) or die("Error!!!");
		fputs ($fp, $s_Request);
		while (!feof($fp)) {
			$s_GatewayResponse .= fgets ($fp, 128);
		}
		fclose ($fp);

		echo "$s_GatewayResponse:$X_PORT\n";

		//Array of official response codes.
		$a_Responses = array(
			"100" => "Server has returned an unspecified error.",
			"101" => "Server successfully received the request.",
			"102" => "Server has returned an database error",
			"103" => "Server has returned an syntax error."
		);

		echo "<HTML>\n<BODY>\n\n";

		//Checks for an official response code.
		foreach ($a_Responses as $s_ResponseCode => $s_ResponseDescription) {
			if (stristr($s_GatewayResponse, "\n$s_ResponseCode\n")) {
				echo "A response code of $s_ResponseCode was returned – ";
				echo $s_ResponseDescription;

				$b_CodeReturned = true;
			}
		}

		//Checks for an authorization failure where an official response code has
		//not been recognized.

		if (!$b_CodeReturned) {
			if (stristr($s_GatewayResponse, "HTTP/1.1 401")) {
				echo "The server rejected your username/password (HTTP 401).";
			} else {
				echo "No recognised response code was returned by the server.";
			}
		}

		echo "\n\n</BODY>\n</HTML>";

		return $s_GatewayResponse;
	}

		// Extract reference
	function _extract_reference($gateway_response) {

		preg_match('/101\nMessage accepted\n([\d\.]+)$/', $gateway_response, $matches);

		return $matches[1];
	}

	// Log reply
	function _log_reply($sms, $content, $reference) {
		$content = urldecode($content);

		$data = array(
		  'sms_id'    => $sms->id,
		  'msisdn'    => $sms->msg_msisdn,
		  'shortcode' => $sms->msg_shortcode,
		  'reference' => $reference,
		  'channel'   => $sms->msg_channel,
		  'content'   => $content,
		  'length'    => strlen($content)
		);

		$replySave = new SmsReply;
		$replySave->fill($data);
		$replySave->save();
	}


	function _create_entry($sms) {
			$smsMessage = $sms->msg_content;
			$uniquecode = $this->_parse_uniquecode($sms->msg_content);

			$sms->msg_content = str_replace($uniquecode,'',$sms->msg_content);
			$barcode1	= $this->_parse_barcode($sms->msg_content);
			$sms->msg_content = str_replace($barcode1,'',$sms->msg_content);

			$full_name 	= $this->_parse_full_name($sms->msg_content);
			$postcode	= $this->_parse_postcode($sms->msg_content);
			$state		= $this->_get_state($postcode);
			/* IF ADDRESS EXISTS IN COMPETITION
			$address	= $this->_parse_address($sms->msg_content, $full_name, $postcode, $state, $barcode1, $barcode2, $barcode3);
			*/

		if ( ($full_name == '' || $full_name == NULL) || ($postcode == '' || $postcode == NULL) || ($state == '' || $state == NULL) || ($barcode1 == '' || $barcode1 == NULL) || ($uniquecode == '' || $uniquecode == NULL) ) {

			$reply_type = 'sms_invalid';

		} else {


			$data['sms_id']			= $sms->id;
			$data['phone']			= $sms->msg_msisdn;
			$data['sms_message']		= $smsMessage;
			$data['firstname']			= $full_name['first'];
			$data['lastname']		= $full_name['last'];
			$data['state']			= $state;
			$data['postcode']		= $postcode;
			$data['barcode']			= $barcode1;
			$data['uniquecode']		= $uniquecode;


			//Check if # of entries has been exceeded
			$entryRestrictions = Promotion::Where('option','=','number_of_entries_per')->pluck('value');
			$entryLimit = Promotion::Where('option','=','number_of_entries')->pluck('value');
			$limitedBy = Promotion::Where('option','=','limited_by')->pluck('value');

			if(($entryRestrictions == 'Promo' && count(Entries::Where($limitedBy,'=',Input::get($limitedBy))->get()) >= $entryLimit) || ($entryRestrictions == 'Day' && count(Entries::Where($limitedBy,'=',Input::get($limitedBy))->WhereRaw('DATE(`created_at`) = CURDATE()')->get()) >= $entryLimit)) {
				$reply_type		= 'limit';
			}else{

				if(Promotion::Where('option','=','has_prizes')->pluck('value') == 1) {
					$winRestrictions = Promotion::Where('option','=','number_of_wins_per')->pluck('value');
					$winLimit = Promotion::Where('option','=','number_of_wins')->pluck('value');

					if(($winRestrictions == 'Promo' && count(Entries::Where($limitedBy,'=',Input::get($limitedBy))->Where('instantwin_id','>',0)->get()) >= $winLimit) || ($winRestrictions == 'Day' && count(Entries::Where($limitedBy,'=',Input::get($limitedBy))->Where('instantwin_id','>',0)->WhereRaw('DATE(`created_at`) = CURDATE()')->get()) >= $winLimit)) {
						$reply_type = SmsController::_doDataEntry($data);

					}
					$uniqueCodeCheck = SmsController::_checkUniqueCode($data['uniquecode']);
					$barcodeCheck = SmsController::_checkBarcode($data['barcode']);
					//_e::prex($uniqueCodeCheck);
					if($uniqueCodeCheck == 'valid' && $barcodeCheck == 'valid') {
						$checkCodes = UniqueCodes::Where('uniquecode','=',$data['uniquecode'])->first();
						$checkCodes->active = 0;
						$checkCodes->save();
						$reply_type = SmsController::_doInstantWin($data);
					}else{
						if($uniqueCodeCheck == 'invalid') {
							$reply_type = 'sms_unique';
						}else if($barcodeCheck == 'invalid') {
							$reply_type = 'sms_barcode';
						}
					}
				}else{
					$uniqueCodeCheck = SmsController::_checkUniqueCode($data['uniquecode']);
					$barcodeCheck = SmsController::_checkBarcode($data['barcode']);

					if($uniqueCodeCheck == 'valid' && $barcodeCheck == 'valid') {

						$checkCodes = UniqueCodes::Where('uniquecode','=',$data['uniquecode'])->first();
						$checkCodes->active = 0;
						$checkCodes->save();

						$reply_type = SmsController::_doDataEntry($data);

					}else{
						if($uniqueCodeCheck == 'invalid') {
							$reply_type = 'sms_unique';
						}else if($barcodeCheck == 'invalid') {
							$reply_type = 'sms_barcode';
						}
					}
				}
			};

		};

		return $reply_type;

	}

	public function receipt() {

			$data	= new SmsReceipt();
			$data->reference = Input::get('Reference');
			$data->status		= Input::get('Status');
			$data->date_delivered	= date('Y-m-d H:i:s', Input::get('DateDelivered'));
			$data->save();
			echo "Success";


	}

	function _checkUniqueCode($uniqueCode) {
		$checkCodes = UniqueCodes::Where('uniquecode','=',$uniqueCode)->first();
		if(isset($checkCodes->active) && $checkCodes->active == 1) {
			return 'valid';
		}else{
			return 'invalid';
		}
	}

	function _checkBarcode($barcode) {
		$barcode = (int)$barcode;
		$barcodeMatch = Barcode::Where('first_set','=',$barcode)->orWhere('second_set','=',$barcode)->first();
		// _se::prex($barcodeMatch);
		if(isset($barcodeMatch)) {
			return 'valid';
		}else{
			return 'invalid';
		}

	}

	public function _doDataEntry($input) {
		//die('doDataEntry');
		$data		= new Entries();
		$data->fill($input);
		$rawData	= array(
			'post'			=> $_POST,
		);
		$data->rawdata	= json_encode($rawData);

		if ( $data->save() ) {
			return 'sms_thanks';
		};
	}

	public function _doInstantWin($data) {
		//die('doInstantWin');
		//Check if prizes are selected by state
		$whereStates = '';
		if(Promotion::Where('option','=','prizes_by_state')->pluck('value') == 1) {
			$stateID = States::Where('state','=',$data['state'])->pluck('id');
			$whereStates = 'AND prize_pool_id IN (SELECT prize_pools_id FROM states_prizes WHERE state_id = '.$stateID.')';
		}
		$rawData	= array(
			'post'			=> $_POST,
		);
		$raw_data	= json_encode($rawData);

		$columns = array();
		$values = array();
		foreach($data as $_i=>$_d) {
			$values[] = $_d;
			$columns[] = $_i;
		}

		if((isset($data['state']) && $data['state'] == 'SA') || (isset($data['postcode']) && $data['postcode'] >= 5000 && $data['postcode'] <= 5950)) {
			$limitWins = '';
		}else{
			$winRestrictions = Promotion::Where('option','=','number_of_wins_per')->pluck('value');
			$winLimit = Promotion::Where('option','=','number_of_wins')->pluck('value');

			if($winRestrictions == 'Promo') {
				$limitWins = "AND (
								  SELECT count(email) AS total
								  	FROM `entries`
										WHERE instantwin_id IS NOT NULL
										AND phone = '".$data['phone']."'
							  ) < ".$winLimit." ";
			}else if($winRestrictions == 'Day') {
				$limitWins = "AND (
								  SELECT count(email) AS total
								  	FROM `entries`
										WHERE instantwin_id IS NOT NULL
										AND phone = '".$data['phone']."'
										AND DATE(`created_at`) = CURDATE()
							  ) < ".$winLimit." ";
			}
		}

		$sql = "INSERT INTO entries (`instantwin_id`,";
		foreach($columns as $_c) {
			$sql .= $_c.',';
		}
		$sql .= 'rawdata,created_at,updated_at) SELECT i.id AS instantwin_id,';

		foreach($values as $_v) {
			$sql .= "'".$_v."',";
		}
		$sql .= "'".$raw_data."',now(),now()";
		$sql .= "FROM `instantwin` i
							LEFT JOIN `entries` e
							  ON e.instantwin_id = i.id
							WHERE now() >= i.time_slot
							  AND e.instantwin_id IS NULL
							  ".$limitWins."
							  ".$whereStates."
						ORDER BY i.time_slot ASC LIMIT 1";

		$doWin = DB::insert($sql);
		$theid= DB::getPdo()->lastInsertId();

		$prize_id = Entries::Where('id','=',$theid)->pluck('instantwin_id');

		$prize_pool = InstantWin::Where('id','=',$prize_id)->pluck('prize_pool_id');
		$prize_desc_id = PrizePool::Where('id','=',$prize_pool)->pluck('prize_details_id');

		$this->_pcn = InstantWin::Where('id','=',$prize_id)->pluck('pcn');

		$this->_prize = PrizeDetails::Where('id','=',$prize_desc_id)->pluck('sms_name');

		if($theid == 0) {
			$reply_type = SmsController::_doDataEntry($data);
			return $reply_type;
		}else{
			return 'sms_winner';
		}
	}

	public function sms_test() {

		$data['ref_no'] = $this->_create_ref_no();

		return View::make('sms.index')
			->with('ref_no', $data['ref_no']);
	}

		// Create a reference number for testing purposes
	function _create_ref_no() {
	    $length = 10;
	    $characters = '0123456789';
	    $string = '';
	    for ($i=0; $i < $length; $i++) {
	        $string .= $characters[mt_rand(0, (strlen($characters) - 1))];
	    }
	    return $string;
	}




	function _parse_full_name($text) {
	    $name = trim(preg_replace('/\s\s+/', ' ', preg_replace('/[^a-z]|Full|First|Last|Sur|Name|Post|Bar|Code|Address|Suburb|State|Test/i', ' ', $text)));

		$pieces = explode(' ', $name);
		//_e::prex($pieces);
		$full_name['first'] = (isset($pieces[0])) ? $pieces[0] : NULL;
	  	$full_name['last']  = (isset($pieces[1])) ? $pieces[1] : NULL;

		return $full_name;
	}


	function _parse_postcode($text) {
		preg_match('/([0-9]{4})/', $text, $matches);
	    $postcode = (isset($matches[0])) ? $matches[0] : NULL;
        return $postcode;
	}

	function _parse_uniquecode($text) {
		preg_match('/[A-z0-9]{10}/', $text, $matches);
	    $uniquecode = (isset($matches[0])) ? $matches[0] : NULL;
			// _e::prex($uniquecode);
        return $uniquecode;
	}

	function _parse_barcode($text) {
		preg_match('/[0-9]{5}/', $text, $matches);
	    $barcode = (isset($matches[0])) ? $matches[0] : NULL;
			//_e::prex($barcode);
        return $barcode;
	}


	/*function _parse_postcode($text) {
		$text = $this->_striptext($text);
		$smsexplode	= explode(' ',$text);
		foreach ($smsexplode as $i=>$v) {
			if (strlen($v) !== 4) {
				unset($smsexplode[$i]);
			};
		};
		$smsimplode	= implode(' ',$smsexplode);
		preg_match('/[0-9]{4}/', $smsimplode, $matches);
	    $postcode = (isset($matches[0])) ? $matches[0] : NULL;
        return $postcode;
	}*/



	function _get_state($pc) {
		if (($pc >= 2000 && $pc <= 2599) || ($pc >= 2619 && $pc <= 2898) || ($pc >= 2921 && $pc <= 2999)) {
			return 'NSW';
		}
		else if (($pc >= 2600 && $pc <= 2618) || ($pc >= 2900 && $pc <= 2920)) {
			return 'ACT';
		}
		else if (($pc >= 3000) && ($pc <= 3999)) {
			return 'VIC';
		}
		else if (($pc >= 4000) && ($pc <= 4999)) {
			return 'QLD';
		}
		else if (($pc >= 5000) && ($pc <= 5799)) {
			return 'SA';
		}
		else if (($pc >= 6000) && ($pc <= 6797)) {
			return 'WA';
		}
		else if (($pc >= 7000) && ($pc <= 7799)) {
			return 'TAS';
		}
		else if (($pc >= 800) && ($pc <= 899)) {
			return 'NT';
		}
		else {
			return NULL;
		}
	}


	function _parse_state($text) {
	    preg_match('/\bVIC\b|\bVictoria\b|\bACT\b|\bAustralian Capital Territory\b|\bNSW\b|\bNew South Wales\b|\bQLD\b|\bQueensland\b|\bNT\b|\bNorthern Territory\b|\bWA\b|\bWestern Australia\b|\bSA\b|\bSouth Australia\b|\bTAS\b|\bTasmania\b/i', $text, $matches);
	    $state = (isset($matches[0])) ? $matches[0] : NULL;

        return $this->_filter_state($state);
	}

	function _filter_state($state) {
		if (preg_match('/VIC|Victoria/i', $state)) {
			return 'VIC';
		}
		else if (preg_match('/ACT|Australian Capital Territory/i', $state)) {
			return 'ACT';
		}
		else if (preg_match('/NSW|New South Wales/i', $state)) {
			return 'NSW';
		}
		else if (preg_match('/QLD|Queensland/i', $state)) {
			return 'QLD';
		}
		else if (preg_match('/NT|Northern Territory/i', $state)) {
			return 'NT';
		}
		else if (preg_match('/WA|Western Australia/i', $state)) {
			return 'WA';
		}
		else if (preg_match('/SA|South Australia/i', $state)) {
			return 'SA';
		}
		else if (preg_match('/TAS|Tasmania/i', $state)) {
			return 'TAS';
		}
		else {
			return NULL;
		}
	}



/*	function _parse_barcode($text, $pos = 1) {
		$i = $pos-1;
		//preg_match('/[0-9]{4}/', $text, $matches); // MATCHES 1 ONLY
		preg_match_all('/[0-9]{4}/', $text, $matches);
	    return (isset($matches[0])) ? $matches[0][$i] : NULL;
	}

*/
}
