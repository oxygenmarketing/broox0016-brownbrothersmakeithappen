<?php

class TestController extends BaseController {

	public function index() {
		_e::prex(Session::all());
	}

	public function reset() {
		
		DB::statement("TRUNCATE TABLE `entries`");

		DB::statement("TRUNCATE TABLE `quiz`");
		DB::statement("TRUNCATE TABLE `quizresponse`");
		DB::statement("TRUNCATE TABLE `winners`");
		
		Session::forget('_quiz');
		Session::forget('_quizp');
		Session::forget('_winner');
		Session::forget('_user');
		
		return 'DATABASE RESET SUCCESSFULLY';

	}
	
	public function email() {

		echo App::Environment();
		
		$messageTo = User::Where('user_type','=','ADMIN')->lists('email');
		//_e::prex($messageTo);
		
		$data = array(
			'exception'	=> 'test',
			'url'			=> url(Request::path())
		);
		
		if (App::Environment() == 'production') {
			Mail::send('edm.error', $data, function($message) use ($messageTo) {
				$message
					->from('rush@oxygencompetitions.com.au')
					->to(array('digital@oxygenmarketing.com.au'))
					->subject('Brown Brothers Error');
			});
		};
		Log::info('Error Email sent to Stuart');
		
		/*
		if (!Request::is('admin/*')) {
			return Redirect::to('/');
		};
		*/
	
	}
	
}
