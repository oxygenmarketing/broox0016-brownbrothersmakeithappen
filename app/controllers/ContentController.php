<?php

class ContentController extends BaseController {

	public function index() {
		return Redirect::action('HomeController@index');
	}
	
	public function rules() {
		return $this->showContent('rules');
	}
	
	public function help() {
		return $this->showContent('help');
	}

	public function terms() {
		return $this->showContent('terms');
	}

	public function winners() {
		return $this->showContent('winners');
	}
	
	public function showContent($tpl) {

		$facebook		= $this->facebook;
		$facebookUser	= $facebook->getUser();

		if ($facebookUser) {
			try { $loggedUser = $facebook->api('/me'); }
			catch (Exception $ex) { $facebookUser = null; }
			$uploadInterval = Config::get('_promotion.upload_interval');
			$matches_upload = DB::select(DB::raw('SELECT * FROM `matches` WHERE `active` = 1 AND now() < DATE_ADD(`date`,INTERVAL '.$uploadInterval.' DAY) AND now() > `date` AND `id` NOT IN (SELECT `matches_id` FROM entries WHERE `fb_id` = '.$facebookUser.')'));
		} else {
			$matches_upload = '';	
		}
			
		return View::make($tpl)
			->with('_promo', $this->Promotion->dates())
			->with('matches_upload', $matches_upload)
			->with('facebookUser', $facebookUser)
			->with('facebook', $facebook)
			;
	}

}
