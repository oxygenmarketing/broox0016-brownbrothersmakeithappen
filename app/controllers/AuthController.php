<?php

class AuthController extends BaseController {

    public function login() {
		
        //_e::prex( Input::all() );
		
		if (Request::ajax()) {
			
			Session::forget('_c');
			Session::forget('_p');
			
			$usernameinput		=	Input::get('email');
			$input	= array(
				'email'    	=> $usernameinput,
				'password' 	=> Input::get('password'),
			);
			$rules	= array(
				'email'			=> 'required',
				'password'		=> 'required'
			);
			$validator = Validator::make($input,$rules);
			
			if ($validator->passes()) {
				$field			= filter_var($usernameinput, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';	
				$credentials	= array(
					$field		=> $usernameinput,
					'password' 	=> $input['password'],
					'active'		=> 1,
					'user_type'	=> 'REGISTERED',
				);
				if (Auth::attempt($credentials)) {
					
					// CHECK FOR OUTSTANDING PAYMENTS
					$payments	= OrdersFinal::where('status','=','NEW')
									->where('user_id','=',Auth::user()->id)
									->where('payments_method','=',2)
									->where('payment_made','=',0)
									->first();
					if (isset($payments) && count($payments) > 0) {
						
						// SET PAYMENT SESSION
						Session::put('_p', (object) array(
							'id'	=> $payments->id
						));

						Msg::add('error', 'You have a finalised order, but a payment if still pending.');
						$_rr	= action('CartController@payment');
					} else {
						
						//Auth::user()->Orders()->where('active','=',1)->first();
						
						// CHECK FOR EXISTING SAVED CART
						$orders		= Auth::user()->Orders()->where('active','=',1)->first();
						if (isset($orders)) {
							$oProducts	= $orders->OrderProducts()->where('active','=',1)->get();
							if (count($oProducts) > 0) {
								Msg::add('info', 'There are still items in your cart from your last session.');
								$_rr	= action('CartController@index');
							} else {
								$_rr	= action('ProductsController@index');
							};
						} else {
							$_rr	= action('ProductsController@index');
						};
					};
					
					$response	= array(
						'c'	=> 1,
						'r'	=> $_rr
					);
				} else {
					$response	= array(
						'c'	=> 0,
						'r'	=> Lang::get('users.message.login.auth_failed')
					);
				};
			} else {
				$response	= array(
					'c'	=> 0,
					'r'	=> Lang::get('users.message.login.fields_required')
				);
			};
			
			return Response::json($response);
			
		} else {
			die('Incorrect method.');
		};
    }
	
    /**
     * Log the user out of the application.
     *
     */
    public function logout() {
		Session::forget('_c');
		Session::flush();
        Auth::logout();
        return Redirect::to('/');
    }
	
    /**
     * Displays the forgot password form
     *
     */
    public function forgot_password() {
		return View::make('admin.auth.forgot_password_form');
    }

    /**
     * Attempt to send change password link to the given email
     *
     */
    public function do_forgot_password() {
		
        //_e::prex( Input::all() );
		
		if (Request::ajax()) {
			
			$email	= Input::get('email');
			$input	= array(
				'email'    	=> $email
			);
			$rules	= array(
				'email'			=> 'required|email',
			);
			$validator = Validator::make($input,$rules);
			
			if ($validator->passes()) {
				switch ($response = Password::remind(array('email'=>$email), function($message) {
					$message->subject('Your Password Reminder');
				})) {
					case Password::INVALID_USER:
						$response	= array(
							'c'	=> 0,
							'r'	=> Lang::get('users.message.login.fields_required')
						);
					case Password::REMINDER_SENT:
						$response	= array(
							'c'	=> 1,
							'r'	=> 'An email has been sent to '.Input::get('email').'. Please check for further information.'
						);
				}
			} else {
				$response	= array(
					'c'	=> 0,
					'r'	=> Lang::get('users.message.login.fields_required')
				);
			};
			
			return Response::json($response);
			
		} else {
			die('Incorrect method.');
		};
    }
	
    /**
     * Shows the change password form with the given token
     *
     */
    public function reset_password( $token ) {
		$_select	= array('user_id','first_name','last_name','position','phone','mobile');
		$contacts	= UserContact::where('show_contact','=',1)->orderBy('first_name','ASC')->get($_select);
		return View::make('users.reset')
			->with('_data', $contacts)
			->with('token', $token);
    }

    /**
     * Attempt change password of the user
     *
     */
    public function do_reset_password() {
		
		$token = '/'.Input::get('token');
		
		$rules = array(
			'email'						=> 'required|email',
			'password'					=> 'required|min:6',
			'password_confirmation'	=> 'same:password',
			//'token'					=> 'exists:token,token'
		);
		$validator = Validator::make(Input::all(),$rules);	
		if ($validator->fails()) {
			Msg::add('error', 'Form validation failed.');
			return Redirect::back()
				->withInput()
				->withErrors($validator);
		} else {
			$credentials = Input::only(
				'email', 'password', 'password_confirmation', 'token'
			);
	
			$response = Password::reset($credentials, function($user, $password) {
				$user->password = $password;
				$user->save();
			});
	
			switch ($response) {
				case Password::INVALID_PASSWORD:
				case Password::INVALID_TOKEN:
				case Password::INVALID_USER:
					Msg::add('error', Lang::get($response));
					return Redirect::back()->withInput()->with('error', Lang::get($response));
				case Password::PASSWORD_RESET:
					return Redirect::action('HomeController@index');
			}
		};
					
    }

}