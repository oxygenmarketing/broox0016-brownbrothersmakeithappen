<?php

/**
	This Controller manages both the displaying of the form
	on the entry page as well as validating the user entry
	and running the entry through the instant win mechanic
**/

class EntriesController extends BaseController {

	public function index() {

		/**
			Get the Form Elements from the database and send them in an array to the view
		**/

		$fields = FormElement::Where('active','=',1)->OrderBy('rowNumber', 'asc')->OrderBy('ordering', 'asc')->get();

		$form = array();

		foreach ($fields as $field) {
			$groupid = $field['rowNumber'];
			if (isset($form[$groupid])) {
				$form[$groupid][] = $field;
			} else {
				$form[$groupid] = array($field);
			}
		}

		return View::make('enter')
			->with('form', $form);
	}

	public function submit() {
	/**
		1) Validate form fields and if errored send back to view with messages and input
		2) If form validates, check if user has exceeded number of entries in this promotion
		3) If the user is allowed to enter, check if unique code is being used. If it is, deactivate that unique code
		4) Check if it is an Instant Win Promotion
		5) If yes, check if user has exceeded maximum number of wins - Send to Thanks screen if they have
		   If No, Send through Data Entry mechanic and send to Thank You Screen
		6) If they are still allowed to win, run them through IW mechanic.
	**/

	if (Request::getMethod() == 'POST')
      {
			//Get the minimum entry age from the database and assign to variable
			$ageToEnter = Promotion::Where('option','=','age_to_enter')->pluck('value');

			//_e::prex(Input::all());
			//If a minimum age is set, validate against the given date_of_birth

			if(!empty($ageToEnter)) {
				Validator::extend('olderThan', function($attribute, $value, $parameters)
				{
					$parameters[] = Promotion::Where('option','=','age_to_enter')->pluck('value');
					$minAge = ( ! empty($parameters)) ? (int) $parameters[0] : (int) $parameters[0];
					$date_array = explode("/",$value); // split the array
					$var_day = $date_array[0]; //day seqment
					$var_month = $date_array[1]; //month segment
					$var_year = $date_array[2]; //year segment
					$new_date_format = "$var_year-$var_month-$var_day";
					return (new DateTime)->diff(new DateTime($new_date_format))->y >= $minAge;
				});
			};

			// Validate Unique code field
			Validator::extend('uniquecode', function($attribute, $value, $parameters)
			{
				$codematch = UniqueCodes::Where('uniquecode','=',$value)->first();
				if(isset($codematch) && $codematch->active == 1) {
					return $codematch->id;
				}else{
					return FALSE;
				}
			});

			// Validate Postcode field
			Validator::extend('postcode', function($attribute, $value, $parameters)
			{
				$getstate = EntriesController::_get_state($value);
				if($getstate != NULL) {
					Input::merge(array('state' => $getstate));
					//_e::prex(Input::all());
					return TRUE;
				}else{
					//_e::prex('No State');
					return FALSE;
				}
			});

			//Validate Barcode field against the database
			Validator::extend('barcode', function($attribute, $value, $parameters)
			{
				$barcodeMatch = Barcode::Where('first_set','=',$value)->orWhere('second_set','=',$value)->first();
				if(isset($barcodeMatch)) {
					return TRUE;
				}else{
					return FALSE;
				}
			});
			Validator::extend('gmailval', function($attribute, $value, $parameters)
			{
				if(preg_match("/[+]/", Input::get('email')) == 1) {
					return FALSE;
				}else{
					return TRUE;
				}
			});


			$except		= array('captcha', 'entercaptcha','submit', 'confirm_email');
			$rules = array();

			$formFields = FormElement::Where('active','=',1)->get();
			foreach($formFields as $f) {
				$rules[$f->name]	= $f->validation;
			}
			$messages = array (
				'older_than'		=> 'You must be older than '.$ageToEnter.' years old to enter',
				'uniquecode'		=> 'The unique code you entered is either invalid or already used',
				'barcode'		=> 'The barcode you entered is invalid',
				'entercaptcha'	=>	'The Captcha code you entered is incorrect.',
				'gmailval'			=>  'You cannot use the symbol + in your email address'
			);
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails())
            {
                return Redirect::back()
								->withInput(Input::except('password', 'entercaptcha'))
								->withErrors($validator);
            }
            else
            {
							if(Input::has('phone')) {
								$phone = preg_replace('/^0/', '61', Input::get('phone'));
								Input::merge(array('phone' => $phone));
							}

							$limitedBy = Promotion::Where('option','=','limited_by')->pluck('value');

							if(preg_match("/@gmail\.com/", Input::get('email')) == 1) {
								$gmailstrip = preg_replace("/\.(?=.*?@gmail\.com)/", '', Input::get('email'));
								Input::merge(array('email' => $gmailstrip));
								//_e::prex($data['email']);
							}


							//Check if # of entries has been exceeded
							$limit = EntriesController::_entrylimit(Input::all());

							// If promotion uses unique codes, deactivate unique code before doing entry
							if(Promotion::Where('option','=','unique_codes')->pluck('value') == 1) {
								EntriesController::_uniquecode(Input::get('uniquecode'));
							};

							$age = EntriesController::_getAge(Input::get('dob'));
							Input::merge(array('age' => $age));

							// Check if promotion is Instant Win. If yes, send to IW mechanic, if No, Send to Data Entry

							if(Promotion::Where('option','=','has_prizes')->pluck('value') == 1) {
								$result = EntriesController::_doInstantWin(Input::except($except),$limitedBy);
							}else{
								$result = EntriesController::_doDataEntry(Input::except($except));
							}
            }
        }

		if($result == 'winner') {
			Session::put('pcn', $this->_pcn);
			Session::put('prize_name', $this->_prize);
		}

		Session::put('result',$result);

		return Redirect::to('result');

	}

	public function result() {
		$result = Session::pull('result');

		if(isset($result)) {
			return View::make($result)
				->with('pcn', Session::pull('pcn'))
				->with('prize_name', Session::pull('prize_name'))
				;
		}else{
			return Redirect::to('/enter');
		}
	}

	public	function _get_state($pc) {
			if (($pc >= 1100 && $pc <= 1299) || ($pc >= 2000 && $pc <= 2999)) {
				return 'NSW';
			}
			else if (($pc >= 2600 && $pc <= 2620)) {
				return 'ACT';
			}
			else if (($pc >= 3000) && ($pc <= 3999) || ($pc >= 8000) && ($pc <= 8399)) {
				return 'VIC';
			}
			else if (($pc >= 4000) && ($pc <= 4999) || ($pc >= 9000) && ($pc <= 9015)) {
				return 'QLD';
			}
			else if (($pc >= 5000) && ($pc <= 5889)) {
				return 'SA';
			}
			else if (($pc >= 6000) && ($pc <= 6999)) {
				return 'WA';
			}
			else if (($pc >= 7000) && ($pc <= 7999)) {
				return 'TAS';
			}
			else if (($pc >= 800) && ($pc <= 899)) {
				return 'NT';
			}
			else {
				return NULL;
			}
		}



	private function _uniquecode($code) {
		$codematch = UniqueCodes::Where('uniquecode','=',$code)->first();
		//$codematch->active	= 0;
		$codematch->save();
	}

	public function _doDataEntry($input) {
		$data		= new Entries();
		$data->fill($input);
		$rawData	= array(
			'post'			=> $_POST,
			'agent'			=> $this->UserAgent->detect(),
		);
		$data->rawdata	= json_encode($rawData);

		if ( $data->save() ) {
			return 'thanks';
		};
	}

	private function _entrylimit($data) {
		$entryRestrictions = Promotion::Where('option','=','number_of_entries_per')->pluck('value');
		$entryLimit = Promotion::Where('option','=','number_of_entries')->pluck('value');
		$limitedBy = Promotion::Where('option','=','limited_by')->pluck('value');
		if(($entryRestrictions == 'Promo' && count(Entries::Where($limitedBy,'=',$data[$limitedBy])->get()) >= $entryLimit) || ($entryRestrictions == 'Day' && count(Entries::Where($limitedBy,'=',$data[$limitedBy])->WhereRaw('DATE(`created_at`) = CURDATE()')->get()) >= $entryLimit)) {
			return Redirect::to('/limit/');
		}
	}

	public function _doInstantWin($data,$limitedBy) {

		// Check if promotion is live yet - Switch between testing table and live table
		$promoStart = Promotion::Where('option','=','start_date')->pluck('value');
		$today = date("Y-m-d H:i:s");
		if($today >= $promoStart) {
			$table_name = 'entries';
		}else{
			$table_name = 'entries_testing';
		}

		//Check if prizes are selected by state
		$whereStates = '';
		if(Promotion::Where('option','=','prizes_by_state')->pluck('value') == 1) {
			$stateID = States::Where('state','=',$data['state'])->pluck('id');
			$whereStates = 'AND prize_pool_id IN (SELECT prize_pools_id FROM states_prizes WHERE state_id = '.$stateID.')';
		}

		$rawData	= array(
			'post'			=> $_POST,
			'agent'			=> $this->UserAgent->detect(),
		);
		$raw_data	= json_encode($rawData);

		$columns = array();
		$values = array();
		foreach($data as $_i=>$_d) {
			$values[] = $_d;
			$columns[] = $_i;
		}

		// Check if entrant is from South Australia - If yes, do not apply win limit to them, otherwise add in Win Restrictions
		if((isset($data['state']) && $data['state'] == 'SA') || (isset($data['postcode']) && $data['postcode'] >= 5000 && $data['postcode'] <= 5950)) {
			$limitWins = '';
		}else{
			$winRestrictions = Promotion::Where('option','=','number_of_wins_per')->pluck('value');
			$winLimit = Promotion::Where('option','=','number_of_wins')->pluck('value');

			if ($winRestrictions == 'Promo') {
				$limitWins = "AND (
								  SELECT count(email) AS total
								  	FROM `".$table_name."`
										WHERE instantwin_id != 0
										AND ".$limitedBy." = '".$data[$limitedBy]."'
										OR instantwin_id != 0
										AND email = '".$data['email']."'
							  ) < ".$winLimit." ";
			} else if ($winRestrictions == 'Day') {
				$limitWins = "AND (
								  SELECT count(email) AS total
								  	FROM `".$table_name."`
										WHERE instantwin_id != 0
										AND DATE(`created_at`) = CURDATE()
										AND ".$limitedBy." = '".$data[$limitedBy]."'
										OR instantwin_id != 0
										AND email = '".$data['email']."'
										AND ".$limitedBy." = '".$data[$limitedBy]."'
							  ) < ".$winLimit." ";
			}
		}

		if($today >= $promoStart) {
			$table = 'instantwin';
		}else{
			$table = 'instantwin_testing';
		}

		$sql = "INSERT INTO ".$table_name." (`instantwin_id`,";
		foreach($columns as $_c) {
			$sql .= $_c.',';
		}
		$sql .= 'rawdata,created_at,updated_at) SELECT i.id AS instantwin_id,';

		foreach($values as $_v) {
			$sql .= "'".$_v."', ";
		}
		$sql .= "'".$raw_data."', now(), now() ";
		$sql .= "FROM `".$table."` i
							LEFT JOIN `".$table_name."` e
							  ON e.instantwin_id = i.id
							WHERE now() >= i.time_slot
							  AND e.instantwin_id IS NULL
							  ".$limitWins."
							  ".$whereStates."
						ORDER BY i.time_slot ASC LIMIT 1";

		$doWin = DB::statement($sql);

		$theid= DB::getPdo()->lastInsertId();

		$prize_id = Entries::Where('id','=',$theid)->pluck('instantwin_id');

		$prize_pool = InstantWin::Where('id','=',$prize_id)->pluck('prize_pool_id');
		$prize_desc_id = PrizePool::Where('id','=',$prize_pool)->pluck('prize_details_id');

		$this->_pcn = InstantWin::Where('id','=',$prize_id)->pluck('pcn');

		$this->_prize = PrizeDetails::Where('id','=',$prize_desc_id)->pluck('web_name');

		if($theid == 0) {
			$thankyou = EntriesController::_doDataEntry($data);
			return $thankyou;
		}else{

			$_data = array(
				'sendto'		=> $data['email'],
				'name'		=> $data['firstname'],
				'pcn'		=> $this->_pcn,
				'id'			=> $theid
			);

			$verifySetup = EntriesController::_verifySetup($_data);

			$_data['rankey'] = $verifySetup;

			$notifyWinner = EntriesController::_notifyWinner($_data);

			Session::put('entry_id', $theid);

			return 'winner';
		}

	}

	public function fbshare() {
		$entryid = Session::get('entry_id');

		$oldentry = Entries::find($entryid)->replicate();
		$oldentry->fbshare = 1;
		$oldentry->who = Input::get('who');
		$oldentry->what = Input::get('what');
		$oldentry->where = Input::get('where');
		if($oldentry->save()) {
			return 'success';
		}else{
			return false;
		}
	}

	private static function _verifySetup($data) {

		$entryData  =  Entries::Where('id','=',$data['id'])->firstOrFail();
		$prizes			=  InstantWin::prize($data['pcn']);

		$verify  										=  new VerifyWinner();
		$verify->entries_id					=	 $entryData['id'];
		$verify->email							=  $entryData['email'];
		$verify->pcn								=	 $data['pcn'];
		$verify->first_name					=	 $entryData['firstname'];
		$verify->last_name					=	 $entryData['lastname'];
		$verify->prize							=	 $prizes;
		$verify->rankey							=  str_random(60);
		$verify->address_line_1			=	 $entryData['address'];
		$verify->suburb							=	 $entryData['suburb'];
		$verify->state							=	 $entryData['state'];
		$verify->postcode						=	 $entryData['postcode'];

		$verify->save();

		return $verify->rankey;

	}

	private static function _notifyWinner($input) {

		$tmpl		= 'edm.winner';
		$edm['edm_address'] = trim(Promotion::Where('option','=','edm_address')->pluck('value'));
		$edm['edm_name'] = Promotion::Where('option','=','edm_name')->pluck('value');
		$edm['edm_subject'] = Promotion::Where('option','=','edm_subject')->pluck('value');
		$edm['email'] = trim($input['sendto']);
		$edm['name'] = $input['name'];
		$data['pcn'] = $input['pcn'];
		$data['url'] = url('verify/'.$input['rankey']);
		//  _e::prex($edm);
		Mail::send('edm.winner', $data, function($message) use ($edm)
		{
				$message->from($edm['edm_address']);
				$message->to($edm['email'])->subject($edm['edm_subject']);
		});

		$update = Entries::Where('id','=',$input['id'])->firstOrFail();
		$update['edm_sent'] = $update['edm_sent'] + 1;
		$update->save();

		return '';

	}


	public function limit() {
		return View::make('limit');
	}

	public function thanks() {
		return View::make('thanks');
	}

	public function winner() {
		$hasPreview = Session::get('preview');
		if ($hasPreview) {
			$IW = InstantWin::first();
			return View::make('winner')
				->with('pcn', $IW->pcn)
				->with('prize_name', $IW->PrizePool->PrizeDetails->web_name)
			;
		} else {
			return Redirect::to('/');
		};
	}

	public function _getAge($dob) {
		$date_array = explode("/",$dob); // split the array
		$var_day = $date_array[0]; //day seqment
		$var_month = $date_array[1]; //month segment
		$var_year = $date_array[2]; //year segment
		$new_date_format = "$var_year-$var_month-$var_day";
		return (new DateTime)->diff(new DateTime($new_date_format))->y;

	}

}
