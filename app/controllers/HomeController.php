<?php
class HomeController extends BaseController {

	public function index() {
		return View::make('home')
			->with('index', 1)
			->with('_title', $this->_title)
			;
	}
	
	public function preview($id) {
		Session::put('preview', $id);
		return Redirect::to('/');
	}
	
	public function prePromo() {
		$promo = Promotion::where('option','=','start_date')->first();
		$promoDate = AdminHelper::formatShortDateOnly($promo->value);
		return View::make('prePromo')
			->with('promoDate', $promoDate)
			->with('_title', $this->_title)
			;
	}

	public function postPromo() {
		return View::make('postPromo')
			->with('_title', $this->_title)
			;
	}

}
