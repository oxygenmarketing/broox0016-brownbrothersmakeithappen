<?php

use Acme\UserAgent\UserAgent;
use Acme\Promotion\Promotion;

class BaseController extends Controller {
	
	protected $UserAgent;
	
	public function __construct(UserAgent $UserAgent, Promotion $Promotion){
		session_start();
		// Set User Agent
		$this->UserAgent = $UserAgent;
		$UserAgent->detect();
		
		// Include Facebook API
		require_once app_path().'/library/facebook.php';
		require_once app_path().'/library/faceconn/faceconn.php';
		$this->facebook		= new Facebook(AppConfig::GetKeyArray());
		
		// Set Promo Vars
		$this->Promotion = $Promotion;
		$this->_title = $Promotion->title();
		$this->_gacode = $Promotion->googleanalytics();
		$sessionarray = array(
			'title' => $this->_title,
			'gacode'	=> $this->_gacode
		);
		Session::put($sessionarray);
	}
	
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout() {
		if ( ! is_null($this->layout)) {
			$this->layout = View::make($this->layout);
		};
	}
	
	public function getFBUser() {
		//$facebook		= new Facebook(AppConfig::GetKeyArray());
		return $this->facebook->getUser();
	}

}
