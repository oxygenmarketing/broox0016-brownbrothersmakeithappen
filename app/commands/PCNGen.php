<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PCNGen extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'python:pcngen';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate PCNS for this competitions prize pool';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//
    //Magically get the count
	$instant = array('InstantWinGen', 'TestWinGen');
	foreach($instant as $i) {
		$count = $i::where('pcn', '=', '')->count();
			//MAGIC
	
		$directory = app_path() . "/storage/python/pcngen.py";
	
		$directory = preg_replace('/\s+/', '\ ', $directory);
	
		$start = microtime(true);
		exec("/usr/bin/python {$directory} {$count}", $output, $return);
		$end = microtime(true);
	
		$time = $end - $start;
	
		if ($return) {
			  throw new \Exception("Error executing command - error code: $return");
		}else {
		  $arr = str_split($output[0], 6);
		  $iws = $i::where('pcn', '=', '')->get();
		  for($i = 0; $i < count($iws); $i++){
			$iws[$i]->pcn = $arr[$i];
			$iws[$i]->save();
		  }
		}
	}
    return 0;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
