<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PrizeDrawCron extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'prize:draw';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Do Prize Draw';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$today = date('Y-m-d');
		$prizeDrawDay = PrizeDraw::Where('winner_id', '=', 0)->Where('draw_date','<=',$today)->get();

		foreach($prizeDrawDay as $pdd) {
			$winners = PrizeDraw::Where('winner_id','>', 0)->lists('winner_id');
			$winner = Entries::Where('created_at','<=',$pdd->end_day)->Where('state','=','SA')->OrWhereNotIn('id', $winners)->Where('created_at','<=',$pdd->end_day)->orderByRaw("RAND()")->pluck('id');
			if($winner) {
				$pdd->winner_id = $winner;
				$pdd->save();
			};
		}

		$open = fopen(url()."/textfile.txt","w+");
		fwrite($open, 'Cron job');
		fclose($open);
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
