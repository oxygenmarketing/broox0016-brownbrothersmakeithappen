<?php

class AdminFormHelper {
	
	public static function _getSession($url = NULL) {
		return ($url) ? Session::get($url) : Session::get(Request::url()) ;
	}
	
	public static function _txtField($errors,$id,$text,$placeholder,$value=NULL,$disabled=NULL,$auto=NULL) {
	
		$opt	= array('id'=>$id, 'class'=>'form-control', 'placeholder'=> $placeholder);
		if (isset($auto) && $auto == 1)				$opt['autocomplete']	= 'off';
		if (isset($disabled) && $disabled == 1)		$opt['disabled']	= 'disabled';
		//$opt['placeholder']	= $text;
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10">';
		$ret .=		'		'. Form::text($id,$value,$opt) .'';
		$ret .=		'	</div>';
		$ret .=		($errors->has($id) ) ? '<span class="glyphicon glyphicon-warning-sign form-control-feedback" '.
		'data-toggle="tooltip" data-placement="left" title="'.$errors->first($id).'"'.
		'></span>' : '' ;
		$ret .=		'</div>';
		
		return $ret;
	}	
	
	public static function _txtFieldOnly($errors,$id,$text,$placeholder,$value=NULL,$disabled=NULL,$auto=NULL) {
	
		$opt	= array('id'=>$id, 'class'=>'form-control', 'placeholder'=> $placeholder);
		if (isset($auto) && $auto == 1)				$opt['autocomplete']	= 'off';
		if (isset($disabled) && $disabled == 1)		$opt['disabled']	= 'disabled';
		//$opt['placeholder']	= $text;
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'		'. Form::text($id,$value,$opt) .'';
		$ret .=		($errors->has($id) ) ? '<span class="glyphicon glyphicon-warning-sign form-control-feedback" '.
		'data-toggle="tooltip" data-placement="left" title="'.$errors->first($id).'"'.
		'></span>' : '' ;
		$ret .=		'</div>';
		
		return $ret;
	}	
	
	public static function _txtShortField($errors,$id,$text,$disabled=NULL,$auto=NULL) {
	
		$opt	= array('id'=>$id, 'class'=>'form-control');
		if (isset($auto) && $auto == 1)				$opt['autocomplete']	= 'off';
		if (isset($disabled) && $disabled == 1)		$opt['disabled']	= 'disabled';
		//$opt['placeholder']	= $text;
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-xs-4">';
		$ret .=		'		'. Form::text($id,null,$opt) .'';
		$ret .=		'	</div>';
		$ret .=		($errors->has($id) ) ? '<span class="glyphicon glyphicon-warning-sign form-control-feedback" '.
		'data-toggle="tooltip" data-placement="left" title="'.$errors->first($id).'"'.
		'></span>' : '' ;
		$ret .=		'</div>';
		
		return $ret;
	}
	
	public static function _txtShortFieldEntry($errors,$id,$title,$text,$disabled=NULL,$auto=NULL) {
	
		$opt	= array('id'=>$id, 'class'=>'form-control');
		if (isset($auto) && $auto == 1)				$opt['autocomplete']	= 'off';
		if (isset($disabled) && $disabled == 1)		$opt['disabled']	= 'disabled';
		//$opt['placeholder']	= $text;
		
		$ret  =		'<div class="form-group ';
		if(isset($errors))	
			$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, strip_tags($title), array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-xs-4">';
		$ret .=		'		'. Form::text($id,null,$opt) .'';
		$ret .=		'	</div>';
		if(isset($errors))		
			$ret .=		($errors->has($id) ) ? '<span class="glyphicon glyphicon-warning-sign form-control-feedback" '.
		'data-toggle="tooltip" data-placement="left" title="'.$errors->first($id).'"'.
		'></span>' : '' ;
		$ret .=		'</div>';
		
		return $ret;
	}	
		
	
	public static function _txtBoxField($errors,$id,$text,$disabled=NULL,$auto=NULL) {
	
		$opt	= array('id'=>$id, 'class'=>'form-control', 'rows'=>10);
		if (isset($auto) && $auto == 1)				$opt['autocomplete']	= 'off';
		if (isset($disabled) && $disabled == 1)		$opt['disabled']	= 'disabled';
		//$opt['placeholder']	= $text;
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10">';
		$ret .=		'		'. Form::textarea($id,null,$opt) .'';
		$ret .=		'	</div>';
		$ret .=		($errors->has($id) ) ? '<span class="glyphicon glyphicon-warning-sign form-control-feedback" '.
		'data-toggle="tooltip" data-placement="left" title="'.$errors->first($id).'"'.
		'></span>' : '' ;
		$ret .=		'</div>';
		
		return $ret;
	}
		
	public static function array_unshift_assoc(&$arr, $key, $val) { 
			$arr = array_reverse($arr, true); 
			$arr[$key] = $val; 
			return array_reverse($arr, true); 
	}
		
	public static function _selField($errors,$id,$text,$options) {
		//_e::prex($options);
		
		$opt	= array('id'=>$id, 'class'=>'form-control');
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10">';
		
		$options = AdminFormHelper::array_unshift_assoc($options, 0, '- SELECT -');
		$ret .=		'		'. Form::select($id, $options, NULL, $opt) .'';
		//_e::pre($options);
		$ret .=		'	</div>';
		$ret .=		($errors->has($id) ) ? '<span class="glyphicon glyphicon-warning-sign form-control-feedback" '.
		'data-toggle="tooltip" data-placement="left" title="'.$errors->first($id).'"'.
		'></span>' : '' ;
		$ret .=		'</div>';

		$ret .=		'<script>';
		$ret .=		'	$(function() {';
		$ret .=		"
		$('#".$id."').css({'width':'initial'});
		";
		$ret .=		'	});';
		$ret .=		'</script>';

		return $ret;
	}

	public static function _selModelField($errors,$id,$text,$model,$rows=NULL) {
		
		$opt	= array('id'=>$id, 'class'=>'form-control');
		$_row 	= (isset($rows)) ? $rows : array('id','name');
		$_db	= DB::table($model)->where('active','=',1)->get($_row);
		
		//_e::pre(array_flatten($_db));
		
		$options		= array();
		$options[]		= '- SELECT -';
		foreach ($_db as $i=>$v) { 
		
		$_v = array_flatten($v);
		//_e::pre($_v[1]);
			$options[$_v[0]]	= $_v[1];  
		
		};
		
		//exit;
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10">';
		
		$ret .=		'		'. Form::select($id, $options, NULL, $opt) .'';
		
		$ret .=		'	</div>';
		$ret .=		($errors->has($id) ) ? '<span class="glyphicon glyphicon-warning-sign form-control-feedback" '.
		'data-toggle="tooltip" data-placement="left" title="'.$errors->first($id).'"'.
		'></span>' : '' ;
		$ret .=		'</div>';

		$ret .=		'<script>';
		$ret .=		'	$(function() {';
		$ret .=		"
		$('#".$id."').css({'width':'initial'});
		";
		$ret .=		'	});';
		$ret .=		'</script>';

		return $ret;
	}

	public static function _selMultiModelField($errors,$id,$text,$model,$rows=NULL) {
		
		$opt	= array('id'=>$id, 'class'=>'form-control', 'multiple'=>'multiple', 'size' => 4);
		$_row 	= (isset($rows)) ? $rows : array('id','name');
		$_db	= DB::table($model)->where('active','=',1)->get($_row);
		
		//_e::pre(array_flatten($_db));
		
		$options		= array();
		foreach ($_db as $i=>$v) { 
		
		$_v = array_flatten($v);
		//_e::pre($_v[1]);
			$options[$_v[0]]	= $_v[1];  
		
		};
		
		//exit;
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10">';
		
		$ret .=		'		'. Form::select($id.'[]', $options, NULL, $opt) .'';
		
		$ret .=		'	</div>';
		$ret .=		($errors->has($id) ) ? '<span class="glyphicon glyphicon-warning-sign form-control-feedback" '.
		'data-toggle="tooltip" data-placement="left" title="'.$errors->first($id).'"'.
		'></span>' : '' ;
		$ret .=		'</div>';

		$ret .=		'<script>';
		$ret .=		'	$(function() {';
		$ret .=		"
		$('#".$id."').css({'width':'initial'});
		";
		$ret .=		'	});';
		$ret .=		'</script>';

		return $ret;
	}

	public static function _pwField($errors,$id,$text) {
	
		$opt	= array('id'=>$id, 'class'=>'form-control', 'autocomplete'=>'off');
		$opt['autocomplete']	= 'off';
		//$opt['placeholder']	= $text;
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10">';
		$ret .=		'		'. Form::password($id,$opt) .'';
		$ret .=		'	</div>';
		$ret .=		($errors->has($id) ) ? '<span class="glyphicon glyphicon-warning-sign form-control-feedback" '.
		'data-toggle="tooltip" data-placement="left" title="'.$errors->first($id).'"'.
		'></span>' : '' ;
		$ret .=		'</div>';
		
		return $ret;
	}	

	public static function _cbField($errors,$id,$text,$inline=0) {
	
		$opt	= array('id' => $id, 'class' => 'form-control');
		//$opt['autocomplete']	= 'off';
		//$opt['placeholder']	= $text;
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		if ($inline==0) {
			$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		} else {
			$ret .=		'	'. Form::label($id, '&nbsp;', array('class'=>'col-sm-2 control-label')) .'';
		};
		$ret .=		'	<div class="col-sm-10">';
		$ret .=		'		<div class="checkbox">';
		$ret .=		'		'. Form::checkbox($id) .'';
		
		if ($inline==1) $ret .=		' '. $text .'';
		
		$ret .=		'		</div>';
		$ret .=		'	</div>';
		$ret .=		($errors->has($id) ) ? '<span class="glyphicon glyphicon-warning-sign form-control-feedback" '.
		'data-toggle="tooltip" data-placement="left" title="'.$errors->first($id).'"'.
		'></span>' : '' ;
		$ret .=		'</div>';
		
		return $ret;
	}	

	/*---------------------------------------------------------------------------------------------------------*/	
	
	// Bootstrap File Browse
	public static function _fileField($errors,$id,$text,$placeholder=NULL) {
	
		$opt	= array('id'=>$id, 'class'=>'form-control file-inputs', 'data-filename-placement'=>'inside');	
		if ($placeholder) 
			$opt['title'] = $placeholder;	
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10">';
		$ret .=		'		'. Form::file($id,$opt) .'';
		$ret .=		'	</div>';
		$ret .=		($errors->has($id) ) ? '<span class="glyphicon glyphicon-warning-sign form-control-feedback" '.
		'data-toggle="tooltip" data-placement="left" title="'.$errors->first($id).'"'.
		'></span>' : '' ;
		$ret .=		'</div>';

		$ret .=		'<script>';
		$ret .=		'	$(function() {';
		$ret .=		"
		$('.file-inputs').bootstrapFileInput();
		$('.file-inputs').css({'width':'initial'});
		";
		$ret .=		'	});';
		$ret .=		'</script>';

		return $ret;
	}	
	
	// TRUMBOWYG Rich Text Editor
	public static function _txtRichField($errors,$id,$text,$disabled=NULL,$auto=NULL) {
		
		// http://alex-d.github.io/Trumbowyg/documentation.html#basic-options
	
		$opt	= array('id'=>$id, 'class'=>'form-control', 'rows'=>10);
		if (isset($auto) && $auto == 1)				$opt['autocomplete']	= 'off';
		if (isset($disabled) && $disabled == 1)		$opt['disabled']	= 'disabled';
		//$opt['placeholder']	= $text;
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10">';
		
		$ret .=		'		'. Form::textarea($id,null,$opt) .'';
		
		$ret .=		'	</div>';
		$ret .=		($errors->has($id) ) ? '<span class="glyphicon glyphicon-warning-sign form-control-feedback" '.
		'data-toggle="tooltip" data-placement="left" title="'.$errors->first($id).'"'.
		'></span>' : '' ;
		$ret .=		'</div>';
		
		$ret .=		'<script>';
		$ret .=		'	$(function() {';
		$ret .=		"
		$('#".$id."').trumbowyg({
			fullscreenable: false,
			closable: false,
			semantic: true,
			btns: [
				'strong', 'em', 'underline', 'strikethrough', 
				'|', 'unorderedList', 'orderedList', 
				'|', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', 
				'|', 'link', 
				'|', 'viewHTML'
			]
		});";
		$ret .=		'	});';
		$ret .=		'</script>';
		
		return $ret;
	}	
	
	// Bootstrap Date Time Picker
	public static function _datePicker($errors,$id,$text,$placeholder,$current=NULL,$together='false') {
		$opt	= array(
			'id'	=>$id,
			'class'	=>'form-control',
			'data-date-format'	=> 'YYYY-MM-DD HH:mm:ss',
			'placeholder'		=> $placeholder
		);
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10">';
		$ret .=		'	  <div class="input-group date btdp_'.$id.' ">';
		$ret .=		'		'. Form::text($id,$current,$opt) .'';
		$ret .=		'		<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>';
		$ret .=		'	  </div>';
		$ret .=		'   </div>';
		$ret .=		($errors->has($id) ) ? '<span class="glyphicon glyphicon-warning-sign form-control-feedback" '.
		'data-toggle="tooltip" data-placement="left" title="'.$errors->first($id).'"'.
		'></span>' : '' ;
		
		$ret .=		'<script>';
		$ret .=		'	$(function() {';
		$ret .=		"
		$('.input-group.date.btdp_".$id."').datetimepicker({
			sideBySide: ".$together.",
		});
		";
		$ret .=		'	});';
		$ret .=		'</script>';
		
		
		$ret .=		'</div>';
		
		return $ret;
	}	
	
	// Bootstrap Date Time Picker
	public static function _datePickerNew($errors,$id,$text,$disabled,$current=NULL) {
	
	
		$opt	= array(
			'id'	=>$id,
			'class'	=>'form-control',
			'data-date-format'	=> 'YYYY-MM-DD hh:mm:ss'
		);
		if (isset($disabled) && $disabled == 1)				$opt['disabled']	= 'true';
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	  <div class="input-group date">';
		$ret .=		'		'. Form::text($id,$current,$opt) .'';
		$ret .=		'		<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>';
		$ret .=		'   </div>';
		$ret .=		($errors->has($id) ) ? '<span class="glyphicon glyphicon-warning-sign form-control-feedback" '.
		'data-toggle="tooltip" data-placement="left" title="'.$errors->first($id).'"'.
		'></span>' : '' ;
		
		
		$ret .=		'<script>';
		$ret .=		'	$(function() {';
		$ret .=		"
		$('.input-group.date').datetimepicker({
			pick12HourFormat: false,
		});
		";
		$ret .=		'	});';
		$ret .=		'</script>';
		
		
		$ret .=		'</div>';
		
		return $ret;
	}	

	/*---------------------------------------------------------------------------------------------------------*/	

	public static function _btnAction($text) {
		
		$_r = explode( '@', Route::currentRouteAction() );
		//_e::pre($_r);

		$ret  =		'<div class="form-group btn-action">';
		$ret .=		'	<div class="col-sm-offset-2 col-sm-10">';
		$ret .=		'		'. Form::submit($text,array('id'=>"btn-submit",'class'=>"btn btn-primary btn-sm")) .'';
		$ret .=		'		<a href="'. action(e($_r[0]).'@index') .'" role="button" class="btn btn-default btn-sm " rel="">Cancel</a>';
		$ret .=		'	</div>';
		$ret .=		'</div>';

		/*
		$ret .=		'<script>';
		$ret .=		'	$(function() {';
		$ret .=		'		$(window).bind(\'beforeunload\', function(){';
		$ret .=		'			return "Are you sure you want to leave?";';
		$ret .=		'		});';
		$ret .=		'	});';
		$ret .=		'</script>';
		*/

		return $ret;
	}	

	public static function _timeStamp($data) {
	
		$ret  =		'<div class="form-group">';
		$ret .=		'	'. Form::label(NULL, 'Created', array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10">';
		$ret .=		'		<p class="form-control-static">'. AdminHelper::formatLongDateTimeFull($data->created_at) .'</p>';
		$ret .=		'	</div>';
		$ret .=		'	'. Form::label(NULL, 'Last Modified', array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10">';
		$ret .=		'		<p class="form-control-static">'. AdminHelper::formatLongDateTimeFull($data->updated_at) .'</p>';
		$ret .=		'	</div>';
		$ret .=		'</div>';
		
		/*
		$ret .=		'		<ul class="_adminTimestamp">';
		$ret .=		'			<li><span>Created</span> '. AdminHelper::formatLongDateTimeFull($data->created_at) .'</li>';
		$ret .=		'			<li><span>Last Modified</span> '. AdminHelper::formatLongDateTimeFull($data->updated_at) .'</li>';
		$ret .=		'		</ul>';
		*/
		
		return $ret;
	}	
	
	public static function formatLabel($title){
		
		$_name = FormElement::Where('name','=',$title)->first();
		$name = $_name['title'];
		return $name;
	}

		
} 