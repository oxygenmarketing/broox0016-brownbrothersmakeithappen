<?php

class EntryHelper {
	
	public static function returnState($id) {
		switch ($id) {
			case 1:
				return 'VIC/TAS';
				break;
			case 2:
				return 'SA';
				break;
			case 3:
				return 'NSW/ACT';
				break;
			case 4:
				return 'WA';
				break;
			case 5:
				return 'QLD';
				break;
			case 6:
				return 'NT';
				break;
		};
	}
	
	public static function returnAge($birthday) {
		
		if (is_string($birthday)) {
			$birthday = strtotime($birthday);
		}
		
		return (int)floor((time() - $birthday) / 31556926);
		
	}
	
	public static function _set_promo_dates($key_dates) {
		$promo_dates = array();
		foreach ($key_dates as $key => $value) {
			$promo_dates[$key] = strtotime($value);
		}
		return $promo_dates;
	}
	

} 