<?php

class FormHelper {

	/**
	 * Builds form from the database
	 *
	 * $name string		- Set from database
	 * $label boolean	- Set to 0 to hide label and div
	 */
	public static function _formField($name,$errors, $label=1) {

		$formElements = FormElement::Where('name', '=', $name)->first();

		$options = json_decode($formElements->options, true);
		$ret = '';
		if($label == 1) {
			$ret  .=		'<div class="container__main__item ';
			// If custom class is used, you must define div width class in the database, e.g. 'quarter newclass'
			if($formElements->divclass != ''){
				$ret .= $formElements->divclass;
			}else{
				$colCount = FormElement::Where('active','=',1)->Where('rowNumber','=',$formElements->rowNumber)->count();
				switch($colCount) {
					case 2:
						$ret .= 'col6__item';
						break;
					case 3:
						$ret .= 'col4__item';
						break;
					case 4:
						$ret .= 'col3__item';
						break;
					default:
						$ret .= 'col12__item';
						break;
				}
			};

			$ret .=		' text--left">';

			if ($formElements->name == 'terms') {
				$labelText = 'I agree to the <a href="#" class="md-trigger" data-modal="modal-1">Terms &amp; Conditions</a> ';
			}else{
				$labelText = $formElements->title;
			}

			$formElName = $formElements->name;
			$required = (preg_match('/required/', $formElements->validation)) ? ' *' : '';
			if($formElements->type == 'checkbox') {
				$ret .= '<div class="checkbox">';
				$ret .= call_user_func('FormHelper::draw'.$formElements->type, $formElements, $options);
				$ret .=		'	<label for="'.$formElements->name.'"><span></span>'.$labelText.$required.'</label>';
				$ret .= '</div>';
			}else{
				$ret .=		'	<label for="'.$formElements->name.'">'.$labelText.$required.'</label>';
				$ret .= call_user_func('FormHelper::draw'.$formElements->type, $formElements, $options);
			}
			$ret .=		'</div>';
		};
	//	_e::pre($options);




		return $ret;
	}

	private static function drawtext($formelement,$opt) {
		return Form::text($formelement->name,null,$opt);
	}

	private static function drawtel($formelement, $opt) {
		return Form::Input('tel', $formelement->name,null,$opt);
	}

	private static function drawsubmit($formelement,$opt) {
		return Form::submit('',null,$opt);
	}

	private static function drawselect($formelement,$opt) {
		$options = $opt['options'];
		unset($opt['options']);
		//_e::pre($opt);
		return Form::select($formelement->name,$options,null,$opt);
	}

	private static function drawcaptcha($formelement,$opt) {
		$return = "<div class='captcha'>";
		$return .= HTML::image(Captcha::img(), 'Captcha image');
		$return .= "</div>";
		return $return;
	}

	private static function drawentercaptcha($formelement,$opt) {
		return Form::text($formelement->name, null, $opt);
	}

	private static function drawuniquecode($formelement,$opt) {
		return Form::text($formelement->name, null, $opt);
	}

	private static function drawnumber($formelement,$opt) {
		return Form::number($formelement->name,null,$opt);
	}

	private static function drawemail($formelement,$opt) {
		return Form::email($formelement->name,null,$opt);
	}

	private static function drawcheckbox($formelement,$opt) {
		//_e::prex($opt);
		/*$check = $opt['options'];

		$checkB = '';
		$x = 0;
		$_n	= (count($check) > 1) ? '[]' : '';
		//_e::prex($check);
		foreach($check as $i=>$r) {*/
			$checkB = Form::checkbox($formelement->name, 1, null, $opt);
		/*	$x++;
	}*/
		return $checkB;
	}

	private static function drawradio($formelement) {
		$radio = json_decode($formelement->options);
		$radioB = '';
		$x = 0;
		foreach($radio as $i=>$r) {
			$radioB .= Form::radio($formelement->name, $i,null, array('id' => $formelement->name.'_'.$x)).Form::Label($formelement->name, $r, array());
			$x++;
		}
		return $radioB;
	}

	private static function drawpassword($formelement,$opt) {
		return Form::password($formelement->name,null,$opt);
	}

	private static function drawfile($formelement,$opt) {
		return Form::file($formelement->name,null,$opt);
	}

	private static function drawtextarea($formelement,$opt) {
		return Form::textarea($formelement->name,null,$opt);
	}

	private static function drawhidden($formelement, $opt) {
		return Form::hidden($formelement->name,null,$opt);
	}

}
