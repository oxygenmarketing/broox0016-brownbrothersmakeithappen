<?php

class AdminHelper {

	public static function init($url = NULL) {

		// DEFAULTS
		$orderbyDef	= 'id';
		$dirDef			= 'asc';
		$perPageDef	= 10;

		// QUERIES
		$sessLoc		= AdminHelper::_getSession($url);
		$perPage		= Input::query('perPage');
		$filterby		= Input::query('filter');
		$orderby		= Input::query('order');

		// SET FILTER OPTION
		$searchterm	= (isset($sessLoc->searchterm)) ? $sessLoc->searchterm : NULL;

		$filterby		= (isset($sessLoc->filterby)) ? $sessLoc->filterby : NULL;

		// SET ORDER OPTION
		if ($orderby == NULL) {
			$orderby	= (isset($sessLoc->orderby)) ? $sessLoc->orderby : $orderbyDef;
			$dir		= (isset($sessLoc->dir)) ? $sessLoc->dir : $dirDef;
		} else {
			$reorder	= explode(',',$orderby);
			$orderby	= $reorder[0];
			$dir		= $reorder[1];
		};

		// SET PAGER OPTION
		if ($perPage == NULL) {
			$perPage	= (isset($sessLoc->perpage)) ? $sessLoc->perpage : $perPageDef;
		} else if ($perPage == 'all') {
			$perPage	= 99999999999999;
		};

		// OUTPUT ARRAY
		$adminHlpr	= (object) array(
			'searchterm'	=> $searchterm
		,	'filterby'		=> $filterby
		,	'orderby'		=> $orderby
		,	'dir'			=> $dir
		,	'pagination'	=> array( 'perPage' => $perPage )
		);

		Session::put(Request::url(), (object) array(
			'searchterm'	=> $searchterm
		,	'filterby'		=> $filterby
		,	'orderby'		=> $orderby
		,	'dir'			=> $dir
		,	'perpage'		=> $perPage
		));

		return $adminHlpr;

	}

	public static function _getSession($url = NULL) {
		return ($url) ? Session::get($url) : Session::get(Request::url()) ;
	}

		public static function drawHeaderRow($row,$text) {

			$_var	= AdminHelper::_getSession();

			$chev=$_dir=$_ico='';

			if ($_var->orderby == $row) {
				 if ($_var->dir == 'asc') {
					 $_dir	= 'desc';
					 $_ico = 'glyphicon-sort-by-attributes';
				 } else {
					 $_dir	= 'asc';
					 $_ico = 'glyphicon-sort-by-attributes-alt';
				 };
				 $chev	= '<span class="glyphicon glyphicon '.$_ico.'"></span> ';
			} else {
				$chev	= '';
				$_dir	= $_var->dir;
			};

			return '<a href="'.url(Request::url().'/?order='.$row.','.$_dir).'">'.$chev.$text.'</a>';

		}

		public static function toggleState($id,$row,$func,$text) {

			$_btn = $_ico = '';

			switch ($row) {
				case 1 :
					$_btn	= 'btn-success';
					$_ico	= 'glyphicon-ok-sign';
					break;
				case 2 :
					$_btn	= 'btn-info';
					$_ico	= 'glyphicon glyphicon-info-sign';
					break;
				case 5 :
					$_btn	= 'btn-primary';
					$_ico	= 'glyphicon glyphicon-plus-sign';
					break;
				case 9 :
					$_btn	= 'btn-danger';
					$_ico	= 'glyphicon glyphicon-remove-circle';
					break;
				case 0 :
					$_btn	= 'btn-warning';
					$_ico	= 'glyphicon glyphicon-minus-sign';
					break;
				default : //??
					$_btn	= 'btn-default';
					$_ico	= 'glyphicon glyphicon-question-sign';
					break;
			};

			return '<a href="'.url(Request::url().'/'.$func.'/'.$id).'" role="button" class="btn '.$_btn.'  btn-sm" title="'.$text.'"><span class="glyphicon '.$_ico.'"></span></a>';
		}

		public static function editStateSelection($prize_id, $pool_id, $state_id, $action) {

			$_btn = $_ico = '';

			switch ($action) {
				case 'delete' :
					$_btn	= 'btn-success';
					$_ico	= 'glyphicon-ok';
					break;
				case 'insert' :
					$_btn	= 'btn-default';
					$_ico	= 'glyphicon-remove';
					break;
			};

			return '<a href="'.url(Request::url().'/'.$pool_id.'/'.$state_id.'/'.$action).'" role="button" class="btn '.$_btn.'  btn-sm" title="'.$action.'"><span class="glyphicon '.$_ico.'"></span></a>';
		}

		public static function toggleWinner($id,$row,$func,$text) {

			$_btn = $_ico = '';

			switch ($row) {
				case 1 :
					$_btn	= 'btn-success';
					$_ico	= 'glyphicon-ok-sign';
					break;

				case 5 :
					$_btn	= 'btn-primary';
					$_ico	= 'glyphicon glyphicon-plus-sign';
					break;
				case 9 :
					$_btn	= 'btn-danger';
					$_ico	= 'glyphicon glyphicon-remove-circle';
					break;
				case 0 :
					$_btn	= 'btn-warning';
					$_ico	= 'glyphicon glyphicon-minus-sign';
					break;
				default : //??
					$_btn	= 'btn-default';
					$_ico	= 'glyphicon glyphicon-question-sign';
					break;
			};

			return '<a href="'.url(Request::url().'/'.$func.'/'.$id).'" role="button" class="btn '.$_btn.'  btn-sm" title="'.$text.'"><span class="glyphicon '.$_ico.'"></span></a>';
		}

		public static function toggleSecond($id,$row,$func,$text) {

			$_btn = $_ico = '';

			switch ($row) {

				case 2 :
					$_btn	= 'btn-info';
					$_ico	= 'glyphicon-ok-sign';
					break;

				case 5 :
					$_btn	= 'btn-primary';
					$_ico	= 'glyphicon glyphicon-plus-sign';
					break;
				case 9 :
					$_btn	= 'btn-danger';
					$_ico	= 'glyphicon glyphicon-remove-circle';
					break;
				case 0 :
					$_btn	= 'btn-warning';
					$_ico	= 'glyphicon glyphicon-minus-sign';
					break;
				default : //??
					$_btn	= 'btn-default';
					$_ico	= 'glyphicon glyphicon-question-sign';
					break;
			};

			return '<a href="'.url(Request::url().'/'.$func.'/'.$id).'" role="button" class="btn '.$_btn.'  btn-sm" title="'.$text.'"><span class="glyphicon '.$_ico.'"></span></a>';
		}

		public static function toggleThird($id,$row,$func,$text) {

			$_btn = $_ico = '';

			switch ($row) {

				case 3 :
					$_btn	= 'btn-primary';
					$_ico	= 'glyphicon-ok-sign';
					break;
				case 5 :
					$_btn	= 'btn-primary';
					$_ico	= 'glyphicon glyphicon-plus-sign';
					break;
				case 9 :
					$_btn	= 'btn-danger';
					$_ico	= 'glyphicon glyphicon-remove-circle';
					break;
				case 0 :
					$_btn	= 'btn-warning';
					$_ico	= 'glyphicon glyphicon-minus-sign';
					break;
				default : //??
					$_btn	= 'btn-default';
					$_ico	= 'glyphicon glyphicon-question-sign';
					break;
			};

			return '<a href="'.url(Request::url().'/'.$func.'/'.$id).'" role="button" class="btn '.$_btn.'  btn-sm" title="'.$text.'"><span class="glyphicon '.$_ico.'"></span></a>';
		}

		public static function btnEdit($id,$text) {
			return '<a href="'.url(Request::url().'/'.$id.'/edit').'" role="button" class="btn btn-default btn-sm" title="'.$text.'">'.$text.'</a>';
		}

		public static function btnWinnerView($id) {
			$winner	= Winners::where('instantwin_id','=',$id)->count();
			$action	= ($winner > 0) ? action('Admin_IWController@show', array($id)) : action('Admin_IWController@edit', array($id)); ;
			$text   = ($winner > 0) ? 'View' : 'Edit' ;

			return '<a href="'.$action.'" role="button" class="btn btn-default btn-sm" title="'.$text.'">'.$text.'</a>';

		}


		public static function btnView($id,$text) {
			return '<a href="'.url(Request::url().'/'.$id.'').'" role="button" class="btn btn-default btn-sm" title="'.$text.'">'.$text.'</a>';
		}

		public static function btnResend($id,$text) {
			return '<a href="'.url(Request::url().'/resend/'.$id.'').'" role="button" class="btn btn-default btn-sm" title="'.$text.'">'.$text.'</a>';
		}

		public static function btnVerify($id,$text) {
			return '<a href="'.url(Request::url().'/verify/'.$id.'').'" role="button" class="btn btn-default btn-sm" title="'.$text.'">'.$text.'</a>';
		}

		public static function viewEntry($id) {
			$ret  =		'<div class="form-group btn-action">';
			$ret .=		'	<div class="col-sm-offset-2 col-sm-10">';
			$ret .=		'		<a href="'.url('/admin/entries/'.$id).'" role="button" class="btn btn-default btn-sm" title="View Full Entry">View Full Entry</a>';
			$ret .=		'		<a href="'.url('/admin/instantwin/revoke/'.$id).'" id="revokeWinner" rel="revokeWinner" role="button" class="btn btn-danger btn-sm" title="Revoke Prize">Revoke Prize</a>';
			$ret .=		'		<a href="javascript:history.back();" role="button" class="btn btn-default btn-sm " rel="">Back</a>';
			$ret .=		'	</div>';
			$ret .=		'</div>';
			return $ret;
		}

		public static function btnAdd($text) {
			return '<a href="'.url(Request::url().'/new').'" role="button" class="btn btn-default btn-sm" title="'.$text.'">'.$text.'</a>';
		}

		public static function btnDelete($id,$ask=0) {


		?>
		<?php echo Form::open(array('url'=>array(url(Request::url().'/'.$id.'')),'method'=>'DELETE','class'=>'form-inline','style'=>'display:inline;')); ?>
			<button class="btn btn-danger btn-sm <?php echo ($ask) ? '_askQuestion' : '' ; ?>" type="submit" rel="delete"><span class="glyphicon glyphicon-remove-sign"></span></button>
		<?php echo Form::close(); ?>
		<?php
		}

		public static function filterList($id,$text,$options) {

			$sessLoc	= AdminHelper::_getSession(Request::url());
			$filterby	= $sessLoc->filterby;
			$selected	= NULL;

			if( isset( $filterby[$id] ) ) {
				$selected	= $filterby[$id];
			} else {
				$_opt		= array('-'=>''.$text.' column','--'=>'--------------------');
			};

			foreach ($options as $i=>$v) {
				$_opt[$i] = ($selected == $i) ? 'by '.$v.' only' : $v.' only' ;
			};
			$_opt['all'] = 'All';

			return Form::select('filter['.$id.']',$_opt,$selected,array('id'=>'filter_'.$id,'class'=>'form-control input-sm form-filterlist','role'=>'filter'));
		}

		public static function filterListModel($id,$text,$model) {

			$sessLoc	= AdminHelper::_getSession(Request::url());
			$filterby	= $sessLoc->filterby;
			$selected	= NULL;

			if( isset( $filterby[$id] ) ) {
				$selected	= $filterby[$id];
			} else {
				$_opt		= array('-'=>''.$text.' column','--'=>'--------------------');
			};

			$_m	=	DB::table($model[0])->where('active','!=',9)->get($model[1]);
			foreach ($_m as $m) {
				$m = (array)$m;

				if(isset($m['date'])){
					$m['date'] = AdminHelper::formatLongDateOnly($m['date']);
				}
				$_opt[$m[$model[1][0]]] = ($selected == $m[$model[1][0]]) ? 'by '.$m[$model[1][1]].' only' : $m[$model[1][1]].' only' ;
			};
			$_opt['all'] = 'All';

			return Form::select('filter['.$id.']',$_opt,$selected,array('id'=>'filter_'.$id,'class'=>'form-control input-sm form-filterlist','role'=>'filter'));
		}

		public static function filterDateRange($defaults=NULL) {

			$opt1	= array(
				'id'					=> 'date-start',
				'class'					=> 'form-control',
				'data-date-format'		=> 'DD-MM-YYYY',
				'name'					=> 'filter[date-start]',
				'placeholder'			=> 'Start Date',
			);
			$opt2	= array(
				'id'					=> 'date-end',
				'class'					=> 'form-control',
				'data-date-format'		=> 'DD-MM-YYYY',
				'name'					=> 'filter[date-end]',
				'placeholder'			=> 'End Date',
			);

			$ret  =		'<div class="form-group" style="width:260px;">';
			$ret .=		'	  <div class="input-group date">';
			$ret .=		'		'. Form::text('date-start',(($defaults['date-start']) ? $defaults['date-start'] : NULL),$opt1) .'';
			$ret .=		'		<span class="input-group-addon">-</span>';
			$ret .=		'		'. Form::text('date-end',(($defaults['date-end']) ? $defaults['date-end'] : NULL),$opt2) .'';
			$ret .=		'	  </div>';
			$ret .=		'</div>';
			$ret .=		'		<button id="date-filter-reset" class="btn btn-default btn-sm" type="submit"><span class="glyphicon glyphicon-remove-sign"></span></button>';

			$ret .=		'<script>';
			$ret .=		'	$(function() {';
			$ret .=		"
			$('#date-start').datetimepicker({
				pickTime: false
			});
			$('#date-end').datetimepicker({
				pickTime: false
			});
			$('#date-filter-reset').bind('click',function(e){
				e.preventDefault();
				$('#date-start, #date-end').val('');
				$(this).parent('form').submit();
			});
			";
			$ret .=		'	});';
			$ret .=		'</script>';

			return $ret;

		}

		public static function renderSearchForm() {

		?>
		<?php echo Form::open(array('url'=>'/admin/search','id'=>'frmSearch','class'=>'navbar-form navbar-left','role'=>'search')); ?>
            <div class="input-group">
                <?php echo Form::text('search',AdminHelper::_getSearchTerm(),array('id'=>'search','placeholder'=>'Search','class'=>'form-control input-sm')); ?>
                <span class="input-group-btn">
                    <button class="btn btn-default btn-sm" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                    <button class="btn btn-default btn-sm _reset" type="submit"><span class="glyphicon glyphicon-remove"></span></button>
                </span>
            </div>
		<?php echo Form::close(); ?>
		<?php
		}

			public static function _getSearchTerm() {
				$sessLoc	= AdminHelper::_getSession(Request::url());
				return e($sessLoc->searchterm);
			}

	public static function siteFooter() {
		return Lang::get('crudadmin.gui.footer.copyright');
	}

	public static function drawDir($arrow = NULL) {
		$sessLoc	= AdminHelper::_getSession();
		switch ($sessLoc->dir) {
			case 'asc' :
				$dir = 'desc';
				$chevron = '<i class="icon-chevron-down"></i>';
				break;
			case 'desc' :
				$dir = 'asc';
				$chevron = '<i class="icon-chevron-up"></i>';
				break;
		};
		return ($arrow == 1) ? $chevron : $dir ;
	}

	public static function drawAdminRoute() {

		$contents	= File::get(app_path().'/config/_admin.xml');
		$xml		= new SimpleXMLElement($contents);

		if ( count($xml) > 0 ) {
			foreach( $xml as $route ) {
				$usr		= Auth::user()->user_type;
				$allow		= $route->attributes();

				$allowList	= NULL;
				if ($allow['allow'] != '') $allowList = explode(',',$allow['allow']);
				if ( isset($allowList)  ) {
					if($allow['type'] == 'IW') {
						if(Promotion::Where('option','=','has_prizes')->pluck('value') == 1) {
							if ( in_array( $usr, $allowList ) ) AdminHelper::_drawAdminRoute($route);
						}
					}else if($allow['type'] == 'SMS') {
						if(Promotion::Where('option','=','sms_entry')->pluck('value') == 1) {
							if ( in_array( $usr, $allowList ) ) AdminHelper::_drawAdminRoute($route);
						}
					}else{
						if ( in_array( $usr, $allowList ) ) AdminHelper::_drawAdminRoute($route);
					}

				} else {
					if($allow['type'] == 'IW') {
						if(Promotion::Where('option','=','has_prizes')->pluck('value') == 1) {
							AdminHelper::_drawAdminRoute($route);
						}
					}else if($allow['type'] == 'SMS') {
						if(Promotion::Where('option','=','sms_entry')->pluck('value') == 1) {
							AdminHelper::_drawAdminRoute($route);
						}
					}else{
						AdminHelper::_drawAdminRoute($route);
					}

				};
			};
		};

	}

		private static function _drawAdminRoute($route) {

			$segments = explode('/',$route->MAIN_LINK);
			echo '<li'. ( ( Request::segment(2) == $segments[2] ) ? ' class="active"' : '' ) .'>'.
			'<a href="'.$route->MAIN_LINK.'"><span class="glyphicon '. ( ( $route->MAIN_ICON ) ? $route->MAIN_ICON : 'glyphicon-th-list' ) .'"></span> '.$route->MAIN_TITLE.'</a>';

			if ( Request::segment(2) == $segments[2] ) {
				if ( count($route->ITEM) > 0 ) {
					echo '<ul class="nav">';
					foreach( $route->ITEM as $sub ) {
						$segments2 = explode('/',$sub->ITEM_LINK);
						echo '<li'. ( (isset($segments2[3])) ? ( ( Request::segment(3) == $segments2[3] ) ? ' class="active"' : '' ) : '' ) .'><a href="'.$sub->ITEM_LINK.'">- '.$sub->ITEM_TITLE.'</a></li>';
					};
					echo '</ul>';
				};
			};

			echo '</li>';

		}


	public static function formatLongDate($date) {
		$format = 'jS F Y @ h:i:sa';
		return date($format,strtotime($date));
	}

	public static function formatLongDateFull($date) {
		$format = 'l, jS F Y';
		return date($format,strtotime($date));
	}

	public static function formatPromoDate($date) {
		$format = 'jS F Y';
		return date($format,strtotime($date));
	}

	public static function formatLongDateTimeFull($date) {
		$format = 'l, jS F Y @ h:i:s A';
		return date($format,strtotime($date));
	}

	public static function formatLongDateOnly($date) {
		$format = 'd F Y';
		return date($format,strtotime($date));
	}
	public static function formatShortDateOnly($date) {
		$format = 'd/m/y';
		return date($format,strtotime($date));
	}
	public static function formatGoogleDate($date) {
		$format = 'Y-m-d';
		return date($format,strtotime($date));
	}

	public static function emailize($address) {
		return '<a href="mailto:'.$address.'">'.$address.'</a>';
	}

	public static function _getDateTime() {
		return date('_d-m-Y_His');
	}

	public static function _create_DateRangeArray($strDateFrom,$strDateTo) {
		$aryRange	= array();
		$iDateFrom	= mktime(1,0,0,	substr($strDateFrom,5,2),	substr($strDateFrom,8,2),	substr($strDateFrom,0,4));
		$iDateTo	= mktime(1,0,0,	substr($strDateTo,5,2),		substr($strDateTo,8,2),		substr($strDateTo,0,4));
		if ($iDateTo>=$iDateFrom) {
			array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
			while ($iDateFrom<$iDateTo) {
				$iDateFrom+=86400; // add 24 hours
				array_push($aryRange,date('Y-m-d',$iDateFrom));
			}
		}
		return $aryRange;
	}

	public static function _getQuestion($id) {
		$q	= Questions::find($id);
		return $q->question_txt.' [<em>'.(($q->answer == 1) ? 'YAY' : 'NEIGH' ).'</em>]';
	}

	public static function _getUserAnswer($id) {
		$q	= Quiz::find($id);
		return (($q->answer == 1) ? 'YAY' : 'NEIGH' );
	}

	public static function _getPrize($id) {
		$q	= InstantWin::find($id);
		return $q->prize;
	}

	public static function _getPCN($id) {
		$q	= InstantWin::find($id);
		return $q->claim_no;
	}

	public static function _getUser($id) {
		$q	= Quiz::find($id);
		$u	= Entries::find($q->entries_id);
		return '<a href="/admin/registrations/'.$u->id.'" title="View">'.$u->firstname.' '.$u->lastname.' [<em>'.$u->fbid.'</em>]</a>';
	}

	public static function _getUserByID($id) {
		$u	= Entries::find($id);
		return '<a href="/admin/registrations/'.$u->id.'" title="View">'.$u->firstname.' '.$u->lastname.' [<em>'.$u->fbid.'</em>]</a>';
	}

	public static function checkWinner($id) {
		$winner	= Winners::where('instantwin_id','=',$id)->count();
		$_ico	= ($winner > 0) ? 'glyphicon-ok-sign' : 'glyphicon glyphicon-minus-sign' ;
		$_btn	= ($winner > 0) ? 'btn-success' : 'btn-warning' ;
		return '<a href="#" role="button" class="btn '.$_btn.' btn-sm"><span class="glyphicon '.$_ico.'"></span></a>';
	}


	public static function checkWinnerPosition($position, $id, $match_id) {
		//TO BE USED WHEN THERE IS A FIRST SECOND AND THIRD PLACE
		$_btn = $_ico = '';
		$lookin = array(1,2,3);
		$lookin = array_except($lookin, array(($position-1)));

		switch ($position) {
			case 1 :
				$_btn	= 'btn-success';
				$_ico	= 'glyphicon-ok-sign';
				$_position = 'winner';
				break;
			case 2 :
				$_btn	= 'btn-info';
				$_ico	= 'glyphicon-ok-sign';
				$_position = 'second';
				break;
			case 3 :
				$_btn	= 'btn-primary';
				$_ico	= 'glyphicon-ok-sign';
				$_position = 'third';
				break;
			case 0 :
				$_btn	= 'btn-warning';
				$_ico	= 'glyphicon glyphicon-minus-sign';
				break;
		};

		$q = Entries::Where('winner', '>', 0)->Where('matches_id', '=', $match_id)->count();

		if($q > 0) {
			$zz = Entries::Where('winner', '>', 0)->Where('matches_id', '=', $match_id)->Where('winner', '=', $position)->first();

			if($zz) {
				if($zz->id == $id) {
					return '<a href="#" role="button" class="btn '.$_btn.'  btn-sm" title="Select Winner"><span class="badge">'.$position.'</span></a>';
				}
			}else{
				$xx = Entries::Where('winner', '>', 0)->Where('matches_id', '=', $match_id)->Where('winner', '!=', $position)->Where('id', '=', $id)->count();
				if($xx) {
					return '';
				}else{
					return '<a href="'.url(Request::url().'/'.$_position.'/'.$id).'" role="button" class="btn '.$_btn.'  btn-sm _askWinnerQuestion" title="Select Winner" rel="'.$_position.'"><span class="glyphicon '.$_ico.'"></span></a>';
				}
			};
		} else {
			return '<a href="'.url(Request::url().'/'.$_position.'/'.$id).'" role="button" class="btn '.$_btn.'  btn-sm _askWinnerQuestion" title="Select Winner" rel="'.$_position.'"><span class="glyphicon '.$_ico.'"></span></a>';
		};
	}


	public static function _set_promo_dates($key_dates) {
		$promo_dates = array();
		foreach ($key_dates as $key => $value) {
			$promo_dates[$key] = strtotime($value);
		}
		return $promo_dates;
	}

	public static function returnAge($birthday) {

		$birthDate = explode("/", $birthday);
	  //get age from date or birthdate
	  $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[2], $birthDate[1], $birthDate[0]))) > date("md")
	    ? ((date("Y") - $birthDate[2]) - 1)
	    : (date("Y") - $birthDate[2]));
	  // echo "Age is:" . $age;
		// die();

		if (is_string($birthday)) {
			$birthday = strtotime($birthday);
		}

		return (int)floor((time() - $birthday) / 31556926);

	}


}
