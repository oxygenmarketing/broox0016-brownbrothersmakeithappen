<?php

class OldFormHelper {
	
	public static function _formField($name) {
		//die();
		$formElements = FormElement::Where('name', '=', $name)->first();
		
		//_e::prex($formElements);
	
		$opt	= array('id'=>$formElements->form_id, 'class'=>$formElements->class);
		if (isset($formElements->readonly) && $formElements->readonly == 1)				$opt['readonly']	= 'readonly';
		if (isset($formElements->disabled) && $formElements->disabled == 1)				$opt['disabled']	= 'disabled';
		if (isset($formElements->placeholder))				$opt['placeholder']	= $formElements->placeholder;
		if (isset($formElements->maxlength))				$opt['maxlength']	= $formElements->maxlength;
		//$opt['placeholder']	= $text;
		
		$ret  =		'<div class="form-group ';
		$ret  .=		$formElements->divclass;
		//$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($formElements->form_id, $formElements->title, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'		'. Form::text($formElements->form_id,null,$opt) .'';
		//$ret .=		($errors->has($id) ) ? '<span class="_errortxt">'.$errors->first($id).'</span>' : '' ;
		$ret .=		'</div>';
		
		return $ret;
	}	
	
	public static function _txtField($name) {
		//die();
		$formElements = FormElement::Where('name', '=', $name)->first();
		
		//_e::prex($formElements);
	
		$opt	= array('id'=>$formElements->form_id, 'class'=>$formElements->class);
		if (isset($formElements->readonly) && $formElements->readonly == 1)				$opt['readonly']	= 'readonly';
		if (isset($formElements->disabled) && $formElements->disabled == 1)				$opt['disabled']	= 'disabled';
		if (isset($formElements->placeholder))				$opt['placeholder']	= $formElements->placeholder;
		if (isset($formElements->maxlength))				$opt['maxlength']	= $formElements->maxlength;
		//$opt['placeholder']	= $text;
		
		$ret  =		'<div class="form-group ';
		$ret  .=		$formElements->divclass;
		//$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($formElements->form_id, $formElements->title, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'		'. Form::text($formElements->form_id,null,$opt) .'';
		//$ret .=		($errors->has($id) ) ? '<span class="_errortxt">'.$errors->first($id).'</span>' : '' ;
		$ret .=		'</div>';
		
		return $ret;
	}	
	
	public static function _txtShortField($errors,$id,$text,$disabled=NULL,$auto=NULL) {
	
		$opt	= array('id'=>$id, 'class'=>'form-control');
		if (isset($auto) && $auto == 1)				$opt['autocomplete']	= 'off';
		if (isset($disabled) && $disabled == 1)		$opt['disabled']	= 'disabled';
		//$opt['placeholder']	= $text;
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-xs-4">';
		$ret .=		'		'. Form::text($id,null,$opt) .'';
		$ret .=		($errors->has($id) ) ? '<span class="_errortxt">'.$errors->first($id).'</span>' : '' ;
		$ret .=		'	</div>';
		$ret .=		'</div>';
		
		return $ret;
	}	
	
	public static function _txtAreaField($errors,$id,$text,$disabled=NULL,$auto=NULL) {
	
		$opt	= array('id'=>$id, 'rows'=>'5', 'class'=>'form-control');
		if (isset($auto) && $auto == 1)				$opt['autocomplete']	= 'off';
		if (isset($disabled) && $disabled == 1)		$opt['disabled']	= 'disabled';
		//$opt['placeholder']	= $text;
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10">';
		$ret .=		'		'. Form::textarea($id,null,$opt) .'';
		$ret .=		($errors->has($id) ) ? '<span class="_errortxt">'.$errors->first($id).'</span>' : '' ;
		$ret .=		'	</div>';
		$ret .=		'</div>';
		
		return $ret;
	}	
	
	public static function _selField($errors,$id,$text,$options) {
		
		$opt	= array('id'=>$id, 'class'=>'form-control');
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10"><div class="custom-select">';
		
		array_unshift($options, '- SELECT -');
		$ret .=		'		'. Form::select($id, $options, NULL, $opt) .'';
		
		$ret .=		'	</div>';
		$ret .=		($errors->has($id) ) ? '<span class="_errortxt">'.$errors->first($id).'</span>' : '' ;
		$ret .=		'	</div></div>';
		$ret .=		'</div>';
		
		return $ret;
	}

	public static function _pwField($errors,$id,$text) {
	
		$opt	= array('id'=>$id, 'class'=>'form-control');
		$opt['autocomplete']	= 'off';
		//$opt['placeholder']	= $text;
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10">';
		$ret .=		'		'. Form::password($id,$opt) .'';
		$ret .=		($errors->has($id) ) ? '<span class="_errortxt">'.$errors->first($id).'</span>' : '' ;
		$ret .=		'	</div>';
		$ret .=		'</div>';
		
		return $ret;
	}	

	public static function _cbField($errors,$id,$text,$inline=0) {
	
		$opt	= array('id' => $id, 'class' => 'form-control');
		//$opt['autocomplete']	= 'off';
		//$opt['placeholder']	= $text;
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		if ($inline==0) {
			$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		} else {
			$ret .=		'	'. Form::label($id, '&nbsp;', array('class'=>'col-sm-2 control-label')) .'';
		};
		$ret .=		'	<div class="col-sm-10">';
		$ret .=		'		<div class="checkbox">';
		$ret .=		'		'. Form::checkbox($id) .'';
		
		if ($inline==1) $ret .=		' '. $text .'';
		
		$ret .=		'		</div>';
		$ret .=		($errors->has($id) ) ? '<span class="_errortxt">'.$errors->first($id).'</span>' : '' ;
		$ret .=		'	</div>';
		$ret .=		'</div>';
		
		return $ret;
	}	

	public static function _selCountries($errors,$id,$text) {
		
		$opt	= array('id'=>$id, 'class'=>'form-control');
		$_db	= DB::table('countries')->get();
		
		$options		= array();
		$options[]		= '- SELECT -';
		foreach ($_db as $db) { $options[$db->iso2]	= $db->short_name; };
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10"><div class="custom-select">';
		
		$ret .=		'		'. Form::select($id, $options, NULL, $opt) .'';
		
		$ret .=		'	</div>';
		$ret .=		($errors->has($id) ) ? '<span class="_errortxt">'.$errors->first($id).'</span>' : '' ;
		$ret .=		'	</div>';
		$ret .=		'</div>';
		
		return $ret;
	}

	public static function _selCurrency($errors,$id,$text) {
		
		$opt	= array('id'=>$id, 'class'=>'form-control');
		$_db	= Config::get('_system.currencies');
		
		$options		= array();
		//options[]		= '- SELECT -';
		foreach ($_db as $i=>$v) { $options[$v]	= $v; };
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10"><div class="custom-select">';
		
		$ret .=		'		'. Form::select($id, $options, NULL, $opt) .'';
		
		$ret .=		'	</div>';
		$ret .=		($errors->has($id) ) ? '<span class="_errortxt">'.$errors->first($id).'</span>' : '' ;
		$ret .=		'	</div>';
		$ret .=		'</div>';
		
		return $ret;
	}
	
	public static function _selProjector($errors,$id,$text) {
		
		$opt	= array('id'=>$id, 'class'=>'form-control');
		$_db	= Config::get('_system.projector');
		
		$options		= array();
		$options[]		= '- SELECT -';
		foreach ($_db as $i=>$v) { $options[$v]	= $v; };
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10"><div class="custom-select">';
		
		$ret .=		'		'. Form::select($id, $options, NULL, $opt) .'';
		
		$ret .=		'	</div>';
		$ret .=		($errors->has($id) ) ? '<span class="_errortxt">'.$errors->first($id).'</span>' : '' ;
		$ret .=		'	</div>';
	$ret .=		'</div>';
		
		return $ret;
	}
	
	public static function _selRepresentative($errors,$id,$text) {
		
		$opt	= array('id'=>$id, 'class'=>'form-control');
		$_db	= User::where('user_type','=','MANAGER')->get(array('id','email'));
		
		$options		= array();
		$options[]		= '- SELECT -';
		foreach ($_db as $v) {
			if ($v->UserContact->show_registration == 1) {
				$options[$v->id]	= $v->email; 
			};
		};
		
		$ret  =		'<div class="form-group ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-2 control-label')) .'';
		$ret .=		'	<div class="col-sm-10"><div class="custom-select">';
		
		$ret .=		'		'. Form::select($id, $options, NULL, $opt) .'';
		
		$ret .=		'	</div>';
		$ret .=		($errors->has($id) ) ? '<span class="_errortxt">'.$errors->first($id).'</span>' : '' ;
		$ret .=		'	</div>';
		$ret .=		'</div>';
		
		return $ret;
	}
	
	public static function _txtInlineField($errors,$id,$text,$disabled=NULL,$auto=NULL) {
	
		$opt	= array('id'=>$id, 'class'=>'cc-control');
		if (isset($auto) && $auto == 1)				$opt['autocomplete']	= 'off';
		if (isset($disabled) && $disabled == 1)		$opt['disabled']	= 'disabled';
		//$opt['placeholder']	= $text;
		
		$ret  =		'<div class="form-group _cc ';
		$ret .=		($errors->has($id) ) ? 'has-error has-feedback' : '' ;
		$ret .=		'">';
		$ret .=		'	'. Form::label($id, $text, array('class'=>'col-sm-1 control-label')) .'';
		$ret .=		'	'. Form::text($id,null,$opt) .'';
		//$ret .=		($errors->has($id) ) ? '<span class="_errortxt">'.$errors->first($id).'</span>' : '' ;
		$ret .=		'</div>';
		
		return $ret;
	}	
	
} 