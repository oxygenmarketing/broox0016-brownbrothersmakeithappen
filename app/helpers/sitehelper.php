<?php

class SiteHelper {
			
	public static function formatToUserCurrency($val) {
		$val		= str_replace(',','',$val);
		$denoms		= Config::get('_system.denominations');
		$currency	= Auth::user()->UserMeta->currency;
		return $denoms[$currency]. number_format((float)$val, 2, '.', '') .'<small>'.$currency.'</small>';
	}

	public static function formatToUserCurrencyShort($val) {
		$val		= str_replace(',','',$val);
		$denoms		= Config::get('_system.denominations');
		$currency	= Auth::user()->UserMeta->currency;
		return $denoms[$currency]. number_format((float)$val, 2, '.', '');
	}

	public static function checkAccessControl() {
		
		// CHECK FOR ACCESSIIBILTY : ACCOUNT LEVEL
		if (Auth::user()->active == 0) 
		return false ;
		//return View::make('_static.no_access');
		//return Redirect::action('StaticController@noaccess');
		
		// CHECK FOR ACCESSIIBILTY : PARENT LEVEL
		if (Auth::user()->parent_id != 0) {
			$_parent	= User::find(Auth::user()->parent_id);
			if ($_parent->active == 0) 
			return false;
			//return View::make('_static.no_access');
			//return Redirect::action('StaticController@noaccess');
		};
		
		return true;
		
	}

	public static function _getDateTime() {
		return date('Y-m-d H:i:s');
	}
	
	public static function array_unshift_assoc(&$arr, $key, $val) { 
		$arr = array_reverse($arr, true); 
		$arr[$key] = $val; 
		return array_reverse($arr, true); 
	}
	
	public static function getContent($name) {
		
		$promo = Promotion::all();
		$promotion = array();
		foreach($promo as $p) {
			$promotion[$p->option] = $p->value;
		}
		
		$contentReplace = array(
			'%%START_DATE_SHORT%%'			=> AdminHelper::formatShortDateOnly($promotion['start_date']),
			'%%START_DATE_SHORT_TEXT%%'		=> AdminHelper::formatLongDateOnly($promotion['start_date']),
			'%%START_DATE_LONG%%'			=> AdminHelper::formatLongDateFull($promotion['start_date']),
			'%%END_DATE_SHORT%%'			=> AdminHelper::formatShortDateOnly($promotion['end_date']),
			'%%END_DATE_SHORT_TEXT%%'		=> AdminHelper::formatLongDateOnly($promotion['end_date']),
			'%%END_DATE_LONG%%'				=> AdminHelper::formatLongDateFull($promotion['end_date']),
			'%%SUPPORTEMAIL%%'				=> $promotion['support_email']
		);
		
		//_e::prex($contentReplace);
		$content = Content::Where('name','=',$name)->first();
		$contentValue = $content->value;
		
		foreach($contentReplace as $y=>$x) {
			$contentValue = str_replace($y,$x,$contentValue);

			//_e::pre($y);
			
		}
		//_e::prex($contentValue);
		
		//_e::prex($contentValue);	
		return $contentValue;	
	}
	
	public static function getFbShare($result) {
		$fbAppId			= Promotion::Where('option','=','fb_app_id')->pluck('value');
		$fbName				= Promotion::Where('option','=','fb_name')->pluck('value');
		$fbLink				= Promotion::Where('option','=','fb_link')->pluck('value');
		$fbDescription		= Promotion::Where('option','=','fb_description')->pluck('value');
		$fbImgUrl			= Promotion::Where('option','=','fb_img_url')->pluck('value');
	
		$fbshare = "<script>
						!function(d,s,id){
							var js,fjs=d.getElementsByTagName(s)[0];
							if(!d.getElementById(id)){
								js=d.createElement(s);
								js.id=id;
								js.src='//connect.facebook.net/en_US/all.js#xfbml=1&appId=".$fbAppId."';
								fjs.parentNode.insertBefore(js,fjs);
							}
						}(document,'script','facebook-jssdk');
					
						post_to_wall	= function() {
							FB.ui({
								method			:	'stream.publish',
								message			:	'',
								target_id		:	'',
								actor_id		:	'',
								attachment		:	{
									name			:	'".$fbName."',
									caption			:	'".$fbLink."',
									description	:	'".$fbDescription."',
				
									href			:	'".$fbLink."',
									media			:	[{
											'type'	:	'image',
											'src'	:	'".$fbImgUrl."',
											'href'	:	'".$fbLink."'
									}]
								},
								action_links:[{
									'text'		:	'Enter now!',
									'href'		:	'".$fbLink."'
								}]
								
							}, function(response) {
								if (response && response.post_id) {
									//window.location.href = '".url('/') ."';
									_fbCallback(response.post_id);
								}
							});
						};
						
						$(function() {
							$('.shareBtn').bind('click', function(e){
								e.preventDefault();
								post_to_wall();
							});
						});
						
					</script>";
        
       return $fbshare;
	}
	
} 