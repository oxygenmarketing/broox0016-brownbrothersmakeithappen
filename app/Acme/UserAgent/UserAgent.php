<?php namespace Acme\UserAgent;

use Agent, _e, Session;

class UserAgent extends Agent {

	public function detect() {
		$browser	= Agent::browser();
		$platform	= Agent::platform();	
		$_agent		= array(
			'device'	=> $this->_device()
		,	'platform'	=> array(
				'name'		=> $platform
			,	'version'	=> Agent::version($platform)
			)
		,	'browser'	=> array(
				'name'		=> $browser
			,	'version'	=> Agent::version($browser)
			,	'engine'	=> Agent::device()
			)
		);
		//_e::prex($_agent);
		Session::put('_uagent', $_agent);
		
		return $_agent;
	}

	public function getDevice() {
		return $this->_device();	
	}
	
		private function _device() {
			return (Agent::isMobile()) ? ((Agent::isTablet()) ? 'tablet' : 'mobile') : 'desktop' ;
		}

}