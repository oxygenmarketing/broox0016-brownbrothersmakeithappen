<?php namespace Acme\UserAgent;

abstract class Agent {

	public abstract function detect();

	public abstract function getDevice();

}