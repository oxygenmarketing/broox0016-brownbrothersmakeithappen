<?php namespace Acme\Promotion;

use Config, _e, Stuart;

class Promotion extends Promo {

	public function dates() {
		return Config::get('_promotion.key_dates');
	}
	public function title() {
		
		$client = Stuart::Where('option','=','client')->pluck('value');
		$projectname = Stuart::Where('option','=','project_name')->pluck('value');
		return $client.' : '.$projectname;

	}

	public function googleanalytics() {
		
		$gacode = Stuart::Where('option','=','ga_code')->pluck('value');
		return $gacode;

	}

}