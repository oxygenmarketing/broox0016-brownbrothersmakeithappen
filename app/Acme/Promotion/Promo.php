<?php namespace Acme\Promotion;

abstract class Promo {

	public abstract function dates();
	public abstract function title();
	public abstract function googleanalytics();

}