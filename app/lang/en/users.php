<?php 

return array(
	//
	'text'		=> array(
		'login'		=> array(
			'sign_in'			=> 'Sign in',
			'username_email'	=> 'Username/Email',
			'password'			=> 'Password',
			'remember'			=> 'Remember me',
		),
		'forgot'	=> array(
			'forgot_password'	=> 'Forgot password',
			'email'				=> 'Email',
		),
		'reset'	=> array(
			'reset_password'	=> 'Reset password',
			'email'				=> 'Email',
			'password'			=> 'Password',
			'password_2'		=> 'Password confirm',
		),
	),
	//
	'button'		=> array(
		'login'		=> array(
			'sign_in'			=> 'Sign in',
			'forgot_password'	=> 'Forgot password',
		),
		'forgot'	=> array(
			'reset'				=> 'Send',
		),
		'reset'					=> 'Reset',
		'cancel'				=> 'Cancel',
	),
	//
	'message'		=> array(
		'login'		=> array(
			'fields_required'	=> 'All fields are required. Try again.',
			'auth_failed'		=> 'The details you provided could not be authenticated. Try again.',
		),
		'forgot'	=> array(
			'reset_error'		=> 'Please enter your email address.',
			'reset_sent'		=> 'An e-mail with the password reset instructions has been sent to your email.',
		),
	),
	//
);