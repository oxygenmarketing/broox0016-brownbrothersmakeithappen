<?php 

return array(

	"gui"		=> array(
		"header"	=> array(
			"logedinas"			=> "Logged in as",
			"logoff"				=> "sign out",
			"settings"				=> "settings",
		
		),
		"footer"	=> array(
			"copyright"			=> "&copy; Copyright ".date('Y').". All rights reserved.",
		
		),
		"route"	=> array(
			"home"				=> "Dashboard",
			"logoff"			=> "Sign Out",
			"logon"				=> "Sign In",
			
			"settings"			=> "Settings",
			"sitesettings"		=> "Site Settings",
			"sysinfo"			=> "System Info",
			"editprofile"		=> "Edit Profile",
		
		),
	),
	"titles"		=> array(
		"login"					=> "Please sign in",
	),
	"error"			=> array(
		"noaccess"				=> "You do not have sufficient access to view the page.",
	),

);
