<?php 

return array(

	'text'		=> array(
		'aa'		=> '',
		'bb'		=> '',
	),
	
	'button'		=> array(
		'save'		=> 'Save',
		'send'		=> 'Send',
		'submit'	=> 'Submit',
		'reset'		=> 'Reset',
		'cancel'	=> 'Cancel',
	),
	
	'message'		=> array(
		'aa'		=> '',
		'bb'		=> '',
	),

);