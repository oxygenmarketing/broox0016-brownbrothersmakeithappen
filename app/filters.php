<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

use Acme\UserAgent\UserAgent;

App::before(function($request)
{
	//
});

Route::filter('fbFrame', function() {
		$uAgent	= new UserAgent();
		$useragents = $uAgent->detect();
		Session::put('platform', $useragents['platform']['name']);
		//_e::prex(Session::get('platform'));
	if (App::environment() == 'production') {
		// lets detect how the user got here???
		$uAgent	= new UserAgent();
		if ($uAgent->getDevice() == 'desktop') {
			if (isset($_SERVER['HTTP_REFERER'])) { 
				
				//_e::pre($_SERVER['HTTP_REFERER']);        
				//_e::pre($_POST);
				//_e::pre($_GET);
				
				$subID	= Input::all();
				//_e::pre($subID);
				//_e::pre(Request::path());
				//_e::prex(Session::get('submissionID'));
				if ( Session::get('submissionID') ) {
					Session::put('sid', Session::get('submissionID'));
					Session::forget('submissionID');
					return Redirect::action('GalleryController@displayEntry');
				};
				  
			} else {
				
				return Redirect::to(Config::get('_system.fbUrl')); //.'?submissionID='.Session::get('submissionID'));
			};
		};
	};
});

App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	
	// DENY ALL GUEST ACCESS
	if (Auth::guest()) return (Request::ajax()) ? Response::make('Unauthorized', 401) : Redirect::guest('login') ;
	
	// DENY INACTIVE USERS
	if (Auth::user()->active == 0) {
		Auth::logout();
		return Redirect::to('/logout');
	};
	
	// DENY FRONTEND REGISTERED USERS FROM ACCESSING THE ADMIN
	if (Auth::user()->user_type == 'REGISTERED') {
		Auth::logout();
		return Redirect::to('/');
	};
	
});

Route::filter('testing', function()
{
	
	$userCode = Session::get('preview');
	$previewCode = Promotion::where('option','=','preview_code')->first();
	$redirect = Promotion::Where('option','=','redirect')->first();

	if($userCode == $previewCode->value) {
		
	}else{
		return Redirect::to('/');	
	}
});

Route::filter('promo', function()
{
	
	$startDate = Promotion::where('option','=','start_date')->first();
	$endDate = Promotion::where('option','=','end_date')->first();
	$postDate = Promotion::where('option','=','post_promo')->first();
	$preDate = Promotion::where('option','=','pre_promo')->first();
	$dateNow = date("Y-m-d H:i:s");
	$userCode = Session::get('preview');
	$previewCode = Promotion::where('option','=','preview_code')->first();
	$redirect = Promotion::Where('option','=','redirect')->first();
	if($dateNow >= $startDate->value && $dateNow < $endDate->value || $userCode == $previewCode->value) {
		
	}else if($dateNow >= $endDate->value && $dateNow < $postDate->value){
		return Redirect::to('/post');	
	}else if($dateNow > $postDate->value || $dateNow < $preDate->value){
		return Redirect::to($redirect->value);
	}else{
		return Redirect::to('/pre');	
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
