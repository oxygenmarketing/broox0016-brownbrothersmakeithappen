<?php

class Promotion extends Eloquent {
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'promotion';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $guarded = array('id','created_at','updated_at');
	
	
	public function user(){
		return $this->hasOne('User', 'value');
	}
	
	public function formElement() {
		return $this->hasOne('formElement', 	'value');
	}
	
	public function scopePre($query) {
		return $query->Where('option','=','pre_promo')->firstOrFail();
	}
	
	public function scopeStart($query) {
		return $query->Where('option','=','start_date')->firstOrFail();	
	}
	
	public function scopeFinish($query) {
		return $query->Where('option','=','end_date')->firstOrFail();	
	}
	
	public function scopePost($query) {
		return $query->Where('option','=','post_promo')->firstOrFail();	
	}
	
	public function scopeGacode($query) {
		return $query->Where('option','=','ga_code')->firstOrFail();	
	}
	
	public function scopeSms($query) {
		return $query->Where('option','=','sms_entry')->pluck('value');	
	}
	
	public function scopeType($query) {
		return $query->Where('option','=','has_prizes')->pluck('value');	
	}
	
	
	
}