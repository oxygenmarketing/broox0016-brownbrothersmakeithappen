<?php

/*

	--
	-- Database: `craox0001_cricket_australia`
	--

	-- --------------------------------------------------------

	--
	-- Table structure for table `entries`
	--

	CREATE TABLE `entries` (
	  `id` int(10) NOT NULL AUTO_INCREMENT,
	  `fb_id` int(50) NOT NULL,
	  `img_url` text NOT NULL,
	  `first_name` varchar(100) NOT NULL,
	  `last_name` varchar(100) NOT NULL,
	  `email` varchar(150) NOT NULL,
	  `mobile` varchar(11) NOT NULL,
	  `terms` tinyint(1) NOT NULL,
	  `proof` tinyint(1) NOT NULL,
	  `opt_in` tinyint(1) NOT NULL,
	  `active` int(1) NOT NULL,
	  `created_at` datetime NOT NULL,
	  `updated_at` datetime NOT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

*/


class PrizeDetails extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'prize_details';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $guarded = array('id','created_at','updated_at');


	public function PrizePool() {
		return $this->hasMany('PrizePool');
	}







}
