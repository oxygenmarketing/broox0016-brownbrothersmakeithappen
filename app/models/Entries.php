<?php

/*

	********** YOU MUST CREATE 2 TABLES EXACTLY THE SAME **********
	********** `entries` and `entries_testing` **********

	CREATE TABLE `entries` (
	  `id` int(10) NOT NULL,
	  `instantwin_id` int(10) NOT NULL DEFAULT '0',
	  `sms_id` int(10) NOT NULL,
	  `sms_message` varchar(255) NOT NULL,
	  		`firstname` varchar(100) NOT NULL,
			  `lastname` varchar(100) NOT NULL,
			  `address` varchar(255) NOT NULL,
			  `suburb` varchar(255) NOT NULL,
			  `state` varchar(255) NOT NULL,
			  `postcode` int(4) NOT NULL,
			  `email` varchar(255) NOT NULL,
			  `phone` varchar(11) NOT NULL,
			  `barcode1` int(4) NOT NULL,
			  `barcode2` int(4) NOT NULL,
				`barcode3` int(4) NOT NULL,
				`receipt` int(4) NOT NULL,
			  `terms` tinyint(1) NOT NULL,
			  `proof` tinyint(1) NOT NULL,
			  `opt_in` tinyint(1) NOT NULL DEFAULT '1',
	  `active` int(1) NOT NULL DEFAULT '1',
	  `edm_sent` int(10) NOT NULL,
	  `rawdata` text NOT NULL,
	  `created_at` datetime NOT NULL,
	  `updated_at` datetime NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `entries`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `entries`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

/////////////////////////////////////////////////////

	CREATE TABLE `entries_testing` (
	  `id` int(10) NOT NULL,
	  `instantwin_id` int(10) NOT NULL DEFAULT '0',
	  `sms_id` int(10) NOT NULL,
	  `sms_message` varchar(255) NOT NULL,
	  		`firstname` varchar(100) NOT NULL,
			  `lastname` varchar(100) NOT NULL,
			  `address` varchar(255) NOT NULL,
			  `suburb` varchar(255) NOT NULL,
			  `state` varchar(255) NOT NULL,
			  `postcode` int(4) NOT NULL,
			  `email` varchar(255) NOT NULL,
			  `phone` varchar(11) NOT NULL,
			  `barcode1` int(4) NOT NULL,
			  `barcode2` int(4) NOT NULL,
				`barcode3` int(4) NOT NULL,
				`receipt` int(4) NOT NULL,
			  `terms` tinyint(1) NOT NULL,
			  `proof` tinyint(1) NOT NULL,
			  `opt_in` tinyint(1) NOT NULL DEFAULT '1',
	  `active` int(1) NOT NULL DEFAULT '1',
	  `edm_sent` int(10) NOT NULL,
	  `rawdata` text NOT NULL,
	  `created_at` datetime NOT NULL,
	  `updated_at` datetime NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	ALTER TABLE `entries`
	  ADD PRIMARY KEY (`id`);
	ALTER TABLE `entries`
	  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

*/


class Entries extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table;



	public function __construct() {
		$promoStart = Promotion::Where('option','=','start_date')->pluck('value');
		$today = date("Y-m-d H:i:s");
		if($today >= $promoStart) {
			$this->table = 'entries';
		}else{
			$this->table = 'entries_testing';
		}

	}
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $guarded = array('id','created_at','updated_at');

	/*
	public function ProductCategories(){
		return $this->belongsTo('ProductCategories');
	}
	*/


	public function FormElement(){
		return $this->hasOne('FormElement');
	}

	public function InstantWin()
    {
        return $this->belongsTo('InstantWin', 'instantwin_id');
    }

	public function scopeState($query) {
		return $query->select(DB::raw('`state`,count(`state`) AS `count`'))
            ->groupBy('state')->get();
	}

}
