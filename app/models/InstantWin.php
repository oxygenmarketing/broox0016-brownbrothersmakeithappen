<?php




class InstantWin extends Eloquent {



	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $guarded = array('id','created_at','updated_at');

	public function PrizePool(){
		return $this->belongsTo('PrizePool');
	}

	public function __construct() {
		$promoStart = Promotion::Where('option','=','start_date')->pluck('value');
		$today = date("Y-m-d H:i:s");
		if($today >= $promoStart) {
			$this->table = 'instantwin';
		}else{
			$this->table = 'instantwin_testing';
		}

	}

	public function getPrizeName() {
		$prizeDetails = PrizePool::Where('id','=',$this->prize_pool_id)->pluck('prize_details_id');
		$prizeName	= PrizeDetails::where('id','=',$prizeDetails)->pluck('web_name');
		return $prizeName;
	}

	public function scopePrize($query, $id) {
		$pool = $query->Where('pcn','=',$id)->pluck('prize_pool_id');
		$prizeDetails = PrizePool::Where('id','=',$pool)->pluck('prize_details_id');
		$prizeName	= PrizeDetails::where('id','=',$prizeDetails)->pluck('web_name');
	//	_e::prex($prizeName);
		return $prizeName;
	}

	public function getWinningEntry() {
		$winningTime = Entries::Where('instantwin_id','=',$this->id)->pluck('created_at');
		return $winningTime;
	}

	public function Entries()
    {
        return $this->hasOne('Entries', 'instantwin_id');
    }

	public function scopePcn($query, $code) {
		return $query->Where('id','=',$code)->pluck('pcn');
	}
}
