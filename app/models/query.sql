    
    INSERT INTO 
    	entries (
        	`instantwin_id`,
            `fn_1`,
            `last_name`,
            `email`,
            `phone`,
            `dob`,
            `address`,
            `suburb`,
            `state`,
            `postcode`,
            `barcode1`,
            `barcode2`,
            `barcode3`,
            `entercaptcha`,
            `proof`,
            `terms`,
            `rawdata`
    ) 
            
            SELECT i.id AS instantwin_id,
            'Stuart',
            'Owen',
            'stuartowen0@hotmail.com',
            '0481159622',
            '1991-12-03',
            '26 Watling Grove',
            'Ferntree Gully',
            'VIC',
            '3651',
            '3615',
            '3513',
            '3513',
            'ZyH8db',
            '1',
            '1',
            '{"post":{"1":"Stuart","last_name":"Owen","email":"stuartowen0@hotmail.com","phone":"0481159622","dob":"1991-12-03","address":"26 Watling Grove","suburb":"Ferntree Gully","state":"VIC","postcode":"3651","barcode1":"3615","barcode2":"3513","barcode3":"3513","entercaptcha":"ZyH8db","proof":"1","terms":"1"},"agent":{"device":"desktop","platform":{"name":"OS X","version":"10_9_5"},"browser":{"name":"Chrome","version":"40.0.2214.91","engine":"WebKit"}}}' 
			
			FROM 
				`instantwin` i
			LEFT JOIN 
				`entries` e
			ON 
				e.instantwin_id = i.id
			WHERE 
				now() >= i.time_slot
			AND 
				e.instantwin_id IS NULL
				
				
			AND (
				SELECT 
					count(email) AS total
				FROM 
					`entries`
				WHERE 
					instantwin_id IS NOT NULL
				AND 
					email = 'stuartowen0@hotmail.com'
			) = 0
				
        
        
    ORDER BY 
    	i.time_slot ASC 
    LIMIT 
    	1