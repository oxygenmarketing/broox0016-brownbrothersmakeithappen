<?php


class SmsReceipt extends Eloquent {
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sms_receipt';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $guarded = array('id','created_at','updated_at');
	
	/*
	public function ProductCategories(){
		return $this->belongsTo('ProductCategories');
	}
	*/
	
	
	
}