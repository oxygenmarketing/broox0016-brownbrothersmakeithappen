<?php

class VerifyWinner extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = '_fnxAWTD7h9E6d91ff4eRbIi4ZzcIleFT';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $guarded = array('id','created_at','updated_at');

	/*
	public function ProductCategories(){
		return $this->belongsTo('ProductCategories');
	}
	*/

}
