<?php




class TestWinGen extends Eloquent {
	
	
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $guarded = array('id','created_at','updated_at');
	
	public function __construct() {
			$this->table = 'instantwin_testing';	
	}
	
	public function getPrizeName() {
		$prizeDetails = PrizePool::Where('id','=',$this->prize_pool_id)->pluck('prize_details_id');
		$prizeName	= PrizeDetails::where('id','=',$prizeDetails)->pluck('web_name');
		return $prizeName;
	}
	
	public function getWinningEntry() {
		$winningTime = Entries::Where('instantwin_id','=',$this->id)->pluck('created_at');
		return $winningTime;
	}
	
	public function Entries()
    {
        return $this->hasOne('Entries', 'instantwin_id');
    }
	
	
	
}