<?php

//use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
//use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/*
	 
	--
	-- Table structure for table `users`
	--
	
	CREATE TABLE `users` (
	  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
	  
		  `user_type` enum('','ADMIN','MANAGER','REGISTERED') NOT NULL DEFAULT '',
		  `username` varchar(255) NOT NULL,
		  `password` varchar(255) NOT NULL,
		  `email` varchar(255) NOT NULL,
		  
		  `activation` text,
		  `active` tinyint(1) NOT NULL,
		  `remember_token` varchar(100) NOT NULL,
		  
		  `been_notified` tinyint(1) NOT NULL,
		  `comment` varchar(255) NOT NULL,
		  
	  `created_at` datetime NOT NULL,
	  `updated_at` datetime NOT NULL,
	  
	  PRIMARY KEY (`id`),
	  UNIQUE KEY `email` (`email`),
	  UNIQUE KEY `username` (`username`)
	  
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;	
	
	--
	-- Dumping data for table `users`
	--
	
	INSERT INTO `users` (`id`, `user_type`, `username`, `password`, `email`, `activation`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'ADMIN', 'admin', '$2a$08$gRast8zsKRniL06JK/vR8uG7s95nqaCzD2Dsrwgi7FJxnpEwZQKZ2', 'admin@oxygenmarketing.com.au', NULL, 1, '8EqWtCkElYJcg0CBaxpazM4bpvBDW9pRU66mZjpYMHeg8QvWnYczzlRQT9PJ', '2013-06-04 00:00:01', '2014-06-04 01:05:17');
	
*/

class User extends Eloquent implements UserInterface, RemindableInterface {

	//use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'active', 'activation');

	protected $guarded = array('created_at','updated_at');

	//public function User() {
    //    return $this->belongsToMany('User');
    //}

	public function UserMeta() {
		return $this->hasOne('UserMeta');
	}

	public function UserContact() {
		return $this->hasOne('UserContact');
	}

	public function Products() {
		return $this->hasMany('Products'); 
	}

	public function UserProducts() {
		return $this->hasMany('UserProducts'); 
	}
	
		public static function allowProd($id,$pid) {
			$data	= UserProducts::where('user_id','=',$id)->where('products_id','=',$pid)->where('active','=',1)->count();
			return ($data > 0) ? TRUE : FALSE;
		}

		public static function getSelectedProductID($id,$pid) {
			$data	= UserProducts::where('user_id','=',$id)->where('products_id','=',$pid)->where('active','=',1)->first(array('id'));
			return ($data) ? $data->id : 'x';
		}
		
		/*
		public static function getProductPrice($id,$type,$pid) {
			$data	= UserProducts::where('user_id','=',$id)->where('products_id','=',$pid)->first();
			$_pp	= UserProductPrices::where('user_products_id','=',$data['products_id'])->where('type','=',$type)->first(array('value'));
			return ($_pp) ? $_pp['value'] : '';
		}
		*/
		
		public static function getProductPrice($id,$type,$pid) {
			$data	= UserProducts::where('user_id','=',$id)->where('products_id','=',$pid)->first();
			$_pp	= UserProductPrices::where('user_id','=',$id)->where('user_products_id','=',$data['id'])->where('type','=',$type)->first();
			return ($_pp) ? $_pp['value'] : '';
		}

		public static function getSubAccount($id) {
			$data	= User::where('user_type','=','REGISTERED')
						->where('parent_id','=',$id)
						->where('active','!=',9)
						->get();
			return ($data) ? $data : NULL;
		}
		
		public static function getViewType($id) {
			
			$_me	= User::find($id);
			$data	= array();
			$show_prices	= 1;
			
			if ($_me->parent_id > 0) { // I'M A CHILD ACCOUNT
			
				//_e::pre($_me->userMeta->payments_id); // payment type... 1=ON ACCOUNT
			
				$_parent	= User::where('user_type','=','REGISTERED')
						->where('id','=',$_me->parent_id)
						->first();
				
				if ($_me->userMeta->payments_id > 1) {
					$show_prices	= 1;
				} else {
					$show_prices	= $_parent->userMeta->show_prices;
				};
						
				$data	= array(
					'email_tmpl'	=> $_parent->userMeta->email_tmpl,
					'show_prices'	=> $show_prices,
				);
			} else { // I'M A HEAD ACCOUNT
				$data	= array(
					'email_tmpl'	=> $_me->userMeta->email_tmpl,
					//'show_prices'	=> $_me->userMeta->show_prices,
					'show_prices'	=> 1, // always show prices
				);
			};
			
			//_e::prex((object)$data);
			return (object)$data;
		}
		
		/*
		
		public static function getProductSalePrice($pid) {
			$data	= UserProducts::where('user_id','=',Auth::user()->id)->where('products_id','=',$pid)->where('active','=',1)->first();
			if (count($data) == 0) {
				// GET PARENT ACCOUNT...
				if (Auth::user()->parent_id == 0) {
					//die('BROKEN A PRICING');
					$_pp = User::_getDefaultPrice($pid);
				} else if (Auth::user()->parent_id > 0){
					// get products for parent account...
					$data	= UserProducts::where('user_id','=',Auth::user()->parent_id)->where('products_id','=',$pid)->where('active','=',1)->first();
				};				
			};
			$_pp	= UserProductPrices::where('user_products_id','=',$data['id'])->where('type','=',Auth::user()->UserMeta->currency)->first(array('value'));
			if ($_pp['value'] == '') $_pp = User::_getDefaultPrice($pid);
			return ($_pp) ? $_pp['value'] : '';
		}
		
			private static function _getDefaultPrice($pid) {
				return ProductPrices::find($pid);
			}
			
		*/
			
	public function Orders() {
		return $this->hasMany('Orders'); 
	}
	public function Promotion() {
		return $this->hasOne('Promotion'); 
	}

	public function OrdersFinal() {
		return $this->hasMany('OrdersFinal'); 
	}
			
	
	/*
	
	public function UserQuestion() {
		return $this->hasMany('UserQuestion');
	}

    public function Conversations1() {
        return $this->belongsToMany('Conversation', 'conversations', 'user_id', 'user_id_1');
    }

    public function Conversations2() {
        return $this->belongsToMany('Conversation', 'conversations', 'user_id', 'user_id_2');
    }
	
	*/

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier() {
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword() {
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail() {
		return $this->email;
	}
	
    // setters
	public function setPasswordAttribute($value) {
		$this->attributes['password'] = Hash::make($value);
	}

	public function getRememberToken() {
		return $this->remember_token;
	}

	public function setRememberToken($value) {
		$this->remember_token = $value;
	}

	public function getRememberTokenName() {
		return 'remember_token';
	}

}