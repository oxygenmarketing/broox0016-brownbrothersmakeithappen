

	select 
		cast(`broox0007_bb_bucketlist`.`entries`.`updated_at` as date) AS `date`,
		count(`broox0007_bb_bucketlist`.`entries`.`id`) AS `total`,
		floor(((to_days(cast(now() as date)) - to_days(cast(`broox0007_bb_bucketlist`.`entries`.`dob` as date))) / 365.25)) AS `age`
	from
		`broox0007_bb_bucketlist`.`entries`
	where
		(`broox0007_bb_bucketlist`.`entries`.`active` = 1)
	group by 
		floor(((to_days(cast(now() as date)) - to_days(cast(`broox0007_bb_bucketlist`.`entries`.`dob` as date))) / 365.25)),
		cast(`broox0007_bb_bucketlist`.`entries`.`updated_at` as date)
	order by
		cast(`broox0007_bb_bucketlist`.`entries`.`updated_at` as date)
	
	---------------------------------------------


	select 
		cast(`broox0007_bb_bucketlist`.`entries`.`updated_at` as date) AS dateA,
		count(`broox0007_bb_bucketlist`.`entries`.`id`) AS `total`,
		floor(((to_days(cast(now() as date)) - to_days(cast(`broox0007_bb_bucketlist`.`entries`.`dob` as date))) / 365.25)) AS ageA
	from
		`broox0007_bb_bucketlist`.`entries`
	where
		(`broox0007_bb_bucketlist`.`entries`.`active` = 1)
	group by 
		ageA ASC,
		dateA
	order by
		dateA
	

