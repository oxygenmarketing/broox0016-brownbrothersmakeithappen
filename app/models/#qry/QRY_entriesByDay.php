	
	select
		cast(`entries`.`created_at` as date) AS dateA,
		(
			select
				count(`entries`.`id`)
			from
				`entries`
			where
				(
					(`entries`.`sms_id` = 0)
					and
					(dateA = cast(`entries`.`created_at` as date))
				)
		) AS `web_total`,
		(
			select
				count(`entries`.`id`)
			from
				`entries`
			where
				(
					(`entries`.`sms_id` > 0)
					and
					(dateA = cast(`entries`.`created_at` as date))
				)
		) AS `sms_total`,
		count(`entries`.`id`) AS `total`
	from 
		`entries`
	group by
		dateA
