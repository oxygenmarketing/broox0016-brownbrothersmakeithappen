
	select count(distinct `broox0007_bb_bucketlist`.`entries`.`id`) AS `total`,str_to_date(concat(yearweek(`broox0007_bb_bucketlist`.`entries`.`updated_at`,0),' Sunday'),'%X%V %W') AS `weekBeg` from `broox0007_bb_bucketlist`.`entries` group by str_to_date(concat(yearweek(`broox0007_bb_bucketlist`.`entries`.`updated_at`,0),' Sunday'),'%X%V %W') order by str_to_date(concat(yearweek(`broox0007_bb_bucketlist`.`entries`.`updated_at`,0),' Sunday'),'%X%V %W')
		
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
	# YAY!!!!!
		
	select
		str_to_date(concat(yearweek(`entries`.`created_at`,0),' Sunday'),'%X%V %W') AS weekBeg,
		
		(
			select
				count(`entries`.`id`)
			from
				`entries`
			where
				`entries`.`sms_id` = 0
			AND
				`entries`.`created_at` BETWEEN 
					weekBeg
					AND 
					(str_to_date(concat(yearweek(`entries`.`created_at`,0),' Sunday'),'%X%V %W') + INTERVAL 1 WEEK)
		) AS `web_total`,
		(
			select
				count(`entries`.`id`)
			from
				`entries`
			where
				`entries`.`sms_id` > 0
			AND
				`entries`.`created_at` BETWEEN 
					weekBeg
					AND 
					(str_to_date(concat(yearweek(`entries`.`created_at`,0),' Sunday'),'%X%V %W') + INTERVAL 1 WEEK)
		) AS `sms_total`,
		
		count(distinct `entries`.`id`) AS `total`
	from
		`entries`
	group by
		weekBeg
	order by
		weekBeg



