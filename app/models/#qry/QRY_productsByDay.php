
		select 
			cast(`entries`.`created_at` AS date) AS dateA,
			`entries`.`barcode` AS barcodeA,
			`barcode`.`product_name` AS product,
			(
				select
					count(`entries`.`id`)
				from
					`entries`
				where
					(
						(`entries`.`sms_id` = 0)
					and
						(dateA = cast(`entries`.`created_at` AS date))
					and
						(barcodeA = `entries`.`barcode`)
					)
			) AS `web_total`,
			(
				select 
					count(`entries`.`id`)
				from
					`entries`
				where
					(
						(`entries`.`sms_id` > 0)
					and
						(dateA = cast(`entries`.`created_at` AS date))
					and
						(barcodeA = `entries`.`barcode`)
					)
			) AS `sms_total`,
			count(distinct `entries`.`id`) AS `total`
		from
			`entries`
		left join
			`barcode`
		on
			`barcode`.`full_barcode` LIKE (CONCAT('%',barcodeA,'%'))
		group by
			dateA,
			barcodeA
		order by
			dateA

------------------

		select 
			cast(`entries`.`created_at` AS date) AS dateA,
			`entries`.`state` AS stateA,
			(
				select
					count(`entries`.`id`)
				from
					`entries`
				where
					(
						(`entries`.`sms_id` = 0)
					and
						(dateA = cast(`entries`.`created_at` AS date))
					and
						(stateA = `entries`.`state`)
					)
			) AS `web_total`,
			(
				select 
					count(`entries`.`id`)
				from
					`entries`
				where
					(
						(`entries`.`sms_id` > 0)
					and
						(dateA = cast(`entries`.`created_at` AS date))
					and
						(stateA = `entries`.`state`)
					)
			) AS `sms_total`,
			count(distinct `entries`.`id`) AS `total`
		from
			`entries`
		group by
			dateA,
			stateA
		order by
			dateA
