
	select 
		str_to_date(concat(yearweek(`entries`.`created_at`,0),' Sunday'),'%X%V %W') AS weekBeg,
		
		(
			select
				count(distinct `entries`.`email`)
			from
				`entries`
			where
				`entries`.`sms_id` = 0
			AND
				`entries`.`created_at` BETWEEN 
					weekBeg
					AND 
					weekBeg + INTERVAL 1 WEEK
		) AS `web_total`,
		
		(
			select
				count(distinct `entries`.`phone`)
			from
				`entries`
			where
				`entries`.`sms_id` > 0
			AND
				`entries`.`created_at` BETWEEN 
					weekBeg
					AND 
					weekBeg + INTERVAL 1 WEEK
		) AS `sms_total`
		
	from
		`entries`
	group by
		weekBeg
	order by
		weekBeg
		
		
		
	
	-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	# YAY!!!!!
		
	select
		str_to_date(concat(yearweek(`entries`.`created_at`,0),' Sunday'),'%X%V %W') AS weekBeg,
		
		(
			select
				count(`entries`.`id`)
			from
				`entries`
			where
				`entries`.`sms_id` = 0
			AND
				`entries`.`created_at` BETWEEN 
					weekBeg
					AND 
					(str_to_date(concat(yearweek(`entries`.`created_at`,0),' Sunday'),'%X%V %W') + INTERVAL 1 WEEK)
		) AS `web_total`,
		(
			select
				count(`entries`.`id`)
			from
				`entries`
			where
				`entries`.`sms_id` > 0
			AND
				`entries`.`created_at` BETWEEN 
					weekBeg
					AND 
					(str_to_date(concat(yearweek(`entries`.`created_at`,0),' Sunday'),'%X%V %W') + INTERVAL 1 WEEK)
		) AS `sms_total`,
		
		count(distinct `entries`.`id`) AS `total`
	from
		`entries`
	group by
		weekBeg
	order by
		weekBeg


