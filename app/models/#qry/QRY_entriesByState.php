		
	select
		cast(`entries`.`created_at` as date) AS `date`,
		(
			select
				count(`entries`.`id`)
			from
				`entries`
			where
				(
					(`entries`.`sms_id` = 0) and (`date` = cast(`entries`.`created_at` as date))
				)
		) AS `web_total`,
		(
			select 
				count(`entries`.`id`)
			from
				`entries`
			where
				(
					(`entries`.`sms_id` > 0) and (`date` = cast(`entries`.`created_at` as date))
				)
		) AS `sms_total`
	from
		`entries`
	group by
		`date`
		
	order by
		`date` ASC
		
		

	
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	
	select
		count(distinct `entries`.`id`) AS `total`,
		cast(`entries`.`updated_at` as date) AS `date`,
		`entries`.`state` AS `state`
	from
		`entries`
	group by
		cast(`entries`.`updated_at` AS date),
		`entries`.`state`
	order by
		cast(`entries`.`updated_at` AS date)
	

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	WORKING VIEW QUERY - z_entriesbydaystate

	ALTER ALGORITHM=UNDEFINED
	DEFINER=`root`@`localhost`
	SQL SECURITY DEFINER VIEW `z_entriesbydaystate` AS
	
		select 
			cast(`entries`.`created_at` AS date) AS `dateA`,
			`entries`.`state` AS `stateA`,
			(
				select
					count(`entries`.`id`)
				from
					`entries`
				where
					(
						(`entries`.`sms_id` = 0)
					and
						(`dateA` = cast(`entries`.`created_at` AS date))
					and
						(`stateA` = `entries`.`state`)
					)
			) AS `web_total`,
			(
				select 
					count(`entries`.`id`)
				from
					`entries`
				where
					(
						(`entries`.`sms_id` > 0)
					and
						(`dateA` = cast(`entries`.`created_at` AS date))
					and
						(`stateA` = `entries`.`state`)
					)
			) AS `sms_total`,
			count(distinct `entries`.`id`) AS `total`
		from
			`entries`
		group by
			`dateA`,
			`stateA`
		order by
			`dateA`

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `z_entriesbydaystate` AS select cast(`entries`.`created_at` as date) AS `dateA`,`entries`.`state` AS `stateA`,(select count(`entries`.`id`) from `entries` where ((`entries`.`sms_id` = 0) and (`dateA` = cast(`entries`.`created_at` as date)) and (`entries`.`state` = `entries`.`state`))) AS `web_total`,(select count(`entries`.`id`) from `entries` where ((`entries`.`sms_id` > 0) and (`dateA` = cast(`entries`.`created_at` as date)) and (`entries`.`state` = `entries`.`state`))) AS `sms_total`,count(distinct `entries`.`id`) AS `total` from `entries` group by cast(`entries`.`created_at` as date),`entries`.`state` order by cast(`entries`.`created_at` as date)