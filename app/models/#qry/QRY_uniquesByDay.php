
	select 
		cast(`entries`.`updated_at` as date) AS dateA,
		
		(
			select
				count(distinct `entries`.`email`)
			from
				`entries`
			where
				(
					(`entries`.`sms_id` = 0)
					and
					(dateA = cast(`entries`.`created_at` as date))
				)
		) AS `web_total`,
		
		(
			select
				count(distinct `entries`.`phone`)
			from
				`entries`
			where
				(
					(`entries`.`sms_id` > 0)
					and
					(dateA = cast(`entries`.`created_at` as date))
				)
		) AS `sms_total`
		
	from
		`entries`
	group by
		dateA
	order by
		dateA
		
		
		
	
	-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	select 
		cast(`entries`.`created_at` as date) AS `date`,
		(select count(`entries`.`id`) from `entries` where ((`entries`.`sms_id` = 0) and (`date` = cast(`entries`.`created_at` as date)))) AS `web_total`,
		(select count(`entries`.`id`) from `entries` where ((`entries`.`sms_id` > 0) and (`date` = cast(`entries`.`created_at` as date)))) AS `sms_total`
	from 
		entries`
	group by
		cast(`entries`.`created_at` as date)
	