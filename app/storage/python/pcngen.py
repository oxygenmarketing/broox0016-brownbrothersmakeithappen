import random, sys

length = 6
seed = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789'
pcns = []

def gen(count):
  global pcns
  string = ''.join(random.choice(seed) for _ in range(length * count))
  codes = [string[i:i+6] for i in range(0, len(string), 6)]
  pcns.extend(codes)
  pcns = list(set(pcns))
  
def init():
  global pcns
  while len(pcns) < int(sys.argv[1]):
    gen(int(sys.argv[1]) - len(pcns))
  print ''.join(pcns)

if __name__ == '__main__':
  init()
