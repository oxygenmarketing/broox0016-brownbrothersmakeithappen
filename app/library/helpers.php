<?php

/*
*
*	Instantiate in AUTOLOAD->CLASSMAP within composer.json
*	&& include paths in START/GLOBAL.php
*
*/

class _e {

	protected $items;

	public static function pre($items) {
		echo '<pre>'; print_r($items); echo '</pre>';
	}

	public static function prex($items) {
		echo '<pre>'; print_r($items); echo '</pre>'; exit;
	}

	public static function dd($items) {
		die(print_r($items));
	}

	public static function xx($items) {
		exit(print_r($items));
	}

	public static function vd($items) {
		var_dump($items);
	}

	public static function sql() {
		$queries	= DB::getQueryLog();
		echo '<pre>'; print_r($queries); echo '</pre>';
	}

}
