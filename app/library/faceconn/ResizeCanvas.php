<?php
/**
 *  This class is used to resize application canvas height depending on content inside Facebook Canvas
 *  and Page applications.
 */
class ResizeCanvas {
    public function Render()
    {
        echo "<script type=\"text/javascript\">\n";
		
        echo "
		function ResizeIFrame() {
			if (graphApiInitialized != true) {
				setTimeout('ResizeIFrame()', 100);
				return;
			}
			FB.Canvas.setAutoGrow();
        }\n";
        echo "
				
			$(function() {
				ResizeIFrame();
			});
				
		\n";

        echo "</script>\n";
		
    }
}
