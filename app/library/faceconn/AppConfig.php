<?php
/**
 * This class is collection of methods for application's configuration.
 */

class AppConfig
{
    // Add STRING values from your Facebook application.
    //private static $ApiId 			= '1477759422491710';
    //private static $Secret			= '070d3ff08bb5781781c9da3b91283c21';

    private static $ApiId 			= '569786919832728';
    private static $Secret			= 'aad1a4c1f1ab94726a05489d601985f7';

    private static $AppName			= 'cricketaus_dressup';
    private static $AppCanvasUrl	= 'https://cadressupcompetition.com.au/';
	
    /**
     * Function gets the Facebook Application Id.
     * @return <string>
     */
    static function GetAppId() 
    {
		return self::$ApiId;
    }

    /**
     * Function gets the Facebook Application Secret Key.
     * @return <string>
     */
    static function GetSecret() 
    {
		return self::$Secret;
    }

    /**
     * Function gets the Facebook Application name
     * @return <string>
     */
    static function GetAppName()
    {
		return self::$AppName;
    }

     /**
     * Function gets the Facebook Application Canvas URL
     * @return <string>
     */
    static function GetAppCanvasUrl()
    {
		return self::$AppCanvasUrl;
    }

    /**
     * Function gets Facebook keys in array.
     * @return <array>
     */
    static function GetKeyArray()
    {
		return array('appId' => self::$ApiId, 'secret' => self::$Secret, 'cookie' => true);
    }
}
