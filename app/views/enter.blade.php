@extends('_tmpl.layout')

@section('_scripts')
@stop

@section('_styles')
@stop

@section('_body') class="home" @stop

@section('_container_main')
<div class="centered__inner">
	<main>
		<div class="container container--basic">
			<div class="grid">
				<div class="container__main">
					<form id="comp-form" method="post" action="enter" data-parsley-validate>
					<div class="container__main__item text--center">
						{{ SiteHelper::getContent('entry_header') }}


							<div class="container container--basic">

								@foreach($form as $_form)
									<div class="container__main col12">
										@foreach($_form as $field)
												{{ FormHelper::_formField($field->name,$errors) }}
										@endforeach
									</div>
								@endforeach

							</div>
							<div class="container container--basic">
									<div class="container__main col12 error-message" <?php if(count($errors) > 0) {?> style="display:block;" <?php }; ?>>
										<p>Please fill in all fields before submitting.</p>
										<?php
											if(count($errors) > 0) {
												echo "<ul>";
												foreach($errors->all() as $e) {
													echo "<li>".$e."</li>";
												}
												echo "</ul>";
											};
										?>
									</div>
							</div>
							<div class="container container--basic clearfix">
								<div class="container__main clearfix">


										<div class="submit">
											<input type="submit" class="button button__submit hover" tabindex="16" />
										</div>
									</div>

							</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</main>
</div>

@stop
