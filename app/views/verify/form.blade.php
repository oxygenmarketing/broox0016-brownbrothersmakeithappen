@extends('_tmpl.layout')

@section('_scripts')

@stop

@section('_styles')
@stop

@section('_body') class="form" @stop

@section('_container_main')
<div class="centered__inner">
	<main>
		<div class="container container--basic">
			<div class="grid">
				<div class="container__main">
          <form name="thisForm" method="POST" action="" class="entryForm">
            Email Address: <input type="text" name="email" id="email" /><br>
            Prize Claim Number: <input type="text" name="pcn" id="pcn" />
            <input type="submit" value="Submit" />
          </form>

				</div>
			</div>
		</div>
	</main>
</div>
@stop
