@extends('_tmpl.layout')

@section('_scripts')
@stop

@section('_styles')
@stop

@section('_body') class="home" @stop

@section('_container_main')

<div class="centered__inner home__animation">

	<main>
		<div class="container container--basic">
			<div class="grid">
				<div class="container__main">
					<div class="container__main__item text--center">

							<div class="status-message">
								<p class="heading1"><br> <br><br>Thank you for validating you prize!</p>
								<p class="heading6">Your prize will be sent out within 28 days!</p>
              </div>
        	</div>
				</div>
		</div>
	</div>
	</main>
</div>
@stop
