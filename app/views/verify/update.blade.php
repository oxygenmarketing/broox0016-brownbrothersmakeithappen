@extends('_tmpl.layout')

@section('_scripts')

@stop

@section('_styles')
@stop

@section('_body') class="home" @stop

@section('_container_main')
<div class="centered__inner">
  <main>
    <div class="container container--basic">
      <div class="grid">
        <div class="container__main">
          <form method="POST" action="#" accept-charset="UTF-8" id="comp-form" enctype="multipart/form-data">
          <!-- <form id="comp-form" method="post" action="form.html" data-parsley-validate> -->
          <div class="container__main__item text--center">

              <p class="semibold heading5">Please update your details below and upload a copy of your receipt and neck tag</p>
              <div class="container container--basic">
                <div class="container__main col12">
                  <div class="container__main__item col12__item text--left">
                    <label for="postal-address">Postal Address Line 1 *</label>
                    <input type="text" name="address_line_1" id="postal-address" tabindex="1" maxlength="100" value="{{ $data['address_line_1'] }}" required />
                  </div>
                </div>
                <div class="container__main col12">
                  <div class="container__main__item col12__item text--left">
                    <label for="postal-address">Postal Address Line 2 *</label>
                    <input type="text" name="address_line_2" id="postal-address" tabindex="2" maxlength="100" value="" />
                  </div>
                </div>
                  <div class="container__main col4">
                    <div class="container__main__item col4__item text--left">
                      <label for="suburb">Suburb *</label>
                      <input type="text" name="suburb" id="suburb" tabindex="3" maxlength="50" value="{{ $data['suburb'] }}" required />
                    </div>
                    <div class="container__main__item col4__item text--left">
                      <label for="postcode">Postcode *</label>
                      <input type="text" value="{{ $data['postcode'] }}" disabled />
                    </div>
                    <div class="container__main__item col4__item text--left">
                      <label for="state">State *</label>
                      <input type="text" value="{{ $data['state'] }}" disabled />
                    </div>
                  </div>
                  <div class="container__main col12">
                    <div class="container__main__item col12__item text--left">
                      {{ Form::label('receipt','Upload a photo of your receipt',array('id'=>'','class'=>'')) }}
                      <input type="file" name="receipt" multiple accept='image/*' />
                    </div>
                  </div>
                  <div class="container__main col12">
                    <div class="container__main__item col12__item text--left">
                      {{ Form::label('necktag','Upload a photo of your neck tag',array('id'=>'','class'=>'')) }}
                      <input type="file" name="necktag" multiple accept='image/*' />
                    </div>
                  </div>
              </div>
              <div class="container container--basic">
                  <div class="container__main col12 error-message">
                    <p>Please fill in all fields before submitting.</p>
                  </div>
              </div>
              <div class="container container--basic clearfix">
                <div class="container__main clearfix">


                    <div class="submit">
                      <input type="submit" class="button button__submit hover" tabindex="16" />
                    </div>
                  </div>

              </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </main>
</div>
@stop
