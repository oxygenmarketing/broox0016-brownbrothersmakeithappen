@extends('_tmpl.layout')

@section('_scripts')
@stop

@section('_styles')
@stop

@section('_body') class="home" @stop

@section('content')

<div class="col12 mainContent">
	<div class="whiteBlock tandc">
    <h1>Terms and Conditions</h1>
    <h2>Conditions of Entry for the &quot;2014-15 <em>Carlton Mid One Day International Series Facebook Best Dressed Competition</em>&rdquo; Promotion</h2>
<ol>
  <li>Information on how to enter and prize details form part of these conditions. Entry into the Promotion is deemed acceptance of these conditions.</li>
  <li>The promoter is Cricket Australia ABN 53 006 089 130 of 60 Jolimont Street, Jolimont, Victoria 3002 (<strong><em>Promoter</em></strong>).</li>
  <li>Entry to the Promotion is open to all Australian residents who attend a nominated Carlton Mid One Day International Series match in the summer of 2014-15. Directors, management and employees of the Promoter and their immediate families, suppliers, associated companies and agencies are not eligible to enter.</li>
  <li>The Promotion commences at 9.00am (AEDST) on Friday 14 November 2014 and closes at 5.00pm (AEDST) on 13 February, 2015</li>
  <li>To enter, an entrant must:    
    <ol type="a" style="list-style:lower-alpha;">
    <li>satisfy the criteria in clause 3 above;</li>
    <li>attend one of the nominated Carlton Mid One Day International Series matches set out in condition 7 below (each, a <strong><em>Match</em></strong>) in a fancy dress costume and have a photograph taken of them in costume at that Match;</li>
    <li>retain their entry ticket as proof of entry to the Match (which may be required by the Promoter in order to claim a prize); and</li>
    <li>use their Facebook account (if the entrant does not have one already the entrant will need to create one) to visit <a href="http://www.facebook.com/CricketAustralia">www.facebook.com/Cricketcomau,</a> and follow the links to the Promotion entry page, register as an entrant by completing the details required on that entry page, indicate their acceptance of these terms and upload the photograph of themselves in their fancy dress taken at that Match,</li>
    </ol>
  </li>

<blockquote>
  <p>(<strong><em>Eligible Entrant</em></strong>). There are different periods of entry depending on which Match the Eligible Entrant attends. See clause 7 below for details.</p>
</blockquote>
  <li>Eligible Entrants can submit only one entry per Match. The Promoter reserves the right to exclude any Eligible Entrant from the Promotion if the Promoter believes that an Eligible Entrant has tried to enter by using more than one user name or is otherwise engaging in any fraudulent or illegal activity (including participation that would be in breach of the law in their local jurisdiction), whether or not that Eligible Entrant would or might have won any prize but for such activity.</li>
  <li>The Carlton Mid One Day International Series Matches part of the Promotion and entry and voting periods are as follows:</li>

<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="175" valign="top"><p><strong>Match Details</strong></p></td>
    <td width="109" valign="top"><p><strong>Round opens</strong></p></td>
    <td width="109" valign="top"><p><strong>Round closes</strong></p></td>
    <td width="90" valign="top"><p><strong>Round Winner Announced</strong></p></td>
  </tr>
  <tr>
    <td width="175" valign="top"><p>Australia vs. South Africa <br>
      Friday 14 November, 2014, WACA </p></td>
    <td width="109" valign="top"><p>9am (AEDST) <br>
      Friday 14 November, 2014 </p></td>
    <td width="109" valign="top"><p>5pm (AEDST) <br>
      Friday 21 November, 2014 </p></td>
    <td width="90" valign="top"><p>12pm (AEDST) <br>
      Tuesday 25 November, 2014 </p></td>
  </tr>
  <tr>
    <td width="175" valign="top"><p>Australia vs. South Africa <br>
      Sunday 16 November, 2014, WACA </p></td>
    <td width="109" valign="top"><p>9am (AEDST) <br>
      Sunday 16 <br>
      November, 2014 </p></td>
    <td width="109" valign="top"><p>5pm (AEDST) <br>
      Sunday 23 <br>
      November, 2014 </p></td>
    <td width="90" valign="top"><p>12pm (AEDST) <br>
      Tuesday 25 <br>
      November, 2014 </p></td>
  </tr>
  <tr>
    <td width="175" valign="top"><p>Australia vs. South Africa <br>
      Wednesday 19 November, 2014, Manuka   Oval, Canberra </p></td>
    <td width="109" valign="top"><p>9am (AEDST) <br>
      Wednesday 19 <br>
      November, 2014 </p></td>
    <td width="109" valign="top"><p>5pm (AEDST) <br>
      Wednesday 26 <br>
      November, 2014 </p></td>
    <td width="90" valign="top"><p>12pm (AEDST) <br>
      Friday 28 <br>
      November, 2014 </p></td>
  </tr>
  <tr>
    <td width="175" valign="top"><p>Australia vs. South Africa <br>
      Friday 21 November, 2014, MCG </p></td>
    <td width="109" valign="top"><p>9am (AEDST) <br>
      Friday 21 November, <br>
      2014 </p></td>
    <td width="109" valign="top"><p>5pm (AEDST) <br>
      Friday 28 November, <br>
      2014 </p></td>
    <td width="90" valign="top"><p>12pm (AEDST) <br>
      Tuesday 2 <br>
      December, 2014 </p></td>
  </tr>
  <tr>
    <td width="175" valign="top"><p>Australia vs. South Africa <br>
      Sunday 23 November, 2014, SCG </p></td>
    <td width="109" valign="top"><p>9am (AEDST) <br>
      Sunday 23 <br>
      November, 2014 </p></td>
    <td width="109" valign="top"><p>5pm (AEDST) <br>
      Sunday 30 November <br>
      2014 </p></td>
    <td width="90" valign="top"><p>12pm (AEDST) <br>
      Tuesday 2 <br>
      December, 2014 </p></td>
  </tr>
  <tr>
    <td width="175" valign="top"><p>Australia vs. India <br>
      Friday 16 January, 2015, MCG </p></td>
    <td width="109" valign="top"><p>9am (AEDST) <br>
      Friday 16 January, <br>
      2015 </p></td>
    <td width="109" valign="top"><p>5pm (AEDST) <br>
      Friday 23 January, <br>
      2015 </p></td>
    <td width="90" valign="top"><p>12pm (AEDST) <br>
      Tuesday 27 <br>
      January, 2015 </p></td>
  </tr>
  <tr>
    <td width="175" valign="top"><p>Australia vs. England <br>
      Sunday 18 January, 2015, SCG </p></td>
    <td width="109" valign="top"><p>9am (AEDST) <br>
      Sunday 18 January, <br>
      2015 </p></td>
    <td width="109" valign="top"><p>5pm (AEDST) <br>
      Sunday 25 January, <br>
      2015 </p></td>
    <td width="90" valign="top"><p>12pm (AEDST) <br>
      Wednesday 28 <br>
      January, 2015 </p></td>
  </tr>
  <tr>
    <td width="175" valign="top"><p>England vs. India <br>
      Tuesday 20 January, 2015, Gabba </p></td>
    <td width="109" valign="top"><p>9am (AEDST) <br>
      Tuesday 20 January, <br>
      2015 </p></td>
    <td width="109" valign="top"><p>5pm (AEDST) <br>
      Tuesday 27 January, <br>
      2015 </p></td>
    <td width="90" valign="top"><p>12pm (AEDST) <br>
      Thursday 29 <br>
      January, 2015 </p></td>
  </tr>
  <tr>
    <td width="175" valign="top"><p>Australia vs. England <br>
      Friday 23 January, 2015, Blundstone Arena </p></td>
    <td width="109" valign="top"><p>9am (AEDST) <br>
      Friday 23 January, <br>
      2015 </p></td>
    <td width="109" valign="top"><p>5pm (AEDST) <br>
      Friday 30 January, <br>
      2015 </p></td>
    <td width="90" valign="top"><p>12pm (AEDST) <br>
      Tuesday 3 Feb, <br>
      2015 </p></td>
  </tr>
  <tr>
    <td width="175" valign="top"><p>Australia vs. India <br>
      Monday 26 January, 2015, SCG </p></td>
    <td width="109" valign="top"><p>9am (AEDST) <br>
      Monday 26 January, <br>
      2015 </p></td>
    <td width="109" valign="top"><p>5pm (AEDST) <br>
      Monday 2 February, <br>
      2015 </p></td>
    <td width="90" valign="top"><p>12pm (AEDST) <br>
      Wednesday 4 <br>
      February, 2015 </p></td>
  </tr>
  <tr>
    <td width="175" valign="top"><p>England vs. India <br>
      Friday 30 January, 2015, WACA </p></td>
    <td width="109" valign="top"><p>9am (AEDST) <br>
      Friday 30 January, <br>
      2015 </p></td>
    <td width="109" valign="top"><p>5pm (AEDST) <br>
      Friday 6 February, <br>
      2015 </p></td>
    <td width="90" valign="top"><p>12pm (AEDST) <br>
      Tuesday 10 <br>
      February, 2015 </p></td>
  </tr>
  <tr>
    <td width="175" valign="top"><p>FINAL <br>
      Sunday 1 February, 2015 WACA </p></td>
    <td width="109" valign="top"><p>9am (AEDST) <br>
      Sunday 1 February, <br>
      2015 </p></td>
    <td width="109" valign="top"><p>5pm (AEDST) <br>
      Sunday 8 February, <br>
      2015 </p></td>
    <td width="90" valign="top"><p>12pm (AEDST) <br>
      Tuesday 10 <br>
      February, 2015 </p></td>
  </tr>
</table>

  <li>The Promotion consists of two separate competition components:
    <ol style="list-style:lower-alpha;">
      <li>Best Dressed of the Match; and</li>
      <li>Best Dressed of the Summer.</li>
    </ol>
  </li>
  <li>By uploading the photograph, an Eligible Entrant warrants that they own the copyright in the uploaded photograph or are otherwise permitted to upload the photograph and that they have all necessary permissions, rights and/or licences covering their use of material included in such photograph. Each Eligible Entrant agrees to indemnify the Promoter, and to keep the Promoter fully indemnified, for any loss, cost, expense that may be incurred or sustained by the Promoter as a result of any breach by an Eligible Entrant of the warranty in this condition 9. Eligible Entrants must ensure that any photograph uploaded does not contain any obscene, offensive or defamatory content. If the Eligible Entrant is under 18, the Eligible Entrant must have the permission of their parent or guardian to participate in the Promotion and upload the photograph and their entry into the Promotion is deemed acceptance of these conditions by the parent or guardian.</li>
  <li>Cricket Australia reserves the right to vet all entry images and reserves the right to remove any entries at any time from the Promotion, and disqualify any Eligible Entrant&rsquo;s entry into the Promotion in its absolute discretion. Cricket Australia is not responsible for any entry images uploaded to the site and visitors to the site view those images at their own risk.</li>
  <li>Eligible Entrants may not enter the Promotion on behalf of a third party and may not directly promote a brand, store, design or other commercial property. Eligible Entrants must not receive any compensation or payment from a third party for entering the Promotion.</li>

<h2>Best Dressed of the Match </h2>

  <li>Twelve (12) &ldquo;Best Dressed of the Match&rdquo; AUD $1000 prizes will be awarded, being one for each of the Matches. If no entries are received in respect of a particular Match no prize will be awarded for that Match and the prize will not be carried over to any other Match.</li>
  <li>The winner of each of the &ldquo;Best Dressed of the Match&rdquo; prizes will be determined by a public vote conducted in respect of each Match via the voting function on  <a href="http://www.facebook.com/cricketcomau">www.facebook.com/cricketcomau</a><u> </u>for the Promotion. Voting in respect of each Match will take place during the respective voting period outlined in condition 7 above. At the end of the designated voting period for a Match, the Eligible Entrant whose image received the most votes during the voting period will be deemed the winner.</li>
  <li>There will be one winner per Match. In the event of a tied number of votes received by more than one Eligible Entrant, the Promoter will determine the winner based on the creativity and originality of the costumes of each tied Eligible Entrant. The prize will go to the most creative and original costume judged solely on the basis of merit.</li>
  <li>Each winner of a &ldquo;Best Dressed of the Match&rdquo; prize will receive a single payment of AUD$1,000 deposited into their designated bank account, provided <strong>they have notified Cricket Australia of their bank account details by 30 March 2015. </strong>Winners must hold an account in their name. The prize will not be transferred to a third party or another individual.</li>
  <li>Each winner will be notified by email on the date listed below and the name of all five winners will be published online at <a href="http://www.cricket.com.au/competitions">www.cricket.com.au/competitions</a><u></u>and <a href="http://www.facebook.com/cricketcomau"> www.facebook.com/cricketcomau.</a></li>

<h2>Best Dressed of the Summer</h2>

  <li>One (1) &ldquo;Best Dressed of the Summer&rdquo; prize will be awarded.</li>
  <li>The Eligible Entrants that receive the first and second highest number of votes in relation to each &ldquo;Best Dressed of the Match&rdquo; (in accordance with the applicable voting period) shall be entered into the &ldquo;Best Dressed of the Summer&rdquo; final. Where there is a tie such that more than two Eligible Entrants would qualify for the &ldquo;Best Dressed of the Summer&rdquo; vote, the Promoter  will determine the two (2) Eligible Entrants to progress to the vote in accordance with the method described in condition 14 above. In addition, the Promoter will select one (1) additional entry from each &ldquo;Best Dressed of the Match&rdquo; voting period, as a wildcard entry into the &ldquo;Best Dressed of the Summer&rdquo; final. The Promoter will select this wildcard entry on the basis of the creativity of the costume. There will be a total of thirty six (36) entries in the &ldquo;Best Dressed of the Summer&rdquo; final. The Eligible Entrants participating in the &ldquo;Best Dressed of the Summer&rdquo; final will be announced and published on  <a href="http://www.facebook.com/cricketcomau">www.facebook.com/cricketcomau</a> at 1.00pm AEDST on Wednesday 11 February 2015.</li>
  <li>There will be one (1) winner for &ldquo;Best Dressed of the Summer&rdquo;. The winner of the &ldquo;Best Dressed of the Summer&rdquo; prize will be determined by a panel of judges selected by the Promoter, who will determine the winner by selecting, in their opinion, the most creative and original costume judged solely on the basis of merit. Judging will take place on Friday 13 February 2015.</li>
  <li>The winner will be notified by telephone on Tuesday 17 February, 2015 and the name of the winner will be published  online at <a href="http://www.cricket.com.au/competitions">www.cricket.com.au/competitions</a> and <a href="http://www.facebook.com/cricketcomau"> www.facebook.com/cricketcomau </a>on Friday 20 February 2014.</li>
  <li>The &ldquo;Best Dressed of the Summer&rdquo; prize winner will receive a single payment of AUD$10,000 deposited directly into their designated bank account, <strong>provided they have notified Cricket Australia of their bank account details by 30 March 2015</strong>. Winners must hold an account in their name. The prize will not be transferred to a third party or another individual.</li>
  <li>The prize is not transferable, exchangeable or redeemable for cash. The winner will need to provide proof of attendance – e.g. their ticket stub – if requested by the Promoter.</li>
  <li>If the prize is not claimed within seven days of the date on which the original winner is selected, the Promoter will make a further determination of the winner, using the criteria set out in conditions 14or 19 above (as applicable).</li>
  <li>The winner of any subsequent prize determination (in accordance with condition 23) will be notified by phone on 13 March 2015.</li>
  <li>As a condition of accepting the prize, the winner must sign any legal documentation as and in the form required by the Promoter and/or prize suppliers in their absolute discretion, including but not limited to a legal release and indemnity form.</li>
  <li>The Promoter reserves the right to disqualify entries in the event of non-compliance with the conditions of entry or if, in the opinion of the Promoter, an Eligible Entrant tampers or interferes with any entry mechanism in any way (including but not limited to the use of crowdsourced votes). No automatic, programmed, robotic or similar means of voting are permitted under any circumstances. Any Eligible Entrant who does not comply with these
Terms and Conditions of Promotion, or who attempt to, or do, interfere with the voting process in any way will be disqualified and their votes will not be counted. The Promoter reserves the right, in its sole discretion, to cancel, terminate, modify, or suspend voting should any virus, bug, non-authorized human intervention, fraud or other causes beyond its control corrupt or affect the administration, security, fairness or proper conduct of the voting process. All decisions regarding the voting process shall be final and shall not be subject to challenge or appeal.</li>
  <li>The Promoter's decision in relation to any aspect of the Promotion and, in particular the selection of winners, is final and binding on each person who enters. No correspondence will be entered into. No responsibility is accepted for late, lost or misdirected entries.</li>
  <li>If for any reason this Promotion is not capable of running as planned, whether caused by infection by computer virus, bugs, tampering, unauthorised intervention, fraud, technical failures, or any other cause beyond the Promoter&rsquo;s control which corrupt or affect the administration, security, fairness or integrity of the promotion, the Promoter reserves the right in its sole discretion to cancel, terminate, modify or suspend the Promotion.</li>
  <li>To the extent permitted by law, the Promoter shall not be liable for any loss or damage whatsoever (including but not limited to direct or consequential loss) or personal injury suffered or sustained in connection with the prize[s]. The promoter accepts no responsibility for any tax liabilities that may arise from winning a prize.</li>
  <li>This promotion is in no way sponsored, endorsed or administered by, or associated with, Facebook. Each Eligible Entrant must understand and agree that they are providing their information to the Promoter and not to Facebook. Any questions, comments or complaints about this promotion must be directed to Promoter and not to Facebook. Facebook will not be liable for any loss or damage or personal injury, which is suffered or sustained by an entrant, as a result of participating in the promotion (including taking/use of a prize), except for any liability which cannot be excluded by law.</li>
  <li>By participating in this Promotion each Eligible Entrant agrees to the reasonable use of their name and the image submitted pursuant to this Promotion (and grants the Promoter a royalty-free, perpetual licence to use the image) in any manner as the Promoter may choose, including on its website and in promotional materials produced by or on behalf of the Promoter (which, for the avoidance of doubt, shall be without payment of any fee or remuneration to the relevant Eligible Entrant).</li>
  <li>By participating in this Promotion, Eligible Entrants understand and agree that Cricket Australia and State or Territory cricket associations, including the Big Bash League teams (together, &quot;Australian Cricket&quot;) may use and disclose the personal information provided by Eligible Entrants for the purpose of conducting the Promotion and for any of the purposes set out in Australian Cricket's Privacy Policy (available at <a href="http://www.cricket.com.au/privacy">www.cricket.com.au/privacy </a>or by emailing <a href="mailto:privacy@cricket.com.au">privacy@cricket.com.au</a>), which contains information about how Eligible Entrants may access and seek correction of their personal information or complain about a breach of their privacy, and how Australian Cricket will deal with that complaint. Australian Cricket may also disclose Eligible Entrants&rsquo; personal information to other parties, including Australian Cricket's third party service providers. From time to time, these third parties may be located (and so your personal information may be disclosed) overseas, including in India, the USA and the UK, and other countries from time to time.  Australian Cricket
may use Eligible Entrants&rsquo; personal information for direct marketing purposes, unless the Eligible Entrant opts out (which an Eligible Entrant can do at any time in accordance with Australian Cricket's Privacy Policy). An Eligible Entrant can request to access, update or correct any personal information we hold about them by writing to Cricket Australia's Privacy Officer at 60 Jolimont Street, Jolimont, VIC, 3002 or sending an email to  <a href="mailto:privacy@cricket.com.au">privacy@cricket.com.au</a>  </li>
</ol>
    </div>
</div>

@stop