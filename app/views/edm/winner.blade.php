<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
<style>
a:hover {text-decoration: underline !important;}td.contentblock h1 {color: #000 !important;font-size: 13px;line-height: 13px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;margin-top: 30px;margin-bottom: 20px;padding-top: 0;padding-bottom: 0;font-weight: bold;}td.contentblock h4 {color: #444444 !important;font-size: 16px;line-height: 24px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;margin-top: 0;margin-bottom: 10px;padding-top: 0;padding-bottom: 0;font-weight: normal;}td.contentblock h4 a {color: #444444;text-decoration: none;}td.contentblock p {color: #888888;font-size: 13px;line-height: 19px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;margin-top: 0;margin-bottom: 12px;padding-top: 0;padding-bottom: 0;font-weight: normal;}td.contentblock p a {color: #3ca7dd;text-decoration: none;}
@media only screen and (max-device-width: 480px) {
div[class="header"] {font-size: 16px !important;}table[class="table"], td[class="cell"] {width: 300px !important;}table[class="hide"], img[class="hide"], td[class="hide"] {display: none !important;}img[class="divider"] {height: 1px !important;}
}
</style>
</head>
<body bgcolor="#f2f2f2" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="-webkit-font-smoothing: antialiased;width:100% !important;background:#f2f2f2;-webkit-text-size-adjust:none;"><table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#e4e4e4"><tr><td bgcolor="#e4e4e4" width="100%"><table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="table"><tr><td width="600" class="cell"><img border="0" src="{{ URL::to('/') }}/images/hero.jpg" width="600" id="screenshot"> <img border="0" src="{{ URL::to('/') }}/images/spacer.gif" width="1" height="15" class="divider"><br><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td width="100%" bgcolor="#ffffff"><table width="100%" cellpadding="20" cellspacing="0" border="0"><tr><td bgcolor="#ffffff" class="contentblock">

<p>CONGRATULATIONS!</p>
<p>You&rsquo;re a provisional Instant Prize winner of a $250 Visa Gift Card. </p>
<p>Your Prize Claim Number is {{ $pcn }}.</p>
<p>To claim your prize, either upload a copy of purchase receipt, or send in a copy of your purchase receipt and neck cone with unique code for all entries you have submitted, to the Oxygen Support team on one of the following:</p>
<p>Upload your copy of purchase receipt here – <a href="{{ $url }}">{{ $url }}</a></p>
<p>Email: <br>
  <a href="mailto:support@brownbrotherspromo.com.au?subject=Support%20required%20for%20Big%20W%20Shopping%20Spree%20Promotion">support@brownbrotherspromo.com.au</a> <br>
  Att: Melissa<br>
  Please reference &ldquo;Brown Brothers Dream Holiday Promotion&rdquo; in subject line<br>
   <br>
  OR</p>
<p>Mail: <br>
  Oxygen Interactive Marketing<br>
  Brown Brothers Dream Holiday Promotion <br>
  117 Wellington St<br>
  St Kilda VIC 3182</p>
<p>Once your claim has been validated your prize will be sent out within 28 days. </p>
<p>If you have any questions regarding the promotion or prize please call 1300 737 728.</p>
<p>Regards,<br>
  Oxygen Marketing (on behalf of Brown Brothers)</p>

</td></tr></table></td></tr></table><img border="0" src="{{ URL::to('/') }}/images/spacer.gif" width="1" height="15" class="divider"><br></td></tr></table><img border="0" src="{{ URL::to('/') }}/images/spacer.gif" width="1" height="25" class="divider"></td></tr></table></body></html>
