<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Error in Brown Brothers Promo</title>
<style>
a:hover {text-decoration: underline !important;}td.contentblock h1 {color: #000 !important;font-size: 13px;line-height: 13px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;margin-top: 30px;margin-bottom: 20px;padding-top: 0;padding-bottom: 0;font-weight: bold;}td.contentblock h4 {color: #444444 !important;font-size: 16px;line-height: 24px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;margin-top: 0;margin-bottom: 10px;padding-top: 0;padding-bottom: 0;font-weight: normal;}td.contentblock h4 a {color: #444444;text-decoration: none;}td.contentblock p {color: #888888;font-size: 13px;line-height: 19px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;margin-top: 0;margin-bottom: 12px;padding-top: 0;padding-bottom: 0;font-weight: normal;}td.contentblock p a {color: #3ca7dd;text-decoration: none;}
@media only screen and (max-device-width: 480px) {
div[class="header"] {font-size: 16px !important;}table[class="table"], td[class="cell"] {width: 300px !important;}table[class="hide"], img[class="hide"], td[class="hide"] {display: none !important;}img[class="divider"] {height: 1px !important;}
}
</style>
</head>
<body bgcolor="#f2f2f2" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="-webkit-font-smoothing: antialiased;width:100% !important;background:#f2f2f2;-webkit-text-size-adjust:none;">
  <table width="100%" cellpadding="20" cellspacing="0" border="0"><tr><td bgcolor="#ffffff" class="contentblock">
ERROR DETAILS<br>
URL: {{ $url }}<br>
REFERRER: {{ $referrer }}<br>
<pre>{{ $exception }}</pre>

</td></tr></table></body></html>
