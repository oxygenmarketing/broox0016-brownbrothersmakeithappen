<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en">
<!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<title>{{{ Session::get('title') }}}</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<meta name="HandheldFriendly" content="true">
		<meta name="MobileOptimized" content="320">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">

		<link rel="shortcut icon" href="/favicon.ico">

		<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="stylesheet" href="<?php echo url(); ?>/css/main.css">
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="<?php echo url(); ?>/css/lt-ie9.css">
    <![endif]-->

		@yield('_styles')
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="<?php echo url(); ?>/scripts/vendor/modernizr.js"></script>

    <!--[if lt IE 9]>
    <script src="scripts/vendor/html5shiv.js"></script>
    <![endif]-->
		<script src="//use.typekit.net/rtu8pdb.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>

		@yield('_scripts')


    @if (App::environment() == 'production')
	    <script type="text/javascript">
	        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	        ga('create', '<?php echo Session::get('gacode'); ?>', 'auto');
	        ga('require', 'displayfeatures');
	        ga('send', 'pageview');
	    </script>
    @endif

	</head>
	<body @yield('_body')>
		<div id="background"></div>
		<div class="centered centered--fixed">
			@yield('_container_main')
		</div>



		@include('_tmpl.partials.footer')
		@yield('overlay')
	</body>
</html>
