<footer>
	 <div class="landmarks text--center"><img src="{{ url() }}/images/backgrounds/bg-landmarks-small.jpg" /></div>
	 <div class="container container--footer text--center">
		 <div class="container--footer--inner">
			 <div class="grid">
				 <nav>
					 <ul>
						 <li><a href="http://www.brownbrothers.com.au" target="_blank">Brown Brothers Website</a></li>
						 <li><a href="/BB_Make_It_Your_Way_Promo_TandCs_FINAL.pdf" target="_blank">Terms &amp; Conditions</a></li>
						 <li><a href="http://www.brownbrothers.com.au/privacy-policy/" target="_blank">Privacy Policy</a></li>
						 <li><a href="#" class="md-trigger" data-modal="modal-1">Help</a></li>
					 </ul>
				 </nav>
			 </div>
			 <div class="container text--center">
			 <div class="grid">
				 <div class="disclaimer">
<p>* Please retain product packaging, unique code &amp; store receipts for entry verification.Starts 14/03/16 at 12:01am AEDT; closes 19/06/16 at 11:59pm AEDT. Entry by SMS OR web at <a href="http://www.brownbrothers.com.au">www.brownbrothers.com.au.</a> For SMS, send text with the Unique Code, the last 5 digits of the barcode, full name and postcode to 19982222 (SMS cost 55c/msg sent). 1 entry/purchase. Max 5 entries/person/day. 50 x Instant Win prizes of a $250 Visa gift card (Inc. GST) awarded throughout the promotional period. Major Prize: 3 x $20,000 Flight Centre vouchers (Inc. GST). Total prize pool is valued at $72,500 (Inc. GST).
Major Prize Draw: 11:00am (AEDT) on 22/06/16. Major Prize winner published: The Australian 29/06/16. Promoter encourages responsible consumption of alcohol. Promoter: Promoter Brown Brothers Milawa Vineyard Pty Limited (ABN 56 005 349 235) of 239 Milawa-Bobinawarrah Road, Milawa VIC 3678. Telephone: 03 5720 5500. For details on how to enter and full Terms visit <a href="http://www.brownbrothers.com.au">www.brownbrothers.com.au.</a> SMS Service Provider is Oxygen Interactive Marketing. SMS Helpdesk: 1300 737 728 (standard call costs apply). Authorised under permit numbers: NSW Permit No. LTPS/15/08569, SA Permit No.T15/1964, ACT Permit No.TP/15/07968
</p>
				 </div>
			 </div>
		 </div>
	 </div>
	 </div>
 </footer>

 <div class="md-modal md-effect-1" id="modal-1">
	 <div class="md-content">
		 <a class="md-close"><span></span>Close</a>
		 <div class="md-content-inner">
			 <h3>Help</h3>
			 <p>Should you experience difficulties or have any questions, please contact us at:</p>
			 <p>Telephone: <strong>1300 737 728</strong></p>
			 <p>Email: <strong><a href="mailto:support@brownbrotherspromo.com.au">support@brownbrotherspromo.com.au</a></strong></p>
		 </div>
	 </div>
 </div>



<script>

	// this is important for IEs
	var polyfilter_scriptpath = '<?php echo url(); ?>/scripts/';
</script>

<script src="<?php echo url(); ?>/scripts/vendor.js"></script>

<script src="<?php echo url(); ?>/scripts/main.js"></script>
