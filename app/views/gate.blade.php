@extends('_tmpl.layout')

@section('_scripts')
<script src="/js/_facebook.js"></script>
<script>

	"use strict";
	var fbAppID = '<?php echo Config::get('_facebook.AppId'); ?>';

	function statusChangeCallback(response) {
		if (response.status === 'connected') {
			getUserInfo(response);
		};
	};

</script>
@stop

@section('_styles')
@stop

@section('content')

{? $frmID = '__f70L2Q6YiE1Hau6W3Oydl142JmW71SWK' ?}
{{ Msg::getHtml() }}


                    {{ Form::open(array('class'=>'form-horizontal','role'=>'form','id'=>$frmID,'name'=>$frmID,'method'=>'POST')) }}

                        <label for="dob_d">Date of Birth</label>
                            <input type="tel" name="dob_d" id="dob_d" class="first" placeholder="DD" maxlength="2">
                            <input type="tel" name="dob_m" id="dob_m" class="" placeholder="MM" maxlength="2">
                            <input type="tel" name="dob_y" id="dob_y" class="" placeholder="YYYY" maxlength="4">

                        <label for="postcode">Postcode</label>
                            <input type="tel" name="postcode" id="postcode" placeholder="POSTCODE*" class="first"  maxlength="4">

                        <button name="btnEnter" id="btnEnter" value="Validate" class="red">Validate</button>

                    {{ Form::close() }}

@include('_tmpl.partials.footer')


@stop
