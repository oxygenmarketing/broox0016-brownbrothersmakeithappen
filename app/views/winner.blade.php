<?php $fbAppId			= Promotion::Where('option','=','fb_app_id')->pluck('value');
$fbName				= Promotion::Where('option','=','fb_name')->pluck('value');
$fbLink				= Promotion::Where('option','=','fb_link')->pluck('value');
$fbDescription		= Promotion::Where('option','=','fb_description')->pluck('value');
$fbImgUrl			= Promotion::Where('option','=','fb_img_url')->pluck('value'); ?>
@extends('_tmpl.layout')

@section('_scripts')
<script>
				!function(d,s,id){
					var js,fjs=d.getElementsByTagName(s)[0];
					if(!d.getElementById(id)){
						js=d.createElement(s);
						js.id=id;
						js.src='//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo $fbAppId; ?>';
						fjs.parentNode.insertBefore(js,fjs);
					}
				}(document,'script','facebook-jssdk');

				post_to_wall	= function() {
					FB.ui({
						method			:	'stream.publish',
						message			:	'',
						target_id		:	'',
						actor_id		:	'',
						attachment		:	{
							name			:	'<?php echo $fbName; ?>',
							caption			:	'<?php echo $fbLink; ?>',
							description	:	"I've just entered the Brown Brothers Win 1 of 3 Dream Holidays competition worth $20,000– and Id take "+$('input#who').val()+" to go "+$('input#what').val()+" in "+$('input#where').val()+" If I WIN!",

							href			:	'<?php echo $fbLink; ?>',
							media			:	[{
									'type'	:	'image',
									'src'	:	'<?php echo $fbImgUrl; ?>',
									'href'	:	'<?php echo $fbLink; ?>'
							}]
						},
						action_links:[{
							'text'		:	'Enter now!',
							'href'		:	'<?php echo $fbLink; ?>'
						}]

					}, function(response) {
						if (response && response.post_id) {
							additional_entry();
							_fbCallback(response.post_id);

						}
					});
				};

				additional_entry	= function() {
					var data = {who: $('input#who').val(), what: $('input#what').val(), where: $('input#where').val()}
					$.ajax({
					  type: "POST",
					  url: 'fbshare',
					  data: data,
						success: function(data) {
							$(".formfields").remove();
						  $(".success").show();
			      }
					});
				}

				$(function() {
					$('form').submit(function(e){
						e.preventDefault();
						post_to_wall();

					});
				});

			</script>
@stop

@section('_styles')
@stop

@section('_body') class="form" @stop

@section('_container_main')

<div class="centered__inner home__animation">

	<main>
		<div class="container container--basic">
			<div class="grid">
				<div class="container__main">
					 <form id="comp-form" method="post" action="" data-parsley-validate>
					<div class="container__main__item text--center">

							<div class="status-message">
								<p class="heading1">CONGRATULATIONS!</p>
								<p class="heading3">You’re an Instant Winner of a $250 Visa Gift Card!</p>
								<p class="heading5">An email has been sent to your email address
provided with details on how to claim your prize.</p>
								<p class="heading3">Your Prize Claim number is:<br />{{ $pcn }}</p>
								<p class="heading5">Tell us who you would take on your dream destination and share on Facebook
								to receive 1 bonus entry into the major prize draw! </p>
							</div>

							<div class="container container--basic formfields">
									<div class="container__main col4">
										<div class="container__main__item col4__item text--left">
											<label for="who">Who would you take? *</label>
											<input type="text" id="who" tabindex="1" name="who" data-parsley-minlength="2" type="text" maxlength="255" required />
										</div>
										<div class="container__main__item col4__item text--left">
											<label for="what">What would you do? *</label>
											<input type="text" id="what" tabindex="2" name="what" data-parsley-minlength="2" type="text" maxlength="255" required />
										</div>
										<div class="container__main__item col4__item text--left">
											<label for="where">Where would you go? *</label>
											<input type="text" id="where" tabindex="2" name="where" data-parsley-minlength="2" type="text" maxlength="255" required />
										</div>

									</div>
							</div>
							<div class="container container--basic clearfix formfields">
								<div class="container__main clearfix">
									<p class="disclaimer">* Please complete all fields before clicking on the Share button</p>
										<div class="share submit">
											<input type="submit" class="button button__share hover" tabindex="16" />
										</div>
									</div>

							</div>

							<div class="container container--basic clearfix success" style="display:none;">
								<p class="heading3">Your second entry was successful! Good luck!</p>
							</div>

							<div class="goodluck-message">
								<span class="heading6">Remember to hold on to your proof of purchase.</span><br />
								<span class="heading6">Proof of purchase for each entry made will be required to claim your prize. </span>

							</div><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
					</div>
					</form>
				</div>
		</div>
	</div>
	</main>
</div>
@stop
