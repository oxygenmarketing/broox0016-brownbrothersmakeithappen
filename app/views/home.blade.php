@extends('_tmpl.layout')

@section('_scripts')
@stop

@section('_styles')
@stop

@section('_body') class="home" @stop


@section('_container_main')

<div class="centered__inner home__animation">

  <main>
    <div class="container container--basic">
        <div class="container__main">
          <div class="container__main__item text--center">
            <div class="win-title text--center">
              <img src="{{ url() }}/images/backgrounds/bg-win-title.png" />
            </div>

            <div class="grid clearfix"><div class="balloon"><img src="{{ url() }}/images/backgrounds/bg-balloon.gif" /></div></div>

            <div class="prizes-enter clearfix">
              <p>Purchase any specially marked Brown Brothers, Devil’s Corner, Tamar Ridge or Pirie product to enter</p>


              <div class="prizes">
                <img src="{{ url() }}/images/buttons/but-prizes.png" />
              </div>

              <div class="enter">
                <a href="enter" class="button button__enter hover pulse"><img src="{{ url() }}/images/buttons/but-enter.png" /></a>
              </div>
            </div>
          </div>
        </div>
    </div>
  </main>
</div>
@stop
