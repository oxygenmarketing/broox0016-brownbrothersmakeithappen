@extends('admin._tmpl.layout')

@section('_scripts')
@stop

@section('_styles')
@stop

{? $title = 'Content' ?}

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="active">{{ link_to_action('Admin_HomeController@index',Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">Manage Content</li>
  </ol>
@stop

@section('content')
  

	<h2 class="page-header">{{{ $title }}}
		<a href="{{ action('Admin_EntriesController@export') }}" role="button" class="btn btn-success pull-right btn-sm"><span class="glyphicon glyphicon glyphicon-import"></span> Export</a>
	</h2>
  
    <nav class="navbar navbar-default navbar-filter">
        <div class="row">
            <div class="col-lg-4">{{ AdminHelper::renderSearchForm() }}</div>       
            <div class="col-lg-8">
            {{ Form::open(array('url'=>'/admin/filter','id'=>'frmFilter','class'=>'navbar-form navbar-filter navbar-right','role'=>'filter')) }}
					<strong>Filter :</strong>
					{{ AdminHelper::filterList('active','Active',array('1'=>'Active','0'=>'Inactive')) }}
					<div class="btn-group">
						<button class="btn btn-default btn-sm" type="submit"><span class="glyphicon glyphicon-filter"></span></button>
					</div>
                {{ Form::close() }}</div>       
        </div>
    </nav><!-- /navbar-filter --> 
     
    <div class="table-responsive">
		<table class="table table-striped table-bordered table-fixed-head">
			<thead>
				<tr>
                <th width="1%" nowrap>{{ AdminHelper::drawHeaderRow('id','ID') }}</th>
                <?php /*
                @if (Auth::user()->user_type === 'ADMIN')
                <th nowrap width="140">{{ AdminHelper::drawHeaderRow('fbpic','FB') }}</th>
                @endif
                */ ?>
                <th nowrap>{{ AdminHelper::drawHeaderRow('title','Title') }}</th>
                <th nowrap>{{ AdminHelper::drawHeaderRow('name','Name') }}</th>
                <th nowrap width="60%">{{ AdminHelper::drawHeaderRow('value','Value') }}</th>
                <th nowrap width="4%">{{ AdminHelper::drawHeaderRow('updated_at','Last Saved') }}</th>
                <th width="1%" style="text-align:center;" nowrap>{{ AdminHelper::drawHeaderRow('active','Active') }}</th>
                <th width="1%" nowrap>Actions</th>
				</tr>  
			</thead>
			<tbody>
				@foreach ($_data->_d as $d)
				<tr>
                <td align="center">{{{ $d->id }}}</td>
                <?php /*
                @if (Auth::user()->user_type === 'ADMIN')
					<td align="center" nowrap>
						{? $fbUsr = json_decode($d->facebookUser) ?}
						<a href="{{ $fbUsr->link }}" target="_blank"><img src="{{ 'http://graph.facebook.com/'.$fbUsr->id.'/picture?width=120&height=120' }}"></a>
					</td>
                @endif
                */ ?>
                <td>{{{ $d->title }}}</td>
                <td>{{{ $d->name }}}</td>
                <td>{{{ str_limit($d->value, $limit = 100, $end = '.....') }}}</td>
                <?php /*
                <td><a href="https://www.facebook.com/profile.php?id={{{ $d->fbid }}}" target="_blank">{{{ $d->fbid }}}</a></td>
                */ ?>
                <td>{{ AdminHelper::formatLongDateTimeFull($d->created_at) }}</td>
                <td align="center">{{ AdminHelper::toggleState($d->id,$d->active,'publish','Active') }}</td>
                <td nowrap>
                    {{ AdminHelper::btnView($d->id,'Edit') }}
                    {{-- AdminHelper::btnDelete($d->id,1) --}}
                </td>
				</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<td colspan="99">Total {{ $_data->_total }} items.</td>
				</tr>
			</tfoot>
		</table>
    </div>
    {{ AdminHelper::btnAdd('Add New') }}

    <a href="{{ action('Admin_EntriesController@export') }}" role="button" class="btn btn-success pull-right btn-sm"><span class="glyphicon glyphicon glyphicon-import"></span> Export</a>
    {{ $_data->_d->links() }}

@stop