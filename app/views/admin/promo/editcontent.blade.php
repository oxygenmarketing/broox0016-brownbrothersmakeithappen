@extends('admin._tmpl.layout')

@section('_scripts')
<script>
  $(function() {
	  
		'use strict';
	
		$('._askQuestion2').bind('click', function(e) {
			var _q		= 'Are you sure you want to revoke this answer?';
			var rel		= $(this).attr('rel');
			if (confirm(_q)) { return true; };
			return false;
		});
	
  });
</script>
@stop

@section('_styles')
<style>
	._revoked {
		color:#FF0000;
		text-decoration:line-through;
	}
</style>
@stop

{? $title = 'Update Content' ?}

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="">{{ link_to_action('Admin_PromoController@index', Lang::get('crudadmin.gui.route.home'), NULL, array()) }}</li>
    <li class="active">{{ $title }}</li>
  </ol>
@stop

@section('content')
	<h2 class="page-header">{{{ $title }}}</h2>
	<div class="table-responsive">
 
    @if (isset($_data))
		{{ Form::model(	$_data,		array('method'=>'PATCH',	'route'=>array('admin.promotion.update', $_data['id']),	'class'=>'form-horizontal'	) ) }}
        {? $disabled = 1; ?}
	@else
		{{ Form::model(	new User,	array('method'=>'POST',	'route'=>array('admin.promotion.store'),					'class'=>'form-horizontal'	) ) }}
        {? $disabled = 0; ?}

	@endif
    
        {{-- _e::prex($_data) --}}
        	
                <div class="form-group">
                    {{ AdminFormHelper::_txtShortFieldEntry($errors,'title','Title', 'Title', $disabled) }}
                </div>
                <div class="form-group">
                    {{ AdminFormHelper::_txtShortFieldEntry($errors,'name','Name', 'Name', $disabled) }}
                </div>
                <div class="form-group">
                    {{ AdminFormHelper::_txtRichField($errors,'value','Value', 'Value') }}
                </div>
            
        	<hr>
            	<div class="form-group">
        			{{ AdminFormHelper::_btnAction('Save') }}
                </div>

		{{ Form::close() }}
	</div>
@stop