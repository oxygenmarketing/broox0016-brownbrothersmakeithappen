@extends('admin._tmpl.layout')

@section('_scripts')
    <script type="text/javascript" src="/packages/plupload-2.1.2/js/plupload.full.min.js"></script>
    <script type="text/javascript" src="/packages/plupload-2.1.2/js/jquery.plupload.queue/jquery.plupload.queue.min.js"></script>
           
	<script type="text/javascript">
        
        $(function() {
			
			'use strict';
			
			$('div#imageSelectBlock #console, #uploadfiles, #filelist').hide();

			var uploader = new plupload.Uploader({
				
				runtimes		: 'html5,flash,silverlight,html4',
				browse_button	: 'pickfiles', // you can pass in id...
				container		: document.getElementById('container'), // ... or DOM Element itself
				url				: "/admin/uploader",
				filters			: {
					max_file_size	: '10mb',
					prevent_duplicates : true,
					mime_types		: [
						{
							title		: "Image files",
							extensions	: "jpg,jpeg"
						},
					]
				},
		        chunk_size		: '1mb',
		        unique_names	: true,
		        resize			: {
					crop	: true,
					width	: 156,
					height	: 116,
					quality	: 80
				},
				flash_swf_url		: '/packages/plupload-2.1.2/js/Moxie.swf',			 
				silverlight_xap_url	: '/packages/plupload-2.1.2/js/Moxie.xap',
				init			: {
					PostInit		: function()			{
						
						var newTable	= new $('<table/>', { 'class' : 'table table-striped table-bordered' });
						newTable.append($('<thead/>')
							.append($('<tr/>')
								.append($('<th/>', {
										'colspan'	: 3,
										'text'		: 'New files',
									})
								)
							)
						).append($('<tbody/>'));
						
						$('#filelist').empty().append(newTable);
						
						$('#uploadfiles').bind('click', function(e) {
							e.preventDefault();
							uploader.start();
						});
					},
					FilesAdded		: function(up, files)	{
						$('div#imageSelectBlock #console').hide();
						$('#uploadfiles, #filelist').show();
						plupload.each(files, function(file) {
							$('#filelist tbody')
							.append($('<tr/>')
								.append($('<td/>', {
										'text'	: file.name + ' (' + plupload.formatSize(file.size) + ')',
									})
								)
								.append($('<td/>', {
										'id'	: file.id,
										'width'	: '80px',
										'align'	: 'right',
										'text'	: '0%',
									})
								)
							);
						});
					},
					UploadProgress	: function(up, file)	{
						$('#'+file.id).html('<strong>' + file.percent + '%</strong>');
					},
					FileUploaded	: function(up, file)	{
						//console.log(file);
						$('#filelist').append($('<input/>', {
							'name'	: 'fb_img_url',
							'type'	: 'hidden',
							'value'	: '<?php echo Config::get('_system.file_url'); ?>/'+file.target_name,
						}).append('\n'));
						
						$('#pickfiles').hide();
						$('#uploadfiles').hide();
					},
					Error			: function(up, err)		{
						$('div#imageSelectBlock #console').show();
						$('div#imageSelectBlock #console').empty().html("Error #" + err.code + ": " + err.message);
					}
				}
			});
			 
			uploader.init();
			
			$('.btn-delete-img').bind('click', function(e) {
				e.preventDefault();
				var $$		= $(this);
				var imgID	= $$.attr('rel');
				if ( $$.parent().parent().hasClass('strikeOut') ) {
					$$.parent().parent().removeClass('strikeOut');
					$('#imageSelectBlock').find('#deleteImage_'+imgID).remove();
				} else {
					$$.parent().parent().addClass('strikeOut');
					$('#imageSelectBlock').append($('<input/>', {
						'name'	: 'deleteImage[]',
						'id'	: 'deleteImage_'+imgID,
						'type'	: 'hidden',
						'value'	: imgID,
					}).append('\n'));
				};
			});
						
		});
		
    </script>


@stop

@section('_styles')
@stop

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="active">{{ link_to_action('Admin_HomeController@index',Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">Manage Promotion</li>    
  </ol>
@stop

@section('content')
  
{{-- _e::prex($promo); --}}

@if(Auth::user()->user_type == 'REGISTERED' )
	{? $attributes = 1; ?}
@else
	{? $attributes = 0; ?}
@endif

<ul class="nav nav-tabs" role="tablist">
  <li class="active"><a href="#main" role="tab" data-toggle="tab">Main Setup</a></li>
  <li><a href="#dates" role="tab" data-toggle="tab">Promo Dates</a></li>
  <li><a href="#entry" role="tab" data-toggle="tab">Entry Setup</a></li>
  <li><a href="#facebook" role="tab" data-toggle="tab">Facebook Setup</a></li>
</ul>


{{-- Form::open(array('url'=>'/admin/promotion/promo_update','id'=>'promoUpdate')) --}}
{{ Form::model(	$promo,		array('method'=>'POST',	'action'=>array('Admin_PromoController@promoUpdate'),	'class'=>'form-horizontal'	) ) }}
<div class="tab-content">
    <div class="tab-pane tab-pane-table active" id="main">
        {{ AdminFormHelper::_txtField($errors, 'client','Client Name','', $promo['client'], $attributes) }}
        {{ AdminFormHelper::_txtField($errors, 'project_number','Project Number','', $promo['project_number'], $attributes) }}
        {{ AdminFormHelper::_txtField($errors, 'project_name','Project Name','', $promo['project_name'], $attributes) }}
       
        <hr>
        <?php $previewURL = url().'/preview/'.$promo['preview_code']; ?>
        {{ AdminFormHelper::_selModelField($errors, 'account_manager','','users', array('id','username')) }}
        {{ AdminFormHelper::_txtShortField($errors, 'preview_code','Preview Code','') }}	
        {{ AdminFormHelper::_txtField($errors, 'preview_url','Preview URL','', $previewURL, 1) }}
        {{ AdminFormHelper::_txtShortField($errors, 'redirect','Redirect URL','') }}	
        <hr>
        {{ AdminFormHelper::_txtShortField($errors, 'ga_code','Google Analytics Code','') }}	
    </div>
    <div class="tab-pane tab-pane-table" id="dates">
        {{ AdminFormHelper::_datePicker($errors,'pre_promo', 'Pre Promo Date','', $promo['pre_promo'], 'true') }}
        {{ AdminFormHelper::_datePicker($errors,'start_date', 'Start Date','', $promo['start_date'], 'true') }}
        {{ AdminFormHelper::_datePicker($errors,'end_date', 'End Date','', $promo['end_date'], 'true') }}
        {{ AdminFormHelper::_datePicker($errors,'post_promo', 'Post Promo Date','', $promo['post_promo'], 'true') }}
	</div> 

    <div class="tab-pane tab-pane-table" id="entry">
        {{ AdminFormHelper::_txtField($errors, 'age_to_enter','Age to Enter','') }}
        <hr>
        {{ AdminFormHelper::_txtShortField($errors, 'number_of_entries','How many times can a person enter?','') }}
        {{ AdminFormHelper::_selField($errors, 'number_of_entries_per','Per',array('Day' => 'Day','Promo' => 'Promotion')) }}
        {{ AdminFormHelper::_selModelField($errors, 'limited_by','Limited By','formElement', array('name','title')) }}
    
    <hr>
        {{ AdminFormHelper::_cbField($errors,'unique_codes','Does this Promo have unique codes?', 1) }}
    <hr>
        {{ AdminFormHelper::_cbField($errors,'barcode_validation','Does this Promo require barcode validation?', 1) }}
    <hr>
        {{ AdminFormHelper::_cbField($errors,'sms_entry','Does this Promo have SMS entry?', 1) }}
    <hr>
        {{ AdminFormHelper::_cbField($errors,'has_prizes','Does this Promotion have Instant Win Prizes?', 1) }}
    @if(isset($promo['has_prizes']) && $promo['has_prizes'] == 1)
        {{ AdminFormHelper::_cbField($errors,'prizes_by_state','Are prizes state specific?',1) }}
        {{ AdminFormHelper::_txtShortField($errors, 'prizes_per_day','How many prizes per day?','') }}
        
        {{ AdminFormHelper::_txtShortField($errors, 'number_of_wins','How many times can a person win?','') }}
        {{ AdminFormHelper::_selField($errors, 'number_of_wins_per','Per',array('Day' => 'Day','Promo' => 'Promotion')) }}
        
    @endif
    </div>
    <div class="tab-pane tab-pane-table" id="facebook">
        {{ AdminFormHelper::_txtField($errors, 'fb_app_id','Facebook App ID','') }}
        {{ AdminFormHelper::_txtField($errors, 'fb_name','Facebook App Name','') }}
        {{ AdminFormHelper::_txtField($errors, 'fb_link','Facebook Return Link','') }}
        {{ AdminFormHelper::_txtField($errors, 'fb_description','Facebook Description','') }}
        <div id="imageSelectBlock">
            
                <pre id="console"></pre>
                
                
                <table class="table table-striped table-bordered" id="currentImages">
                    <thead>
                        <tr>
                            <th colspan="2">Current Facebook Share Tile</th>
                        </tr>  
                    </thead>
                    <tbody>
                    	
                        <tr>
                        
                            <td><img src="{{ $promo['fb_img_url'] }}" /></td>
                        </tr>

                    </tbody>
                </table>
                <div id="container">
                    <a id="pickfiles" href="javascript:;" class="btn btn-success">Select new image...</a>
                    <a id="uploadfiles" href="javascript:;" class="btn btn-default">Begin upload</a>
                </div>
                <div id="filelist" style="margin-top:20px;"></div>
            </div>
    </div>
</div>
    {{ AdminFormHelper::_btnAction('Save') }}  
	{{ Form::close() }}
@stop