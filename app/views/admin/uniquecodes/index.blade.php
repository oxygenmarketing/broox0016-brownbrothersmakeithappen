@extends('admin._tmpl.layout')

@section('_scripts')
@stop

@section('_styles')
@stop

{? $title = 'Unique Codes' ?}

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="">{{ link_to_action('Admin_HomeController@index', Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">{{ link_to_action('Admin_UniquecodeController@index', $title) }}</li>
  </ol>
@stop

@section('content')

	<h2 class="page-header">{{{ $title }}}</h2>
    <nav class="navbar navbar-default navbar-filter">
        <div class="row">
            <div class="col-lg-6">{{ AdminHelper::renderSearchForm() }}</div> 
            <div class="col-lg-6">
            {{ Form::open(array('url'=>'/admin/filter','id'=>'frmFilter','class'=>'navbar-form navbar-filter navbar-right','role'=>'filter')) }}
					<strong>Filter :</strong>
					{{ AdminHelper::filterList('active','Active',array('1'=>'Active','0'=>'Inactive')) }}
					<div class="btn-group">
						<button class="btn btn-default btn-sm" type="submit"><span class="glyphicon glyphicon-filter"></span></button>
					</div>
                {{ Form::close() }}</div>       
        </div>
    </nav><!-- /navbar-filter --> 
        <div class="table-responsive">
		<table class="table table-striped table-bordered table-fixed-head">
			<thead>
				<tr>
					<th width="1%" nowrap>{{ 'ID' }}</th>
					<th nowrap>{{ 'Code' }}</th>
					<th nowrap>{{ 'Date Updated' }}</th>
					<th width="1%" style="text-align:center;" nowrap>{{ AdminHelper::drawHeaderRow('active','Active') }}</th>
				</tr>  
			</thead>
			<tbody>
				@foreach ($_data->_d as $d)
				<tr>
					<td align="center">{{{ $d->id }}}</td>
					<td>{{{ $d->uniquecode }}}</td>
					<td>{{{ AdminHelper::formatLongDateTimeFull( $d->updated_at ) }}}</td>
					<?php /*<td align="center">{{ AdminHelper::toggleState($d->id,$d->active,'publish','Active') }}</td>*/ ?>
                	<td align="center">{{ AdminHelper::toggleState($d->id,$d->active,'publish','Active') }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
    </div>
    
    {{ $_data->_d->links() }}
  
@stop