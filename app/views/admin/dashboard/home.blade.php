@extends('admin._tmpl.layout')
<?php

	use Ghunti\HighchartsPHP\Highchart;
	use Ghunti\HighchartsPHP\HighchartJsExpr;

?>
@section('_scripts')
<script>

	var pre_date	= new Date('<?php echo $_options['promoDates']['preDate']; ?>');
	var start_date	= new Date('<?php echo $_options['promoDates']['startDate']; ?>');
	var end_date	= new Date('<?php echo $_options['promoDates']['endDate']; ?>');
	var post_date	= new Date('<?php echo $_options['promoDates']['postDate']; ?>');

    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour * 24;
    var timer;

    function showRemaining() {
		var now = new Date();

		if (pre_date > now) {
			var end = pre_date;
			var heading = 'The pre promo page will display in: ';
		} else if (start_date > now) {
			var end = start_date;
			var heading = 'The promotion will start in: ';
		} else if (end_date > now) {
			var end = end_date;
			var heading = 'The promotion will end in: ';
		} else {
			var end = post_date;
			var heading = 'The promotion will close in: ';
		};

		var distance = end - now;

		if (distance < 0) {
            clearInterval(timer);
            document.getElementById('countdown').innerHTML = 'EXPIRED!';
            return;
        };

        var days = Math.floor(distance / _day);
        var hours = Math.floor((distance % _day) / _hour);
        var minutes = Math.floor((distance % _hour) / _minute);
        var seconds = Math.floor((distance % _minute) / _second);

        document.getElementById('countdown').innerHTML = '<p style="margin:0; padding: 0;">' + heading + '</p>';
        document.getElementById('countdown').innerHTML += days + ' days ';
        document.getElementById('countdown').innerHTML += hours + 'hrs ';
        document.getElementById('countdown').innerHTML += minutes + 'mins ';
        document.getElementById('countdown').innerHTML += seconds + 'secs';
    }

    timer = setInterval(showRemaining, 1000);

</script>
@stop

@section('_styles')
@stop

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="active">{{ link_to_action('Admin_HomeController@index',Lang::get('crudadmin.gui.route.home')) }}</li>
  </ol>
@stop

@section('content')
<?php

	$chart = new Highchart();
	$chart->chart->renderTo			= "mainStatsContainer";
	$chart->chart->type 				= "bar";
	$chart->chart->height 			= "300";
	$chart->title->text				= "Today's Activity";
	$chart->subtitle->text 			= Config::get('_system.fbUrl');
	$chart->xAxis->categories			= array(date('d/m/Y',time()));
	$chart->xAxis->title->text		= null;
	$chart->yAxis->min					= 0;
	$chart->yAxis->title->text		= "Amount";
	$chart->yAxis->title->align		= "high";
	$chart->yAxis->stackLabels->enabled				= true;
	$chart->yAxis->stackLabels->style->color		= 'gray';
	$chart->yAxis->stackLabels->style->useHTML		= true;
	$chart->yAxis->stackLabels->formatter			= new HighchartJsExpr("function() { return '<em>['+ this.total +']</em>'; }");
	$chart->tooltip->formatter 		= new HighchartJsExpr("function() { return this.series.name +' Entries : '+ this.y +''; }");
	$chart->plotOptions->bar->dataLabels->enabled	= 1;
	$chart->plotOptions->series->stacking = 'normal';
	$chart->legend->layout			= "vertical";
	$chart->legend->align 			= "right";
	$chart->legend->verticalAlign		= "top";
	$chart->legend->x 					= -10;
	$chart->legend->y 					= 0;
	$chart->legend->floating 			= 1;
	$chart->legend->borderWidth 		= 1;
	$chart->legend->backgroundColor	= "#FFFFFF";
	$chart->legend->shadow			= 0;
	$chart->credits->enabled 			= 0;
	foreach ($_data['regTot']['data'] as $i=>$x) {
		$chart->series[] = array(
			'name'	=> $i,
			'data'	=> array((int)$x)
		);
	};

	//------------------------
	if(isset($_options['age'])) {
		$chartAgeRange = new Highchart();
		$chartAgeRange->chart->renderTo				= "AgeBreakdownByRangeContainer";
		$chartAgeRange->chart->plotBackgroundColor	= null;
		$chartAgeRange->chart->plotBorderWidth		= null;
		$chartAgeRange->chart->plotShadow	= false;
		$chartAgeRange->chart->height		= "400";
		$chartAgeRange->title->text			= $_options['age']['range']['name'];
		$chartAgeRange->tooltip->formatter	= new HighchartJsExpr( "function() { return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(0) +'%';}");
		$chartAgeRange->plotOptions->pie->allowPointSelect	= 1;
		$chartAgeRange->plotOptions->pie->cursor			= "pointer";
		$chartAgeRange->plotOptions->pie->dataLabels->enabled	= 1;
		$chartAgeRange->plotOptions->pie->dataLabels->color	= "#000000";
		$chartAgeRange->plotOptions->pie->dataLabels->connectorColor	= "#e4e4e4";
		$chartAgeRange->credits->enabled	= 0;
		$chartAgeRange->plotOptions->pie->dataLabels->formatter	= new HighchartJsExpr("function() {return '<b>'+ this.point.name +'</b>'; }");
		$chartAgeRange->series[0] = array(
			'type' => "pie",
			'name' => $_options['age']['range']['name']
		);
		foreach ($_options['age']['range']['data'] as $i=>$ageR) {
			$chartAgeRange->series[0]['data'][] = array(
				$i.' ['.$ageR.']',
				$ageR
			);
		};
	};

	//------------------------

	$chartStateAll = new Highchart();
	$chartStateAll->chart->renderTo				= "StateBreakdownAllContainer";
	$chartStateAll->chart->plotBackgroundColor	= null;
	$chartStateAll->chart->plotBorderWidth		= null;
	$chartStateAll->chart->plotShadow			= false;
	$chartStateAll->chart->height 				= "400";
	$chartStateAll->title->text					= $_data['state']['name'];
	$chartStateAll->tooltip->formatter	= new HighchartJsExpr( "function() { return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(0) +'%';}");
	$chartStateAll->plotOptions->pie->allowPointSelect	= 1;
	$chartStateAll->plotOptions->pie->cursor			= "pointer";
	$chartStateAll->plotOptions->pie->dataLabels->enabled	= 1;
	$chartStateAll->plotOptions->pie->dataLabels->color	= "#000000";
	$chartStateAll->plotOptions->pie->dataLabels->connectorColor	= "#e4e4e4";
	$chartStateAll->credits->enabled	= 0;
	$chartStateAll->plotOptions->pie->dataLabels->formatter	= new HighchartJsExpr("function() {return '<b>'+ this.point.name +'</b>'; }");
	$chartStateAll->series[0] = array(
		'type' => "pie",
		'name' => $_data['state']['name']
	);
	foreach ($_data['state']['data'] as $f=>$stateR) {
		//_e::prex($_data['state']['data']);
		$chartStateAll->series[0]['data'][] = array(
			$f.' ['.$stateR.']',
			(int)$stateR
		);
	};

	//------------------------
	if(isset($_data['products'])) {
		$chartProducts = new Highchart();
		$chartProducts->chart->renderTo				= "ProductBreakdownAllContainer";
		$chartProducts->chart->plotBackgroundColor	= null;
		$chartProducts->chart->plotBorderWidth		= null;
		$chartProducts->chart->plotShadow			= false;
		$chartProducts->chart->height 				= "500";
		$chartProducts->title->text					= $_data['products']['name'];
		$chartProducts->tooltip->formatter	= new HighchartJsExpr( "function() { return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(0) +'%';}");
		$chartProducts->plotOptions->pie->allowPointSelect	= 1;
		$chartProducts->plotOptions->pie->cursor			= "pointer";
		$chartProducts->plotOptions->pie->dataLabels->enabled	= 1;
		$chartProducts->plotOptions->pie->dataLabels->color	= "#000000";
		$chartProducts->plotOptions->pie->dataLabels->connectorColor	= "#e4e4e4";
		$chartProducts->credits->enabled	= 0;
		$chartProducts->plotOptions->pie->dataLabels->formatter	= new HighchartJsExpr("function() {return '<b>'+ this.point.name +'</b>'; }");
		$chartProducts->series[0] = array(
			'type' => "pie",
			'name' => $_data['products']['name']
		);
		foreach ($_data['products']['data'] as $f=>$prod) {
			//_e::prex($_data['products']['data']);
			$chartProducts->series[0]['data'][] = array(
				$f.' ['.$prod.']',
				(int)$prod
			);
		};

	};

?>
	@if($_options['gacode']->value == '' && Auth::user()->user_type != 'CLIENT')
        <div class="table-responsive jumbotron noGoogle col-md-12">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h1>Warning! No Google Analytics Code is detected. Go to Manage Promo to add one.</h1>
        </div>
    @endif
	<div id="countdown" class="jumbotron col-md-8" style="font-size:34px; text-align:center; padding:38px;"></div>
	<div class="table-responsive col-md-4">
    	<table class="table table-striped table-bordered">
        	<thead>
            	<tr>
                	<th colspan="2">Promotion Dates</th>
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<th width="40%">Pre Promo Date</th>
                    <td>{{ AdminHelper::formatLongDate($_options['promoDates']['preDate']) }}</td>
                </tr>
                <tr>
                    <th width="40%">Start Date</th>
                    <td>{{ AdminHelper::formatLongDate($_options['promoDates']['startDate']) }}</td>
                </tr>
                <tr>
                    <th width="40%">End Date</th>
                    <td>{{ AdminHelper::formatLongDate($_options['promoDates']['endDate']) }}</td>
                </tr>
                <tr>
                    <th width="40%">Post Promo Date</th>
                    <td>{{ AdminHelper::formatLongDate($_options['promoDates']['postDate']) }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="table-responsive col-md-6">
        <table class="table table-striped table-bordered">
        	<thead>
            	<tr>
                	<th colspan="2">Account Manager</th>
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<th width="40%">Name</th>
                    <td>{{ $_options['promoManager']['name'] }}</td>
                </tr>
                <tr>
                    <th width="40%">Position</th>
                    <td>{{ $_options['promoManager']['position'] }}</td>
                </tr>
                <tr>
                    <th width="40%">Phone</th>
                    <td>{{ $_options['promoManager']['phone'] }}</td>
                </tr>
                <tr>
                    <th width="40%">Email</th>
                    <td>{{ $_options['promoManager']['email'] }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="table-responsive col-md-6">
        <table class="table table-striped table-bordered">
        	<thead>
            	<tr>
                	<th colspan="2">Next 3 Prizes Available</th>
                </tr>
            </thead>

            <tbody>
            	<tr>
                	<th>Prize</th>
                    <th>Winning Time Slot</th>
                </tr>

                @foreach($_options['nextfive'] as $_nf)
                	<tr>
                    	<td>{{ $_nf['name'] }}</td>
                        <td>{{ $_nf['time_slot'] }}</td>
                    </tr>
                @endforeach
            </tbody>

        </table>
    </div>


    <div id="mainStatsContainer"></div>
	<hr/>
    <div class="row">
        <div style="width:50%; float:left;">
            <div id="AgeBreakdownByRangeContainer"></div>
        </div>
        <div style="width:50%; float:right;">
            <div id="StateBreakdownAllContainer"></div>
        </div>
    </div>
	<hr/>
    @if(isset($_data['products']))
        <div class="row">
            <div style="width:100%; float:left;">
                <div id="ProductBreakdownAllContainer"></div>
            </div>
        </div>
	@endif

	<?php $chart->printScripts(); ?>
    <script type="text/javascript"><?php echo $chart->render("todayAct"); ?></script>
    <?php if(isset($_options['age'])) { ?><script type="text/javascript"><?php echo $chartAgeRange->render("ageRange"); ?></script><?php }; ?>
    <script type="text/javascript"><?php echo $chartStateAll->render("stateAll"); ?></script>
    @if(isset($_data['products']))
	    <script type="text/javascript"><?php echo $chartProducts->render("products"); ?></script>
	@endif
@stop
