<?php $presenter = new Illuminate\Pagination\BootstrapPresenter($paginator); ?>
<div class="paginator">
<?php if ($paginator->getLastPage() > 1): ?>
	<ul class="pagination pagination-sm">
		<?php echo $presenter->render(); ?>
	</ul>
<?php endif; ?>
<?php 
	/*
	echo '<pre>';
		echo 'getCurrentPage '.$paginator->getCurrentPage().'<br>';
		echo 'getLastPage '.$paginator->getLastPage().'<br>';
		echo 'getPerPage '.$paginator->getPerPage().'<br>';
		echo 'getTotal '.$paginator->getTotal().'<br>';
		echo 'getFrom '.$paginator->getFrom().'<br>';
		echo 'getTo '.$paginator->getTo().'<br>';
		echo 'count '.$paginator->count().'<br>';
	echo '</pre>';
	*/
?>
	<div class="btn-group btn-group-sm dropup">
		<button type="button" class="btn btn-default disabled"><?php echo 'Page '.$paginator->getCurrentPage().' of '.$paginator->getLastPage(); ?></button>
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
			<span class="glyphicon glyphicon-list-alt"></span> Display 
		</button>
		<ul class="dropdown-menu" role="menu">
			<?php
				$perpage	= $paginator->getPerPage();
				$pageArr	= array(10,20,30,50,100);
				$x 			= 1;
				foreach ($pageArr as $_p) {
					echo '<li '.( ($perpage == $_p) ? ' class="active"' : '' ).'><a href="'.url(Request::url().'/?perPage='.$_p.'').'">'.$_p.' '.( ($x == 1) ? 'items per page' : '' ).'</a></li>';
					$x++;
				};
			?>
			<li class="divider"></li>
			<li <?php echo ($perpage == '99999999999999') ? ' class="active"' : ''; ?>><a href="<?php echo url(Request::url().'/?perPage=all'); ?>">View all</a></li>
		</ul>      
	</div>  
</div>    
