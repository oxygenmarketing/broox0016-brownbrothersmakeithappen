@extends('admin._tmpl.layout')

@section('_scripts')
    <script type="text/javascript">	
		
		"use strict";
		
		$(function() {
			//$('#password').CapsLockAlert();
		});	
		
		$(document).ready(function() {
			$('#btn_forgot_password').on('click', function(e){
				e.preventDefault();
				document.location.href='/admin/user/forgot';
			});
		});
		
    </script>
@stop

@section('_styles')
    
    <style>
		body {
		  padding-top: 40px;
		  padding-bottom: 40px;
		  background-color: #eee;
		}
		
		form {
		  max-width: 500px;
		  padding: 15px;
		  margin: 0 auto;
		}
			form .form-signin-heading,
			form .checkbox {
			  margin-bottom: 10px;
			}
			form .checkbox {
			  font-weight: normal;
			}
			form .form-control {
			  position: relative;
			  height: auto;
			  -webkit-box-sizing: border-box;
				 -moz-box-sizing: border-box;
					  box-sizing: border-box;
			  padding: 10px;
			  font-size: 16px;
			}
				form .form-control:focus {
				  z-index: 2;
				}
				form input[name="email"] {
				  margin-bottom: -1px;
				  border-bottom-right-radius: 0;
				  border-bottom-left-radius: 0;
				}
				form input[type="password"] {
				  margin-bottom: 10px;
				  border-top-left-radius: 0;
				  border-top-right-radius: 0;
				}
	</style>
    
@stop

@section('content')
    
    <div class="container">

        {{ Form::open(array(
            'action'			=>	'Admin_AuthController@login',
            'method'			=>	'post',
            'accept-charset'	=>	'UTF-8',
            'class'			=>	'form-signin',
        )) }}
        
            <h2 class="form-signin-heading">{{{ Lang::get('users.text.login.sign_in') }}}</h2>
            
            @include('admin._tmpl.partials.alerts')
            
            {{ Form::text('email', Input::old('email'), array(
                'id'				=>	'email',
                'placeholder'	=>	Lang::get('users.text.login.username_email'),
                'class'			=>	'form-control',
                'tabindex'		=>	'1',
                'autocomplete'	=>	'off',
                'autofocus'		=>	'autofocus',
                'required'		=>	'required',
            )) }}
            {{ Form::password('password', array(
                'id'				=>	'password',
                'placeholder'	=>	Lang::get('users.text.login.password'),
                'class'			=>	'form-control',
                'tabindex'		=>	'2',
                'autocomplete'	=>	'off',
                'required'		=>	'required',
            )) }}
            
            {{ Form::hidden('_token', Session::getToken()) }}
            {{ Form::submit(Lang::get('users.button.login.sign_in'), array(
                'class'			=>	'btn btn-lg btn-success btn-sm',
                'tabindex'		=>	'3',
            )) }}
            {{ Form::button(Lang::get('users.button.login.forgot_password'), array(
                'id'				=>	'btn_forgot_password',
                'class'			=>	'btn btn-lg btn-primary btn-sm',
                'tabindex'		=>	'4',
            )) }}
            
        {{ Form::close() }}

    </div> <!-- /container -->
        
@stop