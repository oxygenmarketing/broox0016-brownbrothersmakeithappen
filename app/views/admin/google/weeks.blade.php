@extends('admin._tmpl.layout')
<?php
	
	use Ghunti\HighchartsPHP\Highchart;
	use Ghunti\HighchartsPHP\HighchartJsExpr;
	
?>
@section('_scripts')
@stop

@section('_styles')
@stop

{? $title = 'Snapshots : '.$_data->title ?}

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="">{{ link_to_action('Admin_HomeController@index', Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">{{ link_to_action('Admin_SnapshotsController@regDays', $title) }}</li>
  </ol>
@stop

@section('content')
{{-- _e::prex($_data) --}}
	<h2 class="page-header">{{{ $title }}}
		<a href="{{ action('Admin_SnapshotsController@export', array($_data->export)) }}" role="button" class="btn btn-success pull-right btn-sm"><span class="glyphicon glyphicon glyphicon-import"></span> Export</a>
    </h2>
  
    
    
	<?php 
    
        $chart = new Highchart();
        
        $chart->chart->renderTo			= "mainStatsContainer";
        $chart->chart->type 				= "bar";
        //$chart->chart->height 			= "1200";
        $chart->title->text				= $_data->title;
        //$chart->subtitle->text 		= Config::get('_system.fbUrl');
        
        $chart->xAxis->categories		= NULL;
        $chart->xAxis->title->text		= NULL;
        
        $chart->yAxis->min				= 0;
        $chart->yAxis->title->text		= $_data->charttip;
        $chart->yAxis->title->align		= "high";
        
        $chart->tooltip->formatter 		= new HighchartJsExpr("function() { return '".$_data->charttip." : '+ this.y +''; }");
    
        $chart->plotOptions->bar->dataLabels->enabled	= 1;
        $chart->legend->enabled 			= 0;
        $chart->credits->enabled 		= false;
        
        $chart->legend->layout			= "vertical";
        $chart->legend->align 			= "right";
        $chart->legend->verticalAlign	= "top";
        $chart->legend->x 				= -10;
        $chart->legend->y 				= 0;
        $chart->legend->floating 		= 1;
        $chart->legend->borderWidth 		= 1;
        $chart->legend->backgroundColor	= "#FFFFFF";
        $chart->legend->shadow			= 0;
        $chart->credits->enabled 		= 0;
        
        //_e::pre($_data->_d);
        foreach ($_data->_d as $gData) {
            $chart->xAxis->categories[]	= $gData->weekBeg;
            //$chart->series[] = $gData;
            $chartseries[] = (int)$gData->total;
        };
        $chart->series[] = array(
            'name'	=> $_data->charttip,
            'data'	=> $chartseries
        );
        
    ?>
      
		<?php $chart->printScripts(); ?>
        <div id="mainStatsContainer"></div>
        <script type="text/javascript">
            jQuery.noConflict();
            <?php echo $chart->render("chart1"); ?>
        </script>
      
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="50%">{{ 'Week Beginning' }}</th>
                        <th width="50%" style="text-align:center;" nowrap>{{ 'Amount' }}</th>
                    </tr>  
                </thead>
                <tbody>
                	{? $cTotal = 0; ?}
                    @foreach ($_data->_d as $d)
                    <tr>
                        <td>{{ AdminHelper::formatLongDateFull($d->weekBeg).' - '.AdminHelper::formatLongDateFull(date('Y-m-d', strtotime($d->weekBeg . " +6days"))) }}</td>
                        <td align="center">{{{ $d->total }}}</td>
                        {? $cTotal = $cTotal + $d->total ?}
                    </tr>
                    @endforeach
                    
                    <tr>
                        <td>&nbsp;</td>
                        <td align="center"><strong>{{{ $cTotal }}}</strong></td>
                    </tr>
                    
                </tbody>
            </table>
        </div>    
       
        <p>
            <a href="{{ action('Admin_SnapshotsController@export', array($_data->export)) }}" role="button" class="btn btn-success pull-right btn-sm"><span class="glyphicon glyphicon glyphicon-import"></span> Export</a>
        </p>
        
@stop