@extends('admin._tmpl.layout')
<?php
	
	use Ghunti\HighchartsPHP\Highchart;
	use Ghunti\HighchartsPHP\HighchartJsExpr;
	
?>
@section('_scripts')
<script>
$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
</script>
@stop

@section('_styles')
@stop

{? $title = 'Snapshots : '.$_data->title ?}

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="">{{ link_to_action('Admin_HomeController@index', Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">{{ link_to_action('Admin_SnapshotsController@regDays', $title) }}</li>
  </ol>
@stop

@section('content')

	<h2 class="page-header">{{{ $title }}}
		<a href="{{ action('Admin_SnapshotsController@export', array($_data->export)) }}" role="button" class="btn btn-success pull-right btn-sm"><span class="glyphicon glyphicon glyphicon-import"></span> Export</a>
    </h2>
  
    <nav class="navbar navbar-default navbar-filter suspended">
        <div class="row">
            <div class="col-lg-12">  
				  {{ Form::open(array('url'=>'/admin/filter','id'=>'frmFilterDate','class'=>'navbar-form navbar-filter navbar-right','role'=>'filter')) }}
					<strong>Filter :</strong>
					{{ AdminHelper::filterDateRange($_data->filterby) }}
					<div class="btn-group">
						<button class="btn btn-default btn-sm" type="submit"><span class="glyphicon glyphicon-filter"></span></button>
						<?php /* <button class="btn btn-default btn-sm _reset" type="reset"><span class="glyphicon glyphicon-remove"></span></button> */ ?>
					</div>
                {{ Form::close() }}
            </div>       
        </div>
    </nav><!-- /navbar-filter --> 
    
	<?php 
    
        $chart = new Highchart();
        
        $chart->chart->renderTo			= "mainStatsContainer";
        $chart->chart->type 				= "bar";
        //$chart->chart->height 			= "1200";
        $chart->title->text				= $_data->title;
        //$chart->subtitle->text 		= Config::get('_system.fbUrl');
        
        $chart->xAxis->categories		= NULL;
        $chart->xAxis->title->text		= NULL;
        
        $chart->yAxis->min				= 0;
        $chart->yAxis->title->text		= $_data->charttip;
        $chart->yAxis->title->align		= "high";
        
        $chart->tooltip->formatter 		= new HighchartJsExpr("function() { return '".$_data->charttip." : '+ this.y +''; }");
    
        $chart->plotOptions->bar->dataLabels->enabled	= 1;
        $chart->legend->enabled 			= 0;
        $chart->credits->enabled 		= false;
        
        $chart->legend->layout			= "vertical";
        $chart->legend->align 			= "right";
        $chart->legend->verticalAlign	= "top";
        $chart->legend->x 				= -10;
        $chart->legend->y 				= 0;
        $chart->legend->floating 		= 1;
        $chart->legend->borderWidth 		= 1;
        $chart->legend->backgroundColor	= "#FFFFFF";
        $chart->legend->shadow			= 0;
        $chart->credits->enabled 		= 0;
        
        //_e::pre($_data->_d);
        foreach ($_data->_d as $gData) {
            $chart->xAxis->categories[]	= $gData['name'];
            //$chart->series[] = $gData;
            $chartseries[] = $gData['data'][0];
        };
        $chart->series[] = array(
            'name'	=> $_data->charttip,
            'data'	=> $chartseries
        );
        
    ?>
    
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li class="active"><a href="#graph" role="tab" data-toggle="tab">Graph view</a></li>
      <li><a href="#table" role="tab" data-toggle="tab">Table view</a></li>
    </ul>
    
    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane active" id="graph">
      
		<?php $chart->printScripts(); ?>
        <div id="mainStatsContainer"></div>
        <script type="text/javascript">
            jQuery.noConflict();
            <?php echo $chart->render("chart1"); ?>
        </script>
      
      </div>
      <div class="tab-pane tab-pane-table" id="table">
      
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="50%">{{ 'Date' }}</th>
                        <th width="50%" style="text-align:center;" nowrap>{{ 'Amount' }}</th>
                    </tr>  
                </thead>
                <tbody>
                	{? $cTotal = 0; ?}
                    @foreach ($_data->_d as $d)
                    <tr>
                        <td>{{{ AdminHelper::formatLongDateFull($d['date']) }}}</td>
                        <td align="center">{{{ $d['data'][0] }}}</td>
                        {? $cTotal = $cTotal + $d['data'][0] ?}
                    </tr>
                    @endforeach
                    
                    <tr>
                        <td>&nbsp;</td>
                        <td align="center"><strong>{{{ $cTotal }}}</strong></td>
                    </tr>
                    
                </tbody>
            </table>
        </div>      
      
      </div>
    </div>

	<br>
   <p class="clearfix">
		<a href="{{ action('Admin_SnapshotsController@export', array($_data->export)) }}" role="button" class="btn btn-success pull-right btn-sm"><span class="glyphicon glyphicon glyphicon-import"></span> Export</a>
   </p>
    
@stop