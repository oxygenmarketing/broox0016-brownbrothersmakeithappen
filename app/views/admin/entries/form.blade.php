@extends('admin._tmpl.layout')

@section('_scripts')
<script>
  $(function() {
	  
		'use strict';
	
		$('._askQuestion2').bind('click', function(e) {
			var _q		= 'Are you sure you want to revoke this answer?';
			var rel		= $(this).attr('rel');
			if (confirm(_q)) { return true; };
			return false;
		});
	
  });
</script>
@stop

@section('_styles')
<style>
	._revoked {
		color:#FF0000;
		text-decoration:line-through;
	}
</style>
@stop

{? $title = 'Entry' ?}

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="">{{ link_to_action('Admin_HomeController@index', Lang::get('crudadmin.gui.route.home'), NULL, array()) }}</li>
    <li class="active">{{ $title }}</li>
  </ol>
@stop

@section('content')
	<h2 class="page-header">{{{ $title }}}</h2>
	<div class="table-responsive">
		{{ Form::model(	$_data,		array('method'=>'PATCH',	'route'=>array('admin.entries.update', $_data['id']),	'class'=>'form-horizontal'	) ) }}
            @foreach($_data as $i=>$v)
                {{ AdminFormHelper::_txtShortFieldEntry(null,$i, AdminFormHelper::formatLabel($i),$v,0) }}
        	@endforeach
            {{ AdminFormHelper::_btnAction('Save') }}
        	<hr>

		{{ Form::close() }}
	</div>
@stop