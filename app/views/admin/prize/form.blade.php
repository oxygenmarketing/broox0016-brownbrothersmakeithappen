@extends('admin._tmpl.layout')

@section('_scripts')
@stop

@section('_styles')
@stop

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="active">{{ link_to_action('Admin_HomeController@index',Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">{{ link_to_action('Admin_PrizesController@index','Manage Prizes') }}</li>
    <li class="active">Create Prize</li>
  </ol>
@stop

@section('content')

{{ Form::model(	new PrizeDetails,		array('method'=>'POST',	'action'=>array('Admin_PrizesController@saveDetails'),	'class'=>'form-horizontal'	) ) }}


	{{ AdminFormHelper::_txtField($errors, 'name','Internal Name','', '', '') }}
	{{ AdminFormHelper::_txtField($errors, 'description','Full Description','', '', '') }}
	{{ AdminFormHelper::_txtField($errors, 'web_name','Web Description','', '', '') }}
	{{ AdminFormHelper::_txtField($errors, 'sms_name','SMS Description','Optional', '', '') }}
<hr>
	{{ AdminFormHelper::_txtField($errors, 'link','Prize URL','Optional', '', '') }}
	{{ AdminFormHelper::_txtField($errors, 'price','Prize Value','Optional', '', '') }}
	{{ AdminFormHelper::_txtField($errors, 'img_url','Image Url','Optional', '', '') }}
    
    {{ AdminFormHelper::_btnAction('Save') }}  
	{{ Form::close() }}

@stop