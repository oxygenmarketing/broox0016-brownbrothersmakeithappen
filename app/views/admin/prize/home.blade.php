@extends('admin._tmpl.layout')

@section('_scripts')
@stop

@section('_styles')
@stop

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="active">{{ link_to_action('Admin_HomeController@index',Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">Manage Prizes</li>    
    
  </ol>
@stop

@section('content')
@if(Session::get('prizeWarning') != '')
	<div class="alert alert-danger" role="alert">{{ Session::get('prizeWarning') }}</div>
@endif
<h2 class="page-header">Current list of Prizes<a href="{{ action('Admin_PrizesController@generatePrizes') }}" role="button" class="btn btn-success pull-right btn-sm">Generate Prizes</a></h2>
<p>Click on the description to view pool breakdown</p>
@if(isset($_iw) && $_iw == 1)
	{{ $_iw }}
@endif
<div class="table-responsive">
		<table class="table table-striped table-bordered table-fixed-head">
        <thead>
            <tr>
            	<th>Name</th>
                <th>Price</th>
                <th width="10%" nowrap>Total Quantity</th>
                <th width="1%" nowrap>Active</th>
                <th width="1%" nowrap>Actions</th>
            </tr>
        </thead>
        <tbody>
        {? $_total = 0 ?}
        @foreach($_data as $_d)
       	            
            	<tr>
                	<td>{{ $_d['name'] }}</td>
                	<td>{{ $_d['price'] }}</td>
                	<td>{{ $_d['quantity'] }}</td>
       		        <td align="center">{{ AdminHelper::toggleState($_d['id'],$_d['active'],'publish','Active') }}</td>
                    <td nowrap>
                        {{ AdminHelper::btnView('pools/'.$_d['id'],'View') }}
                    </td>
                </tr>
                {? $_total = $_total + $_d['quantity'] ?}
        @endforeach
        
		</tbody>
        <tfoot>
        	<tr>
        	<td colspan="2" align="right">
            Total Prizes:
            </td>
        	<td>
            {{ $_total }}
            </td>
            </tr>
        </tfoot>
    </table>
@stop