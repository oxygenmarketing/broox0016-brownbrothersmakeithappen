@extends('admin._tmpl.layout')

@section('_scripts')
@stop

@section('_styles')
@stop

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="active">{{ link_to_action('Admin_HomeController@index',Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">{{ link_to_action('Admin_PrizesController@index','Manage Prizes') }}</li>
    <li class="active">Create Prize Pool</li>    
  </ol>
@stop

@section('content')

<div class="table-responsive">

{{ Form::model(	new PrizePool,		array('method'=>'POST',	'action'=>array('Admin_PrizesController@savePrizePools'),	'class'=>'form-horizontal'	) ) }}

	{{ AdminFormHelper::_selModelField($errors, 'prize_details_id','Select Prize Details','prize_details', array('id','web_name')) }}
	{{ AdminFormHelper::_txtField($errors, 'quantity','Quantity of Prizes in Pool','', '', '') }}
	@if($prizesByState == 1)
    {{ AdminFormHelper::_selMultiModelField($errors, 'state_id','Select Prize Details','states', array('id','state')) }}
    @endif
<hr>
    {{ AdminFormHelper::_datePicker($errors,'start_date', 'Prize Pool Open','', $start_date) }}
    {{ AdminFormHelper::_datePicker($errors,'end_date', 'Prize Pool Closed','', $end_date) }}

    {{ AdminFormHelper::_btnAction('Save') }}  
	{{ Form::close() }}
</div>

@stop