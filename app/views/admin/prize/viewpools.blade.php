@extends('admin._tmpl.layout')

@section('_scripts')
@stop

@section('_styles')
@stop

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="active">{{ link_to_action('Admin_HomeController@index',Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">{{ link_to_action('Admin_PrizesController@index','Prizes Home') }}</li>
  </ol>
@stop

@section('content')

<h2>Breakdown of Prize</h2>
<div class="table-responsive">
{{ Form::model(	$_form,		array('method'=>'PATCH',	'action'=>array('admin.prizes.update', $_data['id']),	'class'=>'form-horizontal'	) ) }}
{{-- Form::model(	$_data,		array('method'=>'PATCH',	'route'=>array('admin.privileges.update', $_data->id),	'class'=>'form-horizontal'	) ) --}}


	{{ AdminFormHelper::_txtField($errors, 'name','Name (Internal use only)','') }}
	{{ AdminFormHelper::_txtBoxField($errors, 'description','Full Description','') }}
	{{ AdminFormHelper::_txtField($errors, 'web_name','Web Description','') }}
	{{ AdminFormHelper::_txtField($errors, 'sms_name','SMS Description','Optional') }}
<hr>
	{{ AdminFormHelper::_txtField($errors, 'link','Prize URL','Optional') }}
	{{ AdminFormHelper::_txtField($errors, 'price','Prize Value','Optional') }}
	{{ AdminFormHelper::_txtField($errors, 'img_url','Image Url','Optional') }}
    
    {{ AdminFormHelper::_btnAction('Save') }}  
	{{ Form::close() }}
    </div>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-fixed-head">
        <thead>
            <tr>
            	<th width="1%">Quantity</th>
                <th>Start Date</th>
                <th>End Date</th>
                @if($prizesByState == 1)
                	<th width="1%">ACT</th>
                    <th width="1%">NSW</th>
                    <th width="1%">NT</th>
                    <th width="1%">QLD</th>
                    <th width="1%">SA</th>
                    <th width="1%">TAS</th>
                    <th width="1%">VIC</th>
                    <th width="1%">WA</th>
                @endif
                <th width="1%">Active</th>
            </tr>
        </thead>
        <tbody>
        @foreach($_data['data'] as $_dd)
            <tr>
                <td>{{ $_dd['quantity'] }}</td>
                <td>{{ AdminHelper::formatLongDateTimeFull($_dd['start_date']) }}</td>
                <td>{{ AdminHelper::formatLongDateTimeFull($_dd['end_date']) }}</td>
                @if($prizesByState == 1)
                	@foreach($stateprize[$_dd['id']] as $st=>$_sp)
                    	<td>
                        @if($_sp == 1)
                            {{ AdminHelper::editStateSelection($_data['id'],$_dd['id'],$st,'delete') }}
                        @else
                        	{{ AdminHelper::editStateSelection($_data['id'],$_dd['id'],$st,'insert') }}
                        @endif
                        </td>
                    @endforeach
                @endif
               <td align="center">{{ AdminHelper::toggleState($_dd['id'],$_dd['active'],'publish','Active') }}</td>
            </tr>
        @endforeach
		</tbody>
    </table>
    </div>
@stop