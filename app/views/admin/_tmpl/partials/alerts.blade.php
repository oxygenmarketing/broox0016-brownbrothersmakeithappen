
    <style>
		.alert {
			padding:10px;
			position:relative;
			/*border:1px solid #f0f;*/
		};
	</style>

    @if ( Session::has('error') )
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right:0;">&times;</button>
            {{{ Session::get('error') }}}
        </div>
    @endif
    @if ( Session::has('notice') )
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right:0;">&times;</button>
            {{{ Session::get('notice') }}}
        </div>
    @endif
    @if ( Session::has('status') )
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right:0;">&times;</button>
            {{{ Session::get('status') }}}
        </div>
    @endif
    
