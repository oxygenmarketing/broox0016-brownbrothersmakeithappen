
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
      
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ url('/admin') }}">{{{ Session::get('title') }}}</a>
        </div>
        
        <div class="navbar-collapse collapse">
        
          <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ url('/admin/privileges/profile') }}"><span class="glyphicon glyphicon-heart"></span> hello {{{ Auth::user()->username }}}</a></li>
            @if (Auth::user()->super == 1)
                <li><a href="{{ url('/admin/settings') }}"><span class="glyphicon glyphicon-wrench"></span> {{{ Lang::get('crudadmin.gui.header.settings') }}}</a></li>
            @endif
            <li><a href="{{ url('/logout') }}"><span class="glyphicon glyphicon-log-out"></span> {{{ Lang::get('crudadmin.gui.header.logoff') }}}</a></li>
          </ul>
          
        </div>
      </div>
    </div>

