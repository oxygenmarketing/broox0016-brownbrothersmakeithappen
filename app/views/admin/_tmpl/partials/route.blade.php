
  <ul class="nav nav-sidebar">
    <li>
        <a href="{{ url('/admin') }}"><span class="glyphicon glyphicon-dashboard"></span> {{{ Lang::get('crudadmin.gui.route.home') }}}</a>
    </li>
    {{ AdminHelper::drawAdminRoute() }}
    <li>
        <a href="{{ url('/logout') }}"><span class="glyphicon glyphicon-log-out"></span> {{{ Lang::get('crudadmin.gui.route.logoff') }}}</a>
    </li>
  </ul>
