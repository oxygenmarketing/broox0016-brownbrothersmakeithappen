<!DOCTYPE html>
<html ng-app="customInterpolationApp">
<head>
    <title>{{{ Session::get('title') }}}</title>
    <meta charset="utf-8">

    <link rel="stylesheet" type="text/css" href="/packages/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/packages/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="/packages/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="/packages/trumbowyg/design/css/trumbowyg.css">
    <link rel="stylesheet" type="text/css" href="/_admin/css/app.css">
@yield('_styles')

    <script src="/js/jquery-1.11.0.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/packages/bootstrap/js/bootstrap.min.js"></script>
    <script src="/js/vendor/moment.js"></script>
    <script src="/packages/bootstrap/js/vendor/bootstrap.file-input.js"></script>
    <script src="/packages/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/js/vendor/jquery.widget.min.js"></script>
    <script src="/js/vendor/jquery.mousewheel.js"></script>
    <script src="/js/vendor/jquery.easing.1.3.min.js"></script>
    <script src="/js/vendor/jquery.floatThead.min.js"></script>
    <script src="/packages/trumbowyg/trumbowyg.min.js"></script>
    <script src="/js/vendor/jquery.loadTemplate-1.2.3.min.js"></script>
@yield('_scripts')
    <script src="/_admin/js/_global.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>
<body role="document" id="body">
	@if (Auth::check())
        @include('admin._tmpl.partials.header')
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">@include('admin._tmpl.partials.route')</div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
              @yield('_breadcrumbs')
    @endif
    {{ Msg::getHtml() }}
    @yield('content')
	@if (Auth::check())
            <a id="top-link" href="#body" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-circle-arrow-up"></span></a>
        </div>
      </div>
    </div>
    @include('admin._tmpl.partials.footer')
    @endif
</body>
@include('admin._tmpl.partials.formtypes')
</html>
