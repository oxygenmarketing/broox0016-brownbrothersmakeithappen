@extends('admin._tmpl.layout')

@section('_scripts')
<script>
  $(function() {
	'use strict';
  });
</script>
<script>
	$(function() {
		
		$('#revokeWinner').bind('click', function(e) {
			var _q		= '';
			var rel		= $(this).attr('rel');
			switch (rel) {
				case 'revokeWinner':
					_q	= 'Are you sure you want to revoke this prize from this user?';
					break;
				
			}			
			if (confirm(_q)) { return true; };
			return false;
		});
			
	});
</script>
@stop

@section('_styles')
@stop

{? $title = 'View Instant Win' ?}

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="">{{ link_to_action('Admin_HomeController@index', Lang::get('crudadmin.gui.route.home'), NULL, array()) }}</li>
    <li class="">{{ link_to_action('Admin_IWController@index', 'Instant Wins', NULL, array()) }}</li>
    <li class="active">{{ $title }}</li>
  </ol>
@stop

@section('content')

	<h2 class="page-header">{{{ $title }}}</h2>
    
    {{-- _e::pre($errors->has('email')) --}}
    
	<div class="table-responsive">
    	
		{{ Form::model(	$_data, array('method'=>'PATCH', 'route'=>array('admin.instantwin.update', $_data->id), 'class'=>'form-horizontal' ) ) }}
		
			{{ AdminFormHelper::_txtShortField($errors,'id','ID',1) }}
			{{ AdminFormHelper::_txtField($errors,'web_name','Prize','',$_data->getPrizeName(),1) }}
			{{ AdminFormHelper::_txtShortField($errors,'time_slot','Time Slot',1) }}
			{{ AdminFormHelper::_txtShortField($errors,'pcn','PCN',1) }}
			
			@if (isset($_data))
			{{ AdminFormHelper::_timeStamp($_data) }}
			@endif
			
			<hr>
			<h3>Winner Details</h3> 
			
			{? $method = ($_data->Entries->sms_id == 0) ? 'WEB' : 'SMS' ?}
			
			{{ AdminFormHelper::_txtField($errors,'method','Method','',$method,1) }}
			{{ AdminFormHelper::_txtField($errors,'created_at','Winning Entry Time','',$_data->Entries->created_at,1) }}
			{{ AdminFormHelper::_txtField($errors,'name','Name','',$_data->Entries->first_name.' '.$_data->Entries->last_name,1) }}
			@if ($method == 'WEB')
			{{ AdminFormHelper::_txtField($errors,'email','Email Address','',$_data->Entries->email,1) }}
			@endif
			{{ AdminFormHelper::_txtField($errors,'phone','Phone Number','',$_data->Entries->phone,1) }}
			
			{{ AdminHelper::viewEntry($_data->Entries->id) }}
            
		{{ Form::close() }}
    
	</div>
  
@stop