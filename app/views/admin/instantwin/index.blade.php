@extends('admin._tmpl.layout')

@section('_scripts')
@stop

@section('_styles')
@stop

{? $title = 'Instant Win Schedule' ?}

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="">{{ link_to_action('Admin_HomeController@index', Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">{{ link_to_action('Admin_IWController@index', $title) }}</li>
  </ol>
@stop

@section('content')
	<h2 class="page-header">{{{ $title }}}</h2>
  
    <div class="table-responsive">
		<table class="table table-striped table-bordered table-fixed-head">
			<thead>
				<tr>
					<th width="1%" nowrap>{{ 'ID' }}</th>
					<th nowrap>{{ 'Prize' }}</th>
					<th nowrap>{{ 'Time Slot' }}</th>
					<th width="1%" style="text-align:center;" nowrap>{{ 'Won' }}</th>
					<th width="1%" nowrap>Actions</th>
				</tr>  
			</thead>
			<tbody>
				@foreach ($_data->_d as $d)
				<tr>
					<td align="center">{{{ $d->id }}}</td>
					<td>{{{ $d->name }}}</td>
					<td>{{{ AdminHelper::formatLongDateTimeFull( $d->time_slot ) }}}</td>
					<?php /*<td align="center">{{ AdminHelper::toggleState($d->id,$d->active,'publish','Active') }}</td>*/ ?>
					<td align="center">{{ AdminHelper::checkWinner($d->id) }}</td>
					<td nowrap>
						{{ AdminHelper::btnWinnerView($d->id) }}
					</td>
				</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<td colspan="99">Total {{ $_data->_total }} items.</td>
				</tr>
			</tfoot>
		</table>
    </div>
    
    {{ $_data->_d->links() }}
  
@stop