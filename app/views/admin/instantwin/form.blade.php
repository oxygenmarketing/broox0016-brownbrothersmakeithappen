@extends('admin._tmpl.layout')

@section('_scripts')
<script>
  $(function() {
	'use strict';
  });
</script>
@stop

@section('_styles')
@stop

{? $title = 'Edit Instant Win' ?}

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="">{{ link_to_action('Admin_HomeController@index', Lang::get('crudadmin.gui.route.home'), NULL, array()) }}</li>
    <li class="">{{ link_to_action('Admin_IWController@index', 'Instant Wins', NULL, array()) }}</li>
    <li class="active">{{ $title }}</li>
  </ol>
@stop

@section('content')

	<h2 class="page-header">{{{ $title }}}</h2>

    {{-- _e::pre($errors->has('email')) --}}

	<div class="table-responsive">

		{{ Form::model(	$_data, array('method'=>'PATCH', 'route'=>array('admin.instantwin.update', $_data->id), 'class'=>'form-horizontal' ) ) }}

            {{ AdminFormHelper::_txtShortField($errors,'id','Instant Win ID',1) }}
        	{{ AdminFormHelper::_txtField($errors,'web_name','Prize','',$_data->getPrizeName(),1) }}
        	{{ AdminFormHelper::_datePicker($errors,'time_slot','Time Slot','') }}
        	{{ AdminFormHelper::_txtShortField($errors,'pcn','PCN',1) }}

			@if (isset($_data))
        	{{ AdminFormHelper::_timeStamp($_data) }}
			@endif

        	{{ AdminFormHelper::_btnAction('Save') }}

		{{ Form::close() }}

	</div>

@stop
