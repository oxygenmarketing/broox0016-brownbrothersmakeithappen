@extends('admin._tmpl.layout')

@section('_scripts')
<script>
$(function() {

	var image = function(itm) {
	};

	$('.popOver').popover({html: true});

})
</script>
@stop

@section('_styles')
@stop

{? $title = 'Winners' ?}

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="">{{ link_to_action('Admin_HomeController@index', Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">{{ link_to_action('Admin_EntriesController@index', $title) }}</li>
  </ol>
@stop

@section('content')
	<h2 class="page-header">{{{ $title }}}
		<a href="{{ action('Admin_WinnersController@export') }}" role="button" class="btn btn-success pull-right btn-sm"><span class="glyphicon glyphicon glyphicon-import"></span> Export</a>
	</h2>

    <nav class="navbar navbar-default navbar-filter">
		<div class="row">
			<div class="col-lg-4">{{ AdminHelper::renderSearchForm() }}</div>
			<div class="col-lg-8">
				{{ Form::open(array('url'=>'/admin/filter','id'=>'frmFilterDate','class'=>'navbar-form navbar-filter navbar-right','role'=>'filter')) }}
					<strong>Filter :</strong>
					{{ AdminHelper::filterDateRange($_data->filterby) }}
					{{ AdminHelper::filterList('active','Active',array('1'=>'Active','0'=>'Inactive')) }}
					{{ AdminHelper::filterList('method','Method',array('web'=>'Web','sms'=>'SMS')) }}
					<div class="btn-group">
						<button class="btn btn-default btn-sm" type="submit"><span class="glyphicon glyphicon-filter"></span></button>
					</div>
				{{ Form::close() }}
			</div>
		</div>
    </nav><!-- /navbar-filter -->

    <div class="table-responsive">
		<table class="table table-striped table-bordered table-fixed-head">
			<thead>
				<tr>
                <th width="1%" nowrap>{{ AdminHelper::drawHeaderRow('id','ID') }}</th>
                <th width="1%" nowrap>{{ AdminHelper::drawHeaderRow('instantwin_id','Instant Win ID') }}</th>
                <th nowrap>{{ AdminHelper::drawHeaderRow('name','Name') }}</th>
                <th nowrap>{{ AdminHelper::drawHeaderRow('email','Email') }}</th>
                <th nowrap width="5%">{{ AdminHelper::drawHeaderRow('state','State') }}</th>
                <th width="1%" nowrap>{{ AdminHelper::drawHeaderRow('postcode','Postcode') }}</th>
                <?php /* <th nowrap>{{ AdminHelper::drawHeaderRow('update_at','Entry Date') }}</th> */ ?>
                <th width="1%" style="text-align:center;" nowrap>{{ AdminHelper::drawHeaderRow('active','Active') }}</th>
                <th width="1%" nowrap>Resend EDM</th>
                <th width="1%" nowrap>EDM Count</th>
								<th width="2%" nowrap>Verification Request</th>
								<th width="2%" nowrap>Verification Status</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($_data->_d as $d)
				<tr>
                <td align="center">{{{ $d->id }}}</td>
                <td align="center">{{{ $d->instantwin_id }}}</td>
                <td><a href="{{ url(Request::url().'/'.$d->id) }}">{{{ $d->first_name }}} {{{ $d->last_name }}}</a></td>
                <td>{{{ $d->email }}}</td>
                <td>{{{ $d->state }}}</td>
                <td>{{{ $d->postcode }}}</td>
                <?php /* <td>{{ AdminHelper::formatLongDateTimeFull($d->created_at) }}</td> */ ?>
                <td align="center">{{ AdminHelper::toggleState($d->id,$d->active,'publish','Active') }}</td>
                <td>{{ AdminHelper::btnResend($d->id, 'Resend') }}</td>
                <td style="text-align:center;">{{{ $d->edm_sent }}}</td>
								<td align="center">{{ AdminHelper::btnVerify($d->id, 'Send Request') }}</td>
								<td></td>
				</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<td colspan="99">Total {{ $_data->_total }} items.</td>
				</tr>
			</tfoot>
		</table>
    </div>

    <a href="{{ action('Admin_WinnersController@export') }}" role="button" class="btn btn-success pull-right btn-sm"><span class="glyphicon glyphicon glyphicon-import"></span> Export</a>
    {{ $_data->_d->links() }}

@stop
