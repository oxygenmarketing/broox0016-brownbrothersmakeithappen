@extends('admin._tmpl.layout')

@section('_scripts')
    <script type="text/javascript">	
		
		"use strict";
		
		$(function() {
			//
		});	
		
		$(document).ready(function() {
			$('#btn_forgot_cancel').on('click', function(e){
				e.preventDefault();
				document.location.href='/admin';
			});
		});
		
    </script>
@stop

@section('_styles')

    <style>
	
		body {
		  padding-top: 40px;
		  padding-bottom: 40px;
		  background-color: #eee;
		}
		
		form {
		  max-width: 500px;
		  padding: 15px;
		  margin: 0 auto;
		}
			form .form-signin-heading,
			form .checkbox {
			  margin-bottom: 10px;
			}
			form .form-control {
			  position: relative;
			  height: auto;
			  -webkit-box-sizing: border-box;
				 -moz-box-sizing: border-box;
					  box-sizing: border-box;
			  padding: 10px;
			  font-size: 16px;
			}
				form .form-control:focus {
				  z-index: 2;
				}
				form input[name="email"] {
				  margin-bottom: 10px;
				}
	</style>
    
@stop

@section('content')

    <div class="container">

        {{ Form::open(array(
            'action'			=>	'Admin_AuthController@forgot_password',
            'method'			=>	'post',
            'accept-charset'	=>	'UTF-8',
            'class'			=>	'form-forgot',
        )) }}
        
            <h2 class="form-signin-heading">{{{ Lang::get('users.text.forgot.forgot_password') }}}</h2>
            
            @include('admin._tmpl.partials.alerts')
            
            {{ Form::email('email', Input::old('email'), array(
                'id'				=>	'email',
                'placeholder'	=>	Lang::get('users.text.forgot.email'),
                'class'			=>	'form-control',
                'tabindex'		=>	'1',
                'autocomplete'	=>	'on',
                'autofocus'		=>	'autofocus',
                'required'		=>	'required',
            )) }}
            
            {{ Form::hidden('_token', Session::getToken()) }}
            {{ Form::submit(Lang::get('users.button.forgot.reset'), array(
                'class'			=>	'btn btn-lg btn-success btn-sm',
                'tabindex'		=>	'3',
            )) }}
            {{ Form::button(Lang::get('users.button.cancel'), array(
                'id'				=>	'btn_forgot_cancel',
                'class'			=>	'btn btn-lg btn-primary btn-sm',
                'tabindex'		=>	'4',
            )) }}
        
        {{ Form::close() }}

    </div> <!-- /container -->
    
@stop