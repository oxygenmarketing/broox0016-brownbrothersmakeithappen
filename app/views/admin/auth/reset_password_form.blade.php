@extends('admin._tmpl.layout')

@section('_scripts')
    <script type="text/javascript">	
		
		"use strict";
		
		$(function() {
			//
		});	
		
		$(document).ready(function() {
			$('#btn_reset_cancel').on('click', function(e){
				e.preventDefault();
				document.location.href='/admin';
			});
		});
		
    </script>
@stop

@section('_styles')

    <style>
	
		body {
		  padding-top: 40px;
		  padding-bottom: 40px;
		  background-color: #eee;
		}
		
		form {
		  max-width: 500px;
		  padding: 15px;
		  margin: 0 auto;
		}
			form .form-signin-heading,
			form .checkbox {
			  margin-bottom: 10px;
			}
			form .form-control {
			  position: relative;
			  height: auto;
			  -webkit-box-sizing: border-box;
				 -moz-box-sizing: border-box;
					  box-sizing: border-box;
			  padding: 10px;
			  font-size: 16px;
			}
				form .form-control:focus {
				  z-index: 2;
				}
				form input[name="email"] {
				  margin-bottom: -1px;
				  border-bottom-right-radius: 0;
				  border-bottom-left-radius: 0;
				}
				form input[name="password"] {
				  margin-bottom: -1px;
				  border-radius: 0;
				}
				form input[name="password_confirmation"] {
				  margin-bottom: 10px;
				  border-top-left-radius: 0;
				  border-top-right-radius: 0;
				}

	</style>
    
@stop

@section('content')

    <div class="container">

        {{ Form::open(array(
            'action'			=>	'Admin_AuthController@do_reset_password',
            'method'			=>	'post',
            'accept-charset'	=>	'UTF-8',
            'class'			=>	'form-reset',
        )) }}
        
            <h2 class="form-signin-heading">{{{ Lang::get('users.text.reset.reset_password') }}}</h2>
            
            @include('admin._tmpl.partials.alerts')
            
            {{ Form::text('email', Input::old('email'), array(
                'id'				=>	'email',
                'placeholder'	=>	Lang::get('users.text.reset.email'),
                'class'			=>	'form-control',
                'tabindex'		=>	'1',
                'autocomplete'	=>	'on',
                'autofocus'		=>	'autofocus',
                'required'		=>	'required',
            )) }}
            {{ Form::password('password', array(
                'id'				=>	'password',
                'placeholder'	=>	Lang::get('users.text.reset.password'),
                'class'			=>	'form-control',
                'tabindex'		=>	'2',
                'autocomplete'	=>	'off',
                'required'		=>	'required',
            )) }}
            {{ Form::password('password_confirmation', array(
                'id'				=>	'password_confirmation',
                'placeholder'	=>	Lang::get('users.text.reset.password_2'),
                'class'			=>	'form-control',
                'tabindex'		=>	'3',
                'autocomplete'	=>	'off',
                'required'		=>	'required',
            )) }}

            {{ Form::hidden('token', $token) }}
            {{ Form::hidden('_token', Session::getToken()) }}
            {{ Form::submit(Lang::get('users.button.reset'), array(
                'class'			=>	'btn btn-lg btn-success btn-sm',
                'tabindex'		=>	'4',
            )) }}
            {{ Form::button(Lang::get('users.button.cancel'), array(
                'id'=>'btn_reset_cancel',
                'class'			=>	'btn btn-lg btn-primary btn-sm',
                'tabindex'		=>	'5',
            )) }}
                
        {{ Form::close() }}
        
    </div> <!-- /container -->
    
@stop