@extends('admin._tmpl.layout')
<?php
	
	use Ghunti\HighchartsPHP\Highchart;
	use Ghunti\HighchartsPHP\HighchartJsExpr;
	
?>
@section('_scripts')
<script>
$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
</script>
@stop

@section('_styles')
@stop

{? $title = 'Snapshots : '.$_data->title ?}

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="">{{ link_to_action('Admin_HomeController@index', Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">{{ link_to_action('Admin_SnapshotsController@index', $title) }}</li>
  </ol>
@stop

@section('content')

	{{-- _e::pre($_data) --}}
	{? $_exp = '<a href="'.action('Admin_SnapshotsController@'.$_data->export).'" role="button" class="btn btn-success pull-right btn-sm"><span class="glyphicon glyphicon glyphicon-import"></span> Export</a>' ?}
	<h2 class="page-header">{{{ $title }}}
		{{ $_exp }}
	</h2>
  
    <nav class="navbar navbar-default navbar-filter suspended">
        <div class="row">
			<div class="col-lg-12">  
				{{ Form::open(array('url'=>'/admin/filter','id'=>'frmFilterDate','class'=>'navbar-form navbar-filter navbar-right','role'=>'filter')) }}
					<strong>Filter :</strong>
					{{ AdminHelper::filterDateRange($_data->filterby) }}
					<div class="btn-group">
						<button class="btn btn-default btn-sm" type="submit"><span class="glyphicon glyphicon-filter"></span></button>
					</div>
				{{ Form::close() }}
			</div>       
        </div>
    </nav><!-- /navbar-filter --> 
    
	<?php 
	
		$total	= 0;
    
		$chart = new Highchart();
		
		$chart->chart->renderTo			= "mainStatsContainer";
		$chart->chart->type 				= "bar";
		$chart->chart->height 			= "1000";
		
		$chart->xAxis->categories			= NULL;
		$chart->xAxis->title->text		= NULL;

		$chart->yAxis->min					= 0;
		$chart->yAxis->title->align		= "high";
		$chart->yAxis->stackLabels->enabled				= true;
		$chart->yAxis->stackLabels->style->color		= 'gray';
		$chart->yAxis->stackLabels->style->useHTML		= true;
		$chart->yAxis->stackLabels->formatter			= new HighchartJsExpr("function() { return '<em>['+ this.total +']</em>'; }");
		
		$chart->tooltip->formatter 		= new HighchartJsExpr("function() { return this.series.name +' Entries : '+ this.y +''; }");
		
		$chart->plotOptions->bar->dataLabels->enabled	= 1;
		$chart->plotOptions->series->stacking = 'normal';
		
		$chart->legend->enabled 			= 1;
		$chart->credits->enabled 			= false;
		
		$chart->legend->layout			= "vertical";
		$chart->legend->align 			= "right";
		$chart->legend->verticalAlign		= "top";
		$chart->legend->x 					= -10;
		$chart->legend->y 					= 0;
		$chart->legend->floating 			= 1;
		$chart->legend->borderWidth	 	= 1;
		$chart->legend->backgroundColor	= "#FFFFFF";
		$chart->legend->shadow			= 0;
		
		//_e::prex($_data->_d);
		
		foreach ($_data->_d as $ii=>$_gdd) {
			foreach ($_gdd as $iv=>$_gdf) {
				$chart->xAxis->categories[]	= AdminHelper::formatPromoDate($_gdf['date']);
				$chartseries[]	= (int)$_gdf['data'][0];
				$total	+= (int)$_gdf['data'][0];
			}
			$chart->series[] = array(
				'name'	=> $ii,
				'data'	=> $chartseries
			);
			$chartseries = array();
		};
		
		$chart->title->text				= $_data->title.' ('.$total.')';
		$chart->yAxis->title->text		= 'Number of Entries ('.$total.')';
		
    ?>
    
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li class="active"><a href="#graph" role="tab" data-toggle="tab">Graph view</a></li>
      <li><a href="#table" role="tab" data-toggle="tab">Table view</a></li>
    </ul>
    
    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane active" id="graph">
      
		<?php $chart->printScripts(); ?>
        <div id="mainStatsContainer"></div>
        <script type="text/javascript">
            jQuery.noConflict();
            <?php echo $chart->render("chart1"); ?>
        </script>
      
      </div>
      <div class="tab-pane tab-pane-table" id="table">
		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th nowrap>Date</th>
						<th nowrap width="120" style="text-align:center">SMS</th>
						<th nowrap width="120" style="text-align:center">Web</th>
						<th nowrap width="120" style="text-align:center">Total</th>
					</tr>  
				</thead>
				<tbody>
				<?php
				
				//_e::pre($_data);
				
				$tableData		= array();
				$tableTotalW	= 0;
				$tableTotalS	= 0;
				$tableTotalA	= 0;
				
				foreach ($_data->_d as $ii=>$_gdd) {
					foreach ($_gdd as $iv=>$_gdf) {
						$tableData[$_gdf['date']][$ii] = $_gdf['data'][0];
					};
				};
						
				foreach ($tableData as $x=>$y) {
				?>
					<tr>
						<td>{{ AdminHelper::formatPromoDate($x) }}</td>
						<td style="text-align:center">{{ $y['SMS'] }} {? $tableTotalS += $y['SMS'] ?}</td>
						<td style="text-align:center">{{ $y['Web'] }} {? $tableTotalW += $y['Web'] ?}</td>
						<td style="text-align:center">{{ $y['Web'] + $y['SMS'] }} {? $tableTotalA += $y['Web'] + $y['SMS'] ?}</td>
					</tr>
				<?php
				};
				?>
					<tr>
						<td style="background:#000; color:#fff;"><strong>TOTAL:</strong></td>
						<td style="text-align:center; background:#000; color:#fff;"><strong>{{ $tableTotalS }}</strong></td>
						<td style="text-align:center; background:#000; color:#fff;"><strong>{{ $tableTotalW }}</strong></td>
						<td style="text-align:center; background:#000; color:#fff;"><strong>{{ $tableTotalA }}</strong></td>
					</tr>
				</tbody>
			</table>
		</div>      
      </div>
    </div>
	
	<br/>
	<p>{{ $_exp }}</p>
  
@stop