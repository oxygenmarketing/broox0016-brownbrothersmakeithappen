@extends('admin._tmpl.layout')
<?php
	
	use Ghunti\HighchartsPHP\Highchart;
	use Ghunti\HighchartsPHP\HighchartJsExpr;
	
?>
@section('_scripts')
<script>
$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
</script>
@stop

@section('_styles')
@stop

{? $title = 'Snapshots : '.$_data->title ?}

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="">{{ link_to_action('Admin_HomeController@index', Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">{{ link_to_action('Admin_SnapshotsController@index', $title) }}</li>
  </ol>
@stop

@section('content')

	{{-- _e::prex($_data) --}}
	{? $_exp = '<a href="'.action('Admin_SnapshotsController@'.$_data->export).'" role="button" class="btn btn-success pull-right btn-sm"><span class="glyphicon glyphicon glyphicon-import"></span> Export</a>' ?}
	<h2 class="page-header">{{{ $title }}}
		{{ $_exp }}
	</h2>
  
    <nav class="navbar navbar-default navbar-filter suspended">
        <div class="row">
            <div class="col-lg-12">  
				  {{ Form::open(array('url'=>'/admin/filter','id'=>'frmFilterDate','class'=>'navbar-form navbar-filter navbar-right','role'=>'filter')) }}
					<strong>Filter :</strong>
					{{ AdminHelper::filterDateRange($_data->filterby) }}
					<div class="btn-group">
						<button class="btn btn-default btn-sm" type="submit"><span class="glyphicon glyphicon-filter"></span></button>
						<?php /* <button class="btn btn-default btn-sm _reset" type="reset"><span class="glyphicon glyphicon-remove"></span></button> */ ?>
					</div>
                {{ Form::close() }}
            </div>
        </div>
    </nav><!-- /navbar-filter --> 
    
	<?php 
    
        $chart = new Highchart();
        
        $chart->chart->renderTo			= "mainStatsContainer";
        $chart->chart->type 				= "bar";
        $chart->chart->height 			= "1200";
        $chart->title->text				= 'Entries By Day';
        //$chart->subtitle->text 		= Config::get('_system.fbUrl');
        
        $chart->xAxis->categories		= NULL;
        $chart->xAxis->title->text		= '';
        
        $chart->yAxis->min				= 0;
        $chart->yAxis->title->text		= 'Number of Entries';
        $chart->yAxis->title->align		= "high";

		$chart->yAxis->stackLabels->enabled				= true;
		$chart->yAxis->stackLabels->style->color		= 'gray';
		$chart->yAxis->stackLabels->style->useHTML		= true;
		$chart->yAxis->stackLabels->formatter			= new HighchartJsExpr("function() { return '<em>['+ this.total +']</em>'; }");
		
        $chart->tooltip->formatter 		= new HighchartJsExpr("function() { return 'Entries : '+ this.y +''; }");
    
        $chart->plotOptions->bar->dataLabels->enabled	= 1;
        $chart->plotOptions->series->stacking	= 'normal';
        $chart->legend->enabled 			= 1;
        $chart->credits->enabled 		= false;

        $chart->legend->layout			= "vertical";
        $chart->legend->align 			= "right";
        $chart->legend->verticalAlign	= "top";
        $chart->legend->x 				= -10;
        $chart->legend->y 				= 0;
        $chart->legend->floating 		= 1;
        $chart->legend->borderWidth	 	= 1;
        $chart->legend->backgroundColor	= "#FFFFFF";
        $chart->legend->shadow			= 0;
		
        $monthGrp	= array();
        $i = 0;

        foreach ($_data->_d as $ii=>$_gdd) {
			foreach ($_gdd['data'] as $xx=>$yy) {
				$chart->xAxis->categories[]	= AdminHelper::formatPromoDate($xx);
			};
			foreach ($_gdd['data'] as $xx=>$yy) {
				$chartseries[]	= (int)$yy;
			};
			$chart->series[] = array(
				'name'	=> $_gdd['stack'].' '.$_gdd['name'],
				'data'	=> $chartseries,
				'stack' => $_gdd['stack']
			);
			$chartseries = array();
			$i++;
        };
		
    ?>
    
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li class="active"><a href="#graph" role="tab" data-toggle="tab">Graph view</a></li>
      <li><a href="#table" role="tab" data-toggle="tab">Table view</a></li>
    </ul>
    
    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane active" id="graph">
      
		<?php $chart->printScripts(); ?>
        <div id="mainStatsContainer"></div>
        <script type="text/javascript">
            jQuery.noConflict();
            <?php echo $chart->render("chart1"); ?>
        </script>
      
      </div>
      <div class="tab-pane tab-pane-table" id="table">
      	<div class="table-responsive">
             <?php
			 	//_e::prex($_data->table);
				$tableData = array();
			  ?> 
              <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <?php
                        $firstkey	= reset($_data->table);
                        $stateTitle	= array();
                    ?>
                        <tr>
                            <th rowspan="2" nowrap>Date</th>
                            <?php 
								foreach($firstkey as $i=>$v) {
									echo '<th colspan="9" nowrap style="text-align:center;">'.$i.'</th>';
									$_tmp = '';
									foreach($v as $state => $value) {
										$_tmp .= '<th style="text-align:center;">'.$state.'</th>';
									}
									$stateTitle[$i] = $_tmp;
								};
                            ?>
                            <th rowspan="2" nowrap style="text-align:center;">Total</th>
                        </tr>  
                        <tr>
                        <?php 
								foreach($stateTitle as $stitle) {
									echo $stitle;
									echo '<th style="text-align:center;">Total</th>';
								
								};
                        ?>
                        </tr>
                    </thead>
                    <tbody class="table-hover">
                    {? $fullTot = 0; ?}
                    {? $footTot = array() ?}
                        @foreach ($_data->table as $datei=>$v)
                            {{-- _e::prex($datei) --}}
                                <tr>
                                    <td>{{ AdminHelper::formatPromoDate($datei) }}</td>
										{? $xx = 0; ?}
                                    {? $bigTot = 0; ?}
                                    @foreach($v as $_v)
                                    {? $smlTot = 0; ?}
                                    	@foreach($_v as $nums)
                                        	<td style="text-align:center;">
                                            	{{ $nums }}
                                                @if(isset($footTot[$xx]))
                                                {? $footTot[$xx] += $nums ?}
                                                @else
                                                {? $footTot[$xx] = $nums ?}
                                                @endif
                                            </td>
                                            {? $smlTot = $smlTot + $nums; ?}
                                            {? $xx++ ?}
											@endforeach
											<td style="text-align:center; background:#e4e4e4; color:#000;">
											{? $bigTot = $bigTot + $smlTot ?}
											<strong>{{ $smlTot }}</strong>
											@if (isset($footTot[$xx]))
												{? $footTot[$xx] += $smlTot ?}
											@else
												{? $footTot[$xx] = $smlTot ?}
											@endif
											{? $xx++ ?}
											</td>
                                    @endforeach
										<td style="text-align:center; background:#666; color:#fff;">
										{{ $bigTot }}
                                    @if(isset($footTot[$xx]))
											{? $footTot[$xx] += $bigTot ?}
                                    @else
											{? $footTot[$xx] = $bigTot ?}
                                    @endif
                                    </td>
                                </tr>
                            {? $fullTot = $fullTot + $bigTot; ?}
                        @endforeach
                        <tr>
							<td style="background:#000; color:#fff;"><strong>TOTAL:</strong></td>
                            @foreach($footTot as $ft)
                                <td style="text-align:center; background:#000; color:#fff;"><strong>{{ $ft }}</strong></td>
                            @endforeach
                       </tr> 	
                    </tbody>
                </table>
        	</div>      
      </div>
    </div>
	
	<br>
	<p>{{ $_exp }}</p>
  
@stop