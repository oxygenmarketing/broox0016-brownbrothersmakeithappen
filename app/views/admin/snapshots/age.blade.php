@extends('admin._tmpl.layout')
<?php
	
	use Ghunti\HighchartsPHP\Highchart;
	use Ghunti\HighchartsPHP\HighchartJsExpr;
	
?>
@section('_scripts')
<script>
$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
</script>
@stop

@section('_styles')
@stop

{? $title = 'Snapshots : '.$_data->title ?}

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="">{{ link_to_action('Admin_HomeController@index', Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">{{ link_to_action('Admin_SnapshotsController@index', $title) }}</li>
  </ol>
@stop

@section('content')

	{{-- _e::prex($_data) --}}
	{? $_exp = '<a href="'.action('Admin_SnapshotsController@'.$_data->export).'" role="button" class="btn btn-success pull-right btn-sm"><span class="glyphicon glyphicon glyphicon-import"></span> Export</a>' ?}
	<h2 class="page-header">{{{ $title }}}
		{{ $_exp }}
	</h2>
  
    <nav class="navbar navbar-default navbar-filter suspended">
        <div class="row">
            <div class="col-lg-12">  
				  {{ Form::open(array('url'=>'/admin/filter','id'=>'frmFilterDate','class'=>'navbar-form navbar-filter navbar-right','role'=>'filter')) }}
					<strong>Filter :</strong>
					{{ AdminHelper::filterDateRange($_data->filterby) }}
					<div class="btn-group">
						<button class="btn btn-default btn-sm" type="submit"><span class="glyphicon glyphicon-filter"></span></button>
						<?php /* <button class="btn btn-default btn-sm _reset" type="reset"><span class="glyphicon glyphicon-remove"></span></button> */ ?>
					</div>
                {{ Form::close() }}
            </div>
        </div>
    </nav><!-- /navbar-filter --> 
    
	<?php 
    
        $chart = new Highchart();
        
        $chart->chart->renderTo			= "mainStatsContainer";
        $chart->chart->type 				= "bar";
        $chart->chart->height 			= "1200";
        $chart->title->text				= 'Entries By Day';
        //$chart->subtitle->text 		= Config::get('_system.fbUrl');
        
        $chart->xAxis->categories		= NULL;
        $chart->xAxis->title->text		= '';
        
        $chart->yAxis->min				= 0;
        $chart->yAxis->title->text		= 'Number of Entries';
        $chart->yAxis->title->align		= "high";
		
        $chart->tooltip->formatter 		= new HighchartJsExpr("function() { return 'Entries : '+ this.y +''; }");
    
        $chart->plotOptions->bar->dataLabels->enabled	= 1;
        $chart->legend->enabled 			= 1;
        $chart->credits->enabled 		= false;

        $chart->legend->layout			= "vertical";
        $chart->legend->align 			= "right";
        $chart->legend->verticalAlign	= "top";
        $chart->legend->x 				= -10;
        $chart->legend->y 				= 0;
        $chart->legend->floating 		= 1;
        $chart->legend->borderWidth	 	= 1;
        $chart->legend->backgroundColor	= "#FFFFFF";
        $chart->legend->shadow			= 0;
		
        $monthGrp	= array();
		$i = 0;
        foreach ($_data->_d as $ii=>$_gdd) {
			$chart->series[$i] = array(
				'name'	=> $_gdd['name'],		// LEGEND LABEL
				'data'	=> array() 	// PLOT POINTS
			);
			foreach($_gdd['data'] as $_date=>$zz) {
				$monthGrp[$_date]	= 0;
				$chart->series[$i]['data'][] = (int)$zz;
			}
			$i++;
        };
		foreach($monthGrp as $_mg=>$xx) {
			$chart->xAxis->categories[]	= AdminHelper::formatPromoDate($_mg);
		}
		//_e::prex($_data->_d);
    ?>
    
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li class="active"><a href="#graph" role="tab" data-toggle="tab">Graph view</a></li>
      <li><a href="#table" role="tab" data-toggle="tab">Table view</a></li>
    </ul>
    
    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane active" id="graph">
      
		<?php $chart->printScripts(); ?>
        <div id="mainStatsContainer"></div>
        <script type="text/javascript">
            jQuery.noConflict();
            <?php echo $chart->render("chart1"); ?>
        </script>
      
      </div>
		<div class="tab-pane tab-pane-table" id="table">
			<div class="table-responsive">
				<?php
				
				$tableData	= array();
				$ranges		= array();
				foreach($_data->_d as $v) {
					$ranges[$v['name']] = 0;
					foreach($v['data'] as $iv=>$vd) {
						$tableData[$iv][$v['name']] = $vd;
					};
				};
				$gTotal = array();
				
				?> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th rowspan="2" nowrap>Date</th>
							<th colspan="8" nowrap>Number of Entries</th>
						</tr>  
						<tr>
							{? $xyz = 0 ?}
							@foreach ($ranges as $ix=>$zsz)
							<th width="90" nowrap style="text-align:center;">{{ $ix }}</th>
							{? $gTotal[$xyz] = 0 ?}
							{? $xyz++ ?}
							@endforeach
							<th width="90" nowrap style="text-align:center;">Total</th>
						</tr>
					</thead>
					<tbody class="table-hover">
						@foreach ($tableData as $_idate=>$_td)
						<tr>
							<td>{{ AdminHelper::formatPromoDate($_idate) }}</td>
							{? $lineTotal = 0 ?}
							{? $xyz = 0 ?}
							@foreach($_td as $re)
							<td style="text-align:center;">
								{{ $re }}
								{? $lineTotal += $re ?}
								{? $gTotal[$xyz] += $re ?}
							</td>
							{? $xyz++ ?}
							@endforeach
							<td style="text-align:center;"><strong>{{ $lineTotal }}</strong></td>
						</tr>
						@endforeach
						<tr>
							<td style="background:#000; color:#fff;"><strong>TOTAL:</strong></td>
							{? $gTT = 0 ?}
							@foreach($gTotal as $gt)
							<td style="text-align:center; background:#000; color:#fff;"><strong>{{ $gt }}</strong></td>
							{? $gTT += $gt ?}
							@endforeach
							<td style="text-align:center; background:#000; color:#fff;"><strong>{{ $gTT }}</strong></td>
						</tr>
					</tbody>
				</table>
			</div>      
		</div>
    </div>
	
	<br>
	<p>{{ $_exp }}</p>
  
@stop