@extends('admin._tmpl.layout')
<?php
	
	use Ghunti\HighchartsPHP\Highchart;
	use Ghunti\HighchartsPHP\HighchartJsExpr;
	
?>
@section('_scripts')
<script>
$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
</script>
@stop

@section('_styles')
@stop

{? $title = 'Snapshots : '.$_data->title ?}

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="">{{ link_to_action('Admin_HomeController@index', Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">{{ link_to_action('Admin_SnapshotsController@index', $title) }}</li>
  </ol>
@stop

@section('content')

	{{ _e::prex($_data) }}

	<h2 class="page-header">{{{ $title }}}
		<a href="{{ action('Admin_SnapshotsController@exportState', array($_data->export)) }}" role="button" class="btn btn-success pull-right btn-sm"><span class="glyphicon glyphicon glyphicon-import"></span> Export</a>
    </h2>
  
    <nav class="navbar navbar-default navbar-filter suspended">
        <div class="row">
            <div class="col-lg-12">  
				  {{ Form::open(array('url'=>'/admin/filter','id'=>'frmFilterDate','class'=>'navbar-form navbar-filter navbar-right','role'=>'filter')) }}
					<strong>Filter :</strong>
					{{ AdminHelper::filterDateRange($_data->filterby) }}
					<div class="btn-group">
						<button class="btn btn-default btn-sm" type="submit"><span class="glyphicon glyphicon-filter"></span></button>
						<?php /* <button class="btn btn-default btn-sm _reset" type="reset"><span class="glyphicon glyphicon-remove"></span></button> */ ?>
					</div>
                {{ Form::close() }}
            </div>
        </div>
    </nav><!-- /navbar-filter --> 
    
	<?php 
	
		$total	= 0;
    
		$chart = new Highchart();
		
		$chart->chart->renderTo			= "mainStatsContainer";
		$chart->chart->type 				= "bar";
		$chart->chart->height 			= "600";
		
		$chart->xAxis->categories			= NULL;
		$chart->xAxis->title->text		= NULL;
		
		$chart->yAxis->min								= 0;
		$chart->yAxis->title->align					= "high";
		$chart->yAxis->stackLabels->enabled				= true;
		$chart->yAxis->stackLabels->style->color		= 'gray';
		$chart->yAxis->stackLabels->style->useHTML		= true;
		$chart->yAxis->stackLabels->formatter			= new HighchartJsExpr("function() { return '<em>['+ this.total +']</em>'; }");

		$chart->tooltip->formatter 		= new HighchartJsExpr("function() { return this.series.name +' Entries : '+ this.y +''; }");
		
		$chart->plotOptions->bar->dataLabels->enabled	= 1;
		$chart->plotOptions->series->stacking = 'normal';
		
		$chart->legend->enabled 			= 1;
		$chart->credits->enabled 			= false;
		
		$chart->legend->layout			= "vertical";
		$chart->legend->align 			= "right";
		$chart->legend->verticalAlign		= "top";
		$chart->legend->x 					= -10;
		$chart->legend->y 					= 0;
		$chart->legend->floating 			= 1;
		$chart->legend->borderWidth	 	= 1;
		$chart->legend->backgroundColor	= "#FFFFFF";
		$chart->legend->shadow			= 0;
		
		//_e::pre($_data->_d);

		foreach ($_data->_d as $ii=>$_gdd) {
			//_e::pre($ii);
			//_e::pre($_gdd);
			$chart->xAxis->categories[]	= AdminHelper::formatPromoDate($ii);
			
			//_e::prex($_gdd);
			
			$chart->series[] = array(
				'name'	=> $_gdd['State'],
				'data'	=> implode(',',$_gdd['Web']),
				'stack'	=> 'Web'
			);
			$chart->series[] = array(
				'name'	=> $_gdd['State'],
				'data'	=> implode(',',$_gdd['SMS']),
				'stack'	=> 'SMS'
			);
		
			
			/*
			foreach ($_gdd as $cc=>$dd) {
				_e::pre($cc);
				_e::pre($dd);
				
				
				//$chartseries[]	= (int)$dd;
				//$chartstack	= $cc;
				/*
				$chart->series[] = array(
					'name'	=> $dd['State'],
					'data'	=> implode(',',$dd),
					'stack'	=> $cc
				);
				
				
				
			};
			
			/*
			//$chartseries[]	= (int)$_gdf['data'][0];
			//$total	+= (int)$_gdf['data'][0];
			
			$chart->series[] = array(
				'name'	=> $ii,
				'data'	=> $chartseries,
				'stack'	=> $chartstack
			);
			$chartseries	= array();
			$chartstack	= '';
			*/
			
		};
		
		$chart->title->text				= $_data->title.' ('.$total.')';
		$chart->yAxis->title->text		= 'Number of Entries ('.$total.')';
		
    ?>
    
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li class="active"><a href="#graph" role="tab" data-toggle="tab">Graph view</a></li>
      <li><a href="#table" role="tab" data-toggle="tab">Table view</a></li>
    </ul>
    
    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane active" id="graph">
      
		<?php $chart->printScripts(); ?>
        <div id="mainStatsContainer"></div>
        <script type="text/javascript">
            jQuery.noConflict();
            <?php echo $chart->render("chart1"); ?>
        </script>
      
      </div>
      <div class="tab-pane tab-pane-table" id="table">
	  
	  <?php /*
	  
      	<div class="table-responsive">
             <?php
			 	//_e::pre($_data->_d);
				
				$tableData = array();
				
				foreach($_data->_d as $state=>$v) {
					foreach($v as $_date) {
						$tableData[$_date['date']][$state] = $_date['data'][0];
					}
				}
				
				//_e::prex($tableData);
			  ?> 
              <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th rowspan="2" nowrap>Date</th>
                            <th colspan="8" nowrap>Number of Entries</th>
                        </tr>  
                        <tr>
                        	@foreach ($_data->_d as $ii=>$_gdd)
                            	<th>{{ $ii }}</th>
                                {{-- _e::prex($_gdd) --}}
                            @endforeach
                        </tr>
                    </thead>
                    <tbody class="table-hover">
                        @foreach ($tableData as $_idate=>$_td)
                            
                                <tr>
                                    <td>{{ $_idate }}</td>
                                    @foreach($_td as $re)
                                    	<td>
                                        	
                                            {{ $re }}
                                        	
                                        </td>
                                    @endforeach
                                </tr>
                            
                        @endforeach
                        	
                    </tbody>
                    
                </table>
				
        	</div>      
			
			*/ ?>
      
      </div>
    </div>
  
@stop