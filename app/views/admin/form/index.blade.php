@extends('admin._tmpl.layout')

@section('_styles')
<style>
	._rowContainer {
		padding:0px;
		background:#fff;
		border-radius:0;
		margin: 0 0 10px 0;
	}
	._rowContainer .headContainer {
		padding:5px;
		margin:0;
		background:#f4f4f4;
		cursor:pointer;
		height:40px;
		display:block;
		overflow:hidden;
		position:relative;
	}
	._rowContainer h1 {
		padding:6px 0 10px 10px;
		font-size:14px;
		height:40px;
		margin:0;
	}
	.btnDeleteRow {
		position:absolute;
		top:5px;
		right:5px;
	}
	.header-sort-placeholder {
		background:#fff;
		border:1px dotted #000;
		margin: 0 0 10px 0;
		height:40px;
		
		/*
		-webkit-animation-delay: 0s;
		-webkit-animation-direction: normal;
		-webkit-animation-duration: 2s;
		-webkit-animation-fill-mode: none;
		-webkit-animation-iteration-count: infinite;
		-webkit-animation-name: progress-bar-stripes;
		-webkit-animation-play-state: running;
		-webkit-animation-timing-function: linear;
		
		-webkit-background-size: 40px 40px;
		-webkit-transition-delay: 0s;
		-webkit-transition-duration: 0.6s;
		-webkit-transition-property: width;
		-webkit-transition-timing-function: ease;
		
		background-color: rgb(20, 155, 223);
		background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.14902) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.14902) 50%, rgba(255, 255, 255, 0.14902) 75%, transparent 75%, transparent);
		background-repeat: repeat-x;
		background-size: 40px 40px;
		box-sizing: border-box;
		
		display: block;
		transition-delay: 0s;
		transition-duration: 0.6s;
		transition-property: width;
		transition-timing-function: ease;
		*/
		
	}
	.addElementContainer {
		display:block;
		background:#e4e4e4;
		border-bottom:1px solid #dcdcdc;
		margin:0px;
		padding:5px;
	}
	
	._isHigh {
		background-color:#888;
	}
	
	._element_node {
		padding-bottom:5px;
		min-height:53px;
	}
	._elementContainer {
		padding:0;
		background:#f6f6f6;
		border-radius:0;
		border:1px solid #dcdcdc;
		margin: 10px 10px 0px 10px;
		width:47%;
		display:inline-block;
	}
	._elementContainer .eheadContainer {
		padding:0;
		margin:0;
		background:#f4f4f4;
		cursor:pointer;
		height:32px;
		display:block;
		overflow:hidden;
		position:relative;
	}
	._elementContainer h1 {
		padding:0 0 0 10px;
		font-size:14px;
		height:32px;
		line-height:32px;
		margin:0;
	}
	.element-sort-placeholder {
		
		padding:0px;
		background:#f6f6f6;
		border-radius:0;
		border:1px dotted #000;
		height:30px;
		margin: 10px 10px 0px 10px;
		width:47%;
		display:inline-block;
					
	}
	._elementContainer ._elFieldContainer {
		background:#fff;
		padding-top:10px;
	}
	._elementContainer ._elFieldContainer .form-group {
		margin-bottom:0;
		padding-bottom:10px;
	}
	
	.optionContainer {
		display:block;
		position:relative;
	}
	._optionsContainerBtn {
		position:absolute;
		top:0;
		left:-40px;
		z-index:99;
	}
	._optionsContainer {
		display:block;
		cursor:pointer;
		margin-bottom:5px;
		z-index:1;
	}

</style>
@stop

@section('_scripts')
<script type="text/javascript">
$(document).ready(function() {
	
	"use strict";
	
	var rowLabel		= 'ROW';
	var fieldLabel		= 'FIELD : ';
	var maxElements	= 4;
	var rowCount		= 0;
	var elmCount		= 0;
	var optCount		= 0;
	var doStuffContainer	= $('#doStuffContainer');
		
	var selectOpts	= {
		'0':'- Field Type -',
		'text':'Text',
		'email':'Email',
		'number':'Number',
		'tel':'Telephone',
		'textarea':'Text Area',
		'select':'Select Dropdown',
		'radio':'Radio Button',
		'checkbox':'Check Box',
		'dateselect':'Date Select',
		'captcha':'Captcha',
		'entercaptcha':'Enter Captcha Code',
		'submit':'Submit',
	};
	var fieldOpts	= [
		//['text',[
		//	['id','txt'],['name','txt'],['elementClass','txt'],['divClass','txt'],['validation','txtarea'],['options','opt'],['maxLength','txts'],['placeholder','txt'],['read_only','cb'],['multiple','cb'],['autocomplete','cb'],['rows','txts'],['cols','txts']
		//]],
		['text',[
			['id','txt'],['name','txt'],['elementClass','txt'],['divClass','txt'],['validation','txtarea'],['maxLength','txts'],['placeholder','txt'],['autocomplete','cb']
		]],
		['email',[
			['id','txt'],['name','txt'],['elementClass','txt'],['divClass','txt'],['validation','txtarea'],['maxLength','txts'],['placeholder','txt'],['autocomplete','cb']
		]],
		['textarea',[
			['id','txt'],['name','txt'],['elementClass','txt'],['divClass','txt'],['validation','txtarea'],['rows','txts'],['cols','txts']
		]],
		['select',[
			['id','txt'],['name','txt'],['elementClass','txt'],['divClass','txt'],['validation','txtarea'],['options','opt'],['multiple','cb'],['rows','txts'],['cols','txts']
		]],
		['radio',[
			['id','txt'],['name','txt'],['elementClass','txt'],['divClass','txt'],['validation','txtarea'],['options','opt'],['multiple','cb']
		]],
		['checkbox',[
			['id','txt'],['name','txt'],['elementClass','txt'],['divClass','txt'],['validation','txtarea'],['options','opt'],['multiple','cb']
		]],
		['dateselect',[
			['id','txt'],['name','txt'],['elementClass','txt'],['divClass','txt'],['validation','txtarea'],['maxLength','txts'],['placeholder','txt'],['read_only','cb']
		]],
		['captcha',[
			['id','txt'],['name','txt'],['elementClass','txt'],['divClass','txt']
		]],
		['entercaptcha',[
			['id','txt'],['name','txt'],['elementClass','txt'],['divClass','txt'],['validation','txtarea'],['maxLength','txts']
		]],
		['number',[
			['id','txt'],['name','txt'],['elementClass','txt'],['divClass','txt'],['validation','txtarea'],['maxLength','txts'],['placeholder','txt'],['autocomplete','cb']
		]],
		['tel',[
			['id','txt'],['name','txt'],['elementClass','txt'],['divClass','txt'],['validation','txtarea'],['maxLength','txts'],['placeholder','txt'],['autocomplete','cb']
		]],
		['submit',[
			['id','txt'],['name','txt'],['elementClass','txt'],['divClass','txt']
		]],
	];
	
	var findElName = function(type) {
		var result;
		$.each(selectOpts, function(i,v) {
			if (i === type) {
				result = v;
			};
		});
		return result;
	};
	
	var makeTitle = function (slug) {
		var words = slug.split('_');
		for(var i = 0; i < words.length; i++) {
			var word = words[i];
			words[i] = word.charAt(0).toUpperCase() + word.slice(1);
		};
		return words.join(' ');
	};
	
	var _makeFormBlock = function(row,elmCount,type) {
		var _data;
		_data = $('<div>', {
			'class' : 'form-group',
		})
			.append($('<label>', {
				'class' : 'col-sm-3 control-label',
				'for' : 'field_'+row+'_'+elmCount+'_type',
				'text' : 'Type :',
			}))
			.append($('<div>', {
				'class' : 'col-sm-9',
			})
				.append(_selectList(row,elmCount,type))
			)
			.append('<div class="clearfix"></div>')
			.append('<hr/>')
			.append(_getFieldRows(row,elmCount,type));
		return _data;
	};
	
	var _selectList = function(row,elmCount,type) { 
		var selectList;
		// create a select list object
		selectList	= $('<select>', {
			'id' : 'field_'+row+'_'+elmCount+'_type',
			'name' : 'field['+row+']['+elmCount+'][type]',
			'class' : 'form-control',
		});
		// insert selectable options from "selectOpts" array
		$.each( selectOpts, function(i,v) {
			$("<option />", {'value':i,'text':v}).appendTo(selectList);
		});
		// SET SELECTED TYPE
		$(selectList).val(type).bind('change', function(e) {
			e.preventDefault();
			castTypeField($(this));
		});
		return 	selectList;
	};
	
	var _getFieldRows = function(row,elmCount,type) {
		var fieldData, listOpt;
		var _data = $('<div>');
		for( var i = 0, len = fieldOpts.length; i < len; i++ ) {
			if( fieldOpts[i][0] === type ) {
				fieldData = fieldOpts[i];
				break;
			};
		};
		$.each(fieldData[1], function(i,v) {
			_data
			.append($('<div>', {
				'class' : 'form-group',
			})
				.append($('<label>', {
					'class' : 'col-sm-3 control-label',
					'for' : 'field_'+row+'_'+elmCount+'_'+v[0],
					'text' : makeTitle(v[0])+' :',
				}))
				.append($('<div>', {
					'class' : 'col-sm-9',
				})
					.append(generateLineFields(row,elmCount,v))
				)
				.append('<div class="clearfix"></div>')
			)
			;
			
		});
		return _data;
	};
	
	var createRowHeader = function(rowCount) {
		var _data = 
			$('<div>', {
				'class'	: 'headContainer',
			})
				.append($('<h1>', {
					'text' : rowLabel+' '+((rowCount/1)+1)
				})
					/*
					.bind('mousedown', function(e) {
						e.preventDefault();
						$('.addElementContainer, ._element_node').hide();
					})
					.bind('mouseup', function(e) {
						e.preventDefault();
						$('.addElementContainer, ._element_node').show();
					})
					*/
				)
				.append($('<a>', {
					'href' : '#',
					'data-id' : rowCount,
					'role'	: 'button',
					'class' : 'btn btn-default btn-sm btnDeleteRow',
				})
					.append('<span class="glyphicon glyphicon-remove"></span>')
					.bind('click', function(e) {
						e.preventDefault();
						bindRemoveRow($(this));
					})
					.disableSelection()
				);
		return _data;
	};
	
	var bindRemoveRow = function(rel) {
		var countRows = $('._rowContainer').length;
		if (countRows == 1) {
			alert('seriously?');
		} else {
			var _q = 'SOME NOTICE HERE...';
			if (confirm(_q)) {
				rel.unbind('click');
				$('#rowBlock_'+rel.data('id')).remove();
				reorderRowHeaders();
			};
		};
	};
	
	var reorderRowHeaders = function() {
		var x = 1;
		$('._rowContainer .headContainer').each(function(){
			$(this).find('h1').text(rowLabel+' '+x);
			x++;
		});
		doPostChecks();
	};

	var addNewRow = function() {
		
		//var rowCount = $('#rowcount').val();
		
		doStuffContainer
			.sortable({
				handle : '.headContainer h1',
				axis : 'y',
				revert: true,
				forcePlaceholderSize: false,
				cancel: '.cancel',
				forceHelperSize: true,
				helper: 'original',
				placeholder: 'header-sort-placeholder',
				opacity: 0.5,
				delay: 150,
				start: function( event, ui ) {
					ui.placeholder.height('42px');
					ui.helper.outerHeight('42px');
					$('.addElementContainer, ._element_node').hide();
				},
				stop: function( event, ui ) {
					$('.addElementContainer, ._element_node').show();
				},
				update: function( event, ui ) {
					reorderRowHeaders();
				},
			})
			.disableSelection()
			.append($('<div>', {
				'id' : 'rowBlock_'+rowCount,
				'data-id' : rowCount,
				'class' : 'well _rowContainer',
			})
				// insert ROW HEADERS
				.append(createRowHeader(rowCount))
				// insert NEW ELEMENT BTNS
				.append(createElementAddBtn(rowCount))
				.append($('<div>', {
					'id'	: 'element_node_'+rowCount,
					'class'	: '_element_node',
				})
				.sortable({
					handle : '.eheadContainer h1',
					items: '._elementContainer',
					cancel: '.cancel',
					connectWith: '._element_node',
					dropOnEmpty: true,
					revert: true,
					forcePlaceholderSize: false,
					forceHelperSize: true,
					placeholder: 'element-sort-placeholder',
					opacity: 0.5,
					delay: 150,
					start: function( event, ui ) {
						ui.placeholder.height('32px');
						ui.helper.outerHeight('32px');
						$('._elFieldContainer').hide();
					},
					beforeStop: function (event, ui) {
						//$(ui).addClass('cancel');
						//console.log(ui);
						//return false;
						//$( ui.item ).sortable('cancel');
					},
					sort: function( event, ui ) {
						//return false;
					},
					stop: function( event, ui ) {
						$('._elFieldContainer').show();
					},
					update: function( event, ui ) {
						doPostChecks();
					},
					receive: function (event, ui) {
						var _me = $(ui.item);
						var curRow	= _me.parents('div._rowContainer').data('id');
						
						// UPDATE ROW REF ID
						_me.attr('data-id',curRow);
						
						// UPDATE LABEL ROW ID
						var _meLabels = _me.find('label');
						$.each( _meLabels, function(i,v) {
							var _tmpName = $(v).attr('for').split('_');
							var _newID		= _tmpName[0]+'_'+curRow+'_'+_tmpName[2]+'_'+_tmpName[3];
							$(v).attr('for',_newID);
						});		
							
						// UPDATE FIELDS ROW ID			
						var _switchFields = ['select','input','textarea'];
						$.each( _switchFields, function(a,b) {
							var _meInputs = _me.find(b);
							$.each( _meInputs, function(i,v) {
								var _tmpName = $(v).attr('id').split('_');
								// LOOK OUT FOR OPTIONS LISTS
								if (_tmpName[3] === 'options') {
									var _newID		= _tmpName[0]+'_'+curRow+'_'+_tmpName[2]+'_'+_tmpName[3]+'_'+_tmpName[4]+'_'+_tmpName[5];
									var _newName	= _tmpName[0]+'['+curRow+']['+_tmpName[2]+']['+_tmpName[3]+']['+_tmpName[4]+']['+_tmpName[5]+']';
								} else {
									var _newID		= _tmpName[0]+'_'+curRow+'_'+_tmpName[2]+'_'+_tmpName[3];
									var _newName	= _tmpName[0]+'['+curRow+']['+_tmpName[2]+']['+_tmpName[3]+']';
								};
								$(v).attr('id',_newID);
								$(v).attr('name',_newName);
							});						
							
						});
					},
				})
				.disableSelection()
				)
			);
		
		rowCount++;
			
	};
	
	var createElementAddBtn = function(rowCount) {
		
		var selectList, listOpt;
		
		// create a select list object
		selectList	= $('<select>', {
			'id' : 'select_node_'+rowCount,
			'class' : 'btn btn-default btn-xs',
		});
		
		// insert selectable options from "selectOpts" array
		$.each( selectOpts, function(i,v) {
		  $("<option />", {value: i, text: v}).appendTo(selectList);
		});
		
		var _data = 
			// create container div object
			$('<div>', {
				'class'	: 'addElementContainer',
			})
				.disableSelection()
				// create 'Add Elements' button object
				.append($('<a>', {
					'href' : '#',
					'data-id' : rowCount,
					'role'	: 'button',
					'class' : 'btn btn-default btn-xs',
				})
					// add glyphicon
					.append('<span class="glyphicon glyphicon-plus"></span> Add Element')
					// attach button action "bindActAddElement"
					.bind('click', function(e) {
						e.preventDefault();
						bindActAddElement($(this));
					})
				)
				// add fiels select container object
				.append($('<div>', {
					'class'	: 'newElementContainer',
				})
				// hide
				.hide()
					// insert SELECT LIST
					.append(selectList)
					// add space
					.append('&nbsp;')
					// create 'Insert Elements' button object
					.append($('<a>', {
						'href' : '#',
						'id' : 'insert_node_'+rowCount,
						'data-id' : rowCount,
						'role'	: 'button',
						'class' : 'btn btn-default btn-xs',
					})
						// add glyphicon
						.append('<span class="glyphicon glyphicon-plus"></span> Insert')
					)
				)
			;
		return _data;
	};
	
	var bindActAddElement = function(rel) {
		
		var _this	= rel;
		var _id		= rel.data('id');
		var _q		= 'Please select a field to insert into this row';
		
		// to do : check for MAX 4 elements
		
		_this.parent('div').addClass('_isHigh');
		_this.hide();
		var _e = _this.next('div.newElementContainer').show();
		$('#insert_node_'+_id).bind('click', function(e) {
			e.preventDefault();
			var id	= $(this).data('id');
			var opt	= 'select_node_'+id;
			var row	= 'element_node_'+id;
			
			if ($('#'+opt).val() == 0) {
				alert(_q);
			} else {
				InsertElement($('#'+opt).val(),row);
				$(this).unbind('click');
				
				// to do GLOBAL RESET OPTIONS AND CHECK FOR MAX ELEMENTS
				$('#'+opt).val(0);
				$(_e).hide();
				_this.parent('div').removeClass('_isHigh');
				_this.show();
				
			};
			
		});
		
	};
	
	var bindActRemoveElement = function(rel) {
		var _q = 'SOME NOTICE HERE...';
		if (confirm(_q)) {
			rel.unbind('click');
			$('#elementBlock_'+rel.data('id')).remove();
		};
		doPostChecks();
	};
	
	var castTypeField = function(rel) {
		
		// define containers
		var container	= $(rel).parents('._elementContainer');
		var type		= $(rel).val();
		var _id			= container.attr('id');
		var _c			= _id.split('_');
		var _r			= container.attr('data-id');
				
		// get all form data
		var _inputs	= $('#'+_id+' :input');
		
		var _values	= new Array();
		var _optK		= new Array();
		var _optV		= new Array();
		
		// STORE EXISTING DATA
		$(_inputs).each(function(i,v) {
			var _n	= v.id.split('_');
			var _x;
			if (_n[3] === 'options') { // HANDLE OPTIONS LIST
				_x	= [_n[4],v.value];
				if (_n[5] === 'key') {
					//if (v.value != '')
						// push into KEYS array
						_optK.push(v.value);
				} else {
					//if (v.value != '')
						// push into VALUES array
						_optV.push(v.value);
				};
			} else if (_n[3] === 'read_only' || _n[3] === 'multiple' || _n[3] === 'autocomplete') { // HANDLE CHECKBOXES
				if (v.checked == true) {
					_x	= [_n[3],v.value];
					_values.push(_x);
				};
			} else {
				if (v.value != '') {
					_x	= [_n[3],v.value];
					_values.push(_x);
				};
			};
		});
		
		// update block title
		$('#'+_id+' .eheadContainer h1').html(findElName(type));
		
		// clear our working area
		var _data = $('#'+_id+' ._elFieldContainer').empty().append(_makeFormBlock(_r,elmCount,type));
		
		// repopulate data
		if (_values.length > 0) {
			$.each(_values, function(i,v){
				var _field = $('#field_'+_r+'_'+elmCount+'_'+v[0]);
				if ( _field.is(':checkbox') ) {
					if (v[1] == 1)
						_field.attr('checked',1);
				} else {
					_field.val(v[1]);
				};
			});
		};
		
		if (type === 'select' || type === 'radio' || type === 'checkbox') {
			if (_optK.length == 0 && _optV.length == 0) {
				$('#optionContainer_'+_r+'_'+elmCount+'_options')
					.append(bindAddElementOption(_r,elmCount));
			} else {
				$.each(_optK, function(i,v){
					$('#optionContainer_'+_r+'_'+elmCount+'_options')
						.append(bindAddElementOption(_r,elmCount,v,_optV[i]));
				});
			};
		};
		
		elmCount++;

	};
	
	var fieldHeaders = function(row,type) {
		var _data = $('<div>', {'class' : '_elFieldContainer'}).append(_makeFormBlock(row,elmCount,type));
		return _data;
	};
	
	var InsertElement = function(type,row) {
		var rowNum = $('#'+row).parent('div._rowContainer').data('id');
		$('#'+row).append($('<div>', {
			'id' : 'elementBlock_'+elmCount,
			'data-id' : rowNum,
			'class' : '_elementContainer',
		})
			.append(createElementHeader(type,elmCount))
			.append(fieldHeaders(rowNum,type))
		);
		if (type === 'select' || type === 'radio' || type === 'checkbox') {
			$('#optionContainer_'+rowNum+'_'+elmCount+'_options')
				.append(bindAddElementOption(rowNum,elmCount));
		};
		elmCount++;
		//alert('xx');
		doPostChecks();
	};
	
	var bindRemoveElementOption = function(rel) {
		$(rel).parent('div._optionsContainer').remove();
	};

	var bindAddElementOption = function(_r,_e,_key,_value) {
		var _v = 'options';
		$('#optionContainer_'+_r+'_'+_e+'_'+_v)
			.append($('<div>', {
				'class' : '_optionsContainer',
			})
				.append($('<input>', {
					'type' : 'text',
					'id' : 'field_'+_r+'_'+_e+'_'+_v+'_'+optCount+'_key',
					'name' : 'field['+_r+']['+_e+']['+_v+']['+optCount+'][key]',
					'class' : 'form-control',
					'style' : 'width:40%; display:inline-block;',
					'value' : _key,
					'placeholder' : 'key'
				}))
				.append('&nbsp;')
				.append($('<input>', {
					'type' : 'text',
					'id' : 'field_'+_r+'_'+_e+'_'+_v+'_'+optCount+'_value',
					'name' : 'field['+_r+']['+_e+']['+_v+']['+optCount+'][value]',
					'class' : 'form-control',
					'style' : 'width:40%; display:inline-block;',
					'value' : _value,
					'placeholder' : 'value'
				}))
				.append('&nbsp;')
				.append($('<a>', {
					'href' : '#',
					'role'	: 'button',
					'class' : 'btn btn-default btn-xs',
					'style' : 'display:inline-block;',
					'tabindex' : '-1',
				})
					.append('<span class="glyphicon glyphicon-remove"></span>')
					.bind('click', function(e) {
						e.preventDefault();
						bindRemoveElementOption($(this));
					})
				)
			);
			optCount++;
	};
	
	var generateLineFields = function(_r,_e,_v) {
		var eLeMeNt;
		switch (_v[1]) {
			case 'txtarea':	// text area (4 rows)
				eLeMeNt = $('<textarea>', {
					'id' : 'field_'+_r+'_'+_e+'_'+_v[0],
					'name' : 'field['+_r+']['+_e+']['+_v[0]+']',
					'class' : 'form-control',
					'rows' : 4,
					'value' : '',
				});
				break;
			case 'cb':			// check boxes
				eLeMeNt = $('<input>', {
					'type' : 'checkbox',
					'id' : 'field_'+_r+'_'+_e+'_'+_v[0],
					'name' : 'field['+_r+']['+_e+']['+_v[0]+']',
					'class' : '',
					'value' : 1,
				});
				break;
			case 'opt':			// dynamic options
				eLeMeNt = $('<div>', {
					'id' : 'optionContainer_'+_r+'_'+_e+'_'+_v[0],
					'class' : 'optionContainer',
				})
				.sortable({
					//handle : '.optionContainer',
					axis : 'y',
					containment : 'parent',
					revert: true,
					forcePlaceholderSize: true,
					//placeholder: 'header-sort-placeholder',
					opacity: 0.5,
				})
				.disableSelection()
					.append($('<a>', {
						'href' : '#',
						'role'	: 'button',
						'class' : 'btn btn-default btn-sm _optionsContainerBtn',
						'style' : 'display:inline-block;',
					})
						.append('<span class="glyphicon glyphicon-plus-sign"></span>')
						.bind('click', function(e) {
							e.preventDefault();
							bindAddElementOption(_r,_e);
						})
					)
					
					/*
					.append($('<div>', {
						'class' : '_optionsContainer',
					})
						.append($('<input>', {
							'type' : 'text',
							'id' : 'field_'+_r+'_'+_e+'_'+_v[0]+'_'+optCount+'_key',
							'name' : 'field['+_r+']['+_e+']['+_v[0]+']['+optCount+'][key]',
							'class' : 'form-control',
							'style' : 'width:40%; display:inline-block;',
							'value' : '',
							'placeholder' : 'key'
						}))
						.append('&nbsp;')
						.append($('<input>', {
							'type' : 'text',
							'id' : 'field_'+_r+'_'+_e+'_'+_v[0]+'_'+optCount+'_value',
							'name' : 'field['+_r+']['+_e+']['+_v[0]+']['+optCount+'][value]',
							'class' : 'form-control',
							'style' : 'width:40%; display:inline-block;',
							'value' : '',
							'placeholder' : 'value'
						}))
						.append('&nbsp;')
						.append($('<a>', {
							'href' : '#',
							'role'	: 'button',
							'class' : 'btn btn-default btn-xs',
							'style' : 'display:inline-block;',
							'tabindex' : '-1',
						})
							.append('<span class="glyphicon glyphicon-remove"></span>')
							.bind('click', function(e) {
								e.preventDefault();
								bindRemoveElementOption($(this));
							})
						)
					)
					*/
							
				;
				optCount++;
				break;
			case 'txts':		// short text field
				eLeMeNt = $('<input>', {
					'type' : 'text',
					'id' : 'field_'+_r+'_'+_e+'_'+_v[0],
					'name' : 'field['+_r+']['+_e+']['+_v[0]+']',
					'class' : 'form-control',
					'style' : 'width:25%;',
					'value' : '',
				});
				break;
			default:			// regular text field
				eLeMeNt = $('<input>', {
					'type' : 'text',
					'id' : 'field_'+_r+'_'+_e+'_'+_v[0],
					'name' : 'field['+_r+']['+_e+']['+_v[0]+']',
					'class' : 'form-control',
					'value' : '',
				});
		};
		return eLeMeNt;
	};
	
	var createElementHeader = function(type,elmCount) {
		var _data = 
			$('<div>', {
				'class'	: 'eheadContainer',
			})
				.append($('<h1>', {
					'text' : findElName(type)
				})
					/*
					.bind('mousedown', function(e) {
						e.preventDefault();
						if(! $(this).parents('._elementContainer').hasClass('cancel'))
							$('._elFieldContainer').hide();
					})
					.bind('mouseup', function(e) {
						e.preventDefault();
						if(! $(this).parents('._elementContainer').hasClass('cancel'))
							$('._elFieldContainer').show();
					})
					*/
				)
				.append($('<a>', {
					'href' : '#',
					'data-id' : elmCount,
					'role'	: 'button',
					'class' : 'btn btn-default btn-xs btnDeleteRow',
				})
					.append('<span class="glyphicon glyphicon-remove-circle"></span>')
					.bind('click', function(e) {
						e.preventDefault();
						bindActRemoveElement($(this));
					})
					.disableSelection()
				)
			;
		return _data;
	};
	
	var doPostChecks = function() {
		/*
		var Rows	= $('._rowContainer');
		$.each(Rows, function(i,v) {
			var id	= $(v).attr('id');
			var countElements = $('#'+id).find('._elementContainer');
			if ((countElements.length/1) <= maxElements) {
				console.log(countElements);
				console.log($('#'+id).find('.addElementContainer'));
				$('#'+id).find('.addElementContainer').show();
			} else {
				console.log(countElements);
				console.log($('#'+id).find('.addElementContainer'));
				$('#'+id).find('.addElementContainer').hide();
			};
		});
		*/
		console.log('doPostChecks()');
	};
	
	// WHERE IT ALL STARTS
	$('.btnAddNewRow').click(function(e) {
		e.preventDefault();
		addNewRow();
	});
	
	// EMPTY? start a row
	addNewRow();

});
</script>
@stop

{? $title = 'Form' ?}

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="">{{ link_to_action('Admin_HomeController@index', Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">{{ link_to_action('Admin_FormController@index', $title) }}</li>
  </ol>
@stop

@section('content')
	<h2 class="page-header">{{{ $title }}}
        <div class="pull-right"><a href="#" role="button" class="btn btn-success btn-sm btnAddNewRow"><span class="glyphicon glyphicon glyphicon-plus-sign"></span> Add New Row</a></div>
	</h2>
    {{ Form::open(array('action'=>'Admin_FormController@checkForm','role'=>'form','class'=>'','id'=>'FormEdit')) }}
		{{-- form::text('rowcount',0,array('id'=>'rowcount')) --}}
        <div id="doStuffContainer"></div>
        {{ AdminFormHelper::_btnAction('Save') }}
    {{ Form::close() }}
@stop
