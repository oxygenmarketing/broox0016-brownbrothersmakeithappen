@extends('admin._tmpl.layout')

@section('_scripts')
@stop

@section('_styles')
@stop

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="active">{{ link_to_action('Admin_HomeController@index',Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">Manage SMS</li>    
  </ol>
@stop

@section('content')

<ul class="nav nav-tabs" role="tablist">
  <li class="active"><a href="#main" role="tab" data-toggle="tab">Main Setup</a></li>
  <li><a href="#messages" role="tab" data-toggle="tab">Messages</a></li>
</ul>

@if($errors->first() != '')
	<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
@endif

{{ Form::model(	$sms_setup,		array('method'=>'POST',	'action'=>array('Admin_SmsController@smsUpdate'),	'class'=>'form-horizontal'	) ) }}
<div class="tab-content">
    <div class="tab-pane tab-pane-table active" id="main">
        {{ AdminFormHelper::_txtField($errors, 'smsGW_host','SMS Host','', $sms_setup['smsGW_host']) }}
        {{ AdminFormHelper::_txtField($errors, 'smsGW_user','SMS Username','', $sms_setup['smsGW_user']) }}
        {{ AdminFormHelper::_txtField($errors, 'smsGW_pass','SMS Password','', $sms_setup['smsGW_pass']) }}
       
    </div>
    <div class="tab-pane tab-pane-table" id="messages">
        {{ AdminFormHelper::_txtField($errors, 'sms_pre','Pre Promo Message','', $sms_setup['sms_pre']) }}
        {{ AdminFormHelper::_txtField($errors, 'sms_winner','Winner Message','', $sms_setup['sms_winner']) }}
        {{ AdminFormHelper::_txtField($errors, 'sms_thanks','Thank You Message','', $sms_setup['sms_thanks']) }}
        {{ AdminFormHelper::_txtField($errors, 'sms_invalid','Invalid Entry Message','', $sms_setup['sms_invalid']) }}
        {{ AdminFormHelper::_txtField($errors, 'sms_limit','Entry Limit Message','', $sms_setup['sms_limit']) }}
        {{ AdminFormHelper::_txtField($errors, 'sms_post','Post Promo Message','', $sms_setup['sms_post']) }}
        {{ AdminFormHelper::_txtField($errors, 'sms_unique','Invalid Unique Code Message','', $sms_setup['sms_unique']) }}
        {{ AdminFormHelper::_txtField($errors, 'sms_barcode','Invalid Barcode Message','', $sms_setup['sms_barcode']) }}
	</div> 

</div>
    {{ AdminFormHelper::_btnAction('Save') }}  
	{{ Form::close() }}

@stop