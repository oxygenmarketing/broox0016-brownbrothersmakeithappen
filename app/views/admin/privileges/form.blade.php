@extends('admin._tmpl.layout')

@section('_scripts')
@stop

@section('_styles')
@stop

{? $title = (isset($_data)) ? 'Edit User - '.$_data->username : 'Create New User' ?}

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="">{{ link_to_action('Admin_HomeController@index', Lang::get('crudadmin.gui.route.home'), NULL, array()) }}</li>
    <li class="">{{ link_to_action('Admin_PrivilegesController@index', 'Users', NULL, array()) }}</li>
    <li class="active">{{ $title }}</li>
  </ol>
@stop

@section('content')

	<h2 class="page-header">{{{ $title }}}</h2>
    
    {{-- _e::pre($errors->has('email')) --}}
    
	<div class="table-responsive">
    	
	@if (isset($_data))
		{{ Form::model(	$_data,		array('method'=>'PATCH',	'route'=>array('admin.privileges.update', $_data->id),	'class'=>'form-horizontal'	) ) }}
	@else
		{{ Form::model(	new User,	array('method'=>'POST',	'route'=>array('admin.privileges.store'),					'class'=>'form-horizontal'	) ) }}
	@endif
        
        	{{ AdminFormHelper::_txtField($errors,'first_name','First Name', 'First Name') }}
        	{{ AdminFormHelper::_txtField($errors,'last_name','Last Name', 'Last Name') }}
        	{{ AdminFormHelper::_txtField($errors,'username','Username', 'Username') }}
        	{{ AdminFormHelper::_txtField($errors,'position','Position', 'Position') }}
        	{{ AdminFormHelper::_txtField($errors,'phone','Phone', 'Phone') }}
        	{{ AdminFormHelper::_txtField($errors,'email','Email Address','Email Address') }}
            
        	{{ AdminFormHelper::_pwField($errors,'password','Password') }}
        	{{ AdminFormHelper::_pwField($errors,'password_confirmation','Password (again)') }}
            
			@if (Auth::user()->user_type == 'ADMIN')
        		{{ AdminFormHelper::_selField($errors,'user_type','User Type',array('ADMIN'=>'Administrator','MANAGER'=>'Managers','REGISTERED'=>'Registered','CLIENT'=>'Client')) }}
			@elseif (Auth::user()->user_type == 'MANAGER')
        		{{ AdminFormHelper::_selField($errors,'user_type','User Type',array('MANAGER'=>'Managers','REGISTERED'=>'Registered','CLIENT'=>'Client')) }}
			@endif
            
        	{{ AdminFormHelper::_cbField($errors,'active','Active') }}
            
			@if (isset($_data))
        	{{ AdminFormHelper::_timeStamp($_data) }}
			@endif
            
        	{{ AdminFormHelper::_btnAction('Save') }}
            
		{{ Form::close() }}
    
	</div>
  
@stop