@extends('admin._tmpl.layout')

@section('_scripts')
@stop

@section('_styles')
@stop

{? $title = 'Users' ?}

@section('_breadcrumbs')
  <ol class="breadcrumb">
    <li class="">{{ link_to_action('Admin_HomeController@index', Lang::get('crudadmin.gui.route.home')) }}</li>
    <li class="active">{{ link_to_action('Admin_PrivilegesController@index', $title) }}</li>
  </ol>
@stop

@section('content')

	<h2 class="page-header">{{{ $title }}}
		<a href="{{ action('Admin_PrivilegesController@create') }}" role="button" class="btn btn-primary pull-right btn-sm"><span class="glyphicon glyphicon-plus"></span> New</a>
	</h2>
  
    <nav class="navbar navbar-default navbar-filter">
        <div class="row">
            <div class="col-lg-4">{{ AdminHelper::renderSearchForm() }}</div>       
            <div class="col-lg-8">  
				  {{ Form::open(array('url'=>'/admin/filter','id'=>'frmFilter','class'=>'navbar-form navbar-filter navbar-right','role'=>'filter')) }}
					<strong>Filter :</strong>
                  {{ AdminHelper::filterList('active','Active',array('1'=>'Active','0'=>'Inactive')) }}
                  @if (Auth::user()->user_type == 'ADMIN')
                  {{ AdminHelper::filterList('user_type','Type',array('ADMIN'=>'Admin','MANAGER'=>'Manager')) }}
                  @endif
					<div class="btn-group">
						<button class="btn btn-default btn-sm" type="submit"><span class="glyphicon glyphicon-filter"></span></button>
						<?php /*<button class="btn btn-default btn-sm _reset" type="reset"><span class="glyphicon glyphicon-remove"></span></button>*/ ?>
					</div>
                {{ Form::close() }}
            </div>       
        </div>
    </nav><!-- /navbar-filter --> 
     
    <div class="table-responsive">
		<table class="table table-striped table-bordered table-fixed-head">
			<thead>
				<tr>
					<th width="1%" nowrap>{{ AdminHelper::drawHeaderRow('id','ID') }}</th>
					<th nowrap>{{ AdminHelper::drawHeaderRow('username','Username') }}</th>
					<th nowrap>{{ AdminHelper::drawHeaderRow('email','Email') }}</th>
					<th nowrap width="1%">{{ AdminHelper::drawHeaderRow('user_type','Type') }}</th>
					<th width="1%" style="text-align:center;" nowrap>{{ AdminHelper::drawHeaderRow('active','Active') }}</th>
					<th width="1%" nowrap>Actions</th>
				</tr>  
			</thead>
			<tbody>
				@foreach ($_data->_d as $d)
				<tr>
					<td align="center">{{{ $d->id }}}</td>
					<td>{{{ $d->username }}}</td>
					<td>{{{ $d->email }}}</td>
					<td>{{{ $d->user_type }}}</td>
					<td align="center">{{ AdminHelper::toggleState($d->id,$d->active,'publish','Active') }}</td>
					<td nowrap>
						{{ AdminHelper::btnEdit($d->id,'View/Edit') }}
						{{ AdminHelper::btnDelete($d->id,1) }}
					</td>
				</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<td colspan="99">Total {{ $_data->_total }} items.</td>
				</tr>
			</tfoot>
		</table>
    </div>
    
    {{ $_data->_d->links() }}
  
@stop