@extends('_tmpl.layout')

@section('_scripts')

<script>

	_fbCallback = function() {
		window.location.href = '{{ URL::to('/') }}';
	}
</script>

@stop

@section('_styles')
@stop

@section('_body') class="home" @stop

@section('content')


{{ SiteHelper::getContent('limit_page') }}


@stop
