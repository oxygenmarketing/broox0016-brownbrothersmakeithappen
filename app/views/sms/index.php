<form name="sms" action="/sms" method="POST">
    <table width="500" border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td width="150"><strong>MSISDN:</strong></td>
        <td><input type="text" name="MSISDN" value=""></td>
      </tr>
      <tr>
        <td><strong>Message Content:</strong></td>
        <td><input type="text" name="Content" value="ABC1234 06844 Stuart Owen 3001" style="width: 250px;"></td>
      </tr>
      <tr>
        <td><strong>Testing Type:</strong></td>
        <td>
            <select name="type" size="1" id="type">
                <option value="">Normal</option>
                <option value="test">[test] Test entry</option>
                <option value="pre">[pre] Pre Promo Message</option>
                <option value="post">[post] Post Promo Message</option>
                <option value="winner">[winner] Winner Message</option>
                <option value="thankyou">[thankyou] Thank You Message</option>
                <option value="limit">[limit] Entry Limit Message</option>
                <option value="invalid">[invalid] Invalid Unique Code Message</option>
            </select>
        </td>
      </tr>
      <tr>
        <td><strong>Shortcode:</strong></td>
        <td><input type="text" name="Shortcode" value="19711444"></td>
      </tr>
      <tr>
        <td><strong>Channel:</strong></td>
        <td>
            <select name="Channel" size="1" id="Channel">
                <option value="AUSTRALIA.OPTUS">AUSTRALIA.OPTUS</option>
                <option value="AUSTRALIA.TELSTRA">AUSTRALIA.TELSTRA</option>
                <option value="AUSTRALIA.VIRGIN">AUSTRALIA.VIRGIN</option>
                <option value="AUSTRALIA.VODAFONE">AUSTRALIA.VODAFONE</option>
            </select>
        </td>
      </tr>
      <tr>
        <td><strong>Campaign ID:</strong></td>
        <td><input type="text" name="CampaignID" value="82460" readonly></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type="submit" value="Send SMS"></td>
      </tr>
    </table>
    <input type="hidden" name="Reference" value="1.280.<?php echo $ref_no;?>.1">
    <input type="hidden" name="DataType" value="0">
    <input type="hidden" name="DateReceived" value="<?php echo time();?>">
    <input type="hidden" name="Trigger" value="*">
</form>