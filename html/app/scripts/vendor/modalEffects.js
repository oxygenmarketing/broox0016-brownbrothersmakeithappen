/**
 * modalEffects.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2013, Codrops
 * http://www.codrops.com
 */
var ModalEffects = (function() {

	function init() {

		$( '.md-trigger' ).each( function() {
			el = this;

			var modal = document.querySelector( '#' + el.getAttribute( 'data-modal' ) ),
				close = modal.querySelector( '.md-close' );

			function removeModal( hasPerspective ) {
				classie.remove( modal, 'md-show' );
				$('body, html').removeClass('modal-open');

				if( hasPerspective ) {
					classie.remove( document.documentElement, 'md-perspective' );
				}
			}

			function removeModalHandler() {
				removeModal( classie.has( el, 'md-setperspective' ) ); 
			}

			$(this).click( function( ev ) {
				classie.add( modal, 'md-show' );
				$('body, html').addClass('modal-open');

				if( classie.has( el, 'md-setperspective' ) ) {
					setTimeout( function() {
						classie.add( document.documentElement, 'md-perspective' );
					}, 25 );
				}
				ev.preventDefault();
    			return false;
			});

			$('.md-close, .md-overlay').click( function( ev ) {
				ev.stopPropagation();
				removeModalHandler();
				ev.preventDefault();
    			return false;
			});
		} );
	}

	init();

})();