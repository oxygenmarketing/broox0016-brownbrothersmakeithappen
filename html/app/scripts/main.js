'use strict';

$(document).ready(function () {
	// center content in the middle of the page if entire page content can fit within size of browser window
	function centerPageContent() {
		if(($(window).height()) > $('body .centered__inner').outerHeight()+140) {
			$('body .centered').addClass('centered--fixed');
		}
		else {
			$('body .centered').removeClass('centered--fixed');
			$('footer').css('margin-top', 0);
		}

			$('body.home .centered__inner').css('margin-bottom', $('footer .container--footer').outerHeight()-60);
	$('body.form .centered__inner').css('padding-bottom', $('footer .container--footer').outerHeight()+40);
		//$('body .centered__inner').css('padding-bottom', $('footer').outerHeight());
	}
	$(window).resize(function () {
	    waitForFinalEvent(function(){
	    	centerPageContent();
	    }, 0, 'centered');
	});
	centerPageContent();


	// validate form
	$.listen('parsley:field:validate', function () {
      validateFront();
    });

    $.listen('parsley:field:success', function () {
      validateFront();
    });

    $('#comp-form .button').on('click', function () {
      $('#comp-form').parsley().validate();
      validateFront();
    });

    var validateFront = function () {
    	centerPageContent();	
        if (true === $('#comp-form').parsley().isValid()) {
            $('form .error-message').fadeOut();
        } else {
        	$('form .error-message').fadeIn();
        }
    };

    $('a.md-trigger').on('click', function (e) { 
    	e.preventDefault();
    	return false;
    });

	var waitForFinalEvent = (function () {
		var timers = {};
		return function (callback, ms, uniqueId) {
	    	if (!uniqueId) {
	      		uniqueId = 'error';
	    	}
	    	if (timers[uniqueId]) {
	      		clearTimeout (timers[uniqueId]);
	    	}
	    	timers[uniqueId] = setTimeout(callback, ms);
	  	};
	})();

	$('.md-').on('show', function () {
	  $('body').addClass('modal-open');
	}).on('hidden', function () {
	  $('body').removeClass('modal-open');
	}); 


	$('#dob').datepicker({
			dateFormat: 'dd/mm/yy',
		    changeMonth: true,
            changeYear: true,
            yearRange: '1890:1998',
            defaultDate: '07/09/1997',
            maxDate: '01/11/1997'
	});

});