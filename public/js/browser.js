(function ($) {
	$.fn.supportedBrowser = function(sb, callback){
		var browserType = '';
		var browserVersion = '';
		var browserPage = '';
		var support = true;

		var nAgt = navigator.userAgent;

		var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0; // Opera 8.0+
		var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
		var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0; // At least Safari 3+:
		var isChrome = !!window.chrome && !isOpera;	// Chrome 1+
		var isIE = /*@cc_on!@*/false || !!document.documentMode;   // At least IE6

		if(isChrome) {
			if( ! isSupported(sb.chrome)){
				var support = false;
			}else{
				var support = true;
			}

			var browserType = 'Chrome';
			var browserPage = 'https://www.google.com/intl/en/chrome/browser/';
		}

		if(isFirefox) {
			if( ! isSupported(sb.firefox)){
				var support = false;
			}else{
				var support = true;
			}

			var browserType = 'Firefox';
			var browserPage = 'http://www.mozilla.org/en-US/firefox/new/';
		}

		if(isIE) {
			if( ! isSupported(sb.ie)){
				var support = false;
			}else{
				var support = true;
			}
			
			var browserType = 'IE';
			var browserPage = 'http://windows.microsoft.com/en-us/internet-explorer/download-ie';
		}

		if(isOpera) {
			if( ! isSupported(sb.opera)){
				var support = false;
			}else{
				var support = true;
			}

			var browserType = 'Opera';
			var browserPage = 'http://www.opera.com/computer';
		}

		if(isSafari) {
			if( ! isSupported(sb.safari)){
				var support = false;
			}else{
				var support = true;
			}

			var browserType = 'Safari';
			var browserPage = 'http://support.apple.com/downloads/#safari';
		}

		if(callback != undefined) {
			var browserVersion = checkVersion();
			callback({browserType: browserType, browserVersion: browserVersion, supported: support, browserUpdate: browserPage});
		}
		
		function isSupported(browserversion){
			if(browserversion != undefined) {
				var currentVersion = checkVersion();

				if(browserversion.eq != undefined && parseFloat(browserversion.eq) == parseFloat(currentVersion)) {
					return true;
				} 

				if(browserversion.gt != undefined && browserversion.lt != undefined) {
					if(parseFloat(currentVersion) > parseFloat(browserversion.gt) && parseFloat(currentVersion) < parseFloat(browserversion.lt)) {
						return true;
					}
				}

				if(browserversion.gt != undefined && browserversion.lte != undefined) {
					if(parseFloat(currentVersion) > parseFloat(browserversion.gt) && parseFloat(currentVersion) <= parseFloat(browserversion.lt)) {
						return true;
					}
				}

				if(browserversion.gte != undefined && browserversion.lt != undefined) {
					if(parseFloat(currentVersion) >= parseFloat(browserversion.gt) && parseFloat(currentVersion) < parseFloat(browserversion.lt)) {
						return true;
					}
				}

				if(browserversion.gte != undefined && browserversion.lte != undefined) {
					if(parseFloat(currentVersion) >= parseFloat(browserversion.gt) && parseFloat(currentVersion) <= parseFloat(browserversion.lt)) {
						return true;
					}
				}

				if(browserversion.gte != undefined) {
					if(parseFloat(currentVersion) >= parseFloat(browserversion.gte)) {
						return true;
					}
				}

				if(browserversion.lte != undefined) {
					if(parseFloat(currentVersion) <= parseFloat(browserversion.lte)) {
						return true;
					}
				}

				if(browserversion.lt != undefined) {
					if(parseFloat(currentVersion) < parseFloat(browserversion.lt)) {
						return true;
					}
				}

				if(browserversion.gt != undefined) {
					if(parseFloat(currentVersion) > parseFloat(browserversion.gt)) {
						return true;
					}
				}

				if($.isArray(browserversion)) {
					$.each(browserversion, function(i,v){
						if(parseFloat(currentVersion) == parseFloat(v)) {
							return true;
						}
					})
				} else if(parseFloat(currentVersion) == parseFloat(browserversion)) {
					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}
		}

		function checkVersion(){
			if((offSet = nAgt.indexOf("OPR")) != -1) { //opera
				fullVersion = nAgt.substring(offSet + 4);

				if ((offSet = nAgt.indexOf("Version")) != -1) {
					fullVersion = nAgt.substring(offSet + 4);
				}
			} else if((offSet = nAgt.indexOf("MSIE")) != -1) { //ie
				fullVersion = nAgt.substring(offSet + 5);
			} else if(nAgt.indexOf("Trident") != -1) { //ie
				var start = nAgt.indexOf("rv:") + 3;
				var end = start + 4;
				fullVersion = nAgt.substring(start, end);
			} else if ((offSet = nAgt.indexOf("Chrome")) != -1) {
				fullVersion = nAgt.substring(offSet + 7);
			} else if ((offSet = nAgt.indexOf("Safari")) != -1) {
				fullVersion = nAgt.substring(offSet + 7);

				if ((offSet = nAgt.indexOf("Version")) != -1) {
					fullVersion = nAgt.substring(offSet + 8);
				}
			} else if ((offSet = nAgt.indexOf("AppleWebkit")) != -1) {
				fullVersion = nAgt.substring(offSet + 7);

				if ((offSet = nAgt.indexOf("Version")) != -1) {
					fullVersion = nAgt.substring(offSet + 8);
				}
			} else if ((offSet = nAgt.indexOf("Firefox")) != -1) {
				fullVersion = nAgt.substring(offSet + 8);
			}

			// trim the fullVersion string at semicolon/space if present
			if ((ix = fullVersion.indexOf(";")) != -1) fullVersion = fullVersion.substring(0, ix);
			if ((ix = fullVersion.indexOf(" ")) != -1) fullVersion = fullVersion.substring(0, ix);

			return fullVersion;
		}
	};
})(jQuery);