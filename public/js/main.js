
(function ($) {
	$('a[data-toggle="modal"]').on('click', function(){
		
		_gaq.push(['_trackEvent', 'App', $(this).data('id'), 'CODO']);
		
		// update modal header with contents of button that invoked the modal
		$('#myModalLabel').html( $(this).data('modaltitle') );
		//fixes a bootstrap bug that prevents a modal from being reused
		$(this).removeData('bs.modal');
		$('.modal-content').load(
			$(this).attr('href'),
			function(response, status, xhr) {
				if (status === 'error') {
					$('.modal-body').html('<h2>Oh boy</h2><p>Sorry, but there was an error:' + xhr.status + ' ' + xhr.statusText+ '</p>');
				}
				return this;
			}
		);
	});
	
	//$('html, body').animate({ scrollTop: 0 }, 0);
	//window.parent.$("body").animate({scrollTop:0}, 0);	
	
})(jQuery);
