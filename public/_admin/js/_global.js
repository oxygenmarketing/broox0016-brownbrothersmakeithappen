
	"use strict";

	var pageTop				= function (){
	  return $('.navbar').height();
	}
	
	var initScrolling			=	function() {
		
		$(window).scroll(function(e) {
			
			var offsetY = $(window).scrollTop();
			
			if (offsetY >= 1000) {
				$('#top-link').show();
			} else if (offsetY < 1000 ) {
				$('#top-link').hide();
			}
			
		});
		
		$('#top-link').click(function() {
			animScrollTo( $(this).attr('href') );
		});
		
	};
	
	var animScrollTo			=	function(id, scrollFinishFunction) {
		var duration	= 500;
		var easing		= 'swing';
		var target		= $(id).offset().top;
		$("html:not(:animated),body:not(:animated)").animate({ scrollTop: target }, duration, easing);
	} ;	

	var srchClear				=	function() {
		$('#frmSearch ._reset').prop('disabled', (( $('#frmSearch #search').val() == '' ) ? true : false ) );
	};
	
	//	-----------------------------------------------------------------	//
	
	$(function() {
		
		// Prevent empty search terms
		$('#frmSearch').bind('submit', function(e) {
			if( $(this).find('#search').val() ) return true;
			return false;
		});
		// Reset search terms
		$('#frmSearch ._reset').bind('click', function(e) {
			e.preventDefault();
			$('#frmSearch #search').val('----------------------------------------------------------------------------------/reset');
			$('#frmSearch').submit();
			return false;
		});
		// Disable search reset button
		$('#frmSearch #search').bind('change', function(e) {
			srchClear();
		});
		srchClear();
		
		// frmSearch empty filter
		$('#frmFilter').bind('submit', function(e) {
			var err = 0;
			var _filters = $(this).find('select[role="filter"]');
			$(_filters).each(function() {
				if ($(this).val() == '-' || $(this).val() == '--') err++;
			});		
			if( err != _filters.length ) return true;
			return false;
		});
		
		// confirm Delete action
		$('._askQuestion').bind('click', function(e) {
			var _q		= '';
			var rel		= $(this).attr('rel');
			switch (rel) {
				case 'cancel':
					_q	= 'Are you sure you want to leave this page?\nAny changes will not be saved.';
					break;
				case 'delete':
					_q	= 'Are you sure you want to delete/remove this item?';
					break;
			}			
			if (confirm(_q)) { return true; };
			return false;
		});
			
	});
	
	$(document).ready(function() {
		
		if ($('table.table-fixed-head').length) {
			$('table.table-fixed-head').floatThead({
				scrollingTop: pageTop,
				//useAbsolutePositioning: false
			});	
			initScrolling();
		};
		
		$('.alert').delay(2500).animate({
			opacity: 0,
			//height: 0
		}, 500, function() {
			$(this).remove();
		});
		
		if ($('.form-control-feedback').length) {
			$('.form-control-feedback').tooltip();
		};

	});
