-- phpMyAdmin SQL Dump
-- version 4.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 20, 2015 at 01:50 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `houox0006_iw_upgrade`
--

-- --------------------------------------------------------

--
-- Table structure for table `barcode`
--

CREATE TABLE `barcode` (
  `id` int(10) NOT NULL,
  `full_barcode` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `first_set` varchar(5) NOT NULL,
  `second_set` varchar(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(5) NOT NULL,
  `title` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `title`, `name`, `value`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Footer Terms', 'footer_terms', '*Please retain product packaging, unique code and store receipts for entry verification and to claim your prize. Starts: 12:01AM 16/03/15 AEST; ends 11:59PM 14/06/15 AEST. Entry open to Aus. residents 18+. To enter: Purchase any one (1) specially marked Brown Brothers, Devil’s Corner, Tamar Ridge or Pirie bottle of wine from a participating store then send a text message in the required SMS format to 19711444 (SMS cost is 55c Inc. GST per msg sent) OR visit www.brownbrothers.com.au and complete online entry form. One entry per Qualifying Purchase, max 5 entries per person per day. Instant Win Prizes: There are nine-hundred and ten (910) Instant Win prizes available to be won, comprising of 10 mixed cases of Brown Brothers wine (valued at $105.70 ea.) to be won daily. Major Prize Draw: 11:00am (AEST) on 15/06/2015 at Oxygen Interactive, 117 Wellington St, St Kilda, VIC 3182. The first valid entry drawn from all entries received will receive a prize cheque for thirty-thousand dollars ($30,000 Inc. GST) payable only to the Major Prize winner.  Major Prize winner notified in writing and their details published in The Australian newspaper on 17/06/15. Promoter encourages responsible consumption of alcohol. For details on how to enter and full Terms and Conditions visit: www.brownbrothers.com.au. Promoter: Brown Brothers Milawa Vineyard  Pty Limited (ABN 56 005 349 235) of 239 Milawa-Bobinawarrah Rd, Milawa VIC 3678. Authorised under: NSW Permit No. LTPS/14/09788, SA Permit No.T14/2313, ACT Permit No.TP/14/04306, VIC Permit No.14/5926. SMS Service Provider is Oxygen Interactive Marketing. SMS Helpdesk: 1300 737 728 (standard call costs apply).', 1, '2015-02-26 10:44:30', '2015-02-26 10:44:30'),
(2, 'Entry Header', 'entry_header', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.</p>\n', 1, '2015-03-02 12:22:03', '2015-03-04 14:50:50'),
(3, 'Winner Page', 'winner_page', '<h1>Congratulations!</h1>\r\n<h2>You''re an Instant Winner of %%PRIZE%%!</h2>\r\n<p>An email has been sent to your email address provided with details on how to claim your prize.</p>\r\n<hr>\r\n<p>You prize claim number is:</p>\r\n<h2>%%PCN%%</h2>\r\n<p>Remember to hold on to your proof of purchase. Proof of purchase for each entry made will be required to claim your prize.</p>', 1, '2015-03-02 12:30:50', '2015-03-02 13:44:29'),
(4, 'Thanks Page', 'thanks_page', '<h2>Thank you for your entry!</h2>\r\n<h3>You''re not an instant winner this time but you''re in the running to win the major prize,</h3>\r\n<h1>$30,000!</h1>\r\n<p>Remember to hold on to your proof of purchase. Proof of purchase for each entry made will be required to claim your prize.</p>', 1, '2015-03-02 13:49:26', '2015-05-19 16:46:08'),
(5, 'Limit Page', 'limit_page', '<h2>You''ve reached your limit!</h2>\r\n                        <h3>You are only able to enter this promotion 5 times per day</h3>\r\n                        <h3>Come back tomorrow to enter again!</h3>', 1, '2015-03-02 13:54:13', '2015-03-02 13:54:13'),
(6, 'Pre Promo Text', 'pre_promo', '<h2>The Brown Brothers</h2>\r\n<h1>“What''s on your bucket list?”</h1>\r\n<h2>Promotion opens on %%START_DATE_SHORT%%</h2>', 1, '2015-03-02 13:59:49', '2015-03-02 14:00:40'),
(7, 'Post Promo Text', 'post_promo', '<h2>The Brown Brothers</h2>\r\n<h1>&ldquo;What''s on your bucket list?&rdquo;</h1>\r\n<h2>Promotion has now closed</h2>', 1, '2015-03-02 14:01:34', '2015-03-02 14:01:34'),
(8, 'Winner EDM', 'winner_edm', '<h1>Congratulations!</h1>\n<p>You''re a Provisional Instant Prize Winner of a mixed case of Brown Brothers wine, valued at $105.70!</p>\n<p>%%URL%%</p>\n<p>Your Prize Claim Number is %%PCN%%</p>\n<p>To claim your prize, please send in a copy of your purchase receipt and neck cone with unique code for all entries you have submitted, to the Oxygen Support team on one of the following:</p>\n<p>Email:</p><p>support@brownbrotherspromo.com.au\n<br>\nAtt: Louise<br>\nPlease reference "Brown Brothers Bucket List Promotion" in subject line</p>\n<p>OR</p>\n<p>\nMail:<br>\nOxygen Interactive Marketing<br>\nAtt: Louise<br>\nBrown Brothers Bucket List Promotion <br>\n117 Wellington St<br>\nSt Kilda VIC 3182</p>\n<p>Once your claim has been validated your prize will be sent out within 28 days. </p>\n<p>If you have any questions regarding the promotion or prize please call 1300 737 728.</p>\n<hr>\n<p>Regards,<br>\nOxygen Marketing (on behalf of Brown Brothers)</p>', 1, '2015-03-03 16:33:01', '2015-03-03 16:35:30');

-- --------------------------------------------------------

--
-- Table structure for table `entries`
--

CREATE TABLE `entries` (
  `id` int(10) NOT NULL,
  `instantwin_id` int(10) NOT NULL DEFAULT '0',
  `sms_id` int(10) NOT NULL,
  `sms_message` varchar(255) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `suburb` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `postcode` int(4) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `barcode1` int(4) NOT NULL,
  `barcode2` int(4) NOT NULL,
  `barcode3` int(4) NOT NULL,
  `receipt` int(4) NOT NULL,
  `terms` tinyint(1) NOT NULL,
  `proof` tinyint(1) NOT NULL,
  `opt_in` tinyint(1) NOT NULL DEFAULT '1',
  `active` int(1) NOT NULL DEFAULT '1',
  `edm_sent` int(10) NOT NULL,
  `rawdata` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `entries_testing`
--

CREATE TABLE `entries_testing` (
  `id` int(10) NOT NULL,
  `instantwin_id` int(10) NOT NULL DEFAULT '0',
  `sms_id` int(10) NOT NULL,
  `sms_message` varchar(255) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `suburb` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `postcode` int(4) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `barcode1` int(4) NOT NULL,
  `barcode2` int(4) NOT NULL,
  `barcode3` int(4) NOT NULL,
  `receipt` int(4) NOT NULL,
  `terms` tinyint(1) NOT NULL,
  `proof` tinyint(1) NOT NULL,
  `opt_in` tinyint(1) NOT NULL DEFAULT '1',
  `active` int(1) NOT NULL DEFAULT '1',
  `edm_sent` int(10) NOT NULL,
  `rawdata` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `formElement`
--

CREATE TABLE `formElement` (
  `id` int(10) NOT NULL,
  `type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `class` text NOT NULL,
  `validation` text NOT NULL,
  `options` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `divclass` varchar(255) NOT NULL,
  `rowNumber` int(3) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `formElement`
--

INSERT INTO `formElement` (`id`, `type`, `name`, `value`, `class`, `validation`, `options`, `title`, `divclass`, `rowNumber`, `active`, `ordering`, `created_at`, `updated_at`) VALUES
(1, 'text', 'firstname', '', '', 'required', '{"id":"firstname", "tabindex":"1", "data-parsley-minlength":"2", "maxlength":"35", "required":"required"}', 'First Name', '', 1, 1, 1, '2014-11-20 16:48:20', '2015-01-22 11:01:10'),
(2, 'text', 'lastname', '', '', 'required', '{"id":"lastname", "tabindex":"2", "data-parsley-minlength":"2", "maxlength":"35", "required":"required"}', 'Last Name', '', 1, 1, 2, '2014-11-20 17:09:54', '2014-11-20 17:09:54'),
(3, 'text', 'address', '', '', 'required', '{"id":"address", "tabindex":"4", "maxlength":"100", "required":"required"}', 'Postal Address', '', 3, 1, 1, '2014-11-21 09:27:04', '2014-11-21 09:27:04'),
(14, 'checkbox', 'proof', '1', '', 'required', '{"id":"proof", "tabindex":"12", "required":"required"}', 'I have kept my proof of purchase', 'col12__item checkboxes', 7, 1, 1, '2014-11-26 14:49:48', '2015-01-22 11:02:28'),
(15, 'checkbox', 'terms', '1', '', 'required', '{"id":"terms", "tabindex":"13", "required":"required"}', 'I agree to the terms & conditions', 'col12__item checkboxes', 8, 1, 1, '2014-11-26 14:49:48', '2015-01-22 11:02:28'),
(17, 'text', 'suburb', '', '', 'required', '{"id":"suburb", "tabindex":"5", "data-parsley-minlength":"2", "maxlength":"50", "required":"required"}', 'Suburb', '', 4, 1, 1, '2014-11-26 15:38:28', '2014-11-26 15:38:28'),
(18, 'text', 'postcode', '', '', 'required|postcode', '{"id":"postcode", "tabindex":"6", "data-parsley-minlength":"4", "data-parsley-type":"number", "maxlength":"4", "required":"required"}\n', 'Postcode', '', 4, 1, 2, '2014-11-26 15:40:10', '2014-11-26 15:40:10'),
(20, 'captcha', 'captcha', '', '', '', '', 'Captcha', '', 6, 1, 1, '2014-11-26 15:53:20', '2015-01-22 11:02:28'),
(22, 'checkbox', 'opt_in', '1', '', '', '{"id":"opt_in", "tabindex":"14", "required":"required"}', 'I would like to join the club and receive all the latest news *', 'col12__item checkboxes', 9, 1, 1, '2015-02-23 10:51:00', '2015-02-23 10:51:00'),
(23, 'text', 'email', '', '', 'required|email', '{"id":"email", "tabindex":"3", "data-parsley-minlength":"2", "maxlength":"100", "required":"required"}', 'Email Address', '', 2, 1, 1, '2014-11-21 09:27:04', '2014-11-21 09:27:04'),
(24, 'text', 'barcode1', '', '', 'required', '{"id":"barcode1", "tabindex":"8", "data-parsley-minlength":"4", "data-parsley-type":"number", "maxlength":"4", "required":"required"}', 'Barcode 1', '', 5, 1, 1, '2014-11-26 15:40:10', '2014-11-26 15:40:10'),
(25, 'text', 'barcode2', '', '', 'required', '{"id":"barcode2", "tabindex":"9", "data-parsley-minlength":"4", "data-parsley-type":"number", "maxlength":"4", "required":"required"}', 'Barcode 2', '', 5, 1, 2, '2015-08-11 12:08:16', '2015-08-11 12:08:16'),
(26, 'text', 'barcode3', '', '', 'required', '{"id":"barcode3", "tabindex":"10", "data-parsley-minlength":"4", "data-parsley-type":"number", "maxlength":"4", "required":"required"}', 'Barcode 3', '', 5, 1, 3, '2015-08-11 12:08:16', '2015-08-11 12:08:16'),
(27, 'text', 'receipt', '', '', 'required', '{"id":"receipt", "tabindex":"11", "data-parsley-minlength":"4", "data-parsley-type":"number", "maxlength":"4", "required":"required"}', 'Receipt', '', 5, 1, 4, '2015-08-11 12:08:16', '2015-08-11 12:08:16'),
(28, 'text', 'phone', '', '', 'required', '{"id":"phone", "tabindex":"7", "data-parsley-minlength":"10", "data-parsley-type":"number", "maxlength":"10", "required":"required"}', 'Phone', '', 4, 1, 3, '2015-08-11 12:08:16', '2015-08-11 12:08:16'),
(29, 'text', 'entercaptcha', '', '', 'required|captcha', '{"id":"entercaptcha", "tabindex":"12", "required":"required"}', 'Enter Captcha Code', '', 6, 1, 2, '2014-11-26 15:53:20', '2015-01-22 11:02:28');

-- --------------------------------------------------------

--
-- Table structure for table `instantwin`
--

CREATE TABLE `instantwin` (
  `id` int(10) NOT NULL,
  `prize_pool_id` int(10) NOT NULL,
  `pcn` varchar(10) NOT NULL,
  `time_slot` datetime NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12101 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `instantwin_testing`
--

CREATE TABLE `instantwin_testing` (
  `id` int(10) NOT NULL,
  `prize_pool_id` int(10) NOT NULL,
  `pcn` varchar(10) NOT NULL,
  `time_slot` datetime NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11297 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_reminders`
--

CREATE TABLE `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prize_details`
--

CREATE TABLE `prize_details` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `web_name` varchar(255) NOT NULL,
  `sms_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `link` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `img_url` varchar(255) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `prize_pools`
--

CREATE TABLE `prize_pools` (
  `id` int(10) NOT NULL,
  `prize_details_id` int(10) NOT NULL,
  `quantity` int(20) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE `promotion` (
  `id` int(5) NOT NULL,
  `option` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `promotion`
--

INSERT INTO `promotion` (`id`, `option`, `value`, `created_at`, `updated_at`) VALUES
(1, 'start_date', '2015-06-16 00:01:00', '2014-12-15 11:06:55', '2015-06-30 11:23:15'),
(2, 'end_date', '2015-12-14 23:59:59', '2014-12-15 11:06:55', '2015-07-29 11:10:01'),
(3, 'support_email', 'stuart@oxygenmarketing.com.au', '2014-12-15 11:07:35', '2014-12-15 11:07:35'),
(4, 'preview_code', '4WR2Wj7spj', '2014-12-15 11:09:39', '2015-02-18 14:27:29'),
(5, 'pre_promo', '2015-02-09 10:00:00', '2015-01-08 10:15:38', '2015-02-18 14:29:03'),
(6, 'post_promo', '2015-12-16 12:00:01', '2015-01-08 10:15:38', '2015-07-29 11:10:01'),
(7, 'account_manager', '1', '2015-01-08 12:00:00', '2015-07-29 10:23:50'),
(8, 'account_manager_phone', '', '2015-01-08 12:15:00', '2015-01-08 12:15:00'),
(9, 'account_manager_email', '', '2015-01-08 12:15:00', '2015-01-08 12:15:00'),
(10, 'client', 'Oxygen Marketing', '2015-01-08 12:17:45', '2015-07-29 10:23:50'),
(11, 'project_number', 'HOUOX0006', '2015-01-08 12:17:45', '2015-07-29 10:23:50'),
(12, 'project_name', 'Oxygen Marketing Promotion', '2015-01-08 12:17:45', '2015-07-29 10:23:50'),
(13, 'redirect', 'http://www.oxygenmarketing.com.au', '2015-01-09 10:15:47', '2015-07-29 10:23:50'),
(14, 'ga_code', '', '2015-01-12 16:37:50', '2015-07-01 16:22:42'),
(15, 'has_prizes', '1', '2015-01-15 16:26:49', '2015-02-05 16:16:09'),
(16, 'prizes_by_state', '0', '2015-01-15 16:26:49', '2015-02-18 15:17:34'),
(17, 'age_to_enter', '18', '2015-01-19 14:09:28', '2015-01-19 14:09:28'),
(18, 'number_of_entries', '5', '2015-01-19 14:09:28', '2015-02-18 14:30:32'),
(19, 'number_of_entries_per', 'Day', '2015-01-19 14:09:28', '2015-01-19 14:27:42'),
(20, 'number_of_wins', '1', '2015-01-19 14:09:28', '2015-03-20 14:40:29'),
(21, 'number_of_wins_per', 'Promo', '2015-01-19 14:09:28', '2015-01-19 14:09:28'),
(22, 'prizes_per_day', '10', '2015-01-20 14:41:29', '2015-02-18 14:30:32'),
(23, 'limited_by', 'phone', '2015-01-23 15:39:16', '2015-01-27 15:14:52'),
(24, 'sms_entry', '1', '2015-02-05 10:47:59', '2015-02-05 16:16:09'),
(25, 'smsGW_host', '/BROOX0007_Brown_Brothers', '2015-02-16 10:33:49', '2015-02-16 10:33:49'),
(26, 'smsGW_user', 'BROOX0007', '2015-02-16 10:33:49', '2015-02-16 10:33:49'),
(27, 'smsGW_pass', 'Ay141dfS5c', '2015-02-16 10:34:05', '2015-02-16 10:34:05'),
(28, 'sms_pre', 'FreeMSG: Brown Brothers What''s On Your Bucket List Promotion opens 16.03.15. Please try again from this date. Help? 1300 737 728. SP: Oxygen Interactive.', '2015-02-16 10:36:52', '2015-02-26 10:09:29'),
(29, 'sms_winner', 'FreeMSG: Brown Brothers Promo. Yay! You''re an instant winner of %%PRIZE%%! Call 1300 737728 to claim/for help. Claim No %%PCN%% SP:Oxygen Interactive', '2015-02-16 10:36:52', '2015-03-02 17:13:27'),
(30, 'sms_thanks', 'FreeMSG: Brown Brothers: UR not an instant winner this time but UR in the Major Draw. Please purchase and try again. Help? 1300 737 728. SP: Oxygen Interactive.', '2015-02-16 10:36:52', '2015-02-26 10:09:29'),
(31, 'sms_invalid', 'FreeMSG: Brown Brothers Promo. Your entry is invalid. Please try again using the correct format. Help? 1300 737 728. SP: Oxygen Interactive.', '2015-02-16 10:36:52', '2015-02-26 10:09:29'),
(32, 'sms_limit', 'FreeMSG: Brown Brothers Promo. You''ve exceeded your daily entry limit, please try again tomorrow. Help? 1300 737 728. SP: Oxygen Interactive.', '2015-02-16 10:36:52', '2015-02-26 10:09:29'),
(33, 'sms_post', 'FreeMSG: Brown Brothers What''s On Your Bucket List Promotion has now closed. Help? 1300 737 728. SP: Oxygen Interactive.', '2015-02-16 10:36:52', '2015-02-26 10:09:29'),
(34, 'sms_unique', 'FreeMSG: Brown Brothers Promo. Your entry is invalid. Please re-enter your unique code. Help? 1300 737 728. SP: Oxygen Interactive.', '2015-02-25 16:53:52', '2015-02-26 10:09:29'),
(35, 'sms_barcode', 'FreeMSG: Brown Brothers Promo. Your entry is invalid. Please re-enter your barcode. Help? 1300 737 728. SP: Oxygen Interactive.', '2015-02-26 10:10:56', '2015-02-26 10:10:56'),
(36, 'edm_address', 'support@brownbrotherspromo.com.au', '2015-03-02 09:44:59', '2015-03-02 09:44:59'),
(37, 'edm_name', 'Brown Brothers', '2015-03-02 09:44:59', '2015-03-02 09:44:59'),
(38, 'edm_subject', 'Brown Brothers Bucket List Promotion', '2015-03-02 09:44:59', '2015-03-02 09:44:59'),
(39, 'fb_app_id', '1439751259650108', '2015-03-03 10:21:44', '2015-03-03 10:21:44'),
(40, 'fb_name', 'Oxygen Marketing Inhouse Promotion', '2015-03-03 10:21:44', '2015-07-29 10:25:35'),
(41, 'fb_link', 'http://www.oxygenmarketing.com.au/', '2015-03-03 10:21:44', '2015-07-29 10:25:35'),
(42, 'fb_description', 'This is an in house promotion and has no real prizes. This is for demonstration purposes only!', '2015-03-03 10:21:44', '2015-07-29 10:25:35'),
(43, 'fb_img_url', 'http://broox0007.oxygencompetitions.com.au/images/_fbshare/o_19fetcnv51lue15pe1h92s5018b97.jpg', '2015-03-03 10:21:44', '2015-03-03 15:13:18'),
(44, 'unique_codes', '0', '2015-06-30 10:37:05', '2015-08-11 13:44:20'),
(45, 'barcode_validation', '0', '2015-06-30 10:37:05', '2015-08-11 13:44:20');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sms_receipt`
--

CREATE TABLE `sms_receipt` (
  `id` int(11) NOT NULL,
  `reference` text,
  `status` varchar(50) DEFAULT NULL,
  `date_delivered` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sms_received`
--

CREATE TABLE `sms_received` (
  `id` int(11) NOT NULL,
  `msg_msisdn` varchar(15) DEFAULT NULL,
  `msg_shortcode` varchar(15) DEFAULT NULL,
  `msg_reference` varchar(50) DEFAULT NULL,
  `msg_content` varchar(255) DEFAULT NULL,
  `msg_channel` varchar(50) DEFAULT NULL,
  `msg_datatype` int(1) DEFAULT NULL,
  `msg_datereceived` datetime DEFAULT NULL,
  `msg_campaignid` varchar(15) DEFAULT NULL,
  `msg_trigger` varchar(15) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sms_reply`
--

CREATE TABLE `sms_reply` (
  `id` int(11) NOT NULL,
  `sms_id` int(11) DEFAULT NULL,
  `msisdn` varchar(50) DEFAULT NULL,
  `shortcode` varchar(50) DEFAULT NULL,
  `reference` varchar(50) DEFAULT NULL,
  `channel` varchar(50) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(5) NOT NULL,
  `state` varchar(20) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state`, `active`, `created_at`, `updated_at`) VALUES
(2, 'ACT', 1, '2015-01-15 09:44:57', '2015-01-15 09:44:57'),
(3, 'NSW', 1, '2015-01-15 09:44:57', '2015-01-15 09:44:57'),
(4, 'NT', 1, '2015-01-15 09:44:57', '2015-01-15 09:44:57'),
(5, 'QLD', 1, '2015-01-15 09:44:57', '2015-01-15 09:44:57'),
(6, 'SA', 1, '2015-01-15 09:44:57', '2015-01-15 09:44:57'),
(7, 'TAS', 1, '2015-01-15 09:44:57', '2015-01-15 09:44:57'),
(8, 'VIC', 1, '2015-01-15 09:44:57', '2015-01-15 09:44:57'),
(9, 'WA', 1, '2015-01-15 09:44:57', '2015-01-15 09:44:57');

-- --------------------------------------------------------

--
-- Table structure for table `states_prizes`
--

CREATE TABLE `states_prizes` (
  `id` int(10) NOT NULL,
  `state_id` int(10) NOT NULL,
  `prize_pools_id` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `uniquecodes`
--

CREATE TABLE `uniquecodes` (
  `id` int(10) NOT NULL,
  `uniquecode` varchar(8) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(20) unsigned NOT NULL,
  `user_type` enum('','ADMIN','MANAGER','REGISTERED','CLIENT') NOT NULL DEFAULT '',
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `position` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `activation` text,
  `active` tinyint(1) NOT NULL,
  `remember_token` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_type`, `username`, `password`, `first_name`, `last_name`, `phone`, `position`, `email`, `activation`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN', 'admin', '$2y$10$vV566iBU4P64dTKVl7EqIuyvIYAjby/H3HXXRBp2SFra3dsG9./zi', 'Stuart', 'Owen', '0481159622', 'Developer', 'admin@oxygenmarketing.com.au', NULL, 1, 'dg3WvBIiklyjogBrR6vqt6kLW5o4mJzy1X3M12f51qZhTikro4DU1aEYMGVl', '2013-06-04 00:00:01', '2015-07-01 16:38:47'),
(7, 'CLIENT', 'client', '$2y$10$Hc0iHYzLk2QuiqpRCwWb/ubAopbXvQMnkuHNnGSivSiwxEctGYHG.', 'Client', '', '', '', 'digital@oxygenmarketing.com.au', NULL, 1, 'MbbZtRZ4f1D9x0GvD34URtvJCSjABYb27ggiJ3BclaRI0MMn8K1rqo8Qkaf7', '2015-02-18 15:38:48', '2015-07-01 16:39:01'),
(8, 'MANAGER', 'diana', '$2y$10$JLt.KKrnWf/9kzjZbvfVOu92rWukpjt8.B2Ae1b0sMi/hVL0gRdV.', 'Diana', 'Da Re', '0380604100', 'Account Manager', 'diana@oxygenmarketing.com.au', NULL, 1, '', '2015-07-29 10:33:19', '2015-07-29 10:33:19'),
(9, 'MANAGER', 'iain', '$2y$10$WzDzL0oGOCbF0H85Rh3hSebS/LcLcDJnDTy00iNfa2bZK9Pt1OMfy', 'Iain', 'Crittenden', '0380604100', 'Strategy Director', 'iain@oxygenmarketing.com.au', NULL, 1, '', '2015-07-29 10:37:17', '2015-07-29 10:37:17'),
(10, 'MANAGER', 'emilia', '$2y$10$lrMRxr3r9QSFXkm5R5qEDuAtIiunWA4IZduqEPjS0.FxxFomCZ9iC', 'Emilia', 'Walerczak', '0380604100', 'Account Manager', 'emilia@oxygenmarketing.com.au', NULL, 1, '', '2015-07-29 10:38:46', '2015-07-29 10:38:46'),
(11, 'MANAGER', 'adamb', '$2y$10$RZGviaFix2MZO//LPX6m5OSgxuqm8Nb7DaI6UENv0GL8pc7rfWQCG', 'Adam', 'Brami', '0380604100', 'Senior Account Director', 'adamb@oxygenmarketing.com.au', NULL, 1, '', '2015-07-29 10:44:20', '2015-07-29 10:44:20'),
(12, 'MANAGER', 'warwick', '$2y$10$8Ye9ue4pZgQi069w1Q4E/efoAv51zsCJMj/3JVQIK/QX9CZNKDMRG', 'Warwick', 'Purves', '0380604100', 'Managing Director', 'warwick@oxygenmarketing.com.au', NULL, 1, '', '2015-07-29 10:49:12', '2015-07-29 10:49:12');

-- --------------------------------------------------------

--
-- Stand-in structure for view `z_instantwin`
--
CREATE TABLE `z_instantwin` (
`id` int(10)
,`pcn` varchar(10)
,`name` varchar(255)
,`web_name` varchar(255)
,`sms_name` varchar(255)
,`time_slot` datetime
,`prize_pool_id` int(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `z_winners`
--
CREATE TABLE `z_winners` (
`id` int(10)
,`instantwin_id` int(10)
,`prize_pool_id` int(10)
,`prize_details_id` int(10)
,`web_name` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `_fnxAWTD7h9E6d91ff4eRbIi4ZzcIleFT`
--

CREATE TABLE `_fnxAWTD7h9E6d91ff4eRbIi4ZzcIleFT` (
  `id` int(10) NOT NULL,
  `entries_id` int(10) NOT NULL,
  `pcn` varchar(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `prize` varchar(255) NOT NULL,
  `rankey` varchar(255) NOT NULL,
  `address_line_1` varchar(255) NOT NULL,
  `address_line_2` varchar(255) NOT NULL,
  `suburb` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `postcode` int(4) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure for view `z_instantwin`
--
DROP TABLE IF EXISTS `z_instantwin`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `z_instantwin` AS select `instantwin`.`id` AS `id`,`instantwin`.`pcn` AS `pcn`,`prize_details`.`name` AS `name`,`prize_details`.`web_name` AS `web_name`,`prize_details`.`sms_name` AS `sms_name`,`instantwin`.`time_slot` AS `time_slot`,`instantwin`.`prize_pool_id` AS `prize_pool_id` from ((`instantwin` join `prize_pools`) join `prize_details`) where ((`instantwin`.`prize_pool_id` = `prize_pools`.`id`) and (`prize_details`.`id` = `prize_pools`.`prize_details_id`)) order by `instantwin`.`time_slot`;

-- --------------------------------------------------------

--
-- Structure for view `z_winners`
--
DROP TABLE IF EXISTS `z_winners`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `z_winners` AS select `entries`.`id` AS `id`,`entries`.`instantwin_id` AS `instantwin_id`,`instantwin`.`prize_pool_id` AS `prize_pool_id`,`prize_pools`.`prize_details_id` AS `prize_details_id`,`prize_details`.`web_name` AS `web_name` from (((`entries` join `instantwin`) join `prize_pools`) join `prize_details`) where ((`entries`.`instantwin_id` > 0) and (`entries`.`instantwin_id` = `instantwin`.`id`) and (`instantwin`.`prize_pool_id` = `prize_pools`.`id`) and (`prize_details`.`id` = `prize_pools`.`prize_details_id`));

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barcode`
--
ALTER TABLE `barcode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entries`
--
ALTER TABLE `entries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entries_testing`
--
ALTER TABLE `entries_testing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `formElement`
--
ALTER TABLE `formElement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instantwin`
--
ALTER TABLE `instantwin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instantwin_testing`
--
ALTER TABLE `instantwin_testing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reminders`
--
ALTER TABLE `password_reminders`
  ADD KEY `password_reminders_email_index` (`email`),
  ADD KEY `password_reminders_token_index` (`token`);

--
-- Indexes for table `prize_details`
--
ALTER TABLE `prize_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prize_pools`
--
ALTER TABLE `prize_pools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `sms_receipt`
--
ALTER TABLE `sms_receipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_received`
--
ALTER TABLE `sms_received`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_reply`
--
ALTER TABLE `sms_reply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states_prizes`
--
ALTER TABLE `states_prizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uniquecodes`
--
ALTER TABLE `uniquecodes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`uniquecode`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `_fnxAWTD7h9E6d91ff4eRbIi4ZzcIleFT`
--
ALTER TABLE `_fnxAWTD7h9E6d91ff4eRbIi4ZzcIleFT`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barcode`
--
ALTER TABLE `barcode`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `entries`
--
ALTER TABLE `entries`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `entries_testing`
--
ALTER TABLE `entries_testing`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `formElement`
--
ALTER TABLE `formElement`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `instantwin`
--
ALTER TABLE `instantwin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12101;
--
-- AUTO_INCREMENT for table `instantwin_testing`
--
ALTER TABLE `instantwin_testing`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11297;
--
-- AUTO_INCREMENT for table `prize_details`
--
ALTER TABLE `prize_details`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `prize_pools`
--
ALTER TABLE `prize_pools`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `sms_receipt`
--
ALTER TABLE `sms_receipt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sms_received`
--
ALTER TABLE `sms_received`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sms_reply`
--
ALTER TABLE `sms_reply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `states_prizes`
--
ALTER TABLE `states_prizes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uniquecodes`
--
ALTER TABLE `uniquecodes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `_fnxAWTD7h9E6d91ff4eRbIi4ZzcIleFT`
--
ALTER TABLE `_fnxAWTD7h9E6d91ff4eRbIi4ZzcIleFT`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
